﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_Template
{
    public class RunTemplate
    {
        private string notes = string.Empty;
        private string operatorName = string.Empty;
        private string unit = string.Empty;

        public string Notes
        {
            get { return notes; }
            set { notes = value; }
        }

        public string OperatorName
        {
            get { return operatorName; }
            set { operatorName = value; }
        }
    }
}
