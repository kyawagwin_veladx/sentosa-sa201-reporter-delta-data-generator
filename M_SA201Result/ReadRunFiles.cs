﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 04/02/2014
 * Author: Liu Rui
 * 
 * Description:
 * Handle XML file of REX to read all needed values and put into list of pages object
 * 
 */
#endregion

using M_Assay;
using M_ReportResult;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using M_Language;
using M_CommonMethods;
using System.Drawing;
using M_AssayFile;

using M_ReadSDSFile;
using M_SampleList;

namespace M_SA201Result
{
    public class ReadRunFiles
    {
        SDSInformation sdsInform;
        ReportResult reportResult;
        AssayInfo assayInfo;
        ComponentSDSInformation componentSDSInformation;
        CtSDSInformation ctSDSInformation;
        ResultSDSInformation resultSDSInformation;
        DeltaRnSDSInformation deltaRnSDSInformation;
        List<string> ctChannelNames;

        public ReadRunFiles(string runfilepath, ReportResult reportRe, AssayInfo assayInfo, SMPObject smpObject = null)
        {
            sdsInform = SDSInformation.GetSDSobject(runfilepath, smpObject);

            componentSDSInformation = sdsInform.GetComponentSDSInformation();

            ctSDSInformation = sdsInform.GetCtSDSInformation();

            deltaRnSDSInformation = sdsInform.GetDeltaRnSDSInformation();

            resultSDSInformation = sdsInform.GetResultSDSInformation();

            this.reportResult = reportRe;

            this.assayInfo = assayInfo;

            reportResult.RunInfo.RunFilePath = Path.GetFileName(runfilepath);
        }

        public void FillReportResult()
        {
            if ((componentSDSInformation == null)
                || (ctSDSInformation == null)
                || (resultSDSInformation == null)
                || (deltaRnSDSInformation == null)
                || (assayInfo.RunSDX == string.Empty))
            {
                return;
            }

            if (reportResult == null)
            {
                return;
            }

            FillResultPart();

            FillDeltaRnPart();

            FillComponentPart();

            FillCtPart();

            AssayFile.FillSDXInfo(assayInfo.RunSDX, reportResult);
        }

        private void FillResultPart()
        {
            FillHoldCycleList();

            FillRunInfo();

            FillSampleResultList();

            //reportResult.ReorderCtChannels();
        }

        private void FillRunInfo()
        {
            string operatorInformation = string.Empty;
            string commentsInformation = string.Empty;
            int PCRVolume = 0;
            string instrumentTypeMode = string.Empty;
            string runDate = string.Empty;
            string lastModifiedDateTime = string.Empty;
            // Get Operator information
            resultSDSInformation.GetOperatorInformation(ref operatorInformation);
            // Get Comments information
            resultSDSInformation.GetCommentsInformation(ref commentsInformation);
            // Get PCR Volume information
            resultSDSInformation.GetPCRVolume(ref PCRVolume);
            // Get Instrument Type mode information
            resultSDSInformation.GetInstrumentTypeMode(ref instrumentTypeMode);
            // Get Run start date information
            resultSDSInformation.GetRunDate(ref runDate);
            // Get Last Modified date time information
            resultSDSInformation.GetLastModifiedDateTime(ref lastModifiedDateTime);

            reportResult.RunInfo.AssayName = this.assayInfo.AssayName;

            //runInfo.RunFilePath = FileOperations.SplitColon(FileOperations.GetCellValue("Document Name", parsedCSV)).Last().Trim();

            //int signatureIndex = FileOperations.GetRowIndex("Electronic Signatures", parsedCSV);

            reportResult.RunInfo.BaseNotes = commentsInformation;

            reportResult.RunInfo.SetOperator(operatorInformation);

            reportResult.RunInfo.SetBaseOperator(operatorInformation);

            reportResult.RunInfo.RunID = Path.GetFileNameWithoutExtension(reportResult.RunInfo.RunFilePath);

            reportResult.RunInfo.RunStart = runDate;

            reportResult.RunInfo.LastModified = lastModifiedDateTime;

            reportResult.RunInfo.InstumentType = "Sentosa SA201";

            // reportResult.RunInfo.InstumentType = instrumentTypeMode;

            reportResult.RunInfo.PCRVolume = PCRVolume.ToString();

            reportResult.RunInfo.IsSignatureValid = true;
        }

        private void FillSampleResultList()
        {
            reportResult.SampleResultList.Clear();

            List<string> sampleIDList = new List<string>();

            string wellNameFromResult = string.Empty;
            string sampleNameFromResult = string.Empty;
            string stringDetectorNameFromResult = string.Empty;
            string taskNameFromResult = string.Empty;
            float cycleOfTransitionFromResult = 0;
            double cycleOfTransitionStandardDeviationFromResult = 0;
            // Get number of Sample Result records
            int numberOfSampleResultRecordsFromResult = resultSDSInformation.GetNumberOfSampleResultRecords();
            // Get record by record Sample Result information from Result Information SDS object
            for (int i = 0; i < numberOfSampleResultRecordsFromResult; i++)
            {
                resultSDSInformation.GetRecordFromSampleResults(i, ref wellNameFromResult, ref sampleNameFromResult, ref stringDetectorNameFromResult,
                    ref taskNameFromResult, ref cycleOfTransitionFromResult, ref cycleOfTransitionStandardDeviationFromResult);

                if (!sampleIDList.Contains(wellNameFromResult))
                {
                    sampleIDList.Add(wellNameFromResult);

                    CreateSampleResult(wellNameFromResult, sampleNameFromResult,
                          taskNameFromResult, stringDetectorNameFromResult, cycleOfTransitionFromResult);
                }
                else
                {
                    UpdateSampleResultChannel(wellNameFromResult, stringDetectorNameFromResult, cycleOfTransitionFromResult);
                }
                // Note that cycleOfTransitionFromResult having value as -999.1 should be treated as 'Undetermined' value in results.csv file
                // Note that cycleOfTransitionStandardDeviationFromResult having value as -1.0 should be treated as <blank value> in results.csv file
                // Note that taskNameFromResult having "" value should be treated as 'Unknown' value in results.csv file
            }
        }

        private void CreateSampleResult(string alphanumpos, string samplename,
            string task, string detectorname, float ctvalue)
        {
            SampleResult sampleResult = new SampleResult();

            ctChannelNames = new List<string>();

            ctChannelNames.Add(detectorname);

            sampleResult.SampleName = samplename;

            sampleResult.AlphaNumPos = alphanumpos;

            sampleResult.Position = InfoConvert.GetNumericPosition(sampleResult.AlphaNumPos);

            sampleResult.SampleType = SampleResult.GetSampleType(sampleResult.SampleName, task);

            sampleResult.M1 = SysMessage.notInCSV;

            sampleResult.PageName = "Page 1";

            if (Convert.ToInt32(sampleResult.Position) > 0)
            {
                sampleResult.Colour = Color.FromArgb((int)Math.Pow((Convert.ToInt32(sampleResult.Position * 45)), 2));
            }

            sampleResult.BinBits = string.Empty;

            sampleResult.CharBits = string.Empty;

            sampleResult.TestResult = SysMessage.noInterpretation;

            sampleResult.Validity = SysMessage.noInterpretation;

            CreateCTChannel(detectorname, ctvalue, sampleResult.CtChannelList);

            reportResult.SampleResultList.Add(sampleResult);
        }


        private void UpdateSampleResultChannel(string alphanumpos, string detectorname, float ctvalue)
        {
            SampleResult sampleResult = SampleResult.GetSampleResultByID(alphanumpos, reportResult.SampleResultList);

            if (!ctChannelNames.Contains(detectorname))
            {
                ctChannelNames.Add(detectorname);

                CreateCTChannel(detectorname, ctvalue, sampleResult.CtChannelList);
            }
        }

        private void CreateCTChannel(string detectorname, float Ctvalue, List<CTChannel> ctChannelList)
        {
            CTChannel ctChannel = new CTChannel();



            var channelName = Enum.GetNames(typeof(ChannelName)).Where(x => detectorname.ToUpper().Contains(x));
            if (channelName.Count() > 0)
            {
                ctChannel.ChannelName = channelName.First();
                ctChannel.Dye = string.Empty;

                try
                {
                    //double.Parse(Ctvalue);
                    if (Ctvalue.ToString() == "-999.1")
                    {
                        string str = Ctvalue.ToString();

                        str = string.Empty;

                        ctChannel.CtValue = str;
                    }
                    else
                    {
                        ctChannel.CtValue = Ctvalue.ToString();
                    }
                }
                catch
                {
                    ctChannel.CtValue = string.Empty;
                }

                ctChannel.IsEnabled = true;

                ctChannel.Selected = true;

                ctChannelList.Add(ctChannel);
            }

            //if (detectorname != null)
            //{
            //    //Hardcode
            //    if (detectorname.ToUpper().Contains("RED"))
            //    {
            //        ctChannel.ChannelName = "RED";
            //    }
            //    else
            //    {
            //        ctChannel.ChannelName = detectorname;
            //    }
            //    //Hardcode
            //}

        }

        private void FillComponentPart()
        {
            int tempWellidentifierFromComponent = 0;

            string tempStringDyeNameFromComponent = string.Empty;

            // Get the number of subcomponents 
            int numberOfSubcomponentsFromComponent = componentSDSInformation.GetNumberOfSubcomponents();

            // Get the number of readings 
            int numberOfReadingsFromComponent = componentSDSInformation.GetNumberOfReadings();

            float[] tempSignalsFromComponent = new float[numberOfReadingsFromComponent];
            // Get record by record information from Component Information SDS object
            for (int i = 0; i < numberOfSubcomponentsFromComponent; i++)
            {
                componentSDSInformation.GetRecord(i, ref tempWellidentifierFromComponent, ref tempStringDyeNameFromComponent, ref tempSignalsFromComponent);

                string position = tempWellidentifierFromComponent.ToString();
                string dye = tempStringDyeNameFromComponent;

                CTChannel ctChannel = reportResult.GetCTChannelByDye(dye, position);

                if (ctChannel.ChannelName != string.Empty)
                {
                    ctChannel.FillRawPoints(tempSignalsFromComponent);
                }
            }

        }

        private void FillCtPart()
        {
            string stringDetectorNameFromCt = string.Empty;
            string virusName = string.Empty;
            float thresholdFromCt = 0;
            int baselineStart = 0;
            int baselineEnd = 0;
            // Get number of entries
            int numberOfEntriesFromCt = ctSDSInformation.GetNumberOfEntries();
            // Get record by record information from Ct Information SDS object
            for (int i = 0; i < numberOfEntriesFromCt; i++)
            {
                ctSDSInformation.GetRecord(i, ref stringDetectorNameFromCt, ref baselineStart, ref baselineEnd, ref thresholdFromCt);

                //if (stringDetectorNameFromCt == Settings.detector)
                //{
                //    int rowIndex = i + 1;

                string channelName = stringDetectorNameFromCt;

                double threshold;
                string start = Convert.ToString(baselineStart);
                string end = Convert.ToString(baselineEnd);

                try
                {
                    threshold = Convert.ToDouble(thresholdFromCt);
                }
                catch
                {
                    threshold = 0;
                }

                //   // start = parsedCSV[rowIndex][2];
                //   // end = parsedCSV[rowIndex][3];

                reportResult.UpdateThreshold(channelName, threshold);
                reportResult.UpdateStartEnd(channelName, start, end);
                //}
            }
        }

        private void FillDeltaRnPart()
        {
            int tempWellidentifierFromDeltaRn = 0;
            string stringDetectorNameFromDeltaRn = string.Empty;
            string stringReporterDyeNameFromDeltaRn = string.Empty;
            // Get the number of readings 
            int numberOfReadingsFromDeltaRn = deltaRnSDSInformation.GetNumberOfReadings();
            // Get the number of subcomponents 
            int numberOfSubcomponentsFromDeltaRn = deltaRnSDSInformation.GetNumberOfSubcomponents();
            float[] tempSignalsFromDeltaRn = new float[numberOfReadingsFromDeltaRn];
            // Get record by record information from Delta RN Information SDS object
            for (int i = 0; i < numberOfSubcomponentsFromDeltaRn; i++)
            {
                deltaRnSDSInformation.GetRecord(i, ref tempWellidentifierFromDeltaRn, ref stringDetectorNameFromDeltaRn, ref stringReporterDyeNameFromDeltaRn, ref tempSignalsFromDeltaRn);

                string position = tempWellidentifierFromDeltaRn.ToString();

                string channel = stringDetectorNameFromDeltaRn;

                string dye = stringReporterDyeNameFromDeltaRn;

                reportResult.FillDye(position, channel, dye);

                CTChannel ctChannel = reportResult.GetCTChannelByChannel(channel, position);

                if (ctChannel.ChannelName != string.Empty)
                {
                    ctChannel.FillDeltaRnPoints(tempSignalsFromDeltaRn);
                }
            }

        }

        private static void RemoveChecksumRow(List<string[]> parsedCSV)
        {
            List<string[]> checksumRowList = FileOperations.GetCSVSignatureRow("Signature", parsedCSV);

            if (checksumRowList.Count >= 0)
            {
                foreach (string[] checksumRow in checksumRowList)
                {
                    parsedCSV.Remove(checksumRow);
                }
            }
        }

        private static void FillDataCollection(List<string[]> parsedCSV, RunProfile runProfile)
        {
            runProfile.DataCollection = FileOperations.SplitColon(FileOperations.GetCellValue("Data Collection", parsedCSV)).Last().Trim();
        }

        private void FillHoldCycleList()
        {
            int stage = 0;

            int time = 0;

            int repetition = 0;

            int temperature = 0;

            int numberOfThermalCyclerProfileEntries = resultSDSInformation.GetNumberOfThermalCyclerProfileEntries();

            reportResult.RunProfile.HoldCycleList.Clear();

            int cyclingCount = 0;

            int stepCount = 1;

            for (int i = 0; i < numberOfThermalCyclerProfileEntries; i++)
            {
                resultSDSInformation.GetRecordInThermalCycleProfileInformation(i, ref stage, ref repetition, ref time, ref temperature);

                if (repetition != 0)
                {
                    if (repetition == 1)
                    {
                        Hold hold = new Hold();
                        hold.HoldName = "Hold " + stage.ToString();
                        hold.Temperature = (temperature / 10).ToString();
                        hold.Second = time.ToString();

                        reportResult.RunProfile.HoldCycleList.Add(hold);
                    }
                    else if (repetition > 1)
                    {
                        cyclingCount++;
                        stepCount = 1;

                        Cycling cycling = new Cycling();
                        cycling.CycleName = "Cycling " + cyclingCount.ToString();
                        cycling.RepeatCount = repetition.ToString();

                        Step step = new Step();
                        step.StepName = "Step " + stepCount.ToString();
                        step.Temperature = (temperature / 10).ToString();
                        step.Second = time.ToString();

                        cycling.StepList.Add(step);

                        reportResult.RunProfile.HoldCycleList.Add(cycling);
                    }
                }
                else
                {
                    stepCount++;
                    Step step = new Step();
                    step.StepName = "Step " + stepCount.ToString();
                    step.Temperature = (temperature / 10).ToString();
                    step.Second = time.ToString();
                    ((Cycling)(reportResult.RunProfile.HoldCycleList[reportResult.RunProfile.HoldCycleList.Count - 1])).StepList.Add(step);
                }
            }

        }

        ///// <summary>
        ///// Get the selected sample information upon the base software completed running 
        ///// </summary>
        ///// <returns></returns>
        //public List<string> GetSelectedSamples(List<SampleSize> ConfigSampleSizes)
        //{
        //    List<string> target = new List<string>();

        //    foreach (SampleSize ConfigSampleSize in ConfigSampleSizes)
        //    {
        //        foreach (Position postion in ConfigSampleSize.PositionList)
        //        {
        //           if( reportResult.SampleResultList.Where(x=>x.PageName.Equals(postion.PageName)).Count() == postion.SampleList.Count())
        //           {
        //              target = postion.SampleList.Select(x=>x.Position.ToString()).ToList();
        //           }
        //        }
        //    }

        //    if (target.Count == 0)
        //    {
        //        target =ConfigSampleSizes.First().PositionList.First().SampleList.Select(x => x.Position.ToString()).ToList();
        //    }

        //    return target;
        //}
    }
}
