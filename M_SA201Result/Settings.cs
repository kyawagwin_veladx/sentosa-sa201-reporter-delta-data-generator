﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 04/02/2014
 * Author: Liu Rui
 * 
 * Description:
 * Handle XML file of REX to read all needed values and put into list of pages object
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_SA201Result
{
    public class Settings
    {
        public static string well = "Well";
        public static string sampleName = "Sample Name";
        public static string detector = "Detector";
        public static string reporterDye = "Reporter Dye";
        public static string task = "Task";
        public static string ct = "Ct";
        public static string qty = "Qty";
        public static string cycle1 = "Cycle 1";
        public static string dyeName = "Dye Name";
        public static string reading1 = "Reading 1";
    }
}
