﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 13/11/2013
 * Author: Liu Rui
 * 
 * Description:
 * 
 */
#endregion

using M_CheckSum;
using M_CommonMethods;
using M_Language;
using System;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using System.Reflection;
namespace M_System
{
    /// <summary>
    /// System Config Class is used to save system configuration information
    /// </summary>
    public class SystemConfig
    {
        private string version = string.Empty;
        private string website = string.Empty;
        private string path_BaseSoftware = string.Empty;
        private string autoRunSoftwareName = string.Empty;
        private string autoAnalyzeSoftwareName = string.Empty;
        private string folder_AutoIt = string.Empty;
        private string folder_AssayConfig = string.Empty;
        private string folder_Config = string.Empty;
        private string folder_Log = string.Empty;
        private string folder_Run = string.Empty;
        private string folder_Template = string.Empty;
        private string folder_LinkRun = string.Empty;
        private List<SupportedAssayConfig> supportedAssayList = new List<SupportedAssayConfig>();  


        public string Folder_LinkRun
        {
            get { return folder_LinkRun; }
        }

        public string Website
        {
            get { return website; }
        }

        public string Version
        {
            get { return version; }
        }

        public string Folder_Template
        {
            get { return folder_Template; }
        }

        public string Folder_AssayConfig
        {
            get { return folder_AssayConfig; }
        }

        public string Folder_Config
        {
            get { return folder_Config; }
        }

        public string Folder_Log
        {
            get { return folder_Log; }
        }

        public string Folder_Run
        {
            get { return folder_Run; }
        }
        public string Folder_AutoIt
        {
            get { return folder_AutoIt; }
        }

        public string Path_BaseSoftware
        {
            get { return path_BaseSoftware; }
        }
        public string AutoRunSoftwareName
        {
            get { return autoRunSoftwareName; }
        }
        public string AutoAnalyzeSoftwareName
        {
            get { return autoAnalyzeSoftwareName; }
        }

        public List<SupportedAssayConfig> SupportedAssayList
        {
            get { return supportedAssayList; }
        }

        /// <summary>
        /// Fill an empty systemConfig object
        /// </summary>
        /// <param name="systemConfig"></param>
        public SystemConfig()
        {
            string systemConfigXMLPath = AppDomain.CurrentDomain.BaseDirectory
                + Settings.configFolderName + Path.DirectorySeparatorChar + Settings.systemConfigXMLName;

            XmlDocument systemConfigXML = FileCheckSum.Load(systemConfigXMLPath);
            if (systemConfigXML.DocumentElement != null)
            {
                XmlNode rootNode = systemConfigXML.SelectSingleNode(Settings.rootNode);
                string rootFolder = AppDomain.CurrentDomain.BaseDirectory;

                string path = rootNode.SelectSingleNode(Settings.baseSoftwareNode).InnerText;
                if (File.Exists(path))
                {
                    this.path_BaseSoftware = path;
                }

                this.folder_AssayConfig = rootFolder + rootNode.SelectSingleNode(Settings.assayConfigFolderNode).InnerText;
                this.folder_Config = rootFolder + rootNode.SelectSingleNode(Settings.configFolderNode).InnerText;
                this.folder_Log = rootFolder + rootNode.SelectSingleNode(Settings.logFolderNode).InnerText;
                this.folder_Run = rootNode.SelectSingleNode(Settings.runFolderNode).InnerText;
                this.folder_Template = rootFolder + rootNode.SelectSingleNode(Settings.templateFolderNode).InnerText;
                this.folder_LinkRun = rootNode.SelectSingleNode(Settings.linkRunFolderNode).InnerText;
                this.folder_AutoIt = rootFolder + rootNode.SelectSingleNode(Settings.autoItFolderNode).InnerText;
                this.version = rootNode.SelectSingleNode(Settings.versionNode).InnerText;
                this.website = rootNode.SelectSingleNode(Settings.websiteNode).InnerText;
                this.autoAnalyzeSoftwareName = rootNode.SelectSingleNode(Settings.autoAnalyzeSoftwareNode).InnerText;
                this.autoRunSoftwareName = rootNode.SelectSingleNode(Settings.autoRunSoftwareNode).InnerText;
                
                VerifyPathes();
                LoadSupportedAssayList();
            }
            else
            {
                throw new ArgumentException(SysMessage.invalidChechsum, systemConfigXMLPath);
            }
        }

        private void VerifyPathes()
        {
            VerifyPath(path_BaseSoftware);
            VerifyPath(folder_AssayConfig);
            VerifyPath(folder_Config);
            VerifyPath(folder_Log);
            CreateFolder(folder_Run);
            VerifyPath(folder_Template);
            VerifyPath(folder_AutoIt+@"\"+autoRunSoftwareName);
            VerifyPath(folder_AutoIt + @"\" + autoAnalyzeSoftwareName);
        }

        private void CreateFolder(string folder_Run)
        {
            if (!Directory.Exists(folder_Run))
            {
                Directory.CreateDirectory(folder_Run);
            }
        }

        private void VerifyPath(string path)
        {
            if ((!Directory.Exists(path))&&(!File.Exists(path)))
            {
                throw new DirectoryNotFoundException(SysMessage.invalidDirectory + path);
            }
        }

        private void LoadSupportedAssayList()
        {
            this.supportedAssayList = new List<SupportedAssayConfig>();
            string supportedAssayListPath = AppDomain.CurrentDomain.BaseDirectory
              + Settings.configFolderName + Path.DirectorySeparatorChar + Settings.supportAssayListName;
            
            XmlDocument SupportedAssayListXML = FileCheckSum.Load(supportedAssayListPath);
             if (SupportedAssayListXML.DocumentElement != null)
             {
                 XmlNodeList NodeList = SupportedAssayListXML.SelectSingleNode("AssayInfo").SelectSingleNode("SupportedAssay").SelectNodes("Assay");
                 foreach (XmlNode myAssayNode in NodeList)
                 {
                     SupportedAssayConfig supportedAssay = new SupportedAssayConfig();
                     foreach (XmlNode childNode in myAssayNode.ChildNodes)
                     {
                         PropertyInfo propertyInfo = supportedAssay.GetType().GetProperty(childNode.Name);
                         if (propertyInfo != null)
                         {
                             propertyInfo.SetValue(supportedAssay, childNode.InnerText.Trim(),null);
                         }
                     }
                     this.supportedAssayList.Add(supportedAssay);
                 }
             }
             else
             {
                 throw new ArgumentException(SysMessage.invalidChechsum, supportedAssayListPath);
             }
        }
    }
}
