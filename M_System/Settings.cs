﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_System
{
    public class Settings
    {
        public static string rootNode = "SystemConfig";
        public static string configFolderNode = "ConfigFolder";
        public static string configFolderName = "Config";
        public static string autoItFolderNode = "AutoItFolder";
        public static string assayConfigFolderNode = "AssayConfigFolder";
        public static string logFolderNode = "LogFolder";
        public static string runFolderNode = "RunFolder";
        public static string systemConfigXMLName = "SystemConfig.cfg";
        public static string supportAssayListName = "SA201_Supported_Assay_List.cfg";
        public static string sa201EXE = "SDSShell.exe";
        public static string versionNode = "Version";
        public static string websiteNode = "Website";
        public static string baseSoftwareNode = "BaseSoftware";
        public static string autoRunSoftwareNode = "AutoRunSoftware";
        public static string autoAnalyzeSoftwareNode = "AutoAnalyzeSoftware";
        public static string templateFolderNode = "TemplateFolder";
        public static string linkRunFolderNode = "LinkRunFolder";
        

    }
}
