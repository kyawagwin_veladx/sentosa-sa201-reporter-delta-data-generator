﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_System
{
    public class SupportedAssayConfig
    {
        public string AssayName { get; set; }
        public string AssayMode { get; set; }
    }
}
