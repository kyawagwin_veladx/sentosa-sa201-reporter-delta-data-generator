﻿using System;
using System.Windows.Forms;
using M_Language;
using M_CheckExit;
using UI_Access;
using System.Diagnostics;
using System.IO;

namespace Sentosa_SA201_Reporter
{
    static class Program
    {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]

        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            SystemController systemController = new SystemController();
            //try
            //{
     
                systemController.GetSystemConfig();
                systemController.Run();
      
            //}
            //catch(Exception ex)
            //{
                //MessageBox.Show(SysMessage.softwareinitializedFailMsg
                //    , SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                //Environment.Exit(1);
            //}
            

        }

     
    }
}
