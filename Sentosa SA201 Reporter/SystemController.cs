﻿using System.Windows.Forms;
using UI_Login;
using UI_Access;
using UI_Main;
using M_System;
using M_Assay;
using M_ReportResult;
using M_SampleList;
using M_Template;
using M_Language;
using M_Log;
using M_CheckExit;

namespace Sentosa_SA201_Reporter 
{
    class SystemController
    {
        LoginController loginController;
        AccessController accessController;
        MainController mainController;

        public SystemConfig MySystem { get; set; }
        
        private AssayInfo assayInfo;
        private SMPObject smpObject;
        private RunTemplate template;
        private ReportResult reportResult;

        internal void GetSystemConfig()
        {
            MySystem = new SystemConfig();
        }

        private void InitialObjects()
        {
            assayInfo = new AssayInfo();
            smpObject = new SMPObject();
            template = new RunTemplate();
            reportResult = new ReportResult();
        }

        internal void Run()
        {
            InitialObjects();
            SystemMode systemMode = SystemMode.Normal;
            loginController = new LoginController();
            loginController.SetValues(template);

            if (loginController.Display() == DialogResult.OK)
            {
                //ExitCheck class is defined by Personal, which is under the namespace M_CheckExit
                if (ExitCheck.IsProcessExisting("SDSShell"))
                {
                    systemMode = SystemMode.AnalysisOnly;
                }
                else if (ExitCheck.IsSentosaSA201Existing("Sentosa SA201 Reporter"))
                {
                    systemMode = SystemMode.AnalysisOnly;
                }
                else
                {
                    systemMode = SystemMode.Normal;
                }
                
                if (systemMode == SystemMode.AnalysisOnly)
                {
                    if (MessageBox.Show(SysMessage.copySoftwareRunningMsg
                        , SysMessage.confirmCaption
                        , MessageBoxButtons.OKCancel
                        , MessageBoxIcon.Information) == DialogResult.OK)
                    {
                        Log.WriteLog(LogType.system, SysMessage.user + "Select Analysis-Only login.");
                        RunSystem(systemMode);
                    }
                    else
                    {
                        Log.WriteLog(LogType.system, SysMessage.user + "Cancels Analysis-Only login.");
                        Application.Exit();
                    }
                }
                else
                {
                    RunSystem(systemMode);
                }
            }
            else
            {
                Application.Exit();
            }
        }

        private void RunSystem(SystemMode systemMode)
        {
            accessController = new AccessController();
            accessController.SetValues(systemMode, MySystem, assayInfo, smpObject, reportResult);

            if (accessController.Display())
            {
                AccessMode accessMode = accessController.GetAccessMode();

                if (accessMode == AccessMode.EXIT)
                {
                    Log.WriteLog(LogType.system, SysMessage.user + SysMessage.exists);
                    Application.Exit();
                }
                else
                {
                    mainController = new MainController();
                    mainController.dLogoffCallback = new dLogoff(this.Run);
                    mainController.SetValues(accessMode, MySystem, assayInfo, smpObject, template, reportResult);
                    mainController.Display(accessMode, systemMode);
                }
            }
        }

        
    }
}
