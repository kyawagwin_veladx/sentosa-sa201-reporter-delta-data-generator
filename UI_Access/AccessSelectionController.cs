﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using M_System;
using M_ReportResult;
using M_SampleList;
using M_Template;
using M_Assay;
using UI_ImportFiles;
using UI_Run;
using M_Log;
using M_Language;
using System.Collections;
using M_AssayFile;

using M_CheckExit;
using System.IO;


namespace UI_Access
{
    public class AccessController
    {
        Form_AccessSelection myForm;

        Access access;

        ImportFiles importFileclass = new ImportFiles();

        public AccessController()
        {
            myForm = new Form_AccessSelection(this);
          
            access = new Access();
        }

        public void SetValues(SystemMode systemMode, SystemConfig systemConfig, AssayInfo assayInfo, SMPObject smpObject, ReportResult reportResult)
        {
            access.SetValues(systemMode, systemConfig, assayInfo, smpObject);
         
            importFileclass.SetValues(systemConfig, assayInfo, reportResult);
        }

        public bool Display()
        {
            if (access.SystemMode == SystemMode.AnalysisOnly)
            {
                myForm.TurnToAnalysisMode();
            }
            //Form_AccessSelection myForm;
            if (myForm.ShowDialog() == DialogResult.OK)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public AccessMode GetAccessMode()
        {
            return access.Mode;
        }
        public AssayInfo GetAssayInfo()
        {
            return importFileclass.AssayInfo;
        }

        internal bool IDListSelect(string filepath)
        {
            //Fill in the smp object data according to selected IDList xml by the customer
            if (access.ImportSampleList(filepath))
            {   //Fill in assay information in assayinfo
                if (access.FillAssayInfo())
                {
                    if (access.GetAssayName() != string.Empty)
                    {
                        if (access.IsAssaySupported() == false)
                        {
                            MessageBox.Show(SysMessage.noSupportedAssayMsg, SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }

                        if (access.GetIndexOfSelectedSize() >= 0)
                        {
                            if (MessageBox.Show(SysMessage.SA201 + " " + access.GetAssayName()
                                + SysMessage.selectedRunMsg, SysMessage.confirmCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                            {
                                access.FillSampleType();// fill in the smp objet sampletype from configuration file, since the xml file is not exist such a kind of the field
                                return true;
                            }
                        }
                        else
                        {
                            MessageBox.Show(SysMessage.noSamplesize, SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show(SysMessage.noAssayTempMsg, SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show(SysMessage.noAssayTempMsg, SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(SysMessage.invalidSampleFileMsg, SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);  
            }
            return false;
        }

        internal bool DataAnalysis(string folder)
        {
            try
            {
                if (access.DataAnalysis(folder))
                {
                    string[] sdsFileList = Directory.GetFiles(folder, "*.sds");

                    if (sdsFileList.Length > 0)
                    {
                        //Path.GetDirectoryName(sdsFileList[0]);
                        importFileclass.SetRunFolder(Path.GetDirectoryName(sdsFileList[0]));
                        importFileclass.UpdateFlags(sdsFileList[0]);
                        if (!importFileclass.IsAssayXMLFound())
                        {
                            return false;
                        }
                        importFileclass.UpdateReportResult(sdsFileList[0]);

                        importFileclass.FillAssayInfoSDS();

                        if (access.IsAssaySupported() == false)
                        {
                            MessageBox.Show(SysMessage.noSupportedAssayMsg, SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                    return true;    
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
                

            }
        }

        internal void Exit()
        {
            access.Mode = AccessMode.EXIT;
            myForm.DialogResult = DialogResult.Cancel;
        }

        internal bool IsProcessExisting(string exeName)
        {
            return ExitCheck.IsProcessExisting(exeName);
        }
    }

    public enum AccessMode
    {
        IMPORT = 0,
        SELECT,
        ANALYSIS,
        EXIT,
        LOGOFF
    }

    public enum SystemMode
    {
        Normal,
        AnalysisOnly
    }
}
