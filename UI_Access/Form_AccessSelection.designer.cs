﻿
using System.Windows.Forms;

namespace UI_Access
{
    partial class Form_AccessSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_AccessSelection));
            this.btnIDList = new Button();
            this.btnExit = new Button();
            this.btnAnalysis = new Button();
            this.ImportIDList = new System.Windows.Forms.OpenFileDialog();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.OpenRunFolder = new System.Windows.Forms.FolderBrowserDialog();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnIDList
            // 
            this.btnIDList.Location = new System.Drawing.Point(152, 3);
            this.btnIDList.Margin = new System.Windows.Forms.Padding(4);
            this.btnIDList.Name = "btnIDList";
            this.btnIDList.Size = new System.Drawing.Size(175, 80);
            this.btnIDList.TabIndex = 2;
            this.btnIDList.Text = "Import ID List";
            this.btnIDList.Click += new System.EventHandler(this.btnIDList_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(152, 202);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(175, 80);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "Exit";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnAnalysis
            // 
            this.btnAnalysis.Location = new System.Drawing.Point(152, 103);
            this.btnAnalysis.Margin = new System.Windows.Forms.Padding(4);
            this.btnAnalysis.Name = "btnAnalysis";
            this.btnAnalysis.Size = new System.Drawing.Size(175, 80);
            this.btnAnalysis.TabIndex = 6;
            this.btnAnalysis.Text = "Data Analysis";
            this.btnAnalysis.Click += new System.EventHandler(this.btnAnalysis_Click);
            // 
            // ImportIDList
            // 
            this.ImportIDList.Filter = "Entry List File|*.xml;*.smp";
            this.ImportIDList.InitialDirectory = "C:\\Sentosa SA\\ID Lists";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::UI_Access.Properties.Resources.mode2;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(144, 281);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // OpenRunFolder
            // 
            this.OpenRunFolder.Description = "Open Run Folder";
            this.OpenRunFolder.SelectedPath = "C:\\Vela Diagnostics\\SDS Documents\\";
            // 
            // Form_AccessSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(332, 288);
            this.ControlBox = false;
            this.Controls.Add(this.btnAnalysis);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnIDList);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_AccessSelection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Access Mode";
            //((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Button btnIDList;
        private Button btnExit;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Button btnAnalysis;
        private System.Windows.Forms.OpenFileDialog ImportIDList;
        private System.Windows.Forms.FolderBrowserDialog OpenRunFolder;
    }
}