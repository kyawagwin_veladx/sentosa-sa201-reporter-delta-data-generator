﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 01/11/2013
 * Author: Liu Rui
 * 
 * Description:
 */
/*
 * Date: 9/01/2014
 * Modified: Yang Yi
 * 
 * 
 */
#endregion

using System;
using System.Windows.Forms;

using M_CheckExit;
using M_Language;
using M_Log;
using System.Collections;
using M_AssayFile;
using M_ReadSDSFile;


namespace UI_Access
{
    public partial class Form_AccessSelection : Form
    {
        #region Private Member Variables
        private AccessController accessSelectionController;
        
        #endregion

        #region Constructors
        public Form_AccessSelection(AccessController accessSelectionController)
        {
            InitializeComponent();
            this.accessSelectionController = accessSelectionController;
           
        }
        #endregion

        #region Private Methods
        private void btnExit_Click(object sender, EventArgs e)
        {
            if (!accessSelectionController.IsProcessExisting("SDSShell"))
            {
                if (MessageBox.Show(SysMessage.confirmExitMsg
                , SysMessage.confirmCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                {
                    Log.WriteLog(LogType.system, SysMessage.user + Log.UserName + SysMessage.exists);
                    accessSelectionController.Exit();
                }
            }
            else
            {
                MessageBox.Show(SysMessage.runningMsg
                    , SysMessage.errorCaption
                    , MessageBoxButtons.OK
                    , MessageBoxIcon.Error);
            }
        }

        private void btnIDList_Click(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "User selects to import ID List file");
            
            if (this.ImportIDList.ShowDialog() == DialogResult.OK)
            {
                if (accessSelectionController.IDListSelect(this.ImportIDList.FileName))
                {
                    this.DialogResult = DialogResult.OK;
                }
            }
        }
        #endregion

        private void btnAnalysis_Click(object sender, EventArgs e)
        { 
            Log.WriteLog(LogType.system, "User selects to do data analysis");

            try
            {
                SysMessage.errorMsg = string.Empty;
                if (this.OpenRunFolder.ShowDialog() == DialogResult.OK)
                {
                    //this.OpenRunFolder.SelectedPath = @"C:\Vela Diagnostics\SDS Documents\Sentosa SA ZIKV RT-PCR Test RunTemplate 2016-10-14_17-20-56";
                    Cursor.Current = Cursors.WaitCursor;
                    if (accessSelectionController.DataAnalysis(this.OpenRunFolder.SelectedPath))
                    {


                        if (MessageBox.Show(accessSelectionController.GetAssayInfo().AssayName + SysMessage.selectedRunMsg
                         , SysMessage.confirmCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                        {
                            this.DialogResult = DialogResult.OK;
                        }
                        
                        
                    }
                    else
                    {
                        string sErrorMsg = "Load data analysis unsuccessfully, please check about the content of loaded folder.";
                        if (!string.IsNullOrEmpty(SysMessage.errorMsg))
                        {
                            sErrorMsg = SysMessage.errorMsg;
                        }
                        MessageBox.Show(sErrorMsg, SysMessage.errorCaption,
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch
            {
                Cursor.Current = Cursors.Arrow;
            }
            finally
            {
                SysMessage.errorMsg = "";
            }
        }

        internal void TurnToAnalysisMode()
        {
            this.btnIDList.Enabled = false;
        }
    }
}
