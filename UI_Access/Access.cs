﻿using M_Assay;
using M_AssayFile;
using M_SA201Result;
using M_SampleList;
using M_System;
using M_Template;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace UI_Access
{
    public class Access
    {
        /// <summary>
        ///  this is the base class which will be used in the accessselectioncontroller.cs
        /// </summary>
        public AccessMode Mode { get; set; }
        private AssayInfo assayInfo;
        private SMPObject smpObject;
        //private RunTemplate template;
        private SystemConfig systemConfig;
        public SystemMode SystemMode { get; set; }
        public string DataAnaysisPath { get; set; }
        private ReadRunFiles readRunFiles;
        //public AssayInfo AssayInfo
        //{
        //    get { return assayInfo; }
        //}
        //public SystemConfig SystemConfig
        //{
        //    get { return systemConfig; }
        //}
       
        //public SystemMode SystemMode
        //{
        //    get { return systemMode; }
        //}

        internal bool ImportSampleList(string filePath)
        {

            if (Path.GetExtension(filePath).Equals(".smp", StringComparison.OrdinalIgnoreCase))
            {
                if (smpObject.ReadFromSMPFile(SMPSettings.smp_samplesXpath,filePath))
                {
                    this.Mode = AccessMode.IMPORT;

                    return true;
                }
            }
            
            else
            {
                if (smpObject.FillSMPObjectByIDList(filePath))
                {
                    this.Mode = AccessMode.IMPORT;

                    return true;
                }
            }
            
           return false;
        }

        internal bool FillAssayInfo()
        {
            return assayInfo.GetAssayInfoBySMPObject(smpObject, systemConfig.Folder_AssayConfig);
        }

        internal bool IsAssaySupported()
        {
            var supportedAssay = this.systemConfig.SupportedAssayList.Where(x => x.AssayName.Equals(this.assayInfo.AssayName) && x.AssayMode.Equals(this.assayInfo.AssayMode.ToString()));

            if (supportedAssay.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal string GetAssayName()
        {
            return assayInfo.AssayName;
        }

        internal int GetIndexOfSelectedSize()
        {
            return assayInfo.SelectedSizeIndex;
        }

        internal void FillSampleType()
        {
            assayInfo.FillSampleType(smpObject);
        }

        internal bool DataAnalysis(string folderPath)
        {
            assayInfo.SetRunFolder(folderPath);

            this.Mode = AccessMode.ANALYSIS;

            return true;
        }

        internal void SetValues(SystemMode systemMode, SystemConfig systemConfig, AssayInfo assayInfo, SMPObject smpObject)
        {
           
            this.SystemMode = systemMode;
            this.assayInfo = assayInfo;
            this.systemConfig = systemConfig;
          //  this.template = template;
            this.smpObject = smpObject;
        }

        //internal void UpdateReportResult(string runfilepath)
        //{
        //    //set the software version in the system config file
        //    reportResult.RunInfo.SetVersion(systemConfig.Version);

        //    readRunFiles = new ReadRunFiles(runfilepath, reportResult, assayInfo);

        //    readRunFiles.FillReportResult();
        //}

        //internal void FillAssayInfoSDS()
        //{
        //    List<string> m1ItemList = AssayFile.GetM1ItemList(assayInfo.RunSDX); //such as 201161

        //    assayInfo.FillAssayInfo(assayInfo.GetAssayConfigFile(m1ItemList, systemConfig.Folder_AssayConfig));

        //    List<string> selectedSamples = readRunFiles.GetSelectedSamples();

        //    assayInfo.UpdateSelectedSizeIndex(selectedSamples);
        //}

    }
}
