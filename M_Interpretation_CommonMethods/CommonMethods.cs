﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 25/01/2014
 * Author: Liu Rui
 * 
 */
#endregion

using M_Assay;
using M_ReportResult;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace M_Interpretation_CommonMethods
{
    /// <summary>
    /// flag to indicate CT value range
    /// </summary>
    public enum CTRange
    {
        belowRange = 1,
        withinRange = 2,
        aboveRange = 3,
        noValue = 4,
        multipleValues = 5,
        noValueNTC = 6,
    }

    public enum ChannelName
    {
        //[Description("Cycling A.Green")]
        GREEN,
        //[Description("Cycling A.Yellow")]
        YELLOW,
        //[Description("Cycling A.Red")]
        RED,
        //[Description("Cycling A.Orange")]
        ORANGE
    }

    public class CommonMethods
    {
        public static bool IsSampleResultPassed(SampleResult sampleResult, AssayLogic assayLogic)
        {
            sampleResult.CharBits = CommonMethods.GetCharBits(sampleResult.BinBits, assayLogic);
            sampleResult.TestResult = assayLogic.GetResultMapping(sampleResult.CharBits).Result;
            sampleResult.Validity = assayLogic.GetResultMapping(sampleResult.CharBits).Validity;
            
            if ((sampleResult.TestResult.ToUpper().Contains("Invalid".ToUpper()))
                ||(sampleResult.TestResult.ToUpper().Contains("Fail".ToUpper()))
                ||(sampleResult.Validity.ToUpper().Contains("Invalid".ToUpper())))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static string GetCharBits(string binbits, AssayLogic assayLogic)
        {
            string targetCharBits = string.Empty;

            foreach (ResultMapping resultMapping in assayLogic.ResultMappingList)
            {
                string configCharbits = resultMapping.CharBits;
                if (configCharbits.Length == binbits.Length)
                {
                    if (Judge(configCharbits, binbits, assayLogic))
                    {
                        targetCharBits = resultMapping.CharBits;
                        break;
                    }
                }
            }

            return targetCharBits;
        }

        private static bool Judge(string configCharbits, string binbits, AssayLogic assayLogic)
        {
            bool ret = true;
            for (int i = 0; i < binbits.Length; i++)
            {
                string searchBinbits = assayLogic.GetBinbitRange(configCharbits[i]).Range;

                if (!searchBinbits.Contains(binbits[i]))
                {
                    ret = false;
                    break;
                }
            }
            return ret;
        }

        public static string GetBinBits(AssayLogic assayLogic, SampleResult sampleResult)
        {
            StringBuilder digitBinBits = new StringBuilder();
            string singleChannelBinbit = string.Empty;

            //Green
            foreach (CTChannel ctChannel in sampleResult.CtChannelList)
            {
                string channelName = ChannelName.GREEN.ToString();
                if (ctChannel.ChannelName == channelName.ToString())
                {
                    if (assayLogic.IsChannelContained(ctChannel.ChannelName))
                    {
                        singleChannelBinbit = CommonMethods.GetSingleBinbit(sampleResult.PageName, sampleResult.SampleType, ctChannel, assayLogic);
                        digitBinBits.Append(singleChannelBinbit);
                        break;
                    }
                }
            }

            //Yellow
            foreach (CTChannel ctChannel in sampleResult.CtChannelList)
            {
                string channelName = ChannelName.YELLOW.ToString();
                if (ctChannel.ChannelName == channelName.ToString())
                {
                    if (assayLogic.IsChannelContained(ctChannel.ChannelName))
                    {
                        singleChannelBinbit = CommonMethods.GetSingleBinbit(sampleResult.PageName, sampleResult.SampleType, ctChannel, assayLogic);
                        digitBinBits.Append(singleChannelBinbit);
                        break;
                    }
                }
            }

            //Orange
            foreach (CTChannel ctChannel in sampleResult.CtChannelList)
            {
                string channelName = ChannelName.ORANGE.ToString();

                if (ctChannel.ChannelName == channelName.ToString())
                {
                    if (assayLogic.IsChannelContained(ctChannel.ChannelName))
                    {
                        singleChannelBinbit = CommonMethods.GetSingleBinbit(sampleResult.PageName, sampleResult.SampleType, ctChannel, assayLogic);
                        digitBinBits.Append(singleChannelBinbit);
                        break;
                    }
                }
            }

            //Red
            foreach (CTChannel ctChannel in sampleResult.CtChannelList)
            {
                string channelName = ChannelName.RED.ToString();

                if (ctChannel.ChannelName == channelName.ToString())
                {
                    if (assayLogic.IsChannelContained(ctChannel.ChannelName))
                    {
                        singleChannelBinbit = CommonMethods.GetSingleBinbit(sampleResult.PageName, sampleResult.SampleType, ctChannel, assayLogic);
                        digitBinBits.Append(singleChannelBinbit);
                        break;
                    }
                }
            }
            
            return digitBinBits.ToString();
        }

        //public static string GetDescription(Enum value)
        //{
        //    Type type = value.GetType();
        //    string name = Enum.GetName(type, value);
        //    if (name != null)
        //    {
        //        FieldInfo field = type.GetField(name);
        //        if (field != null)
        //        {
        //            DescriptionAttribute attr =
        //                   Attribute.GetCustomAttribute(field,
        //                     typeof(DescriptionAttribute)) as DescriptionAttribute;
        //            if (attr != null)
        //            {
        //                return attr.Description;
        //            }
        //        }
        //    }
        //    return string.Empty;
        //}

        public static void UpdateAllCharBits(string charBits, List<SampleResult> sampleResultList)
        {
            foreach (SampleResult sampleResult in sampleResultList)
            {
                if (sampleResult.CharBits == string.Empty)
                {
                    sampleResult.CharBits = charBits;
                }
            }
        }

        public static void UpdateAllInterpretations(string result, string validity, List<SampleResult> sampleResultList)
        {
            foreach (SampleResult sampleResult in sampleResultList)
            {
                sampleResult.TestResult = result;
                sampleResult.Validity = validity;
            }
        }

        public static void GetInterpretations(List<ResultMapping> resultMappingList, List<SampleResult> sampleResultList)
        {
            foreach (SampleResult sampleResult in sampleResultList)
            {
                foreach (ResultMapping resultMapping in resultMappingList)
                {
                    if (resultMapping.CharBits == sampleResult.CharBits)
                    {
                        sampleResult.TestResult = resultMapping.Result;
                        sampleResult.Validity = resultMapping.Validity;
                        break;
                    }
                }
            }
        }

        
        public static string GetSingleBinbit(string pageName, SampleType sampleType, CTChannel ctChannel, AssayLogic assayLogic)
        {
            string targetBinbit;
            double rangeMax =0;
            double rangeMin =0;

            Range targetRange = assayLogic.GetRange(ctChannel.ChannelName, pageName, sampleType, assayLogic.RangeChannelList);

            if ((targetRange.Type != string.Empty) && (targetRange.Type != ""))
            {
                rangeMax = Convert.ToDouble(targetRange.MaximumValue);
                rangeMin = Convert.ToDouble(targetRange.MinimumValue);
            }

            return targetBinbit = CompareValue(sampleType, ctChannel, rangeMax, rangeMin);
        }
        
        private static string CompareValue(SampleType sampleType, CTChannel ctChannel, double rangeMax, double rangeMin)
        {   
            string targetBinbit = string.Empty;
            string targetValue = string.Empty;

            if (sampleType != SampleType.DeltaSample)
            {
                targetValue = ctChannel.CtValue;
            }
            else
            {
                targetValue = ctChannel.CustomList[0].Value;
            }

            if (targetValue == string.Empty)
            {
                //if (ctChannel.CtComment.Trim().ToUpper().Contains("MULTI"))
                //{
                //    return targetBinbit = "5";
                //}
                //else if (ctChannel.CtComment.Trim().ToUpper() == "NEG (NTC)")
                //{
                //    return targetBinbit = "6";
                //}
                //else if ((ctChannel.CtComment == string.Empty) || (ctChannel.CtComment == ""))
                //{
                    return targetBinbit = "4";
                //}
            }
            else
            {
                double doubleTargetValue = Convert.ToDouble(targetValue);
                if (doubleTargetValue < rangeMin)
                {
                    targetBinbit = "1";
                }
                else if (doubleTargetValue > rangeMax)
                {
                    targetBinbit = "3";
                }
                else
                {
                    targetBinbit = "2";
                }
            }

            return targetBinbit;
        }
        
        public static string GetRealCalcConc(string calcConc)
        {
            string target = string.Empty;

            if (calcConc != string.Empty)
            {
                target = Math.Pow(10, Convert.ToDouble(calcConc)).ToString();
            }

            return target;
        }

        //public static string GetQuantBinbits(List<QuantRange> quantRangeList, SampleResult sampleResult)
        //{
        //    StringBuilder quantBinBits = new StringBuilder();
        //    string singleQuantBinbit = string.Empty;
        //    Custom calcConc;

        //    for (int i = 0; i < sampleResult.CtChannelList.Count; i++)
        //    {
        //        calcConc = Custom.GetCustom(CustomHeadText.CalcConc, sampleResult.CtChannelList[i].CustomList);
        //        calcConc.IsShow = true;
        //        calcConc.Value = GetRealCalcConc(sampleResult.CtChannelList[i].CalcConc);
        //    }

        //    string channelName;
        //    QuantRange quantRange;
        //    List<Custom> customList;

        //    //Green
        //    channelName = CommonMethods.GetDescription(ChannelName.Green);
        //    quantRange = AssayLogic.GetQuantRange(channelName, sampleResult.SampleType, quantRangeList);

        //    if (quantRange.Channel != string.Empty)
        //    { 
        //        customList = CTChannel.GetCTChannel(channelName, sampleResult.CtChannelList).CustomList;
        //        calcConc = Custom.GetCustom(CustomHeadText.CalcConc, customList);

        //        singleQuantBinbit = CommonMethods.GetSingleQuantBinbit(quantRange, sampleResult, calcConc);
               
        //        quantBinBits.Append(singleQuantBinbit);
        //    }

        //    //Yellow
        //    channelName = CommonMethods.GetDescription(ChannelName.Yellow);
        //    quantRange = AssayLogic.GetQuantRange(channelName, sampleResult.SampleType, quantRangeList);

        //    if (quantRange.Channel != string.Empty)
        //    {
        //        customList = CTChannel.GetCTChannel(channelName, sampleResult.CtChannelList).CustomList;
        //        calcConc = Custom.GetCustom(CustomHeadText.CalcConc, customList);

        //        singleQuantBinbit = CommonMethods.GetSingleQuantBinbit(quantRange, sampleResult, calcConc);

        //        quantBinBits.Append(singleQuantBinbit);
        //    }

        //    //Yellow
        //    channelName = CommonMethods.GetDescription(ChannelName.Orange);
        //    quantRange = AssayLogic.GetQuantRange(channelName, sampleResult.SampleType, quantRangeList);

        //    if (quantRange.Channel != string.Empty)
        //    {
        //        customList = CTChannel.GetCTChannel(channelName, sampleResult.CtChannelList).CustomList;
        //        calcConc = Custom.GetCustom(CustomHeadText.CalcConc, customList);

        //        singleQuantBinbit = CommonMethods.GetSingleQuantBinbit(quantRange, sampleResult, calcConc);

        //        quantBinBits.Append(singleQuantBinbit);
        //    }

        //    return quantBinBits.ToString();
        //}

        //private static string GetSingleQuantBinbit(QuantRange quantRange, SampleResult sampleResult, Custom custom)
        //{
        //    string targetQuantBinbit = string.Empty;

        //    double rangeMax = quantRange.MaximumValue;
        //    double rangeMin = quantRange.MinimumValue;

        //    if (custom.HeadText == CustomHeadText.Ratio.ToString())
        //    {
        //        if (custom.Value == string.Empty)
        //        {
        //            custom.Value = " - ";
        //            targetQuantBinbit = "4";
        //        }
        //        else
        //        {
        //            double doubleCalcConc = Convert.ToDouble(custom.Value);

        //            if (doubleCalcConc < rangeMin)
        //            {
        //                custom.Value = Math.Round(doubleCalcConc * 100, 4) + "% ";
        //                targetQuantBinbit = "1";
        //            }
        //            else if (doubleCalcConc > rangeMax)
        //            {
        //                custom.Value = Math.Round(doubleCalcConc * 100, 4) + "% ";
        //                targetQuantBinbit = "3";
        //            }
        //            else
        //            {
        //                custom.Value = Math.Round(doubleCalcConc * 100, 4) + "% ";
        //                targetQuantBinbit = "2";
        //            }
        //        }
        //    }
        //    else /*if (custom.HeadText == CustomHeadText.CalcConc.ToString())*/
        //    {
        //        foreach (CTChannel ctChannel in sampleResult.CtChannelList)
        //        {
        //            if (ctChannel.ChannelName == quantRange.Channel)
        //            {
        //                if (ctChannel.CalcConc == string.Empty)
        //                {
        //                    return targetQuantBinbit = "4";
        //                }
        //                else
        //                {
        //                    double doubleCalcConc = Convert.ToDouble(ctChannel.CalcConc);

        //                    if (doubleCalcConc < rangeMin)
        //                    {
        //                        return targetQuantBinbit = "1";
        //                    }
        //                    else if (doubleCalcConc > rangeMax)
        //                    {
        //                        return targetQuantBinbit = "3";
        //                    }
        //                    else
        //                    {
        //                        return targetQuantBinbit = "2";
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    //string percentRange = GetPercentRange(rangeMin, rangeMax);
        //    return targetQuantBinbit;
        //}

        //public static bool FillDeltaCTValues(AssayLogic assayLogic
        //    , SampleResult sampleResult
        //    , string icPageName
        //    , List<SampleResult> sampleResultList)
        //{
        //    List<SampleResult> icSampleResultList = ReportResult.GetSampleResultsByPage(icPageName, sampleResultList);

        //    PositionGroup positionGroup = assayLogic.SampleSize.GetPositionGroup(sampleResult.Position);

        //    foreach (SampleResult icSampleResult in icSampleResultList)
        //    {
        //        if (positionGroup.PositionList.Contains(icSampleResult.Position))
        //        {
        //            for (int i = 0; i < icSampleResult.CtChannelList.Count;i++ )
        //            {
        //                sampleResult.CtChannelList[i].CustomList[0].IsShow = true;

        //                if (icSampleResult.CtChannelList[i].CtValue != string.Empty)
        //                {
        //                    double ctBaseValue = Convert.ToDouble(icSampleResult.CtChannelList[i].CtValue);

        //                    if (sampleResult.CtChannelList[i].CtValue == string.Empty)
        //                    {
        //                        sampleResult.CtChannelList[i].CustomList[0].Value = string.Empty;
        //                    }
        //                    else
        //                    {
        //                        sampleResult.CtChannelList[i].CustomList[0].Value = Math.Abs(Convert.ToDouble(sampleResult.CtChannelList[i].CtValue)
        //                            - ctBaseValue).ToString();
        //                    }
        //                    return true;
        //                }
        //            }
        //            break;
        //        }
        //    }
        //    return false;
        //}

        //public static void AddOncologyPrefix(List<SampleResult> sampleResultList)
        //{
        //    foreach (SampleResult sampleResult in sampleResultList)
        //    {
        //        if (!sampleResult.TestResult.ToUpper().Contains("All".ToUpper()))
        //        {
        //            sampleResult.TestResult = sampleResult.PageName + " " + sampleResult.TestResult;
        //        }

        //        if (!sampleResult.Validity.ToUpper().Contains("All".ToUpper()))
        //        {
        //            sampleResult.Validity = sampleResult.PageName + " " + sampleResult.Validity;
        //        }

        //        //if ((sampleResult.TestResult.ToUpper().Contains("Test".ToUpper()))
        //        //    ||(sampleResult.TestResult.ToUpper().Contains("Positive".ToUpper()))
        //        //    ||(sampleResult.TestResult.ToUpper().Contains("Negative".ToUpper())))
        //        //{
        //        //    sampleResult.TestResult = sampleResult.PageName + " " + sampleResult.TestResult;
        //        //}

        //        //if (sampleResult.Validity.ToUpper().Contains("Test".ToUpper()))
        //        //{
        //        //    sampleResult.Validity = sampleResult.PageName + " " + sampleResult.Validity;
        //        //}
        //    }
        //}

        //public static string GetQuantBinbits(List<QuantRange> quantRangeList
        //    , SampleResult sampleResult1
        //    , SampleResult sampleResult2)
        //{
        //    StringBuilder quantBinBits = new StringBuilder();
        //    string singleQuantBinbit = string.Empty;
        //    Custom ratio;

        //    for (int i = 0; i < sampleResult1.CtChannelList.Count; i++)
        //    {
        //        ratio = Custom.GetCustom(CustomHeadText.Ratio, sampleResult1.CtChannelList[i].CustomList);
        //        ratio.IsShow = true;

        //        if ((sampleResult1.CtChannelList[i].CalcConc != string.Empty)
        //            && (sampleResult2.CtChannelList[i].CalcConc != string.Empty))
        //        {
        //            ratio.Value = ((Convert.ToDouble(GetRealCalcConc(sampleResult1.CtChannelList[i].CalcConc)))/
        //                (Convert.ToDouble(GetRealCalcConc(sampleResult2.CtChannelList[i].CalcConc)))).ToString();
        //        }
        //    }

        //    string channelName;
        //    QuantRange quantRange;
        //    List<Custom> customList;
        //    Custom iS;

        //    //Green
        //    channelName= CommonMethods.GetDescription(ChannelName.Green);
        //    quantRange = AssayLogic.GetQuantRange(channelName, sampleResult1.SampleType, quantRangeList);
           
        //    if (quantRange.Channel != string.Empty)
        //    {
        //        customList = CTChannel.GetCTChannel(channelName, sampleResult1.CtChannelList).CustomList;
        //        ratio = Custom.GetCustom(CustomHeadText.Ratio, customList);

        //        if (sampleResult1.SampleType == SampleType.Sample)
        //        {
        //            iS = Custom.GetCustom(CustomHeadText.IS, customList);
        //            iS.IsShow = true;
        //            singleQuantBinbit = CommonMethods.GetSampleSingleQuantBinbit(quantRange, sampleResult1, ratio, iS);
        //        }
        //        else
        //        {
        //            singleQuantBinbit = CommonMethods.GetSingleQuantBinbit(quantRange, sampleResult1, ratio);
        //        }

        //        quantBinBits.Append(singleQuantBinbit);
        //    }
            
        //    //Yellow
        //    channelName = CommonMethods.GetDescription(ChannelName.Yellow);
        //    quantRange = AssayLogic.GetQuantRange(channelName, sampleResult1.SampleType, quantRangeList);
            
        //    if (quantRange.Channel != string.Empty)
        //    {
        //        customList = CTChannel.GetCTChannel(channelName, sampleResult1.CtChannelList).CustomList;
        //        ratio = Custom.GetCustom(CustomHeadText.Ratio, customList);

        //        if (sampleResult1.SampleType == SampleType.Sample)
        //        {
        //            iS = Custom.GetCustom(CustomHeadText.IS, customList);
        //            iS.IsShow = true;
        //            singleQuantBinbit = CommonMethods.GetSampleSingleQuantBinbit(quantRange, sampleResult1, ratio, iS);
        //        }
        //        else
        //        {
        //            singleQuantBinbit = CommonMethods.GetSingleQuantBinbit(quantRange, sampleResult1, ratio);
        //        }

        //        quantBinBits.Append(singleQuantBinbit);
        //    }

        //    //Orange
        //    channelName = CommonMethods.GetDescription(ChannelName.Orange);
        //    quantRange = AssayLogic.GetQuantRange(channelName, sampleResult1.SampleType, quantRangeList);

        //    if (quantRange.Channel != string.Empty)
        //    {
        //        customList = CTChannel.GetCTChannel(channelName, sampleResult1.CtChannelList).CustomList;
        //        ratio = Custom.GetCustom(CustomHeadText.Ratio, customList);

        //        if (sampleResult1.SampleType == SampleType.Sample)
        //        {
        //            iS = Custom.GetCustom(CustomHeadText.IS, customList);
        //            iS.IsShow = true;
        //            singleQuantBinbit = CommonMethods.GetSampleSingleQuantBinbit(quantRange, sampleResult1, ratio, iS);
        //        }
        //        else
        //        {
        //            singleQuantBinbit = CommonMethods.GetSingleQuantBinbit(quantRange, sampleResult1, ratio);
        //        }

        //        quantBinBits.Append(singleQuantBinbit);
        //    }

        //    return quantBinBits.ToString();
        //}


        private static string GetPercentRange(double rangeMin, double rangeMax)
        {
            return "(" + rangeMin * 100 + "% - " + rangeMax * 100 + "%)";
        }

        private static List<int> GetGroupSamples(SampleSize sampleSize, int p)
        {
            List<int> targetPositions = new List<int>();

            foreach (PositionGroup positionGroup in sampleSize.GroupList)
            {
                foreach (int position in positionGroup.PositionList)
                {
                    if (p == position)
                    {
                        return targetPositions = positionGroup.PositionList;
                    }
                }
            }

            return targetPositions;
        }

        public static int GetDuplicateSampleIndex(AssayLogic assayLogic, SampleResult sampleResult)
        {
            int targetIndex = -1;

            List<int> groupIndex = CommonMethods.GetGroupSamples(assayLogic.SampleSize, sampleResult.Position);
            foreach (int i in groupIndex)
            {
                if (i != sampleResult.Position)
                {
                    targetIndex = i;
                    break;
                }
            }

            return targetIndex;
        }

        //private static string GetSampleSingleQuantBinbit(QuantRange quantRange
        //    , SampleResult sampleResult
        //    , Custom ratio
        //    , Custom iS)
        //{
        //    string targetQuantBinbit = string.Empty;

        //    double rangeMax = quantRange.MaximumValue;
        //    double rangeMin = quantRange.MinimumValue;
        //    double multiplier = Convert.ToDouble(quantRange.Multiplier);

        //    //string percentRange = GetPercentRange(rangeMin, rangeMax);

        //    if (ratio.Value == string.Empty)
        //    {
        //        ratio.Value = " - ";
        //        iS.Value = string.Empty;
        //        targetQuantBinbit = "4";
        //    }
        //    else
        //    {
        //        double doubleCalcConc = Convert.ToDouble(ratio.Value);

        //        if (doubleCalcConc < rangeMin)
        //        {
        //            ratio.Value = Math.Round(doubleCalcConc * 100, 4) + "% ";
        //            targetQuantBinbit = "1";
        //        }
        //        else if (doubleCalcConc > rangeMax)
        //        {
        //            ratio.Value = Math.Round(doubleCalcConc * 100, 4) + "%" ;
        //            targetQuantBinbit = "3";
        //        }
        //        else
        //        {
        //            ratio.Value = Math.Round(doubleCalcConc * 100, 4) + "%" ;
        //            targetQuantBinbit = "2";
        //        }

        //        iS.Value = Math.Round(doubleCalcConc * multiplier * 100,4) + "%";
        //    }
        //    return targetQuantBinbit;
        //}

        public static void AddCustomObject(CustomHeadText headText, bool flag, List<SampleResult> sampleResultList)
        {
            foreach (SampleResult sampleResult in sampleResultList)
            {
                foreach (CTChannel ctChannel in sampleResult.CtChannelList)
                {
                    //ctChannel.CustomList.Clear();

                    Custom custom = new Custom();
                    custom.HeadText = headText.ToString();
                    custom.IsShow = flag;
                    ctChannel.CustomList.Add(custom);
                }
            }
        }

        public static bool IsAssayIncluded(string assayName, string[] assayList)
        {
            bool target = false;

            foreach (string assay in assayList)
            {
                if (assay == assayName)
                {
                    return target = true;
                }
            }

            return target;
        }

        public static void RemoveCustomObject(List<SampleResult> list)
        {
            foreach (SampleResult sampleResult in list)
            {
                foreach (CTChannel ctChannel in sampleResult.CtChannelList)
                {
                    ctChannel.CustomList.Clear();
                }
            }
        }

        public static void SplitResultForHBV(string assayName, List<SampleResult> sampleResultList)
        {
            if ((assayName == "HBV Quant") || (assayName.ToUpper().Contains("HBV".ToUpper())))
            {
                foreach (SampleResult sample in sampleResultList)
                {
                    if (sample.TestResult.Contains("@"))
                    {
                        foreach (CTChannel channel in sample.CtChannelList)
                        {
                            if (channel.ChannelName.ToUpper().Contains("Green".ToUpper()))
                            {
                                double calcConc = Convert.ToDouble(channel.CalcConc);

                                if ((calcConc >= 12) && (calcConc < 15))
                                {
                                    sample.TestResult = sample.TestResult.Split('@').First();
                                }
                                else if ((calcConc>=15)&&(calcConc<=100000000))
                                {
                                    sample.TestResult = sample.TestResult.Split('@').Last();
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void SplitBCRABLResult(List<SampleResult> sampleResultList)
        {
            foreach (SampleResult sample in sampleResultList)
            {
                if (sample.TestResult.Contains("@"))
                {
                    if ((sample.PageName.ToUpper().Contains("Major".ToUpper()))
                        || (sample.PageName.ToUpper().Contains("minor".ToUpper())))
                    {
                        sample.TestResult = sample.TestResult.Split('@').First();
                    }
                    else if (sample.PageName.ToUpper().Contains("ABL".ToUpper()))
                    {
                        sample.TestResult = sample.TestResult.Split('@').Last();
                    }
                }
            }
        }

        public static void AddOncologyQuantPrefix(List<SampleResult> sampleResultList)
        {
            foreach (SampleResult sample in sampleResultList)
            {
                if (sample.SampleType == SampleType.Sample)
                {
                    sample.TestResult = sample.PageName + " " + sample.TestResult;
                }
            }
        }
    }
}
