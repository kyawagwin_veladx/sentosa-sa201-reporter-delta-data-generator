﻿using M_Assay;
using System.IO;
using System.Xml;
using M_CheckSum;
using M_Template;
using M_SampleList;
using M_ReportResult;
using M_CommonMethods;
using System.Collections;
using System.Collections.Generic;

namespace M_AssayFile
{
    public class AssayFile
    {
        public static void GenerateAssayFile(string runTime, AssayInfo assayInfo, SMPObject smpObject, RunTemplate template)
        {
            string filePath = assayInfo.AnalysisFolder + Path.DirectorySeparatorChar + "Run " + runTime + ".sdx";

            XmlDocument xml = new XmlDocument();
            XmlElement root = xml.CreateElement(Settings.assayFile);
            xml.AppendChild(root);

            XmlElement assayNameNode = xml.CreateElement(Settings.assayName);
            assayNameNode.InnerText = assayInfo.AssayName;
            root.AppendChild(assayNameNode);

            XmlElement notesNode = xml.CreateElement(Settings.notes);
            notesNode.InnerText = template.Notes;
            root.AppendChild(notesNode);

            XmlElement operatorNode = xml.CreateElement(Settings.operatorName);
            operatorNode.InnerText = template.OperatorName;
            root.AppendChild(operatorNode);

            XmlElement idListInfoNode = xml.CreateElement(Settings.idListInfo);
            root.AppendChild(idListInfoNode); 

            foreach (IDListItem iDListItem in smpObject.IDListItems)
            {
                XmlElement element = xml.CreateElement(iDListItem.Item);
                element.InnerText = iDListItem.Value;
                idListInfoNode.AppendChild(element);
            }

            XmlElement pagesNode = xml.CreateElement(Settings.pages);
            root.AppendChild(pagesNode);

            foreach (SMPPage page in smpObject.PageList)
            {
                XmlElement pageNode = xml.CreateElement(Settings.page);
                pagesNode.AppendChild(pageNode);

                XmlElement pageNameNode = xml.CreateElement(Settings.name);
                pageNameNode.InnerText = page.PageName;
                pageNode.AppendChild(pageNameNode);

                XmlElement m1Node = xml.CreateElement(Settings.m1);
                m1Node.InnerText = page.M1;
                pageNode.AppendChild(m1Node);
            }

            xml.Save(filePath);

            FileCheckSum.ModifyCheckSum(filePath);
        }

        public static void FillSDXInfo(string filePath, ReportResult reportResult)
        {
            XmlDocument xml = FileOperations.LoadXML(filePath);

            XmlNode assayNameNode = xml.DocumentElement.SelectSingleNode(Settings.assayName);
            if (assayNameNode != null)
            {
                reportResult.RunInfo.AssayName = assayNameNode.InnerText;
            }

            XmlNode operatorNode = xml.DocumentElement.SelectSingleNode(Settings.operatorName);
            if (operatorNode != null)
            {
                reportResult.RunInfo.SetOperator(operatorNode.InnerText);
            }

            XmlNode notesNode = xml.DocumentElement.SelectSingleNode(Settings.notes);
            if (notesNode != null)
            {
                reportResult.RunInfo.SetNotes(notesNode.InnerText);
            }

            reportResult.IDListItems.Clear();
            if (xml.DocumentElement.SelectSingleNode(Settings.idListInfo) != null)
            {
                XmlNodeList idListInfoNodes = xml.DocumentElement.SelectSingleNode(Settings.idListInfo).ChildNodes;

                if (idListInfoNodes != null)
                {
                    foreach (XmlNode node in idListInfoNodes)
                    {
                        IDListItem iDListItem = new IDListItem();
                        iDListItem.Item = node.Name;
                        iDListItem.Value = node.InnerText;
                        iDListItem.DisplayName = GetDisplayName(node.Name);
                        iDListItem.IsDisplayed = IsDisplayed(node.Name);
                        reportResult.IDListItems.Add(iDListItem);
                    }
                }
            }

            XmlNodeList pageNames = xml.DocumentElement.SelectSingleNode(Settings.pages).SelectSingleNode(Settings.page).SelectNodes(Settings.name);
            if (pageNames.Count >0)
            {
                reportResult.FillPageNames(pageNames[0].InnerText);
            }

            XmlNodeList m1s = xml.DocumentElement.SelectSingleNode(Settings.pages).SelectSingleNode(Settings.page).SelectNodes(Settings.m1);
            if (m1s.Count > 0)
            {
                reportResult.FillM1s(m1s[0].InnerText);
            }
        }

        private static bool IsDisplayed(string itemName)
        {
            switch (itemName)
            { 
                case "ListID":
                case "ListName":
                case "CreationDate":
                case "AssayKit":
                case "LysisKit":
                case "PlateID":
                    return true;
                default:
                    return false;
            }
        }

        private static string GetDisplayName(string itemName)
        {
            switch (itemName)
            { 
                case "ListID":
                    return "ID List";
                case "ListName":
                    return "ID List Name";
                case "ListType":
                    return "ID List Type";
                case "CreationDate":
                    return "ID Creation Date";
                case "AssayKit":
                    return "Assay Kit";
                case "LysisKit":
                    return "Lysis Kit";
                case "PlateID":
                    return "Plate ID";
                default:
                    return itemName;
            }
        }
        /// <summary>
        /// Get M1 Item List
        /// </summary>
        /// <param name="xmlPath"></param>
        /// <returns></returns>
        public static List<string> GetM1ItemList(string xmlPath)
        {
            List<string> target = new List<string>();

            XmlDocument xml = FileOperations.LoadXML(xmlPath);

            XmlNodeList m1s = xml.DocumentElement.SelectSingleNode(Settings.pages).SelectSingleNode(Settings.page).SelectNodes(Settings.m1);

            if (m1s.Count > 0)
            {
                foreach (XmlNode m1 in m1s)
                {
                    string m1Item = InfoConvert.GetM1ItemNumber(m1.InnerText);  //get M1 item number

                    target.Add(m1Item);
                }
            }

            return target;
        }
    }
}
