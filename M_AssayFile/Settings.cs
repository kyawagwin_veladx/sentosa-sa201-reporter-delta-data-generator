﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_AssayFile
{
    internal class Settings
    {
        internal static string assayName = "AssayName";
        internal static string assayFile = "AssayFile";
        public static string notes = "Notes";
        public static string operatorName = "Operator";
        public static string pages = "Pages";
        public static string page = "Page";
        public static string name = "Name";
        public static string m1 = "M1";
        public static string assayKit = "AssayKit";
        public static string lysisKit = "LysisKit";
        public static string plateID = "PlateID";
        public static string idListInfo = "IDListInfo";
    }
}
