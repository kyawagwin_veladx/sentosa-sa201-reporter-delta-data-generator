﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 23/1/2014
 * Author: Yang Yi
 * 
 * Modification Date: 23/06/2014 - Liu Rui
 * Modification: Added ModifyCheckSum, CompareCheckSum, GetCheckSumNodeName and Load methods
 * 
 * Description:
 * CheckSum module
 * 
 */
#endregion


using M_CommonMethods;
using M_Language;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace M_CheckSum
{
    public class FileCheckSum
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private static string GetSHA256(string text)
        {
            ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            byte[] message = encoding.GetBytes(text);
            byte[] hashValue;

            SHA256Managed hashString = new SHA256Managed();
            string hex = string.Empty;

            hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue)
            {
                hex += String.Format("{0:x2}", x);
            }
            return hex;
        }

        /// <summary>
        /// Modify file's checksum
        /// </summary>
        /// <param name="filePath">file path</param>
        public static void ModifyCheckSum(string filePath)
        {
            XmlDocument xDoc = FileOperations.LoadXML(filePath);

            XmlNode signatureNode = xDoc.DocumentElement.SelectSingleNode(GetCheckSumNodeName(filePath));

            if (signatureNode != null)
            {
                xDoc.DocumentElement.RemoveChild(signatureNode);
            }

            string checksum = string.Empty;

            if (Path.GetExtension(filePath) == ".xml")
            {
                checksum = GetSHA256(xDoc.InnerXml);
            }
            else
            {
                checksum = GetSHA256(xDoc.InnerXml + Settings.velaChecksumCode);
            }

            XmlNode Xnode = xDoc.CreateNode(XmlNodeType.Element, GetCheckSumNodeName(filePath), null);

            Xnode.InnerText = checksum;

            xDoc.DocumentElement.AppendChild(Xnode);

            xDoc.Save(filePath);
        }

        public static bool CompareCSVCheckSum(string filePath)
        {
            if (Path.GetExtension(filePath) == ".csv")
            {
                List<string[]> parsedData = FileOperations.LoadCSV(filePath);

                /*string[] checksumRow = FileOperations.GetCSVSignatureRow(GetCheckSumNodeName(filePath), parsedData);

                if ((checksumRow!=null)&&(checksumRow.Length>=2))
                {
                    string oldChecksum = checksumRow[1];

                    parsedData.Remove(checksumRow);

                    string newChecksum = GetSHA256(GetCSVString(parsedData));

                    if (oldChecksum == newChecksum)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }*/
                List<string[]> checksumRow = FileOperations.GetCSVSignatureRow(GetCheckSumNodeName(filePath), parsedData);

                if ((checksumRow[0] != null) && (checksumRow[0].Length >= 2))
                {
                    string oldChecksum = checksumRow[0][1];

                    parsedData.Remove(checksumRow[0]);

                    string newChecksum = GetSHA256(GetCSVString(parsedData));

                    if (oldChecksum == newChecksum)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private static string GetCSVString(List<string[]> parsedData)
        {
            string target = string.Empty;

            foreach (string[] array in parsedData)
            {
                if (array[0].ToString() != "Signature")
                {
                    foreach (string value in array)
                    {
                        target += value;
                    }
                }
            }

            return target;
        }

        /*private static bool IsChecksumExisting(string filePath, List<string[]> parsedData)
        {
            if (FileOperations.GetCSVSignatureRow(GetCheckSumNodeName(filePath), parsedData) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        */

        /// <summary>
        /// Verify checksum of the file
        /// </summary>
        /// <param name="filePath">file path</param>
        /// <returns>true/false</returns>
        public static bool CompareCheckSum(string filePath)
        {
            XmlDocument xDoc = FileOperations.LoadXML(filePath);

            if (xDoc == null)
            {
                return false;
            }

            XmlNode xNode = xDoc.DocumentElement.SelectSingleNode(GetCheckSumNodeName(filePath));

            if (xNode == null)
            {
                return false;
            }

            string oldchecksum = xNode.InnerText;

            xDoc.DocumentElement.RemoveChild(xNode);

            string newChecksum = string.Empty;

            if (Path.GetExtension(filePath) == Settings.xmlExtension || Path.GetExtension(filePath) == Settings.smpExtension)
            {
                newChecksum = GetSHA256(xDoc.InnerXml);
            }
            else
            {
                newChecksum = GetSHA256(xDoc.InnerXml + Settings.velaChecksumCode);
            }

            if (newChecksum.CompareTo(oldchecksum) == 0)
            {
                return true;
            }
            else
            {
                MessageBox.Show(SysMessage.invalidChechsum + SysMessage.filePath + filePath, 
                    SysMessage.fileChecksumCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
              
                return false;
            }
        }

      
        private static string GetCheckSumNodeName(string filePath)
        {
            string extension = Path.GetExtension(filePath);

            string target = string.Empty;

            switch (extension)
            {
                case ".xml":
                    target = Settings.checkSumNode;//checkSumNode = "Checksum";
                    break;
                case ".smp":
                    target = Settings.checkSumNode;//checkSumNode = "Checksum";
                    break;
                default:
                    target = Settings.signatureNode;//signatureNode = "Signature";
                    break;
            }

            return target;
        }

        /// <summary>
        /// Load XML file and verify the checksum
        /// </summary>
        /// <param name="filePath">file path</param>
        /// <returns>XmlDocument</returns>
        /// 
        public static XmlDocument Load(string filePath)
        {
            XmlDocument targetxDoc = new XmlDocument();

            if (!File.Exists(filePath))
            {
                MessageBox.Show(SysMessage.fileMissMsg + filePath, SysMessage.invalidPath, 
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return targetxDoc;
            }
            if (CompareCheckSum(filePath))
            {
                return targetxDoc = FileOperations.LoadXML(filePath); 
            }
            else
            {
                return targetxDoc;
            }
        }
    }
}
