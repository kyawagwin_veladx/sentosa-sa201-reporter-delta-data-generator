﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 21/04/2014
 * Author: Liu Rui
 * 
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_CheckSum
{
    internal class Settings
    {
        internal static string smpExtension = ".smp";
        internal static string checkSumNode = "Checksum";
        internal static string signatureNode = "Signature";
        internal static string velaChecksumCode = "VELADX_REPORT";
        internal static string xmlExtension = ".xml";
    }
}
