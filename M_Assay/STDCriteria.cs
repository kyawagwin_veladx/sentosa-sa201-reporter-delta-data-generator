﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 09/01/2013
 * Author: Liu Rui
 * 
 * Description:
 * 
 */
#endregion


using M_CommonMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace M_Assay
{
    //public class STDCriteria
    //{
    //    private string standardCurveAssayName = string.Empty;
    //    private Criteria criteria = new Criteria();
    //    private List<DisplayChannel> displayChannelList = new List<DisplayChannel>();

    //    public List<DisplayChannel> DisplayChannelList
    //    {
    //        get { return displayChannelList; }
    //        set { displayChannelList = value; }
    //    }

    //    public Criteria Criteria
    //    {
    //        get { return criteria; }
    //        set { criteria = value; }
    //    }

    //    public string StandardCurveAssayName
    //    {
    //        get { return standardCurveAssayName; }
    //        set { standardCurveAssayName = value; }
    //    }

    //    public bool IsSTDPassed(string pageDisplayName, double reactionEfficiency, double rValue, bool isSTDImport)
    //    {
    //        bool targetResult = true;

    //        if (!pageDisplayName.Contains(criteria.IgnoredChannel))
    //        {
    //            if (!isSTDImport)
    //            {
    //                if ((reactionEfficiency < criteria.Efficiency.MinimumValue)
    //                || (reactionEfficiency > criteria.Efficiency.MaximumValue)
    //                || (Math.Pow(rValue, 2) < criteria.RSquare.MinimumValue))
    //                {
    //                    return false;
    //                }
    //            }
    //            else
    //            {
    //                if ((reactionEfficiency < criteria.Efficiency.MinimumValue)
    //                || (reactionEfficiency > criteria.Efficiency.MaximumValue))
    //                {
    //                    return false;
    //                }
    //            }
    //        }

    //        return targetResult;
    //    }

    //    public void FillSTDCriteria(string assayConfigPath)
    //    {

    //        XmlDocument assayConfigXML = FileOperations.LoadXML(assayConfigPath);
    //        if (assayConfigXML.DocumentElement != null)
    //        {
    //            XmlNode stdCriteriaNode = assayConfigXML.SelectSingleNode(Settings.rootNode)
    //                .SelectSingleNode(Settings.assayInfoNode).SelectSingleNode(Settings.stdInfoNode);

    //            if (stdCriteriaNode != null)
    //            {
    //                standardCurveAssayName = stdCriteriaNode.SelectSingleNode(Settings.stdAssayNameNode).InnerText;

    //                XmlNode criteriaNode = stdCriteriaNode.SelectSingleNode(Settings.criteriaNode);

    //                XmlNode ignoreChannelNode = criteriaNode.SelectSingleNode(Settings.ignoredChannelNode);
    //                if (ignoreChannelNode != null)
    //                {
    //                    criteria.IgnoredChannel = criteriaNode.SelectSingleNode(Settings.ignoredChannelNode).InnerText;
    //                }

    //                XmlNode efficiencyNode = criteriaNode.SelectSingleNode(Settings.reactionEfficiencyNode);
    //                criteria.Efficiency.MaximumValue = Convert.ToDouble(efficiencyNode.SelectSingleNode(Settings.maximumValueNode).InnerText);
    //                criteria.Efficiency.MinimumValue = Convert.ToDouble(efficiencyNode.SelectSingleNode(Settings.minimumValueNode).InnerText);

    //                XmlNode rSquareNode = criteriaNode.SelectSingleNode(Settings.rSquareValueNode);
    //                criteria.RSquare.MinimumValue = Convert.ToDouble(efficiencyNode.SelectSingleNode(Settings.minimumValueNode).InnerText);

    //                XmlNodeList displayChannelNodeList = stdCriteriaNode.SelectSingleNode(Settings.channelDisplayNode).SelectNodes(Settings.channelNode);
    //                foreach (XmlNode displayChannelNode in displayChannelNodeList)
    //                {
    //                    DisplayChannel channel = new DisplayChannel();
    //                    channel.ChannelName = displayChannelNode.SelectSingleNode(Settings.nameNode).InnerText;

    //                    string isShow = displayChannelNode.SelectSingleNode(Settings.showNode).InnerText;
    //                    if (isShow == "1")
    //                    {
    //                        channel.IsShow = true;
    //                    }
    //                    else if (isShow == "0")
    //                    {
    //                        channel.IsShow = false;
    //                    }

    //                    displayChannelList.Add(channel);
    //                }
    //            }
    //        }
    //    }
    //}
}
