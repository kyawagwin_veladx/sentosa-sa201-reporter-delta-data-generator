﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 09/01/2013
 * Author: Liu Rui
 * 
 * Description:
 * 
 */
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;
using M_CheckSum;
using M_CommonMethods;
using M_SampleList;
using M_ReportResult;
using System.Linq;

namespace M_Assay
{
    /// <summary>
    /// Define assay mode
    /// </summary>
    public enum AssayMode
    {
        Qualitative = 0,
    }

    /// <summary>
    /// This class is used for storing the infomation of certain assay config file
    /// </summary>
    public class AssayInfo
    {
        private string assayName = string.Empty;
        private string templateName = string.Empty;
        private AssayMode assayMode;
        private string diseaseArea = string.Empty;
        private List<SampleSize> sampleSizeList = new List<SampleSize>();
        private List<ConfigPage> pageList = new List<ConfigPage>();
        private List<ConfigChannel> channelList = new List<ConfigChannel>();
        private List<BarcodeFormat> barcodeFormatList = new List<BarcodeFormat>();

        private string assayConfigPath = string.Empty;
        private string message = string.Empty;
        private string runLogFolder = string.Empty;

        private int selectedSizeIndex = -1;
        private string assayConfigFolder = string.Empty;

        private string componentCSV = string.Empty;
        private string deltarnCSV = string.Empty;
        private string ctCSV = string.Empty;
        private string resultCSV = string.Empty;
        private string runSDX = string.Empty;

        private string txtFilePath = string.Empty;
        private string analysisFolder = string.Empty;

        public string AnalysisFolder
        {
            get { return analysisFolder; }
        }

        public string TxtFilePath
        {
            get { return txtFilePath; }
        }

        public string ComponentCSV
        {
            get { return componentCSV; }
            set { componentCSV = value; }
        }

        public string DeltarnCSV
        {
            get { return deltarnCSV; }
            set { deltarnCSV = value; }
        }

        public string CtCSV
        {
            get { return ctCSV; }
            set { ctCSV = value; }
        }

        public string ResultCSV
        {
            get { return resultCSV; }
            set { resultCSV = value; }
        }

        public string RunSDX
        {
            get { return runSDX; }
            set { runSDX = value; }
        }

        public string Message
        {
            get { return message; }
        }

        public string DiseaseArea
        {
            get { return diseaseArea; }
        }

        public string AssayConfigPath
        {
            get { return assayConfigPath; }
        }

        public List<BarcodeFormat> BarcodeFormatList
        {
            get { return barcodeFormatList; }
            set { barcodeFormatList = value; }
        }

        public string AssayConfigFolder
        {
            get { return assayConfigFolder; }
            set { assayConfigFolder = value; }
        }

        public List<ConfigChannel> ChannelList
        {
            get { return channelList; }
            set { channelList = value; }
        }

        public string AssayName
        {
            get { return assayName; }
            set { assayName = value; }
        }

        public string TemplateName
        {
            get { return templateName; }
        }

        public List<ConfigPage> PageList
        {
            get { return pageList; }
            set { pageList = value; }
        }

        public List<SampleSize> SampleSizeList
        {
            get { return sampleSizeList; }
            set { sampleSizeList = value; }
        }

        public AssayMode AssayMode
        {
            get { return assayMode; }
            set { assayMode = value; }
        }

        public int SelectedSizeIndex
        {
            get { return this.selectedSizeIndex; }
            set { selectedSizeIndex = value; }
        }

        /// <summary>
        /// Fill AssayInfo object from the assay config file
        /// </summary>
        /// <param name="assayConfigPath">Assay config file path</param>
        /// <returns>true/false</returns>
        public bool FillAssayInfo(string assayConfigPath)
        {
            if (assayConfigPath == string.Empty)
            {
                return false;
            }

            this.assayConfigPath = assayConfigPath;

            this.assayConfigFolder = Path.GetDirectoryName(assayConfigPath);

            XmlDocument assayConfigXML = FileCheckSum.Load(assayConfigPath);

            if (assayConfigXML.DocumentElement != null)
            {
                XmlNode assayInfoNode = assayConfigXML.SelectSingleNode(Settings.rootNode).SelectSingleNode(Settings.assayInfoNode);
                //Settings.rootNode = "AssayConfig";
                //Settings.assayInfoNode = "AssayInfo";
                //Settings.assayNameNode = "AssayName";
                this.assayName = assayInfoNode.SelectSingleNode(Settings.assayNameNode).InnerText;
                
                XmlNode messageNode = assayInfoNode.SelectSingleNode(Settings.messageNode);

                if (messageNode != null)
                {
                    this.message = messageNode.InnerText;
                }

                this.templateName = assayInfoNode.SelectSingleNode(Settings.templateNameNode).InnerText;

                this.assayMode = (AssayMode)Enum.Parse(typeof(AssayMode), assayInfoNode.SelectSingleNode(Settings.assayModeNode).InnerText);

                this.diseaseArea = assayInfoNode.SelectSingleNode(Settings.diseaseAreaNode).InnerText;

                this.pageList = GetPageList(assayInfoNode);

                this.channelList = GetConfigChannelList(assayInfoNode);

                this.sampleSizeList = GetSampleSizeList(assayInfoNode);

                this.barcodeFormatList = GetBarcodeFormatList(assayInfoNode);

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Fill AssayInfo object according to the M1s of SMPObject and update the selected size
        /// </summary>
        /// <param name="smpObject">SMPObject</param>
        /// <param name="assayConfigFolder">The directory where all the assay config files are located</param>
        /// <returns>true/false</returns>
        public bool GetAssayInfoBySMPObject(SMPObject smpObject, string assayConfigFolder)
        {
            if (smpObject.PageList.Count == 0)
            {
                return false;
            }

            if (assayConfigFolder == string.Empty)
            {
                return false;
            }

            List<string> smpM1List = smpObject.GetM1List();//get assay page names(= 201161), currently only one page 

            string targetAssayConfigFile = GetAssayConfigFile(smpM1List, assayConfigFolder);//get assayconfig file

            if (targetAssayConfigFile != string.Empty)
            {
                FillAssayInfo(targetAssayConfigFile);//fill in assayinfo

                List<string> selectedSamples = smpObject.GetSelectedSamples(smpM1List);//check selected samples

                UpdateSelectedSizeIndex(selectedSamples);//choose relevanted sample size to use

                return true;
            }
            else
            {
                return false;
            }
        }

       /// <summary>
       /// Use pagename to find the assay config file
       /// </summary>
       /// <param name="smpM1List"></param>
       /// <param name="assayConfigFolder"></param>
       /// <returns></returns>
        public string GetAssayConfigFile(List<string> smpM1List, string assayConfigFolder)
        {
            int count = 0;

            string targetAssayConfigFile = string.Empty;

            string[] assayConfigFileList = Directory.GetFiles(assayConfigFolder, Settings.configExtention);// setting.configExtention = "*.config";

            foreach (string assayConfigFile in assayConfigFileList)
            {
                List<string> m1List = new List<string>();

                try 
                {
                    m1List = GetM1List(assayConfigFile); //get each assay's M1 from reagentID
                }
                catch
                {
                    m1List = new List<string>();
                }

                if (smpM1List.Count == m1List.Count)
                {
                    for (int i = 0; i < smpM1List.Count; i++)
                    {
                        foreach (string m1 in m1List)
                        {
                            if (smpM1List[i].ToString().ToUpper()==m1.ToUpper())
                            {
                                count++;
                                break;
                            }
                        }
                    }

                    if (count == smpM1List.Count)
                    {
                        targetAssayConfigFile = assayConfigFile;
                        break;
                    }
                }
            }

            return targetAssayConfigFile;
        }

        /// <summary>
        /// Update the index of the selected sample size
        /// </summary>
        /// <param name="flag">Assay mode</param>
        /// <param name="selectedSamples">Selected Samples</param>
        public void UpdateSelectedSizeIndex(List<string> selectedSamples)
        {
            int index = -1;
            foreach (SampleSize sampleSize in this.sampleSizeList)//sampleSize and sampleSizeList are in AssayInfo
            {
                index++;
                List<string> configSamples = sampleSize.GetSamples();

                if (selectedSamples.Count == configSamples.Count)
                {
                    if (IsSamplePositionsSame(selectedSamples, configSamples))
                    {
                        selectedSizeIndex = index;
                        break;
                    }
                }
            }
        }

        public void UpdateChannelVirusName(List<SampleResult> resultList)
        {
            Dictionary<string,string> ChannelVirusDictonary = this.GetChannelVirusNameDictonary();
            foreach (SampleResult sampleResult in resultList)
            {
                foreach (CTChannel ctChannel in sampleResult.CtChannelList)
                {
                    string sTempVirusName = string.Empty;
                    if (ChannelVirusDictonary.TryGetValue(ctChannel.ChannelName.ToUpper(), out sTempVirusName))
                    {
                        ctChannel.VirusName = sTempVirusName;
                    }
                }
            }
       
        }

        private bool IsSamplePositionsSame(List<string> selectedSamples, List<string> configSamples)
        {
            int count = 0;

            foreach (string position in selectedSamples)
            {
                if (configSamples.Contains(position))
                {
                    count++;
                }
            }

            if (count == selectedSamples.Count)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static List<string> GetM1List(string assayConfigFile)
        {
            List<string> targetPageNameList = new List<string>();

            XmlDocument assayConfigXML = FileCheckSum.Load(assayConfigFile);

            if (assayConfigXML.DocumentElement != null)
            {
                XmlNodeList pageNodeList = assayConfigXML.SelectSingleNode(Settings.rootNode)
                    .SelectSingleNode(Settings.assayInfoNode).SelectSingleNode(Settings.pagesNode).SelectNodes(Settings.pageNode);
                //Settings.rootNode = "AssayConfig";
                //SettingsassayInfoNode = "AssayInfo";
                //Settings.pagesNode = "Pages";
                //Settings.pageNode = "Page";
                foreach (XmlNode pageNode in pageNodeList)
                {
                    targetPageNameList.Add(pageNode.SelectSingleNode(Settings.reagentIDNode).InnerText);
                }
            }

            return targetPageNameList;
        }

        /// <summary>
        /// Fill the sample types of SMPObject according to the assay config settings
        /// </summary>
        /// <param name="smpObject"></param>
        public void FillSampleType(SMPObject smpObject)
        {
            SampleSize selectedSampleSize = sampleSizeList[selectedSizeIndex];

            for (int i = 0; i < selectedSampleSize.PositionList.Count;i++ )
            {
                SMPPage page = smpObject.PageList[i];
                page.PageName = selectedSampleSize.PositionList[i].PageName;

                foreach (SMPSample smpSample in page.SampleList)
                {
                    foreach (ConfigSample configSample in selectedSampleSize.PositionList[i].SampleList)
                    {
                        if (smpSample.TubePosition == configSample.Position.ToString())
                        {
                            smpSample.Type = configSample.SampleType;
                            break;
                        }
                    }
                }
            }
        }

        public void RefillSampleStatus(List<SampleResult> sampleResultList)
        {
            SampleSize selectedSampleSize = sampleSizeList[selectedSizeIndex];
            List<SampleResult> removeList = new List<SampleResult>();
            
            foreach (SampleResult sampleResult in sampleResultList)
            {

                foreach (Position position in selectedSampleSize.PositionList)
                {
                    if (position.PageName == sampleResult.PageName)
                    {

                        var samplePosition =  position.SampleList.Where(x => x.Position.Equals(sampleResult.Position));
                        if (samplePosition.Count() > 0)
                        {
                            sampleResult.SampleType = (SampleType)Enum.Parse(typeof(SampleType), samplePosition.First().SampleType);
                        }
                        else
                        {
                            removeList.Add(sampleResult);
                        }
                       
                    }
                }
            }
            foreach (SampleResult removeSample in removeList)
            {
                sampleResultList.Remove(removeSample);
            }
         
        }

        private List<ConfigPage> GetPageList(XmlNode assayInfoNode)
        {
            if (assayInfoNode == null)
            {
                throw new ArgumentNullException();
            }

            List<ConfigPage> pageList = new List<ConfigPage>();

            XmlNodeList pageNodeList = assayInfoNode.SelectSingleNode(Settings.pagesNode).SelectNodes(Settings.pageNode);

            if (pageNodeList.Count > 0)
            {
                foreach (XmlNode pageNode in pageNodeList)
                {
                    ConfigPage page = new ConfigPage();
                    page.PageName = pageNode.SelectSingleNode(Settings.nameNode).InnerText;
                    page.M1ItemList = page.GetM1ItemList(pageNode);
                    pageList.Add(page);
                }
            }

            return pageList;
        }

        private List<ConfigChannel> GetConfigChannelList(XmlNode assayInfoNode)
        {
            if (assayInfoNode == null)
            {
                throw new ArgumentNullException();
            }

            List<ConfigChannel> channelList = new List<ConfigChannel>();

            XmlNodeList channelNodeList = assayInfoNode.SelectSingleNode(Settings.channelsNode).SelectNodes(Settings.channelNode);

            if (channelNodeList.Count > 0)
            {
                foreach (XmlNode channelNode in channelNodeList)
                {
                    ConfigChannel channel = new ConfigChannel();
                    foreach (XmlNode childNode in channelNode.ChildNodes)
                    {
                        var objPropertyInfo = channel.GetType().GetProperty(childNode.Name);
                        if (objPropertyInfo != null)
                        {
                            objPropertyInfo.SetValue(channel, childNode.InnerText, null);
                        }

                    }
                    //channel.Name = channelNode.SelectSingleNode(Settings.nameNode).InnerText;
                    //channel.ShortName = channelNode.SelectSingleNode(Settings.shortNameNode).InnerText;
                    //channel.DyeName = channelNode.SelectSingleNode(Settings.dyeNameNode).InnerText;
                    //channel.VirusName = channelNode.
                    channelList.Add(channel);
                }
            }

            return channelList;
        }

        private List<SampleSize> GetSampleSizeList(XmlNode assayInfoNode)
        {
            List<SampleSize> sampleSizeList = new List<SampleSize>();

            XmlNodeList sampleSizeNodeList = assayInfoNode.SelectSingleNode(Settings.sampleSizesNode).SelectNodes(Settings.sampleSizeNode);
            
            if (sampleSizeNodeList.Count > 0)
            {
                foreach (XmlNode sampleSizeNode in sampleSizeNodeList)
                {
                    SampleSize sampleSize = new SampleSize();
                    sampleSize.Name = sampleSizeNode.SelectSingleNode(Settings.nameNode).InnerText;

                    sampleSize.PositionList = GetPositionList(sampleSizeNode);
                    sampleSize.GroupList = GetGroupList(sampleSizeNode);

                    sampleSizeList.Add(sampleSize);
                }
            }
            return sampleSizeList;
        }

        private static List<PositionGroup> GetGroupList(XmlNode sampleSizeNode)
        {
            List<PositionGroup> groupList = new List<PositionGroup>();
            XmlNodeList groupNodeList = sampleSizeNode.SelectSingleNode(Settings.groupsNode).SelectNodes(Settings.groupNode);
            if (groupNodeList.Count > 0)
            {
                foreach (XmlNode groupNode in groupNodeList)
                {
                    PositionGroup group = new PositionGroup();
                    group.PositionList = new List<int>();

                    XmlNodeList positionNodeList = groupNode.SelectNodes(Settings.positionNode);
                    foreach (XmlNode positionNode in positionNodeList)
                    {
                        group.PositionList.Add(Convert.ToInt32(positionNode.InnerText));
                    }
                    groupList.Add(group);
                }
            }
            return groupList;
        }

        private List<Position> GetPositionList(XmlNode sampleSizeNode)
        {
            List<Position> positionList = new List<Position>();
            XmlNodeList positionNodeList = sampleSizeNode.SelectNodes(Settings.positionNode);
            if (positionNodeList.Count > 0)
            {
                foreach (XmlNode positionNode in positionNodeList)
                {
                    Position position = new Position();
                    position.PageName = positionNode.Attributes[Settings.pageNameNode].Value;
                    position.SampleList = GetConfigSampleList(positionNode);
                    positionList.Add(position);
                }
            }
            return positionList;
        }

        private List<ConfigSample> GetConfigSampleList(XmlNode positionNode)
        {
            List<ConfigSample> sampleList = new List<ConfigSample>();
            XmlNodeList sampleNodeList = positionNode.SelectNodes(Settings.sampleNode);
            if (sampleNodeList.Count > 0)
            {
                foreach (XmlNode sampleNode in sampleNodeList)
                {
                    ConfigSample sample = new ConfigSample();
                    sample.Position = Convert.ToInt32(sampleNode.Attributes[Settings.positionNode].Value);
                    sample.SampleType = sampleNode.Attributes[Settings.sampleTypeNode].Value;
                    sampleList.Add(sample);
                }
            }
            return sampleList;
        }

        private List<BarcodeFormat> GetBarcodeFormatList(XmlNode assayInfoNode)
        {
            if (assayInfoNode == null)
            {
                throw new ArgumentNullException();
            }

            List<BarcodeFormat> formatList = new List<BarcodeFormat>();

            XmlNode barcodeFormatNode = assayInfoNode.SelectSingleNode(Settings.barcodeFormatsNode);
            if (barcodeFormatNode!=null)
            {

                XmlNodeList formatNodeList = barcodeFormatNode.SelectNodes(Settings.formatNode);

                if (formatNodeList.Count > 0)
                {
                    foreach (XmlNode formatNode in formatNodeList)
                    {
                        BarcodeFormat format = new BarcodeFormat();

                        XmlNode regexNode = formatNode.SelectSingleNode(Settings.regex);
                        if (regexNode != null)
                        {
                            format.Regex = regexNode.InnerText;
                        }

                        XmlNode typeNode = formatNode.SelectSingleNode(Settings.type);
                        if (typeNode != null)
                        {
                            format.Type = typeNode.InnerText;
                        }

                        formatList.Add(format);
                    }
                }
            }
            return formatList;
        }

        /// <summary>
        /// Verify the new sample name according to the barcode format in assay config file
        /// </summary>
        /// <param name="newSampleName">New barcode</param>
        /// <param name="sampleType">Sample type</param>
        /// <returns>True/false</returns>
        public bool IsSampleNameValid(string newSampleName, string sampleType)
        {
            bool targetResult = false;

            if (sampleType.ToUpper() != SampleType.Sample.ToString().ToUpper())
            {
                foreach (BarcodeFormat format in barcodeFormatList)
                {
                    if (format.Type.ToUpper().Equals(sampleType.ToUpper()))
                    {
                        targetResult = Regex.IsMatch(newSampleName, format.Regex, RegexOptions.IgnoreCase);

                        if (targetResult == true)
                        {
                            return targetResult;
                        }
                    }
                }
            }
            else
            {
                targetResult = true;
            }

            return targetResult;
        }


        private ConfigPage GetConfigPage(string pageName)
        {
            ConfigPage target = new ConfigPage();

            foreach (ConfigPage configPage in pageList)
            {
                if (configPage.PageName == pageName)
                {
                    return target = configPage;
                }
            }

            return target;
        }

        /// <summary>
        /// Verify M1 barcode
        /// </summary>
        /// <param name="newM1">New M1 barcode</param>
        /// <param name="pageName">Page name</param>
        /// <returns>true/false</returns>
        public bool IsM1Valid(string newM1, string pageName)
        {
            bool target = false;

            ConfigPage configPage = GetConfigPage(pageName);

            if (configPage.PageName != string.Empty)
            {
                if (newM1.Length < 8)
                {
                    return target = false;
                }
                else
                {
                    string m1BenchMark = newM1.Substring(0, 8);
                    if (Regex.IsMatch(m1BenchMark, "[AV]\\*\\d{6}", RegexOptions.IgnoreCase))
                    {
                        if (configPage.M1ItemList.Contains(newM1.Substring(2, 6)))
                        {
                            target = true;
                        }
                        else
                        {
                            target = false;
                        }
                    }
                    else
                    {
                        target = false;
                    }
                }
            }

            return target;
        }

        /// <summary>
        /// Get the path of generated TXT file
        /// </summary>
        public void GetTxtFilePath()
        {
            if (!Directory.Exists(this.analysisFolder))
            {
                Directory.CreateDirectory(this.analysisFolder);
            }
           
            string txtName = Path.GetFileNameWithoutExtension(analysisFolder) + ".txt";

            txtFilePath = analysisFolder + Path.DirectorySeparatorChar + txtName;
            
        }

        /// <summary>
        /// Get all the channel names from config file
        /// </summary>
        /// <returns>List of channel names</returns>
        public List<string> GetChannelNames()
        {
            List<string> target = new List<string>();

            foreach (ChannelName channal in Enum.GetValues(typeof(ChannelName)))
            {
                var configChannel = this.channelList.Where(x => x.ShortName.ToUpper().Contains(channal.ToString()));
                if (configChannel.Count() > 0)
                {
                    target.Add(channal.ToString());
                }

            }
            return target;
        }

        public Dictionary<string,string> GetChannelDyeNameDictonary()
        {
            Dictionary<string, string> dictChannel = new Dictionary<string, string>();
            foreach (ChannelName channal in Enum.GetValues(typeof(ChannelName)))
            {
                var configChannel = this.channelList.Where(x => x.ShortName.ToUpper().Contains(channal.ToString()));
                if (configChannel.Count() > 0)
                {
                    dictChannel.Add(channal.ToString(), configChannel.First().DyeName);
                }

            }
            return dictChannel;
        }

        public Dictionary<string, string> GetChannelVirusNameDictonary()
        {
            Dictionary<string, string> dictChannel = new Dictionary<string, string>();
            foreach (ChannelName channelName in Enum.GetValues(typeof(ChannelName)) )
            {
                var configChannel = this.channelList.Where(x => x.ShortName.ToUpper().Contains(channelName.ToString()));
                if (configChannel.Count() > 0)
                {
                    dictChannel.Add(channelName.ToString(), configChannel.First().VirusName);
                }
                
            }
            return dictChannel;
        }


        public string GetVirusNameByChannel(string channel)
        {
            string sRet = string.Empty;
            var channelObj = this.channelList.Where(x=>x.ShortName.Equals(channel));
            if (channelObj.Count()>0)
            {
                sRet = channelObj.First().VirusName;
            }
            return sRet;
        }
        /// <summary>
        /// Fill page names to sampleResults from the assay config file
        /// </summary>
        /// <param name="reportResult">ReportResult object</param>
        public void RefillPageName(ReportResult reportResult)
        {
            foreach (SampleResult sampleResult in reportResult.SampleResultList)
            {
                foreach (Position position in this.sampleSizeList[selectedSizeIndex].PositionList)
                {
                    foreach(ConfigSample sample in position.SampleList)
                    {
                        if (sample.Position == sampleResult.Position)
                        {
                            sampleResult.PageName = position.PageName;
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Set run folder path
        /// </summary>
        /// <param name="folderPath">Run folder path</param>
        public void SetRunFolder(string folderPath)
        {
            this.analysisFolder = folderPath;
        }

        /// <summary>
        /// Set real run folder path for each specific run
        /// </summary>
        /// <param name="runFolder">Default run folder</param>
        /// <param name="runTime">Assay run date time</param>
        public void SetRealRunFolder(string runFolder, string runTime)
        {
            if (templateName != string.Empty)
            {
                //templateName=xxxx.sdt
                string subFolder = Path.GetFileNameWithoutExtension(templateName) + " " + runTime;
                //Path.GetFileNameWithoutExtension(templateName)=xxx
                this.analysisFolder = runFolder + Path.DirectorySeparatorChar + subFolder;
                //Path.DirectorySeparatorChar=\\
                //runFolder=C:\\Vela Diagnostics\\SDS Documents
                //analysisFolder=C:\\Vela Diagnostics\\SDS Documents\\xxx runTime
            }
        }

        public bool IsAllBarcodePassed(ReportResult reportResult)
        {
            string assayName = reportResult.RunInfo.AssayName;

            //Hard code for PQ, email date: 11-Sep-2014
            if (assayName == "van A/van B")
            {
                foreach (SampleResult sr in reportResult.SampleResultList)
                {
                    if (sr.SampleType == SampleType.NC)
                    {
                        if (!Regex.IsMatch(sr.SampleName, "V\\*200644\\*NC\\*\\*", RegexOptions.IgnoreCase))
                        {
                            return false;
                        }
                    }

                    if (sr.SampleType == SampleType.PC)
                    {
                        if (!Regex.IsMatch(sr.SampleName, "V\\*200634\\*PC\\*\\*", RegexOptions.IgnoreCase))
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            else
            {
                foreach (SampleResult sr in reportResult.SampleResultList)
                {
                    if (!this.IsSampleNameValid(sr.SampleName, sr.SampleType.ToString()))
                    {
                        return false;
                    }
                }

                return true;
            }
        }
    }
}
