﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 09/01/2013
 * Author: Liu Rui
 * 
 * Description:
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_Assay
{
    public class Position
    {
        private string pageName = string.Empty;
        private List<ConfigSample> sampleList = new List<ConfigSample>();

        public List<ConfigSample> SampleList
        {
            get { return sampleList; }
            set { sampleList = value; }
        }

        public string PageName
        {
            get { return pageName; }
            set { pageName = value; }
        }

        //internal bool FindSample(string ID)
        //{
        //    bool targetResult = false;

        //    foreach (ConfigSample configSample in sampleList)
        //    {
        //        if (configSample.Position.ToString() == ID)
        //        {
        //            return targetResult = true;
        //        }
        //    }

        //    return targetResult;
        //}
    }
}
