﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 09/01/2013
 * Author: Liu Rui
 * 
 * Description:
 * 
 */
#endregion

using M_CheckSum;
using M_ReportResult;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using M_Language;

namespace M_Assay
{
    /// <summary>
    /// This class is used for storing the information related to result interpretation from the assay config file
    /// </summary>
    public class AssayLogic
    {
        #region Private Member Variables
        private SampleSize sampleSize = new SampleSize();
        private AssayMode assayMode = new AssayMode();

        private List<RangeChannel> rangeChannelList = new List<RangeChannel>();
        private List<ResultMapping> resultMappingList = new List<ResultMapping>();
        private List<BinbitRange> binbitRangeList = new List<BinbitRange>();

        private string assayConfigFile;
        #endregion

        #region Public Properties

        public string AssayConfigFolder
        {
            get { return Path.GetDirectoryName(assayConfigFile); }
        }

        public AssayMode AssayMode
        {
            get { return assayMode; }
            set { assayMode = value; }
        }

        public List<BinbitRange> BinbitRangeList
        {
            get { return binbitRangeList; }
            set { binbitRangeList = value; }
        }

        public SampleSize SampleSize
        {
            get { return sampleSize; }
            set { sampleSize = value; }
        }

        public List<RangeChannel> RangeChannelList
        {
            get { return rangeChannelList; }
            set { rangeChannelList = value; }
        }

        public List<ResultMapping> ResultMappingList
        {
            get { return resultMappingList; }
            set { resultMappingList = value; }
        }
        #endregion

        public List<RangeChannel> GetCTChannelList(XmlDocument assayConfigXML)
        {
            List<RangeChannel> channelList = new List<RangeChannel>();

            XmlNodeList channelNodeList = assayConfigXML.SelectNodes(Settings.rangeChannelNodePath);
            if (channelNodeList.Count > 0)
            {
                foreach (XmlNode channelNode in channelNodeList)
                {
                    RangeChannel ctChannel = new RangeChannel();
                    ctChannel.Name = channelNode.SelectSingleNode(Settings.nameNode).InnerText;

                    ctChannel.PageList = GetCTPageList(channelNode);
                    channelList.Add(ctChannel);
                }
            }

            return channelList;
        }

        private List<RangePage> GetCTPageList(XmlNode channelNode)
        {
            List<RangePage> ctPageList = new List<RangePage>();

            XmlNodeList ctPageNodeList = channelNode.SelectNodes(Settings.pageNode);
            if (ctPageNodeList.Count > 0)
            {
                foreach (XmlNode ctPageNode in ctPageNodeList)
                {
                    RangePage ctPage = new RangePage();
                    ctPage.PageName = ctPageNode.SelectSingleNode(Settings.nameNode).InnerText;
                    ctPage.RangeList = GetRanges(ctPageNode);
                    ctPageList.Add(ctPage);
                }
            }
            return ctPageList;
        }

        private List<Range> GetRanges(XmlNode ctPageNode)
        {
            List<Range> rangeList = new List<Range>();

            XmlNodeList rangeNodeList = ctPageNode.SelectNodes(Settings.range);
            if (rangeNodeList.Count > 0)
            {
                foreach (XmlNode rangeNode in rangeNodeList)
                {
                    Range range = new Range();

                    range.Type = rangeNode.SelectSingleNode(Settings.type).InnerText;
                    range.MaximumValue = rangeNode.SelectSingleNode(Settings.maximumValueNode).InnerText;
                    range.MinimumValue = rangeNode.SelectSingleNode(Settings.minimumValueNode).InnerText;

                    rangeList.Add(range);
                }
            }
            return rangeList;
        }

        private List<ResultMapping> GetResultMappingList(XmlDocument assayConfigXML)
        {
            List<ResultMapping> resultMappingList = new List<ResultMapping>();

            XmlNodeList resultMappingNodeList = assayConfigXML.SelectNodes(Settings.resultMappingNodePath);

            if (resultMappingNodeList.Count > 0)
            {
                foreach (XmlNode resultMappingNode in resultMappingNodeList)
                {
                    ResultMapping resultMapping = new ResultMapping();

                    string result = resultMappingNode.SelectSingleNode(Settings.result).InnerText;
                    if (!String.IsNullOrEmpty(result))
                    {
                        result = result.Replace("\\r\\n", Environment.NewLine); // In the xml, new lines are represented by \r\n character sequence
                    }
                    resultMapping.Result = result;

                    resultMapping.Validity = resultMappingNode.SelectSingleNode(Settings.validity).InnerText;
                    resultMapping.CharBits = resultMappingNode.SelectSingleNode(Settings.charBits).InnerText;

                    resultMappingList.Add(resultMapping);
                }
            }

            return resultMappingList;
        }

        private List<BinbitRange> GetBinbitRangeList(XmlDocument assayConfigXML)
        {
            List<BinbitRange> binbitMappingList = new List<BinbitRange>();

            XmlNodeList binbitMappingNodeList = assayConfigXML.SelectNodes(Settings.binbitRangeNodePath);

            if (binbitMappingNodeList.Count > 0)
            {
                foreach (XmlNode binbitMappingNode in binbitMappingNodeList)
                {
                    BinbitRange binbitMapping = new BinbitRange();
                    binbitMapping.Data = binbitMappingNode.SelectSingleNode(Settings.dataNode).InnerText;
                    binbitMapping.Range = binbitMappingNode.SelectSingleNode(Settings.rangeNode).InnerText;

                    binbitMappingList.Add(binbitMapping);
                }
            }

            return binbitMappingList;
        }

        private Range GetRange(string ctChannelName, string pageName, SampleType sampleType)
        {
            Range targetRange = new Range();

            foreach (RangeChannel rangeChannel in this.rangeChannelList)
            {
                if (rangeChannel.Name.Equals(ctChannelName, StringComparison.OrdinalIgnoreCase))
                {
                    foreach (RangePage rangePage in rangeChannel.PageList)
                    {
                        if (pageName == rangePage.PageName)
                        {
                            foreach (Range range in rangePage.RangeList)
                            {
                                if (range.Type == sampleType.ToString())
                                {
                                    targetRange = range;
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    break;
                }
            }

            return targetRange;
        }

        private BinbitRange GetBinbitRange(char charBit)
        {
            BinbitRange targetBinbitMapping = new BinbitRange();
            foreach (BinbitRange binbitMapping in BinbitRangeList)
            {
                if (charBit.ToString() == binbitMapping.Data)
                {
                    return targetBinbitMapping = binbitMapping;
                }
            }
            return targetBinbitMapping;
        }

        private ResultMapping GetResultMapping(string charBits)
        {
            ResultMapping targetResultMapping = new ResultMapping();

            foreach (ResultMapping resultMapping in resultMappingList)
            {
                if (resultMapping.CharBits == charBits)
                {
                    return targetResultMapping = resultMapping;
                }
            }
            return targetResultMapping;
        }

        /// <summary>
        /// Fille AssayLogic object from the assay config file
        /// </summary>
        /// <param name="assayConfigPath"></param>
        /// <returns></returns>
        public bool FillAssayLogic(string assayConfigPath)
        {
            this.assayConfigFile = assayConfigPath;

            XmlDocument assayConfigXML = FileCheckSum.Load(assayConfigFile);

            if (assayConfigXML.DocumentElement != null)
            {
                try
                {
                    this.rangeChannelList = GetCTChannelList(assayConfigXML);

                    this.resultMappingList = GetResultMappingList(assayConfigXML);

                    this.binbitRangeList = GetBinbitRangeList(assayConfigXML);

                    XmlNode assayModeNode = assayConfigXML.SelectSingleNode(Settings.rootNode)
                        .SelectSingleNode(Settings.assayInfoNode).SelectSingleNode(Settings.assayModeNode);

                    this.assayMode = (AssayMode)Enum.Parse(typeof(AssayMode), assayModeNode.InnerText);

                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private bool IsChannelContained(string channelName)
        {
            bool target = false;

            foreach (RangeChannel rangeChannel in rangeChannelList)
            {
                if (rangeChannel.Name.Equals(channelName, StringComparison.OrdinalIgnoreCase))
                {
                    return target = true;
                }
            }

            return target;
        }

        /// <summary>
        /// Verify whether the sample result is passed or not via the TestResult or Validity property
        /// </summary>
        /// <param name="sampleResult">SampleResult object</param>
        /// <returns>True/False</returns>
        public bool IsSampleResultPassed(SampleResult sampleResult)
        {
            sampleResult.CharBits = GetCharBits(sampleResult.BinBits);

            ResultMapping resultMapping = GetResultMapping(sampleResult.CharBits);

            if ((resultMapping.Result != string.Empty) && (resultMapping.Validity != string.Empty))
            {
                sampleResult.TestResult = resultMapping.Result;
                sampleResult.Validity = resultMapping.Validity;
            }

            if ((sampleResult.TestResult.ToUpper().Contains("Invalid".ToUpper()))
                || (sampleResult.TestResult.ToUpper().Contains("Fail".ToUpper()))
                || (sampleResult.Validity.ToUpper().Contains("Invalid".ToUpper()))
                || (sampleResult.TestResult == string.Empty))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Get Character bits via the binbits
        /// </summary>
        /// <param name="binbits">Binbits</param>
        /// <returns>Character bits</returns>
        public string GetCharBits(string binbits)
        {
            string targetCharBits = string.Empty;

            foreach (ResultMapping resultMapping in this.resultMappingList)
            {
                string configCharbits = resultMapping.CharBits;
                if (configCharbits.Length == binbits.Length)
                {
                    if (Judge(configCharbits, binbits))
                    {
                        targetCharBits = resultMapping.CharBits;
                        break;
                    }
                }
            }

            return targetCharBits;
        }

        private bool Judge(string configCharbits, string binbits)
        {
            bool ret = true;
            for (int i = 0; i < binbits.Length; i++)
            {
                string searchBinbits = GetBinbitRange(configCharbits[i]).Range;

                if (!searchBinbits.Contains(binbits[i]))
                {
                    ret = false;
                    break;
                }
            }
            return ret;
        }

        /// <summary>
        /// Get binbits for SampleResult object
        /// </summary>
        /// <param name="sampleResult">SampleResult object</param>
        /// <returns>Binbits</returns>
        public string GetBinBits(SampleResult sampleResult)
        {
            StringBuilder digitBinBits = new StringBuilder();
            string singleChannelBinbit = string.Empty;

            //Green
            foreach (CTChannel ctChannel in sampleResult.CtChannelList)
            {
                string channelName = ChannelName.GREEN.ToString();
                if (ctChannel.ChannelName.Equals(channelName.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    if (IsChannelContained(ctChannel.ChannelName))
                    {
                        singleChannelBinbit = GetSingleBinbit(sampleResult.PageName, sampleResult.SampleType, ctChannel);
                        digitBinBits.Append(singleChannelBinbit);
                        break;
                    }
                }
            }

            //Yellow
            foreach (CTChannel ctChannel in sampleResult.CtChannelList)
            {
                string channelName = ChannelName.YELLOW.ToString();
                if (ctChannel.ChannelName.Equals(channelName.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    if (IsChannelContained(ctChannel.ChannelName))
                    {
                        singleChannelBinbit = GetSingleBinbit(sampleResult.PageName, sampleResult.SampleType, ctChannel);
                        digitBinBits.Append(singleChannelBinbit);
                        break;
                    }
                }
            }

            //Orange
            foreach (CTChannel ctChannel in sampleResult.CtChannelList)
            {
                string channelName = ChannelName.ORANGE.ToString();

                if (ctChannel.ChannelName.Equals(channelName.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    if (IsChannelContained(ctChannel.ChannelName))
                    {
                        singleChannelBinbit = GetSingleBinbit(sampleResult.PageName, sampleResult.SampleType, ctChannel);
                        digitBinBits.Append(singleChannelBinbit);
                        break;
                    }
                }
            }

            //Red
            foreach (CTChannel ctChannel in sampleResult.CtChannelList)
            {
                string channelName = ChannelName.RED.ToString();

                if (ctChannel.ChannelName.Equals(channelName.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    if (IsChannelContained(ctChannel.ChannelName))
                    {
                        singleChannelBinbit = GetSingleBinbit(sampleResult.PageName, sampleResult.SampleType, ctChannel);
                        digitBinBits.Append(singleChannelBinbit);
                        break;
                    }
                }
            }

            return digitBinBits.ToString();
        }


        private string GetSingleBinbit(string pageName, SampleType sampleType, CTChannel ctChannel)
        {
            string targetBinbit;
            double rangeMax = 0;
            double rangeMin = 0;

            Range targetRange = GetRange(ctChannel.ChannelName, pageName, sampleType);

            if ((targetRange.Type != string.Empty) && (targetRange.Type != ""))
            {
                rangeMax = Convert.ToDouble(targetRange.MaximumValue);
                rangeMin = Convert.ToDouble(targetRange.MinimumValue);
            }

            return targetBinbit = CompareValue(sampleType, ctChannel, rangeMax, rangeMin);
        }

        private static string CompareValue(SampleType sampleType, CTChannel ctChannel, double rangeMax, double rangeMin)
        {
            string targetBinbit = string.Empty;
            string targetValue = string.Empty;

            if (sampleType != SampleType.DeltaSample)
            {
                targetValue = ctChannel.CtValue;
            }
            else
            {
                targetValue = ctChannel.CustomList[0].Value;
            }

            if (targetValue == string.Empty)
            {
                return targetBinbit = "4";
            }
            else
            {
                double doubleTargetValue = Convert.ToDouble(targetValue);
                doubleTargetValue = Math.Round(doubleTargetValue, 2);   // To sync with the user-displayed value in UI

                if (doubleTargetValue < rangeMin)
                {
                    targetBinbit = "1";
                }
                else if (doubleTargetValue > rangeMax)
                {
                    targetBinbit = "3";
                }
                else if (doubleTargetValue == 0)
                {
                    targetBinbit = "4";
                }
                else
                {
                    targetBinbit = "2";
                }
            }

            return targetBinbit;
        }

        /// <summary>
        /// Get interpretation strings from assay config file
        /// </summary>
        /// <param name="sampleResultList">All the </param>
        public void GetInterpretations(List<SampleResult> sampleResultList)
        {
            foreach (SampleResult sampleResult in sampleResultList)
            {
                foreach (ResultMapping resultMapping in resultMappingList)
                {
                    if (resultMapping.CharBits == sampleResult.CharBits)
                    {
                        sampleResult.TestResult = resultMapping.Result;
                        sampleResult.Validity = resultMapping.Validity;
                        break;
                    }
                }
            }
        }

        public void GetInterpretationsForPQ(List<SampleResult> sampleResultList)
        {
            List<double> sampleCtValues = new List<double>();
            string sampleFailedCharBit = string.Empty;
            string NCResut = string.Empty;
            string PCResult = string.Empty;

            // If NC or PC fails, continue with normal interpretation and skip std dev calculation
            var NCSample = sampleResultList.Where(x => x.SampleType == SampleType.NC).FirstOrDefault();
            var PCSample = sampleResultList.Where(x => x.SampleType == SampleType.PC).FirstOrDefault();
            GetInterpretations(sampleResultList);
            if (!IsSampleResultPassed(NCSample) || !IsSampleResultPassed(PCSample))
            {
                return;
            }

            // Get all applicable sample Ct's for std dev calculation
            var sampleList = sampleResultList.Where(x => x.SampleType == SampleType.Sample).ToList();
            foreach (SampleResult sampleResult in sampleList)
            {
                var ctChannel = sampleResult.CtChannelList.Where(x => x.ChannelName == ChannelName.RED.ToString()).FirstOrDefault();
                if (ctChannel != null)
                {
                    if (!string.IsNullOrEmpty(ctChannel.CtValue))
                    {
                        sampleCtValues.Add(Convert.ToDouble(ctChannel.CtValue));
                    }
                    else
                    {
                        sampleCtValues.Add(45);
                    }
                }

                if (!IsSampleResultPassed(sampleResult) && string.IsNullOrEmpty(sampleFailedCharBit))
                {
                    sampleFailedCharBit = sampleResult.CharBits;
                }
            }

            // Calculate std dev
            double? stdDev = CalculateStdDev(sampleCtValues);

            // Update TestResult and Validity as necessary
            Range StadDevRange = GetRange(ChannelName.RED.ToString(), "SA PQ", SampleType.PQ);
            double rangeMax = 0;
            double rangeMin = 0;
            if (!string.IsNullOrEmpty(StadDevRange.MaximumValue) && !string.IsNullOrEmpty(StadDevRange.MinimumValue))
            {
                rangeMax = Convert.ToDouble(StadDevRange.MaximumValue);
                rangeMin = Convert.ToDouble(StadDevRange.MinimumValue);
                if (stdDev != null && stdDev >= rangeMin && stdDev <= rangeMax && string.IsNullOrEmpty(sampleFailedCharBit))
                {
                    foreach (SampleResult sampleResult in sampleResultList.Where(x => x.SampleType == SampleType.Sample))
                    {
                        foreach (ResultMapping resultMapping in resultMappingList)
                        {
                            if (resultMapping.CharBits == sampleResult.CharBits)
                            {
                                sampleResult.TestResult = resultMapping.Result + Environment.NewLine + "Ct Std Dev of red channel is " + stdDev.Value.ToString("F2");
                            }
                        }
                    }
                }
                else
                {
                    foreach (SampleResult sampleResult in sampleResultList.Where(x => x.SampleType == SampleType.Sample))
                    {
                        if ((stdDev != null && stdDev >= rangeMin && stdDev <= rangeMax) == false && !string.IsNullOrEmpty(sampleFailedCharBit))
                        {
                            sampleResult.CharBits = "BOTHFAILED";
                        }
                        else if (!string.IsNullOrEmpty(sampleFailedCharBit))
                        {
                            sampleResult.CharBits = "SAMPLEFAILED";
                        }
                        else
                        {
                            sampleResult.CharBits = "STDDEVFAILED";
                        }

                        foreach (ResultMapping resultMapping in resultMappingList)
                        {
                            if (resultMapping.CharBits == sampleResult.CharBits)
                            {
                                if (stdDev != null)
                                {
                                    sampleResult.TestResult = resultMapping.Result + Environment.NewLine + "Ct Std Dev of red channel is " + stdDev.Value.ToString("F2");
                                }
                                else
                                {
                                    sampleResult.TestResult = resultMapping.Result + Environment.NewLine + "Ct Std Dev of red channel cannot be calculated";
                                }
                            }
                        }
                    }
                }
            }
        }

        public double? CalculateStdDev(List<double> dataPoints)
        {
            if (dataPoints == null)
            {
                return null;
            }

            if (dataPoints.Count <= 1)
            {
                return null;
            }

            // Calculate Std Dev
            double mean = dataPoints.Average();

            List<double> diffsFromMeanSquared = new List<double>();
            foreach (double dataPoint in dataPoints)
            {
                diffsFromMeanSquared.Add(Math.Pow(dataPoint - mean, 2));
            }
            double sum = diffsFromMeanSquared.Sum();

            double stdDev = Math.Sqrt(sum / (dataPoints.Count - 1));
            stdDev = Math.Round(stdDev, 2);

            return stdDev;
        }
    }
}
