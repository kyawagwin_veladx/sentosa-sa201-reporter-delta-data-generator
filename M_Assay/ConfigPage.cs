﻿
#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 12/02/2013
 * Author: Liu Rui
 * 
 * Description:
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace M_Assay
{
    public class ConfigPage
    {
        private string pageName = string.Empty;
        private List<string> m1ItemList = new List<string>();

        public string PageName
        {
            get { return pageName; }
            set { pageName = value; }
        }

        public List<string> M1ItemList
        {
            get { return m1ItemList; }
            set { m1ItemList = value; }
        }

        public List<string> GetM1ItemList(XmlNode pageNode)
        {
            List<string> targetList = new List<string>();

            XmlNodeList m1ItemNodeList = pageNode.SelectNodes(Settings.reagentIDNode);

            foreach (XmlNode m1ItemNode in m1ItemNodeList)
            {
                targetList.Add(m1ItemNode.InnerText);
            }

            return targetList;
        }
    }
}
