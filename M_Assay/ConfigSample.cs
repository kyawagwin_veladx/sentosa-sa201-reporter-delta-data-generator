﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 19/01/2013
 * Author: Liu Rui
 * 
 * Description:
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_Assay
{
    public class ConfigSample
    {
        private int position = -1;
        private string sampleType = string.Empty;

        public int Position
        {
            get { return position; }
            set { position = value; }
        }

        public string SampleType
        {
            get { return sampleType; }
            set { sampleType = value; }
        }
    }
}
