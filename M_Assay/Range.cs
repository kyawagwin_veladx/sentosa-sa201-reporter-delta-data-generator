﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 26/12/2013
 * Author: Liu Rui
 * 
 * Description: 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_Assay
{
    /// <summary>
    /// This class stores the variables and properties of range object
    /// </summary>
    public class Range
    {
        #region Private Member Variables
        private string type = string.Empty;
        private string maximumValue = string.Empty;
        private string minimumValue = string.Empty; 
        #endregion

        #region Public Properties
        public string Type
        {
            get { return type; }
            set { type = value; }
        }
        public string MaximumValue
        {
            get { return maximumValue; }
            set { maximumValue = value; }
        }

        public string MinimumValue
        {
            get { return minimumValue; }
            set { minimumValue = value; }
        } 
        #endregion
    }
}
