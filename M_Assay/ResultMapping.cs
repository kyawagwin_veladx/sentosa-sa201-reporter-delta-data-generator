﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 26/12/2013
 * Author: Liu Rui
 * 
 * Description: 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_Assay
{
    /// <summary>
    /// This class stores the variables and properties of ResultMapping object
    /// </summary>
    public class ResultMapping
    {
        #region Private Member Variabls
        private string result = string.Empty;
        private string validity = string.Empty;
        private string charBits = string.Empty; 
        #endregion

        #region Public Properties
        public string Result
        {
            get { return result; }
            set { result = value; }
        }

        public string Validity
        {
            get { return validity; }
            set { validity = value; }
        }

        public string CharBits
        {
            get { return charBits; }
            set { charBits = value; }
        }
        #endregion
    }
}
