﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 09/01/2013
 * Author: Liu Rui
 * 
 * Description:
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_Assay
{
    internal class Settings
    {
        internal static string rootNode = "AssayConfig";
        internal static string assayInfoNode = "AssayInfo";
        internal static string assayNameNode = "AssayName";
        internal static string templateNameNode = "TemplateName";
        internal static string assayModeNode = "AssayMode";
        internal static string pagesNode = "Pages";
        internal static string pageNode = "Page";
        internal static string pageNameNode = "PageName";
        internal static string reagentIDNode = "ReagentID";
        internal static string sampleSizeNode = "SampleSize";
        internal static string sampleSizesNode = "SampleSizes";
        internal static string positionNode = "Position";
        internal static string startAttribute = "Start";
        internal static string endAttribute = "End";
        internal static string groupsNode = "Groups";
        internal static string groupNode = "Group";
        internal static string ctRangeNode = "CTRange";
        internal static string ctChannelNode = "RangeChannel";
        internal static string resultMappingsNode = "ResultMappings";
        internal static string resultMappingNode = "ResultMapping";
        internal static string pageSampleCountNode = "PageSampleCount";
        internal static string range = "Range";
        internal static string type = "Type";
        internal static string maximumValueNode = "MaximumValue";
        internal static string minimumValueNode = "MinimumValue";
        internal static string nameNode = "Name";
        internal static string pc = "PC";
        internal static string ntc = "NTC";
        internal static string binBits = "BinBits";
        internal static string charBits = "CharBits";
        internal static string result = "Result";
        internal static string validity = "Validity";
        internal static string sampleNode = "Sample";
        internal static string sampleTypeNode = "SampleType";
        internal static string displaySTDNode = "DisplaySTD";
        internal static string stdAssayNameNode = "StandardCurveAssayName";
        internal static string stdInfoNode = "StandardCurveInfo";
        internal static string criteriaNode = "Criteria";
        internal static string daysNode = "Days";
        internal static string reactionEfficiencyNode = "ReactionEfficiency";
        internal static string rSquareValueNode = "RxRValue";
        internal static string ignoredChannelNode = "IgnoredChannel";
        internal static string channelDisplayNode = "ChannelDisplay";
        internal static string channelNode = "Channel";
        internal static string showNode = "Show";
        internal static string quantRangesNode = "QuantRanges";
        internal static string indexNode = "Index";
        internal static string multiplierNode = "Multiplier";
        internal static string binbitRangesNode = "BinBitsRanges";
        internal static string binbitRangeNode = "BinBitRange";
        internal static string dataNode = "Data";
        internal static string rangeNode = "Range";
        internal static string channelsNode = "Channels";
        internal static string shortNameNode = "ShortName";
        internal static string dyeNameNode = "DyeName";
        internal static string virusNameNode = "VirusName";
        internal static string barcodeFormatsNode = "BarcodeFormats";
        internal static string formatNode = "Format";
        internal static string name = "Name";
        internal static string regex = "Regex";
        internal static string givenConc = "GivenConc";
        internal static string exampleBarcodeNode = "ExampleBarcode";
        internal static string configExtention = "*.config";
        internal static string pageListsNode = "PageLists";
        internal static string orderNode = "Order";
        internal static string diseaseAreaNode = "DiseaseArea";
        internal static string rangeChannelNodePath = "AssayConfig/CTRange/RangeChannel";
        internal static string resultMappingNodePath = "AssayConfig/ResultMappings/ResultMapping";
        internal static string quantRangesNodePath = "AssayConfig/QuantRanges/Range";
        internal static string binbitRangeNodePath = "AssayConfig/BinBitsRanges/BinBitRange";
        internal static string standardCurveInfoNodePath = "AssayConfig/AssayInfo/StandardCurveInfo";
        internal static string messageNode = "Message";
        internal static string configChannelNodePath = "AssayConfig/AssayInfo/Channels/Channel";
        internal static string checkBulkNodePath = "AssayConfig/AssayInfo/CheckBulk";
    }
}
