﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 09/01/2013
 * Author: Liu Rui
 * 
 * Description:
 * 
 */
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_Assay
{
    public class SampleSize
    {
        private string name = string.Empty;
        private List<PositionGroup> groupList = new List<PositionGroup>();
        private List<Position> positionList = new List<Position>();
        //private List<ComboPage> comboPageList = new List<ComboPage>();

        //public List<ComboPage> ComboPageList
        //{
        //    get { return comboPageList; }
        //    set { comboPageList = value; }
        //}

        public List<PositionGroup> GroupList
        {
            get { return groupList; }
            set { groupList = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public List<Position> PositionList
        {
            get { return positionList; }
            set { positionList = value; }
        }

        internal List<string> GetSamples()
        {
            List<string> targetArrayList = new List<string>();

            foreach (Position position in positionList)
            {
                foreach (ConfigSample sample in position.SampleList)
                {
                    targetArrayList.Add(sample.Position.ToString());
                }
            }

            return targetArrayList;
        }

        public PositionGroup GetPositionGroup(int position)
        {
            PositionGroup target = new PositionGroup();

            if (position>=0)
            {
                foreach (PositionGroup positionGroup in groupList)
                {
                    foreach (int position2 in positionGroup.PositionList)
                    {
                        if (position == position2)
                        {
                            return target = positionGroup;
                        }
                    }
                }
            }
            return target;
        }
    }
}
