﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 06/02/2013
 * Author: Liu Rui
 * 
 * Description:
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_Assay
{
    /// <summary>
    /// This class is used for storing the barcode format information reading from an assay config file
    /// </summary>
    public class BarcodeFormat
    {
        private string regex = string.Empty;
        private string type = string.Empty;

        public string Regex
        {
            get { return regex; }
            set { regex = value; }
        }

        public string Type
        {
            get { return type; }
            set { type = value; }
        } 
    }
}
