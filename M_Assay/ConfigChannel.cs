﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 12/02/2013
 * Author: Liu Rui
 * 
 * Description:
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace M_Assay
{
    /// <summary>
    /// This class stores the channel information reading from an assay config file
    /// </summary>
    public class ConfigChannel
    {
        private string name = string.Empty;
        private string shortName = string.Empty;
        private string dyeName = string.Empty;
        private string virusName = string.Empty;
        
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string ShortName
        {
            get { return shortName; }
            set { shortName = value; }
        }
        public string DyeName
        {
            get { return dyeName; }
            set { dyeName = value; }
        }
        public string VirusName
        {
            get { return virusName; }
            set { virusName = value; }
        }

    }
}
