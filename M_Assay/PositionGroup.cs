﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 17/01/2013
 * Author: Liu Rui
 * 
 * Description: 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_Assay
{
    public class PositionGroup
    {
        private List<int> positionList = new List<int>();

        public List<int> PositionList
        {
            get { return positionList; }
            set { positionList = value; }
        }
    }
}
