﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 26/12/2013
 * Author: Liu Rui
 * 
 * Description: 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_Assay
{
    /// <summary>
    /// This class stores the variables and properties of page object of logic table
    /// </summary>
    public class RangePage
    {
        #region Private Member Variables
        private string pageName = string.Empty;
        private List<Range> rangeList = new List<Range>(); 
        #endregion

        #region Public Properties
        public string PageName
        {
            get { return pageName; }
            set { pageName = value; }
        }

        public List<Range> RangeList
        {
            get { return rangeList; }
            set { rangeList = value; }
        } 
        #endregion
    }
}
