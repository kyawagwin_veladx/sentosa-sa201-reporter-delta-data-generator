﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 27/01/2013
 * Author: Liu Rui
 * 
 * Description:
 * 
 */
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_Assay
{
    /// <summary>
    /// This class is used for storing the binbits range data from an assay config file
    /// </summary>
    public class BinbitRange
    {
        private string data = string.Empty;
        private string range = string.Empty;

        public string Data
        {
            get { return data; }
            set { data = value; }
        }

        public string Range
        {
            get { return range; }
            set { range = value; }
        }
    }
}
