﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 17/01/2013
 * Author: Liu Rui
 * 
 * Description: 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_Assay
{
    public class RangeChannel
    {
        private string name = string.Empty;
        private List<RangePage> pageList = new List<RangePage>();

        public List<RangePage> PageList
        {
            get { return pageList; }
            set { pageList = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
    }
}
