﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 13/2/2014
 * Author: Yang Yi
 * 
 * Description:
 * Export txt file
 * 
 */
#endregion

using M_CommonMethods;
using M_ReportResult;
using M_SampleList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M_SA201EntryList
{
    public class ExportFile
    {
        public static void GenerateTXTFile(string despath, SMPObject smp, Dictionary<string,string> channelNameList)
        {
            if (File.Exists(despath))
            {
                File.Delete(despath);
            }
            FileStream fs = File.Create(despath);
            fs.Close();

            //InfoConvert.ReorderChannelName(channelNameList.Keys.ToList());

            Create(despath, smp, channelNameList);
        }

        private static string FormatedSpace(string val, int fixedLen)
        {
            int len = 0;
            string retVal = string.Empty;

            len = val.Length;
            retVal = val;
            for (int i = 0; i < fixedLen - len - 1; i++)
            {
                retVal = retVal + " ";
            }

            return retVal;
        }

        private static void Create(string path, SMPObject smp, Dictionary<string,string> channelName)
        {
            //string[] dyeName;

            //if (smp.PageList[0].PageName.Equals("ZIKV-DENV-CHIKV", StringComparison.OrdinalIgnoreCase))
            //{
            //    //GREEN, YELLOW, ORANGE, RED 
            //    dyeName = new string[] { "FAM", "VIC", "ROX", "CY5" };
            //}
            //else if (smp.PageList[0].PageName.Equals("Direct MRSA/SA", StringComparison.OrdinalIgnoreCase))
            //{
            //    //GREEN, YELLOW, ORANGE, RED 
            //    dyeName = new string[] { "FAM", "JOE", "ROX", "CY5" };
            //}

            //else if (smp.PageList[0].PageName.Equals("ZIKV", StringComparison.OrdinalIgnoreCase))
            //{
            //    //GREEN,RED 
            //    dyeName = new string[] { "FAM", "CY5" };
            //}
            //else if (smp.PageList[0].PageName.Equals("van A/van B", StringComparison.OrdinalIgnoreCase))
            //{
            //    //GREEN, ORANGE, RED
            //    dyeName = new string[] { "FAM", "ROX", "CY5" };
            //}
            //else if (smp.PageList[0].PageName.Equals("HSV1/2 Qual", StringComparison.OrdinalIgnoreCase))
            //{
            //    //GREEN, ORANGE, RED
            //    dyeName = new string[] { "FAM", "ROX", "CY5" };
            //}
            //else
            //{
            //    //GREEN, YELLOW, ORANGE, RED 
            //    dyeName = new string[] { "FAM", "JOE", "ROX", "CY5" };
            //}

            StreamWriter sw = new StreamWriter(path);
            sw.WriteLine("*** SDS Setup File Version	3");
            sw.WriteLine("*** Output Plate Size	" + smp.PageList[0].SampleList.Count.ToString());
            sw.WriteLine("*** output Plate ID	");
            sw.WriteLine("*** Number of Detectors	" + channelName.Count.ToString());
            sw.WriteLine("Detector	Reporter	Quencher	Description	Comments");

            foreach (KeyValuePair<string, string> entry in channelName)
            {
                sw.WriteLine(FormatedSpace(entry.Key, 2) + "\t" + entry.Value);  
            }

            sw.WriteLine("Well	Sample Name	Detector	Task	Quantity");

            for (int i = 0; i < smp.PageList[0].SampleList.Count; i++)
            {
                foreach (KeyValuePair<string, string> entry in channelName)
                {
                    sw.WriteLine(FormatedSpace(smp.PageList[0].SampleList[i].TubePosition, 4) + "\t"
                        + smp.PageList[0].SampleList[i].SampleName + "	"
                        + FormatedSpace(entry.Key, 1) + "\t" + "UNKN");
                }
            }

            sw.Close();
        }
    }
}
