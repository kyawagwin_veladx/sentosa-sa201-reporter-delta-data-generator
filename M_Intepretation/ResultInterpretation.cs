﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 04/12/2013
 * Author: Liu Rui
 * 
 * Description:
 * Fill all the info into RunInfo object in Report module and provide the only interface for report module
 * 
 */
#endregion

using System;
using System.Reflection;
using System.IO;
using M_ReportResult;
using M_Assay;
using M_Log;
using M_Interpretation_Qualitative;
//using M_Interpretation_Qualitative;

namespace M_Interpretation
{
    public class Interpretation
    {
        #region Private Member Variables
        private ReportResult reportResult;
        private AssayLogic assayLogic;
        private AssayInfo assayInfo;
        #endregion

        #region Constructors
        public Interpretation(AssayInfo assayInfo
            , AssayLogic assayLogic
            , ReportResult reportResult)
        {
            this.assayLogic = assayLogic;
            this.assayInfo = assayInfo;
            this.reportResult = reportResult;
        }
        #endregion

        /// <summary>
        /// Fill all the info for RunInfo object and provide this only interface for report module
        /// </summary>
        /// <returns>RunInfo object</returns>
        public bool InterpretResults()
        {
            string assayName = assayInfo.AssayName;
            reportResult.Message = assayInfo.Message;

            Log.WriteLog(LogType.system, "Result Interpret - Start");
            return GetInterpretations1(assayName, assayLogic, reportResult);
            //return GetInterpretations1(assayName, assayLogic, reportResult);
        }

        /// <summary>
        /// Fills all the samples on pages with binbits, result and validity
        /// </summary>
        /// <returns>List of pages</returns>
        //private bool GetInterpretations0(string assayName
        //    , AssayLogic assayLogic
        //    , ReportResult reportResult)
        //{
        //    string path = AppDomain.CurrentDomain.BaseDirectory + "Assay" + Path.DirectorySeparatorChar + RemoveSlash(assayName) + ".dll";

        //    if (!File.Exists(path))
        //    {
        //        return false;
        //    }

        //    Assembly ass = Assembly.LoadFrom(path);
        //    Type type;
        //    Object obj;
             
        //    type = ass.GetType("M_Interpretation_" + GetClassName(assayName, assayLogic.AssayMode) + "." + GetClassName(assayName, assayLogic.AssayMode));

        //    if (type != null)
        //    {
        //        obj = Activator.CreateInstance(type);
        //        MethodInfo method = type.GetMethod("GetAllResults");

        //        object[] parameters = new object[3];
        //        parameters[0] = assayName;
        //        parameters[1] = assayLogic;
        //        parameters[2] = reportResult;

        //        object result;

        //        try
        //        {
        //            result = method.Invoke(obj, parameters);
        //        }
        //        catch (Exception e)
        //        {
        //            throw new System.ArgumentException(e.Message);
        //        }

        //        Log.WriteLog(LogType.system, "Result Interpret - Finish");

        //        if (!(bool)result)
        //        {
        //            return false;
        //        }
        //        else
        //        {

        //            return true;
        //        }
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        /// Fills all the samples on pages with binbits, result and validity
        /// </summary>
        /// <returns>List of pages</returns>
        private bool GetInterpretations1(string assayName
            , AssayLogic assayLogic
            , ReportResult reportResult)
        {
            AssayMode assayMode = assayLogic.AssayMode;

            switch (assayMode)
            {
                case AssayMode.Qualitative:
                    Log.WriteLog(LogType.system, "Result Interpret - Qualitative - Start");
                    Qualitative f = new Qualitative();
                    f.GetAllResults(assayName, assayLogic, reportResult);
                    Log.WriteLog(LogType.system, "Result Interpret - Qualitative - Finish");
                    break;
            }

            return true;
        }

        private string RemoveSlash(string assayName)
        {
            if (assayName.Contains("/"))
            {
                return assayName.Replace('/', ',');
            }
            else
            {
                return assayName;
            }
        }

        private string GetClassName(string assayName, AssayMode assayMode)
        {
            if ((assayName == "KRAS") || (assayName == "BRAF V600") || (assayName == "NRAS"))
            {
                if (assayName == "BRAF V600")
                {
                    return "BRAF";
                }
                else
                {
                    return assayName;
                }
            }
            else
            {
                //if ((assayMode != AssayMode.StandardCurve) && (assayMode != AssayMode.Warmup))
                //{
                    return assayMode.ToString();
                //}
                //else
                //{
                //    return string.Empty;
                //}
            }
        }
    }
}
