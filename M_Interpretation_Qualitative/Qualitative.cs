﻿using M_Assay;
using M_ReportResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using M_CommonMethods;
using M_Log;

namespace M_Interpretation_Qualitative
{
    public class Qualitative
    {
        private string[] assayList = { "Strep A"
                                        , "MTC"
                                        , "Influenza A/B, RSV"
                                        , "H7N9"
                                        , "MERS-CoV"
                                        , "Norovirus"
                                        , "Norovirus v2.0"
                                        , "Salmonella"
                                        , "Rotavirus"
                                        , "C. diff"
                                        ,"C. diff v2.0"
                                        ,"van A/van B"
                                        ,"MRSA/SA"
                                        ,"Direct MRSA/SA"
                                        ,"HSV 1/2 Qualitative"
                                        ,"Dengue I-IV"
                                        ,"CHIKV"
                                        ,"ZIKV"
                                        ,"ZIKV-DENV-CHIKV"};

        public bool GetAllResults(string assayName
            , AssayLogic assayLogic
            , ReportResult reportResult)
        {
            string sumBinBits = string.Empty;

            List<string> pageNameList = reportResult.GetUniquePageNameList();

            string pageName = pageNameList[0];

            HandleNC(assayLogic, reportResult.SampleResultList, sumBinBits, pageName);

            if (pageName.Equals("SA PQ"))
            {
                assayLogic.GetInterpretationsForPQ(reportResult.SampleResultList);
            }
            else
            {
                assayLogic.GetInterpretations(reportResult.SampleResultList);
            }
            

            Log.WriteLog(LogType.system, "All samples got interpretations");

            return true;
        }

        private static string HandleNC(AssayLogic assayLogic, List<SampleResult> sampleResultList, string sumBinBits, string pageName)
        {
            SampleResult ncSampleResult = ReportResult.GetSampleResultByType(SampleType.NC, pageName, sampleResultList);

            ncSampleResult.BinBits = sumBinBits + assayLogic.GetBinBits(ncSampleResult);

            if (!assayLogic.IsSampleResultPassed(ncSampleResult))
            {
                Log.WriteLog(LogType.system, "Global NC failed, update all samples");

                SampleResult.UpdateAllCharBits(ncSampleResult.CharBits, sampleResultList);
            }
            else
            {
                sumBinBits = ncSampleResult.BinBits;

                HandlePC(assayLogic, sampleResultList, sumBinBits, pageName);
            }
            return sumBinBits;
        }

        private static void HandlePC(AssayLogic assayLogic, List<SampleResult> sampleResultList, string sumBinBits, string pageName)
        {
            SampleResult pcSampleResult = ReportResult.GetSampleResultByType(SampleType.PC, pageName, sampleResultList);
            pcSampleResult.BinBits = sumBinBits + assayLogic.GetBinBits(pcSampleResult);

            if (!assayLogic.IsSampleResultPassed(pcSampleResult))
            {
                Log.WriteLog(LogType.system, "Global PC failed, update all samples");

                List<SampleResult> targetSampleResultList = ReportResult.ExcludeSampleResultByType(pageName, SampleType.NC, sampleResultList);

                SampleResult.UpdateAllCharBits(pcSampleResult.CharBits, targetSampleResultList);
            }
            else
            {
                HandleSamples(assayLogic, sampleResultList, sumBinBits, pcSampleResult, pageName);
            }
        }

        private static void HandleSamples(AssayLogic assayLogic, List<SampleResult> sampleResultList, string sumBinBits, SampleResult qsSampleResult, string pageName)
        {
            foreach (SampleResult sampleResult in sampleResultList)
            {
                if (sampleResult.SampleType == SampleType.Sample)
                {
                    sumBinBits = qsSampleResult.BinBits;

                    string sampleCTBinbits = assayLogic.GetBinBits(sampleResult);
                    sampleResult.BinBits = sumBinBits + sampleCTBinbits;
                    sampleResult.CharBits = assayLogic.GetCharBits(sampleResult.BinBits);
                }
            }

            Log.WriteLog(LogType.system, "All samples got binbits and charbits");
        }
    }
}
