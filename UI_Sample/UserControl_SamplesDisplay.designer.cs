﻿
using System.Windows.Forms;

namespace UI_Sample
{
    partial class UserControl_SamplesDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUnit1 = new Label();
            this.dtgSamples = new DataGridView();
            this.Page = new DataGridViewTextBoxColumn();
            this.Position = new DataGridViewTextBoxColumn();
            this.ID = new DataGridViewTextBoxColumn();
            this.Type = new DataGridViewTextBoxColumn();
            this.GivenConc = new DataGridViewTextBoxColumn();
            this.Column1 = new DataGridViewTextBoxColumn();
            this.lblUnit2 = new Label();
            this.cmbPageName = new ComboBox();
            this.lblPageName = new Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chk_wellsort = new CheckBox();
            //((System.ComponentModel.ISupportInitialize)(this.dtgSamples)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.cmbPageName)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblUnit1
            // 
            this.lblUnit1.Location = new System.Drawing.Point(18, 13);
            this.lblUnit1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnit1.Name = "lblUnit1";
            this.lblUnit1.Size = new System.Drawing.Size(36, 20);
            this.lblUnit1.TabIndex = 2;
            this.lblUnit1.Text = "Unit:";
            // 
            // dtgSamples
            // 
            this.dtgSamples.AllowUserToAddRows = false;
            this.dtgSamples.AllowUserToDeleteRows = false;
            this.dtgSamples.AllowUserToResizeRows = false;
            this.dtgSamples.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgSamples.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgSamples.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgSamples.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Page,
            this.Position,
            this.ID,
            this.Type,
            this.GivenConc,
            this.Column1});
            this.dtgSamples.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dtgSamples.Location = new System.Drawing.Point(3, 88);
            this.dtgSamples.MultiSelect = false;
            this.dtgSamples.Name = "dtgSamples";
            this.dtgSamples.Size = new System.Drawing.Size(1064, 482);
            this.dtgSamples.TabIndex = 9;
            this.dtgSamples.SortCompare += new System.Windows.Forms.DataGridViewSortCompareEventHandler(this.dtgSamples_SortCompare);
            // 
            // Page
            // 
            this.Page.HeaderText = "Test/Target";
            this.Page.Name = "Page";
            this.Page.ReadOnly = true;
            this.Page.Width = 171;
            // 
            // Position
            // 
            this.Position.HeaderText = "Well";
            this.Position.Name = "Position";
            this.Position.ReadOnly = true;
            this.Position.Width = 170;
            // 
            // ID
            // 
            this.ID.HeaderText = "Sample Name";
            this.ID.Name = "ID";
            this.ID.Width = 171;
            // 
            // Type
            // 
            this.Type.HeaderText = "Sample Type";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            this.Type.Width = 170;
            // 
            // GivenConc
            // 
            this.GivenConc.HeaderText = "Given Conc.";
            this.GivenConc.Name = "GivenConc";
            this.GivenConc.ReadOnly = true;
            this.GivenConc.Width = 171;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "M1";
            this.Column1.Name = "Column1";
            this.Column1.Width = 170;
            // 
            // lblUnit2
            // 
            this.lblUnit2.Location = new System.Drawing.Point(180, 16);
            this.lblUnit2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnit2.Name = "lblUnit2";
            this.lblUnit2.Size = new System.Drawing.Size(33, 20);
            this.lblUnit2.TabIndex = 11;
            this.lblUnit2.Text = "Unit";
            // 
            // cmbPageName
            // 
            this.cmbPageName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPageName.DropDownWidth = 150;
            this.cmbPageName.FormattingEnabled = true;
            this.cmbPageName.Location = new System.Drawing.Point(163, 48);
            this.cmbPageName.Name = "cmbPageName";
            this.cmbPageName.Size = new System.Drawing.Size(150, 21);
            this.cmbPageName.TabIndex = 14;
            this.cmbPageName.SelectedIndexChanged += new System.EventHandler(this.cmbPageName_SelectedIndexChanged);
            // 
            // lblPageName
            // 
            this.lblPageName.Location = new System.Drawing.Point(18, 48);
            this.lblPageName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPageName.Name = "lblPageName";
            this.lblPageName.Size = new System.Drawing.Size(117, 20);
            this.lblPageName.TabIndex = 15;
            this.lblPageName.Text = "Display Test Target:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chk_wellsort);
            this.panel1.Controls.Add(this.cmbPageName);
            this.panel1.Controls.Add(this.lblPageName);
            this.panel1.Controls.Add(this.lblUnit1);
            this.panel1.Location = new System.Drawing.Point(17, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(426, 79);
            this.panel1.TabIndex = 16;
            // 
            // chk_wellsort
            // 
            //this.chk_wellsort.LabelStyle = LabelStyle.NormalControl;
            this.chk_wellsort.Location = new System.Drawing.Point(319, 49);
            this.chk_wellsort.Name = "chk_wellsort";
            this.chk_wellsort.Size = new System.Drawing.Size(90, 20);
            this.chk_wellsort.TabIndex = 16;
            this.chk_wellsort.Text = "Well Sorting";
            this.chk_wellsort.Text = "Well Sorting";
            this.chk_wellsort.CheckedChanged += new System.EventHandler(this.chk_wellsort_CheckedChanged);
            // 
            // UserControl_SamplesDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lblUnit2);
            this.Controls.Add(this.dtgSamples);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UserControl_SamplesDisplay";
            this.Size = new System.Drawing.Size(1070, 573);
            this.Load += new System.EventHandler(this.UserControl_SamplesDisplay_Load);
            //((System.ComponentModel.ISupportInitialize)(this.dtgSamples)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.cmbPageName)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal Label lblUnit1;
        internal DataGridView dtgSamples;
        internal Label lblUnit2;
        internal ComboBox cmbPageName;
        internal Label lblPageName;
        private System.Windows.Forms.Panel panel1;
        private DataGridViewTextBoxColumn Page;
        private DataGridViewTextBoxColumn Position;
        private DataGridViewTextBoxColumn ID;
        private DataGridViewTextBoxColumn Type;
        private DataGridViewTextBoxColumn GivenConc;
        private DataGridViewTextBoxColumn Column1;
        private CheckBox chk_wellsort;

    }
}
