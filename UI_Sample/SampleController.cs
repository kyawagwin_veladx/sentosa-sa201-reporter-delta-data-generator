﻿using M_SampleList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UI_Sample;
using M_Assay;


namespace UI_Sample
{
    public class SampleController
    {
        UserControl_SamplesDisplay sampleDisplay;
        SampleList sampleList;
        
        public SampleController()
        {
            sampleDisplay = new UserControl_SamplesDisplay(this);
            sampleList = new SampleList();
        }

        public void SetValues(SMPObject smpObject, AssayInfo assayInfo)
        {
            //Object->UI
            sampleList.SetValues(smpObject, assayInfo);    
        }

        private void DisplayUnit()
        {
            if (sampleList.IsQuantAssay())
            {
                sampleDisplay.lblUnit1.Visible = true;
                sampleDisplay.lblUnit2.Visible = true;
                sampleDisplay.lblUnit2.Text = sampleList.GetUnit();
            }
            else
            {
                sampleDisplay.lblUnit1.Visible = false;
                sampleDisplay.lblUnit2.Visible = false;
            }
        }

        private void LoadPageNamesDropDown()
        {
            foreach (SMPPage page in sampleList.SmpObject.PageList)
            {
                if (!sampleDisplay.cmbPageName.Items.Contains(page.PageName))
                {
                    sampleDisplay.cmbPageName.Items.Add(page.PageName);
                }
            }

            sampleDisplay.cmbPageName.Items.Add("All tests/ targets");

            sampleDisplay.cmbPageName.SelectedItem = 
                sampleDisplay.cmbPageName.Items[sampleDisplay.cmbPageName.Items.IndexOf("All tests/ targets")];
        }

        private void LoadDataGridView()
        {
            sampleDisplay.dtgSamples.Rows.Clear();

            foreach (SMPPage page in sampleList.SmpObject.PageList)
            {
                foreach (SMPSample sample in page.SampleList)
                {
                    if (sample.Selected.ToUpper() == true.ToString().ToUpper())
                    {
                        object[] values = new object[6];
                        values[0] = page.PageName;
                        values[1] = sampleList.GetAlphanumericPosition(sample.TubePosition);
                        values[2] = sample.SampleName;
                        values[3] = sample.Type;
                        values[4] = string.Empty;
                        values[5] = sample.ReagentName;
                        sampleDisplay.dtgSamples.Rows.Add(values);
                    }
                }
            }
        }

        private void SetReadOnly()
        {
            if (sampleList.IsReadOnly)
            {
                sampleDisplay.dtgSamples.Columns[2].ReadOnly = true;
                sampleDisplay.dtgSamples.Columns[5].ReadOnly = true;
            }
            else
            {
                sampleDisplay.dtgSamples.Columns[2].ReadOnly = false;
                sampleDisplay.dtgSamples.Columns[5].ReadOnly = false;
            }
        }

        internal void LoadSampleDisplay()
        {
            DisplayUnit();
            LoadPageNamesDropDown();
            LoadDataGridView();
            SetReadOnly();
        }

        //internal string GetNumericPosition(string alphanumericPosition)
        //{
        //    return sampleList.GetNumericPosition(alphanumericPosition);
        //}

        public bool AllBarcodePassed()
        {
            return CheckSampleNames();
        }

        public void ShowBarcodeError()
        {
            sampleDisplay.ShowErrorBarcode(sampleList.GetBarcodeErrorPrompt());
        }
        public void ShowInCompleteSDSError()
        {
            sampleDisplay.ShowInCompleteSDSFileError();
        }

        public bool AllM1Passed()
        {
            return CheckM1s();
        }

        public void ShowM1Error()
        {
            sampleDisplay.ShowErrorBarcode(sampleList.GetM1ErrorPrompt());
        }

        public void SetIsReadOnly(bool isReadOnly)
        {
            sampleList.IsReadOnly= isReadOnly;
        }

        public UserControl_SamplesDisplay GetSampleDisplay()
        {
            return sampleDisplay;
        }

        private bool CheckM1s()
        {
            sampleList.IncorrectM1List.Clear();

            foreach (System.Windows.Forms.DataGridViewRow row in sampleDisplay.dtgSamples.Rows)
            {
                string newM1 = row.Cells[5].Value.ToString();

                string pageName = row.Cells[0].Value.ToString();

                if (!sampleList.CheckM1(newM1, pageName))
                {
                    if (!sampleList.IncorrectM1List.Contains(row.Cells[0].Value.ToString()))
                    {
                        sampleList.IncorrectM1List.Add(row.Cells[0].Value.ToString());
                    }
                }

            }

            if (sampleList.IncorrectM1List.Count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool CheckSampleNames()
        {
            sampleList.IncorrectBarcodeList.Clear();

            foreach (System.Windows.Forms.DataGridViewRow row in sampleDisplay.dtgSamples.Rows)
            {
                string sampleType = row.Cells[3].Value.ToString();

                string newSampleName = row.Cells[2].Value.ToString();

                if (!sampleList.CheckNewBarcode(sampleType, newSampleName))
                {
                    if (!sampleList.IncorrectBarcodeList.Contains(row.Cells[3].Value.ToString()))
                    {
                        sampleList.IncorrectBarcodeList.Add(row.Cells[3].Value.ToString());
                    }
                }
            }

            if (sampleList.IncorrectBarcodeList.Count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
