﻿
using M_Assay;
using M_SampleList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using M_CommonMethods;
using System.Collections;
using M_Language;

namespace UI_Sample
{
    public class SampleList
    {
        private SMPObject smpObject;
        private AssayInfo assayInfo;
        private ArrayList incorrectBarcodeList = new ArrayList();
        private ArrayList incorrectM1List = new ArrayList();
        public bool IsReadOnly { get; set; }

        public ArrayList IncorrectM1List
        {
            get { return incorrectM1List; }
            set { incorrectM1List = value; }
        }

        public ArrayList IncorrectBarcodeList
        {
            get { return incorrectBarcodeList; }
            set { incorrectBarcodeList = value; }
        }

        public SMPObject SmpObject
        {
            get { return smpObject; }
        }

        public AssayInfo AssayInfo
        {
            get { return assayInfo; }
        }

        internal void SetValues(SMPObject smpObject, AssayInfo assayInfo)
        {
            this.smpObject = smpObject;
            this.assayInfo = assayInfo;
        }

        internal bool IsQuantAssay()
        {
            if (assayInfo.AssayMode==AssayMode.Qualitative)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        internal string GetUnit()
        {
            return smpObject.Unit;
        }

        internal void UpdateSample(string position, string sampleName, string m1)
        {
            smpObject.UpdateM1(position, m1);

            SMPSample sample = smpObject.GetSampleByPosition(position);

            if (sample != null)
            {
                sample.SampleName = sampleName;
            }
        }

        internal string GetAlphanumericPosition(string position)
        {
            string target = string.Empty;
            
            int posInt =-1;

            try
            {
                posInt = Convert.ToInt32(position);
            }
            catch
            { 
                
            }

            if (posInt > 0)
            {
                target = InfoConvert.GetAlphanumericPosition(posInt);
            }

            return target;
        }

        internal string GetNumericPosition(string alphanumericPosition)
        {
            return InfoConvert.GetNumericPosition(alphanumericPosition).ToString();
        }

        internal bool CheckNewBarcode(string sampleType, string newSampleName)
        {
            return assayInfo.IsSampleNameValid(newSampleName, sampleType);
        }

        internal string GetBarcodeErrorPrompt()
        {
            StringBuilder warningMessage = new StringBuilder();

            warningMessage.Append(SysMessage.incorrectSampleName);
            foreach (string type in incorrectBarcodeList)
            {
                warningMessage.Append(type + " ");
            }
            warningMessage.AppendLine(".");
            warningMessage.AppendLine(SysMessage.correctBarcode);

            return warningMessage.ToString();
        }

        internal bool CheckM1(string newM1, string pageName)
        {
            return assayInfo.IsM1Valid(newM1, pageName);
        }

        internal string GetM1ErrorPrompt()
        {
            StringBuilder warningMessage = new StringBuilder();

            warningMessage.Append(SysMessage.incorrectM1);
            foreach (string type in incorrectM1List)
            {
                warningMessage.Append(type + " ");
            }
            warningMessage.AppendLine(".");
            warningMessage.AppendLine(SysMessage.correctM1);

            return warningMessage.ToString();
        }
    }
}
