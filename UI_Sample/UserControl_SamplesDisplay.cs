﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 08/11/2013
 * Author: Liu Rui
 * 
 * Description:
 */
#endregion

using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.Text;

using System.Collections.Generic;
using M_SampleList;
using M_ReportResult;
using M_Language;
using System.ComponentModel;
using M_CommonMethods;

namespace UI_Sample
{
    public partial class UserControl_SamplesDisplay : UserControl
    {
        SampleController sampleController;
        public UserControl_SamplesDisplay(SampleController sampleController)
        {
            InitializeComponent();
            this.sampleController = sampleController;
        }

        private void UserControl_SamplesDisplay_Load(object sender, EventArgs e)
        {
            DisableSorting();

            sampleController.LoadSampleDisplay();

            ChangeColor();

            HideColumns();
            dtgSamples.Sort(dtgSamples.Columns[1], ListSortDirection.Ascending);
        }

        //internal void UpdateSMPObject()
        //{
        //    foreach (DataGridViewRow row in dtgSamples.Rows)
        //    {
        //        string position = sampleController.GetNumericPosition(row.Cells[1].Value.ToString());
        //        string sampleName = row.Cells[2].Value.ToString();
        //        string m1 = row.Cells[5].Value.ToString();

        //        sampleController.sampleList.UpdateSample(position, sampleName, m1);
        //    }
        //}
        
        private void DisableSorting()
        {
            foreach (DataGridViewColumn column in dtgSamples.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }
  
        private void ChangeColor()
        {
            foreach (DataGridViewColumn column in dtgSamples.Columns)
            {
                if (column.ReadOnly)
                {
                    column.DefaultCellStyle.BackColor = Color.LightGray;
                }
            }
        }

        private void HideColumns()
        {
            dtgSamples.Columns[4].Visible = false;
            dtgSamples.Columns[5].Visible = true;
        }

        private void ShowAllRows(DataGridView dtgSamples)
        {
            foreach (DataGridViewRow row in dtgSamples.Rows)
            {
                row.Visible = true;
            }
        }

        private void cmbPageName_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowAllRows(dtgSamples);

            string pageName = cmbPageName.SelectedItem.ToString();

            if (pageName != "All tests/ targets")
            {
                foreach (DataGridViewRow row in dtgSamples.Rows)
                {
                    string rowPageName = row.Cells[0].Value.ToString();
                    if (pageName != rowPageName)
                    {
                        row.Visible = false;
                    }
                }
            }
        }

        internal void ShowErrorBarcode(string errorString)
        {
            MessageBox.Show(errorString, SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        internal void ShowInCompleteSDSFileError()
        {
            string sErrorMsg = "Load data analysis unsuccessfully, please check about the content of loaded folder.";
            if (!string.IsNullOrEmpty(SysMessage.errorMsg))
            {
                sErrorMsg = SysMessage.errorMsg;
            }
            MessageBox.Show(sErrorMsg, SysMessage.errorCaption,
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void chk_wellsort_CheckedChanged(object sender, EventArgs e)
        {
           
            dtgSamples.Sort(dtgSamples.Columns[1], ListSortDirection.Ascending);
        }

      

        private void dtgSamples_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            // Try to sort based on the cells in the current column.
            if (e.Column.HeaderText == "Well")
            {
                if (chk_wellsort.Checked)
                {
                    e.SortResult= InfoConvert.sortWellAscending(e.CellValue1.ToString(), e.CellValue2.ToString());
                }
                else
                {
                    e.SortResult= InfoConvert.sortPositionAscending(e.CellValue1.ToString(), e.CellValue2.ToString());
                }
                e.Handled = true;
            }
        }
    }
}
