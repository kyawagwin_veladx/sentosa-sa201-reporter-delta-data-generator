﻿
using System.Windows.Forms;

namespace UI_Run
{
    partial class UserControl_RunDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblStep2 = new Label();
            this.lblStartRun = new Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkInsert = new CheckBox();
            this.lblStep1 = new Label();
            this.lblInsert = new Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button1 = new Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picArrow = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.picArrow)).BeginInit();
            this.SuspendLayout();
            // 
            // lblStep2
            // 
            this.lblStep2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStep2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStep2.Location = new System.Drawing.Point(490, 30);
            this.lblStep2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStep2.Name = "lblStep2";
            this.lblStep2.Size = new System.Drawing.Size(48, 20);
            this.lblStep2.TabIndex = 1;
            this.lblStep2.Text = "Step 2:";
            // 
            // lblStartRun
            // 
            this.lblStartRun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStartRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartRun.Location = new System.Drawing.Point(534, 30);
            this.lblStartRun.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStartRun.Name = "lblStartRun";
            this.lblStartRun.Size = new System.Drawing.Size(231, 20);
            this.lblStartRun.TabIndex = 3;
            this.lblStartRun.Text = "Launch Sentosa® SA201 Series Software";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.lblStartRun);
            this.panel1.Controls.Add(this.chkInsert);
            this.panel1.Controls.Add(this.lblStep2);
            this.panel1.Controls.Add(this.lblStep1);
            this.panel1.Controls.Add(this.lblInsert);
            this.panel1.Location = new System.Drawing.Point(19, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(795, 70);
            this.panel1.TabIndex = 11;
            // 
            // chkInsert
            // 
            this.chkInsert.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            //this.chkInsert.LabelStyle = LabelStyle.NormalControl;
            this.chkInsert.Location = new System.Drawing.Point(247, 32);
            this.chkInsert.Margin = new System.Windows.Forms.Padding(4);
            this.chkInsert.Name = "chkInsert";
            this.chkInsert.Size = new System.Drawing.Size(19, 13);
            this.chkInsert.TabIndex = 4;
            this.chkInsert.Text = "";
            this.chkInsert.CheckedChanged += new System.EventHandler(this.chkInsert_CheckedChanged);
            // 
            // lblStep1
            // 
            this.lblStep1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStep1.Location = new System.Drawing.Point(11, 28);
            this.lblStep1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStep1.Name = "lblStep1";
            this.lblStep1.Size = new System.Drawing.Size(48, 20);
            this.lblStep1.TabIndex = 0;
            this.lblStep1.Text = "Step 1:";
            // 
            // lblInsert
            // 
            this.lblInsert.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInsert.Location = new System.Drawing.Point(55, 28);
            this.lblInsert.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInsert.Name = "lblInsert";
            this.lblInsert.Size = new System.Drawing.Size(193, 20);
            this.lblInsert.TabIndex = 2;
            this.lblInsert.Text = "Insert the plate and close the tray";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.picArrow);
            this.panel2.Location = new System.Drawing.Point(19, 88);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(795, 334);
            this.panel2.TabIndex = 12;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pictureBox2.Image = global::UI_Run.Properties.Resources.Screen_Shot_2014_07_11_at_9_56_27_AM;
            this.pictureBox2.Location = new System.Drawing.Point(10, 12);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(295, 228);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 24;
            this.pictureBox2.TabStop = false;
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(680, 281);
            this.button1.Margin = new System.Windows.Forms.Padding(0, 0, 10, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 32);
            //this.button1.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.button1.TabIndex = 23;
            this.button1.Text = "Start SA201";
            this.button1.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Image = global::UI_Run.Properties.Resources.Veladx_020_Edit;
            this.pictureBox1.Location = new System.Drawing.Point(473, 12);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0, 40, 0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(295, 228);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // picArrow
            // 
            this.picArrow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.picArrow.Image = global::UI_Run.Properties.Resources.arrow;
            this.picArrow.Location = new System.Drawing.Point(337, 87);
            this.picArrow.Margin = new System.Windows.Forms.Padding(4);
            this.picArrow.Name = "picArrow";
            this.picArrow.Size = new System.Drawing.Size(110, 108);
            this.picArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picArrow.TabIndex = 7;
            this.picArrow.TabStop = false;
            // 
            // UserControl_RunDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UserControl_RunDisplay";
            this.Size = new System.Drawing.Size(830, 433);
            this.Load += new System.EventHandler(this.UserControl_RunDisplay_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            //((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.picArrow)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblStep2;
        private Label lblStartRun;
        private System.Windows.Forms.PictureBox picArrow;
        private Panel panel1;
        private Panel panel2;
        internal CheckBox chkInsert;
        private Label lblStep1;
        private Label lblInsert;
        private PictureBox pictureBox1;
        internal Button button1;
        internal PictureBox pictureBox2;

    }
}
