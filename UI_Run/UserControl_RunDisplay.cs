﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 01/11/2013
 * Author: Liu Rui
 * 
 * Description:
 */
#endregion

using System;
using System.Windows.Forms;
using M_Log;

namespace UI_Run
{
    public partial class UserControl_RunDisplay : UserControl
    {
        RunController runController;

        public UserControl_RunDisplay(RunController runController)
        {
            InitializeComponent();
            this.runController = runController;
        }

        #region Private Methods
        private void UserControl_RunDisplay_Load(object sender, EventArgs e)
        {
            ControlRunButton();
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "User clicks to launch Sentosa SA201 Software");

            runController.RunBaseSoftware();
        }
        #endregion

        private void chkInsert_CheckedChanged(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[UserControl_RunDisplay] - Insert plate and close the tray check is changed");
            ControlRunButton();
        }

        private void ControlRunButton()
        {
            if (chkInsert.Checked == false)
            {
                button1.Visible = false; //button of starting sentosa base software
                lblStep2.Enabled = false;
                lblStartRun.Enabled = false;
                pictureBox1.Visible = false;
            }
            else
            {
                button1.Visible = true;
                lblStep2.Enabled = true;
                lblStartRun.Enabled = true;
                pictureBox1.Visible = true;
            }
        }

        //private void btnInsert_Click(object sender, EventArgs e)
        //{
        //    if (chkInsert.Checked == true)
        //    {
        //        Log.WriteLog(LogType.system, "[UserControl_RunDisplay]  - Cancel insertion of plate");

        //        chkInsert.Checked = false;
        //    }
        //    else
        //    {
        //        Log.WriteLog(LogType.system, "[UserControl_RunDisplay] - Insert plate and close the tray button click");

        //        chkInsert.Checked = true;
        //    }
        //}
    }
}
