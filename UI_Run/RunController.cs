﻿using M_Assay;
using M_ReportResult;
using M_SampleList;
using M_System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UI_ImportFiles;
using M_Template;
using System.Windows.Forms;
using M_Log;

using M_Language;
using M_ReadSDSFile;
using UI_RunNote;
using System.Threading;
using System.IO;
using System.Diagnostics;

namespace UI_Run
{
    public delegate string dGetRunNotes();
    public delegate void dEnableNextButton(bool text);
    public delegate void dEnablePreviousButton(bool text);
    public delegate void dEnableNotes(bool text);


    public class RunController
    {
        UserControl_RunDisplay RunDisplay;
        AssayRun assayRun;

        public dGetRunNotes GetRunNotesCallBack;
        public dEnableNextButton EnableNextButton;
        public dEnablePreviousButton EnablePreviousButton;
        public dEnableNotes EnableNotes;
        private event EventHandler SDSCompeleted;
        private Thread monitoringThread;
        public RunController()
        {
            RunDisplay = new UserControl_RunDisplay(this);
            assayRun = new AssayRun();
            SDSCompeleted = new EventHandler(OnCompleted);
        }

        public void SetValues(SystemConfig systemConfig, AssayInfo assayInfo, SMPObject smpObject, RunTemplate template, ReportResult reportResult)
        {
            //Object->UI
            assayRun.SetValues(systemConfig, assayInfo, smpObject, template, reportResult);
        }

        public void InterpretResult(AssayLogic assayLogic)
        {
            assayRun.InterpretResult(assayLogic);
        }

        public void SDSFileUpdate()
        {
            assayRun.TempImport();
        }

        public void KillAutoItScripts()
        {
            KillAutoRunScripts();
            KillAutoAnalyseScripts();
        }

        internal void RunBaseSoftware()
        {
            if (MessageBox.Show(SysMessage.confirmRun
               , SysMessage.confirmCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
            {
              
                Log.WriteLog(LogType.system, "User confirms the run");
                string runNotes = this.GetRunNotesCallBack();
                Log.WriteLog(LogType.system, "User notes: " + runNotes);
                assayRun.GetRunTime(DateTime.Now);
                assayRun.SetRunFolder();

                if (MessageBox.Show(SysMessage.putFiles + Environment.NewLine + assayRun.GetAnalysisFolder()
                , SysMessage.confirmCaption, MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
                {
                    Log.WriteLog(LogType.system, "User confirms the run folder");

                    KillAutoItScripts();
                    GenerateTXT();

                    GenerateAssayFile(runNotes);
                    SystemConfig sysConfig = assayRun.GetSystemConfig();
                    string sBaseProcessName = Path.GetFileNameWithoutExtension(sysConfig.Path_BaseSoftware);
                    string sAutoAnalyzeName = Path.GetFileNameWithoutExtension(sysConfig.AutoAnalyzeSoftwareName);
                    if (MoveTemplate())
                    {
                        EnableNextButton(false);
                        EnablePreviousButton(false);
                        EnableNotes(false);

                        RunDisplay.button1.Enabled = false;
                        RunDisplay.pictureBox2.Enabled = false;
                        RunDisplay.chkInsert.Enabled = false;

                        LaunchBaseSoftware();
                        do
                        {
                            Thread.Sleep(1000);
                        }
                        while (IsLaunchBaseSoftware(sBaseProcessName) == false);
                        StartMornitoringThread(sBaseProcessName, sAutoAnalyzeName);
                    }
                    else
                    {
                        MessageBox.Show(SysMessage.noTemplateMsg, SysMessage.errorCaption,
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        internal void StartMornitoringThread(string sBaseProcessName, string sAutoAnalyzeName)
        {
            monitoringThread = new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                while (assayRun.GetSDSFilePath() == null)
                {
                    Thread.Sleep(1000);
                };
             
                do
                {
                    if (!IsFileLocked(assayRun.GetSDSFilePath()))
                    {
                        string sStatus = SDSInformation.GetAcquisitionStatus(assayRun.GetSDSFilePath());

                        if (!IsLaunchBaseSoftware(sBaseProcessName))
                        {
                            KillAutoItScripts();
                            OnCompleted(this, new EventArgs());
                            Log.WriteLog(LogType.system, "Sentosa SA201 Series Software was closed by user");
                            break;
                        }


                        if (sStatus.Equals("PROG"))
                        {
                            KillAutoRunScripts();
                        }


                        if (sStatus.Equals("DONE"))
                        {
                            KillAutoItScripts();
                            RunAnalyzeScript();
                            do
                            {
                                Thread.Sleep(2000);
                            }
                            while (IsLaunchBaseSoftware(sAutoAnalyzeName) == false);

                            OnCompleted(this, new EventArgs());
                            break;
                        }

                    }
                    Thread.Sleep(2000);

                } while (true);
            });
            monitoringThread.Start();
        }
        internal void GenerateTXT()
        {
            Log.WriteLog(LogType.system, "Start to generate TXT file ");
            assayRun.GenerateTXT();
            Log.WriteLog(LogType.system, "Run Page - TXT file generated.");
        }

        internal bool MoveTemplate()
        {
            Log.WriteLog(LogType.system, "Start to generate new template file ");
            return assayRun.MoveTemplate();
        }

        internal void LaunchBaseSoftware()
        {
            try
            {
                Log.WriteLog(LogType.system, "Try to lauch basesoftware");
                assayRun.LaunchBaseSoftware();
                Log.WriteLog(LogType.system, "Run Page - Sentosa SA201 Software launched.");
            }
            catch (Exception ex)
            {
                Log.WriteLog(LogType.system, "LunchBaseSoftware Error: " + ex.Message);
            }
           

        }
        internal void RunAnalyzeScript()
        {
            try
            {
                Log.WriteLog(LogType.system, "Try to lauch analyze script");
                assayRun.RunAnalyzeScript();
            }
            catch (Exception)
            {
                Log.WriteLog(LogType.system, "Try to lauch analyze script");
            }


        }

        protected void KillAutoRunScripts()
        {

            foreach (Process pSDS in Process.GetProcesses())
            {
                if (pSDS.ProcessName.Equals(Path.GetFileNameWithoutExtension(assayRun.GetSystemConfig().AutoRunSoftwareName), StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        pSDS.Kill();
                    }
                    catch (Exception ex)
                    {
                        Log.WriteLog(LogType.system, "Failed to kill " + assayRun.GetSystemConfig().AutoRunSoftwareName + ". " + ex.Message);
                    }

                }
            }

        }
        protected void KillAutoAnalyseScripts()
        {

            foreach (Process pSDS in Process.GetProcesses())
            {
                if (pSDS.ProcessName.Equals(Path.GetFileNameWithoutExtension(assayRun.GetSystemConfig().AutoAnalyzeSoftwareName), StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        pSDS.Kill();
                    }
                    catch (Exception ex)
                    {
                        Log.WriteLog(LogType.system, "Failed to kill " + assayRun.GetSystemConfig().AutoAnalyzeSoftwareName + ". " + ex.Message);
                    }

                }
            }

        }
        internal bool IsLaunchBaseSoftware(string exeName)
        {
            //Log.WriteLog(LogType.system, "Check lauched basesoftware");
            return assayRun.IsLaunchBaseSoftware(exeName);
        }

        internal void GenerateAssayFile(string notes)
        {
            assayRun.UpdateRunNotes(notes);
            assayRun.GenerateAssayFile();
        }

        public UserControl_RunDisplay GetRunDisplay()
        {
            return RunDisplay;
        }

        internal bool IsFileLocked(string filename)
        {
            FileStream stream = null;

            try
            {
                stream = File.Open(filename, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return false;
        }

        protected void OnCompleted(Object obj, EventArgs e)
        {
            try
            {
                RunDisplay.Invoke(EnableNextButton, new object[] { true });
                Thread.CurrentThread.Abort();
                KillAutoItScripts();
                MessageBox.Show(SysMessage.rgqCompletedMsg, SysMessage.InfoCaption,
                               MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                //do later
            }
        }


    }
}
