﻿using M_Assay;
using M_ReportResult;
using M_SampleList;
using M_System;
using M_Template;
using System;
using System.Collections.Generic;
using M_SA201EntryList;
using M_SA201Run;
using System.IO;
using M_SA201Result;
using M_Interpretation;
using M_AssayFile;
using System.Collections;
using M_Log;
using UI_ImportFiles;
using M_Language;

namespace UI_Run
{
    public class AssayRun
    {
        private SMPObject smpObject;
        private AssayInfo assayInfo;
        private SystemConfig systemConfig;
        private RunTemplate template;
        private ReportResult reportResult;
        private string runTime;
        private string templateDestPath;
        ImportFiles importFileclass=new ImportFiles();
       
        internal void InterpretResult(AssayLogic assayLogic)
        {
            if (assayLogic.FillAssayLogic(assayInfo.AssayConfigPath))
            {
                if (assayInfo.SelectedSizeIndex >= 0)
                {
                    assayLogic.SampleSize = assayInfo.SampleSizeList[assayInfo.SelectedSizeIndex];
                }

                assayInfo.RefillPageName(reportResult);

                Interpretation resultInterpretation = new Interpretation(assayInfo, assayLogic, reportResult);

                resultInterpretation.InterpretResults();
            }
            else
            {
                
            }
        }
        internal SystemConfig GetSystemConfig()
        {
            return systemConfig;
        }

        internal void TempImport()
        {
            importFileclass.SetValues(systemConfig, assayInfo, reportResult);

            importFileclass.UpdateFlags(GetSDSFilePath());

            importFileclass.UpdateReportResult(GetSDSFilePath(),smpObject);

            importFileclass.FillAssayInfoSDS();
        }

        internal void SetValues(SystemConfig systemConfig, AssayInfo assayInfo, SMPObject smpObject, RunTemplate template, ReportResult reportResult)
        {
            this.smpObject = smpObject;
            this.assayInfo = assayInfo;
            this.systemConfig = systemConfig;
            this.template = template;
            this.reportResult = reportResult;
        }

        internal string GetSDSFilePath()
        {
            string [] files= Directory.GetFiles(assayInfo.AnalysisFolder, "*.sds");

            if (files.Length > 0)
            {
                return files[0];
            }
            else
                return null;
            
        }

        internal void GenerateTXT()
        {
            assayInfo.GetTxtFilePath();

            Dictionary<string,string> channelNames = assayInfo.GetChannelDyeNameDictonary();

            ExportFile.GenerateTXTFile(assayInfo.TxtFilePath, smpObject, channelNames);
        }

        internal bool MoveTemplate()
        {
            string templatePath = systemConfig.Folder_Template + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(assayInfo.TemplateName)+".sds";

            this.templateDestPath = Path.GetDirectoryName(assayInfo.TxtFilePath) + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(assayInfo.TemplateName) + " " + runTime + ".sds";

            if (File.Exists(templatePath))
            {
                File.Copy(templatePath, templateDestPath, true);
                return true;
            }
            else
            {
                return false;
            }
        }

        internal void GetRunTime(DateTime dateTime)
        {
            this.runTime = string.Format("{0:yyyy-MM-dd_HH-mm-ss}", dateTime);
        }

        internal void LaunchBaseSoftware()
        {

            string sSDSFileName = Path.GetFileName(this.GetTemplateDir());
            string sTXTFileName = Path.GetFileName(assayInfo.TxtFilePath);
            string sSDSFolder = Path.GetDirectoryName(assayInfo.TxtFilePath);
            string sRunExe = systemConfig.Folder_AutoIt + @"\" + systemConfig.AutoRunSoftwareName;
            // Lunch Base Sentosa SA201 Series Software
            BaseSoftwareController.LaunchEXE(systemConfig.Path_BaseSoftware, @""""+this.GetTemplateDir()+@"""");
            // Run auto run script 
            string sParm = @" """ + sSDSFolder + @""" " + @" """ + sSDSFileName + @""" " + @" """ + sTXTFileName + @""" " + @" """ + SysMessage.username  + @""" " + @" """ + SysMessage.password + @""" ";
            BaseSoftwareController.LaunchEXE(sRunExe, sParm);
            //BaseSoftwareController.LaunchEXE(systemConfig.Path_BaseSoftware, @""""+this.GetTemplateDir()+@"""");
        }

        internal void RunAnalyzeScript()
        {

            string sSDSFileName = Path.GetFileName(this.GetTemplateDir());
            string sTXTFileName = Path.GetFileName(assayInfo.TxtFilePath);
            string sSDSFolder = Path.GetDirectoryName(assayInfo.TxtFilePath);
            string sRunExe = systemConfig.Folder_AutoIt + @"\" + systemConfig.AutoAnalyzeSoftwareName;
            string sParm =  @" """ + sSDSFileName + @""" "+ @" """ + SysMessage.username + @""" " + @" """ + SysMessage.password + @""" ";
            BaseSoftwareController.LaunchEXE(sRunExe, sParm);
            //BaseSoftwareController.LaunchEXE(systemConfig.Path_BaseSoftware, @""""+this.GetTemplateDir()+@"""");
        }

        internal bool IsLaunchBaseSoftware(string exeName)
        {
            return BaseSoftwareController.IsLaunchedEXE(exeName);
        }

        internal void GenerateAssayFile()
        {
            AssayFile.GenerateAssayFile(runTime, assayInfo, smpObject, template);

            Log.WriteLog(LogType.system, "Run Page - SDX file generated.");
        }

        //internal void FillAssayInfo()
        //{
        //    List<string> m1ItemList = AssayFile.GetM1ItemList(assayInfo.RunSDX);            

        //    assayInfo.FillAssayInfo(assayInfo.GetAssayConfigFile(m1ItemList, systemConfig.Folder_AssayConfig));

        //    List<string> selectedSamples = ReadRunFiles.GetSelectedSamples(assayInfo.ResultCSV);

        //    assayInfo.UpdateSelectedSizeIndex(assayInfo.AssayMode, selectedSamples);
        //}

        internal void UpdateRunNotes(string notes)
        {
            template.Notes = notes;
        }

        internal void SetRunFolder()
        {
            assayInfo.SetRealRunFolder(systemConfig.Folder_Run, runTime);
        }

        internal string GetAnalysisFolder()
        {
            return assayInfo.AnalysisFolder;
        }
        internal string GetTemplateDir()
        {
            return this.templateDestPath;
        }
    }
}
