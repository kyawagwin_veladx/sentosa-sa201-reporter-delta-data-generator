﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using Microsoft.Reporting.WinForms;
using M_Report.DataSetCollection;
using M_CommonMethods;
using M_ReportResult;
using M_Assay;
using M_Curve;
using System.Windows.Forms.DataVisualization.Charting;
using System.IO;
using System.Globalization;

namespace M_Report
{
    public partial class ReportControl : UserControl
    {
        private SelectedReport myReport;
        private ReportResult myResult;
        private AssayLogic myAssayLogic;

        private ReportViewer reportViewer1;

        public ReportControl()
        {
            InitializeComponent();

            this.reportViewer1 = new ReportViewer();
            this.reportViewer1.Dock = DockStyle.Fill;
            this.reportViewer1.Font = new Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reportViewer1.Location = new Point(0, 0);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "M_Report.QualReport.rdlc";
            this.reportViewer1.Location = new Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.PageCountMode = PageCountMode.Actual;
            this.reportViewer1.Size = new Size(800, 635);
            this.reportViewer1.TabIndex = 0;
            this.reportViewer1.Load += new System.EventHandler(this.reportViewer1_Load);
            
            this.Controls.Add(this.reportViewer1);
        }

        public void DisplayReport(SelectedReport myreport, AssayLogic assaylogic, ReportResult reportresult)
        {
            this.myReport = myreport;
            this.myResult = reportresult;
            this.myAssayLogic = assaylogic;
            this.reportViewer1.LocalReport.DisplayName = myResult.RunInfo.RunID;
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                this.reportViewer1.LocalReport.EnableExternalImages = true;

                this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("Run", FillRunTable()));

                this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("Profile", FillProfileTable()));

                this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("PCurve", GetPageValue()));

                for (int i = 0; i <4; i++)
                {
                    if (i < myResult.SampleResultList[0].CtChannelList.Count)
                    {
                        this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("QuanInfo" + (i + 1).ToString(), FillQuant(i)));
                    }
                    else
                    {
                        this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("QuanInfo" + (i + 1).ToString(), new DataTable()));
                    }
                }

                if (myReport.SelectedResultSample.Count > 0)
                {
                    this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("QualResult", ResultQualSamples()));
                }
                else
                {
                    this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("QualResult", new DataTable()));
                }

                ParaStatus();
                DisableUnwantedFormat("Word");
                this.reportViewer1.ZoomMode = ZoomMode.Percent;
                this.reportViewer1.ZoomPercent = 100;
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }

        private DataTable GetPageValue()
        {
            DataSetPage datasetpage = new DataSetPage();
            string filepathr1=string.Empty;
            string filepathn1=string.Empty;
            string filepathr2=string.Empty;
            string filepathn2=string.Empty;
            string filepathr3=string.Empty;
            string filepathn3=string.Empty;
            string filepathr4=string.Empty;
            string filepathn4=string.Empty;
            
            try
            {
                for (int i = 0; i < 4; i++)
                {
                    if (i <myResult.SampleResultList[0].CtChannelList.Count)
                    {
                        DisplayCurve dc = new DisplayCurve(myResult.SampleResultList[0].CtChannelList[i].ChannelName, myResult.SampleResultList[0].PageName, myResult.SampleResultList[0].CtChannelList[i].VirusName);
                      
                       
                        string filepathr = AppDomain.CurrentDomain.BaseDirectory + @"Image\Raw\mypic"+(i+1).ToString()+".png";
                        string filepathn = AppDomain.CurrentDomain.BaseDirectory + @"Image\Nom\mypic"+(i+1).ToString()+".png";

                        double threshold = myResult.SampleResultList[0].CtChannelList[i].Threshold;

                        if (myReport.SelectedResultSample.Count > 0)
                        {
                            foreach (SampleResult sr in myReport.SelectedResultSample)
                            {
                                dc.AddCurve(sr, i);

                            }
                            dc.GetCurve(CurveMode.RAW, threshold);
                            
                            dc.SaveImage(filepathr);
                            dc.GetCurve(CurveMode.LOG, threshold);
                            dc.SaveImage(filepathn);
                        }

                        if (i == 0)
                        {
                            filepathr1 = @"file:\\\" + filepathr;
                            filepathn1 = @"file:\\\" + filepathn;
                        }
                        else if (i == 1)
                        {
                            filepathr2 = @"file:\\\" + filepathr;
                            filepathn2 = @"file:\\\" + filepathn;
                        }
                        else if (i == 2)
                        {
                            filepathr3 = @"file:\\\" + filepathr;
                            filepathn3 = @"file:\\\" + filepathn;
                        }
                        else if (i == 3)
                        {
                            filepathr4 = @"file:\\\" + filepathr;
                            filepathn4 = @"file:\\\" + filepathn;
                        }
                    }
                }
                datasetpage.Page.AddPageRow(myResult.SampleResultList[0].PageName, filepathr1, filepathn1, filepathr2, filepathn2, filepathr3, filepathn3, filepathr4, filepathn4);
            }
            catch 
            {
                MessageBox.Show("Unable to display report.Please ensure assay configuration is right.");
                
                
            }
           return datasetpage.Page;
        }

        private DataTable FillQuant(int channelindex)
        {
            DataSetQuanInfo datasetquan = new DataSetQuanInfo();
            datasetquan.QuanInfo.AddQuanInfoRow("Threshold", FormatThreshold(myResult.SampleResultList[0].CtChannelList[channelindex].Threshold.ToString()));
            datasetquan.QuanInfo.AddQuanInfoRow("Dye", ConvertDye(myResult.SampleResultList[0].CtChannelList[channelindex].Dye.ToString()));
            datasetquan.QuanInfo.AddQuanInfoRow("Start", myResult.SampleResultList[0].CtChannelList[channelindex].Start.ToString());
            datasetquan.QuanInfo.AddQuanInfoRow("End", myResult.SampleResultList[0].CtChannelList[channelindex].End.ToString());
            
            return datasetquan.QuanInfo;
        }


        private string FormatThreshold(string threshold)
        {
            string sRet = threshold;
            //double dThreshold = 0;
            //if (Double.TryParse(threshold, out dThreshold))
            //{
            //    sRet = dThreshold.ToString("G", CultureInfo.InvariantCulture);
            //}
            return sRet;

        }
        private string ConvertDye(string dyename)
        {
            string str = dyename.ToUpper();

            if (str == "CY5" && myResult.SampleResultList[0].PageName != "ZIKV-DENV-CHIKV" && myResult.SampleResultList[0].PageName != "ZIKV" && myResult.SampleResultList[0].PageName != "SA PQ")
            {
                return "QUASAR 670";
            }
            else
            {
                return dyename;
            }
        }

        private DataTable FillRunTable() 
        {
            DataSetRun datasetrun = new DataSetRun();
            datasetrun.Run.AddRunRow("Run ID", myResult.RunInfo.RunID);

            
            datasetrun.Run.AddRunRow("User", myResult.RunInfo.CombinedUsers);
            datasetrun.Run.AddRunRow("Run Signature", InfoConvert.ConvertONOFF(myResult.RunInfo.IsSignatureValid));

            datasetrun.Run.AddRunRow("Run Start", InfoConvert.ConvertRunTime(myResult.RunInfo.RunStart));
            
            datasetrun.Run.AddRunRow("Last Modified", InfoConvert.ConvertRunTime(myResult.RunInfo.LastModified));
            
            datasetrun.Run.AddRunRow("Software Version", myResult.RunInfo.CombinedVersions);
            datasetrun.Run.AddRunRow("Instrument Type", myResult.RunInfo.InstumentType);
            //2014-Oct-07 Liu Rui
            //datasetrun.Run.AddRunRow("Operator", myResult.RunInfo.BaseOperator);
            datasetrun.Run.AddRunRow("Lot No. of "+myResult.RunInfo.AssayName+" M1", myResult.GetM1Lot());
            datasetrun.Run.AddRunRow("PCR Volume", myResult.RunInfo.PCRVolume+ "uL");
            //datasetrun.Run.AddRunRow("Notes/Remarks", myResult.RunInfo.Notes);

          

            foreach (IDListItem item in myResult.IDListItems)
            {
                if (item.IsDisplayed)
                {
                    if (item.Item.ToUpper() == "AssayKit".ToUpper())
                    {
                        string strAssay="Sentosa SA201 "+ myResult.RunInfo.AssayName + " PCR Test";
                        datasetrun.Run.AddRunRow(item.DisplayName, strAssay + "\n" + item.Value);
                    }
                    else if (item.Item.ToUpper() == "LysisKit".ToUpper())
                    {
                        string strLysis = "Sentosa SX Lysis Beads Kit";
                        datasetrun.Run.AddRunRow(item.DisplayName, strLysis + "\n" + item.Value);
                    }
                    else 
                    {
                        datasetrun.Run.AddRunRow(item.DisplayName, item.Value);
                    }
                }
            }

            datasetrun.Run.AddRunRow("Comments", myResult.RunInfo.CombinedComments);

            return datasetrun.Run;
        }

        private DataTable FillProfileTable()
        {
            DataSetProfile datasetprofile = new DataSetProfile();
            
            for (int i = 0; i < myResult.RunProfile.HoldCycleList.Count; i++)
            {
                if (myResult.RunProfile.HoldCycleList[i].GetType() == typeof(Hold))
                {
                    Hold h = (Hold)myResult.RunProfile.HoldCycleList[i];
                    datasetprofile.Profile.AddProfileRow("Stage "+(i+1).ToString(), "Hold @ " + h.Temperature + (Char)176+'C'
                                                      + "; " + InfoConvert.ConvertTime(h.Second));
                }
                else if (myResult.RunProfile.HoldCycleList[i].GetType() == typeof(Cycling))
                {
                    Cycling c = (Cycling)myResult.RunProfile.HoldCycleList[i];

                    datasetprofile.Profile.AddProfileRow("Stage " + (i + 1).ToString() + " (" + c.RepeatCount + " repeats) ", c.StepList[0].StepName + ": Hold @ " + c.StepList[0].Temperature + (Char)176 + 'C'
                                                               + "; " + InfoConvert.ConvertTime(c.StepList[0].Second));
                    if (c.StepList.Count > 1)
                    {
                        for (int j = 1; j < c.StepList.Count; j++)
                        {
                            datasetprofile.Profile.AddProfileRow("", c.StepList[j].StepName + ": Hold @ " + c.StepList[j].Temperature + (Char)176 + 'C'
                                                                                        + "; " + InfoConvert.ConvertTime(c.StepList[j].Second));
                        }
                    }
                }
            }

           // datasetprofile.Profile.AddProfileRow("Data Collection", myResult.RunProfile.DataCollection);
            return datasetprofile.Profile;
        }

        private void ParaStatus()
        {
            try
            {
                ReportParameter hideMessage = new ReportParameter("HideMessage", (!myReport.IsDisplayMessage).ToString());
                reportViewer1.LocalReport.SetParameters(hideMessage);

                ReportParameter quan = new ReportParameter("Quan", (!myReport.IsDisplayQuan).ToString());
                reportViewer1.LocalReport.SetParameters(quan);

                ReportParameter cycle = new ReportParameter("Cycle", (!myReport.IsDisplayCycle).ToString());
                reportViewer1.LocalReport.SetParameters(cycle);

                ReportParameter img = new ReportParameter("Img", (!myReport.IsDisplayImg).ToString());
                reportViewer1.LocalReport.SetParameters(img);

                ReportParameter header = new ReportParameter("RP_Header", myResult.RunInfo.RunID);
                reportViewer1.LocalReport.SetParameters(header);

                ReportParameter selectedsam = new ReportParameter("Sam", (!myReport.IsDisplayReportResult).ToString());
                reportViewer1.LocalReport.SetParameters(selectedsam);

                
                if (!string.IsNullOrEmpty(myResult.Message))
                {

                    //string[] splitMessage = myResult.Message.Split(new string[1] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                    //if (splitMessage.Count() > 1)
                    //{
                    //    ReportParameter specialMsg = new ReportParameter("SpecialMessage", splitMessage.First());
                    //    reportViewer1.LocalReport.SetParameters(specialMsg);
                    //}
                    myResult.Message = myResult.Message.Replace("\t", string.Empty);
                    ReportParameter message = new ReportParameter("Message", myResult.Message);
                    reportViewer1.LocalReport.SetParameters(message);
                }

                ReportParameter version = new ReportParameter("version", myResult.RunInfo.GetVersion());
                reportViewer1.LocalReport.SetParameters(version);


                int i = 0;
                foreach (CTChannel ct in myResult.SampleResultList[0].CtChannelList)
                {
                    ReportParameter Channel;

                    if (!string.IsNullOrEmpty(ct.VirusName))
                    {

                        Channel = new ReportParameter("C" + (i + 1).ToString(), string.Format("{0} ({1}) Channel", ChannelFormat(ct.ChannelName), ct.VirusName));
                    }
                    else
                    {
                        Channel = new ReportParameter("C" + (i + 1).ToString(), ChannelFormat(ct.ChannelName) + " Channel");
                    }
                    reportViewer1.LocalReport.SetParameters(Channel);

                    i++;
                }

                ReportParameter intepretation = new ReportParameter("Intepretation", "Intepretation");
                reportViewer1.LocalReport.SetParameters(intepretation);
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }

        private DataTable ResultQualSamples()
        {
            DataSetQualResult datasetQualResult = new DataSetQualResult();
            DataTable dt = datasetQualResult.QualResult;
           
            foreach (SampleResult sr in myReport.SelectedResultSample)
            {
                object[] obj = new object[dt.Columns.Count];
                obj[0] = (object)sr.Position;
                obj[1] = (object)sr.AlphaNumPos;
                obj[2] = (object)InfoConvert.BackgroundColor(sr.colour);
                obj[3] = (object)sr.SampleName;
                obj[4] = (object)sr.SampleTypeString(sr.SampleType);
                obj[5] = (object)sr.GetCombinedCtValue();
                obj[6] = sr.Validity;
                obj[7] = sr.TestResult;
                dt.Rows.Add(obj);
            }
            return dt;
        }

        private void DisableUnwantedFormat(string strFormatName)
        {
            FieldInfo info;
            foreach (RenderingExtension extension in reportViewer1.LocalReport.ListRenderingExtensions())
            {
                if (extension.LocalizedName == strFormatName)
                {
                    info = extension.GetType().GetField("m_isVisible", BindingFlags.Instance | BindingFlags.NonPublic);

                    info.SetValue(extension, false);
                }
            }
        }

        private string ChannelFormat(string channel)
        {
            string str = string.Empty;
            str = channel.ToLower();
            str = char.ToUpper(str[0]) + str.Substring(1);
            return str;
        }
        
    }
}
