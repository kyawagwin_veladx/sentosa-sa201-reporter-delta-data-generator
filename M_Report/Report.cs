﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 1/12/2013
 * Author: Yang Yi
 * 
 * Description:
 * report class
 * 
 */
#endregion

using M_ReportResult;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace M_Report
{
    public class SelectedReport
    {
        private List<SampleResult> selectedResultSample = new List<SampleResult>();

        private bool isDisplayMessage;

        private bool isDisplayQuan;

        private bool isDisplayCycle;

        private bool isDisplayImg;

        private bool isDisplayExperimentInfo;

        private bool isDisplayReportResult;


        public SelectedReport()
        {

        }

        public List<SampleResult> SelectedResultSample
        {
            get
            {
                return selectedResultSample;
            }
            set
            {
                selectedResultSample = value;
            }
        }


        public bool IsDisplayMessage
        {
            get
            {
                return isDisplayMessage;
            }
        }

        public bool IsDisplayQuan
        {
            get
            {
                return isDisplayQuan;
            }

        }

        public bool IsDisplayCycle
        {
            get
            {
                return isDisplayCycle;
            }
           
        }

        public bool IsDisplayImg
        {
            get
            {
                return isDisplayImg;
            }
        }


        public bool IsDisplayExperimentInfo
        {
            get
            {
                return isDisplayExperimentInfo;
            }
        }

        public bool IsDisplayReportResult
        {
            get
            {
                return isDisplayReportResult;
            }
        }

        public void SetContetDisplay(bool epm, bool quan, bool cycle, bool img, bool msg, bool resample)
        {

            isDisplayMessage = msg;

            isDisplayQuan = quan;

            isDisplayCycle = cycle;

            isDisplayImg = img;

            isDisplayExperimentInfo = epm;

            isDisplayReportResult = resample;

        }
    }
}
