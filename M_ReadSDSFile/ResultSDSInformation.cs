﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using M_Log;//TODO: uncomment this to enable logging functionality

namespace M_ReadSDSFile
{
    /// <summary>
    /// This class stores information parsed from SDS file similar to the information in 'Result.csv' file
    /// </summary>
    public class ResultSDSInformation
    {
        public class SampleResult
        {
	        internal string wellName;
	        internal string detectorName;
	        internal string sampleName;
	        internal string taskName;
            internal float cycleOfTransition;
            internal double cycleOfTransitionStandardDeviation;
        }

        public List<SampleResult> sampleResultList = null;
        private int numberOfSampleResultRecords = 0;
        private string operatorInformation = string.Empty;
        private string commentsInformation = string.Empty;
        private string sdsFileNameInformation = string.Empty;
        private int PCRVolume = 0;
        private string instrumentTypeMode = string.Empty;
        private string runStartDate = string.Empty;
        private string lastModifiedDateTime = string.Empty;

        List<int> stage = null;
        List<int> repetitions = null;
        List<int> time = null;
        List<int> temperature = null;
        private int numberOfThermalCyclerProfileEntries = 0;

        public ResultSDSInformation()
        {
            sampleResultList = new List<SampleResult>();
            numberOfSampleResultRecords = 0;

            stage = new List<int>();
            repetitions = new List<int>();
            time = new List<int>();
            temperature = new List<int>();
        }

        /// <summary>
        /// Insert the record containing well name, sample name, detector name, task name, cycle of transition value, cycle of transition standard deviation value in sample results in the Result SDS Information object
        /// </summary>
        /// <param name="wellName">well name</param>
        /// <param name="sampleName">sample name</param>
        /// <param name="detectorName">detector name</param>
        /// <param name="taskName">task name</param>
        /// <param name="cycleOfTransition">cycle of transition value</param>
        /// <param name="cycleOfTransitionStandardDeviation">cycle of transition standard deviation value</param>
        /// <returns>This function doesn't return any value</returns>          
        internal void InsertRecordInSampleResults(string wellName, string sampleName, string detectorName, string taskName, float cycleOfTransition, double cycleOfTransitionStandardDeviation)
        {
            SampleResult sampleResult = new SampleResult();
            this.numberOfSampleResultRecords++;
            sampleResult.wellName = wellName;
            sampleResult.detectorName = detectorName;
            sampleResult.sampleName = sampleName;
            sampleResult.taskName = taskName;
            sampleResult.cycleOfTransition = cycleOfTransition;
            sampleResult.cycleOfTransitionStandardDeviation = cycleOfTransitionStandardDeviation;

            this.sampleResultList.Add(sampleResult);
        }

        /// <summary>
        /// Read the record containing well name, sample name, detector name, task name, cycle of transition value, cycle of transition standard deviation value at specified index location from sample results in the Result SDS Information object
        /// </summary>
        /// <param name="index">index location of the record to be read</param>
        /// <param name="wellName">well name</param>
        /// <param name="sampleName">sample name</param>
        /// <param name="detectorName">detector name</param>
        /// <param name="taskName">task name</param>
        /// <param name="cycleOfTransition">cycle of transition value</param>
        /// <param name="cycleOfTransitionStandardDeviation">cycle of transition standard deviation value</param>
        /// <returns>This function doesn't have any return value</returns>          
        public void GetRecordFromSampleResults(int index, ref string wellName, ref string sampleName, ref string detectorName, ref string taskName, ref float cycleOfTransition, ref double cycleOfTransitionStandardDeviation)
        {
            try
            {
                if (index >= numberOfSampleResultRecords)
                {
                    //Log.WriteLog(LogType.system, "Error: Index location argument is beyond the count of total number of sample result records in the Result SDS Information object");
                    Console.WriteLine("Error: Index location argument is beyond the count of total number of sample result records in the Result SDS Information object");
                }

                wellName = this.sampleResultList[index].wellName;
                detectorName = this.sampleResultList[index].detectorName;
                sampleName = this.sampleResultList[index].sampleName;
                taskName = this.sampleResultList[index].taskName;
                cycleOfTransition = this.sampleResultList[index].cycleOfTransition;
                cycleOfTransitionStandardDeviation = this.sampleResultList[index].cycleOfTransitionStandardDeviation;
            }
            catch (Exception e)
            {
                //Log.WriteLog(LogType.system, "Error while reading parsed information from SDS file: " + e.ToString());
                Console.WriteLine("Error while reading parsed information from SDS file: " + e.ToString());
                throw new System.ArgumentException(e.Message);
            }
        }

        /// <summary>
        /// Return the count of total number of sample result records in the Result SDS Information object
        /// </summary>
        /// <returns>count of total number of sample result records</returns>        
        public int GetNumberOfSampleResultRecords()
        {
            return numberOfSampleResultRecords;
        }

        /// <summary>
        /// Set the operator information in the Result SDS Information object
        /// </summary>
        /// <param name="operatorInformation">operator information</param>
        /// <returns>This function doesn't return any value</returns>   
        public void SetOperatorInformation(string operatorInformation)
        {
            this.operatorInformation = operatorInformation;
        }

        /// <summary>
        /// Return the operator information from the Result SDS Information object
        /// </summary>
        /// <param name="operatorInformation">operator information is filled in this argument</param>
        /// <returns>This function doesn't return any value</returns> 
        public void GetOperatorInformation(ref string operatorInformation)
        {
            operatorInformation = this.operatorInformation;
        }

        /// <summary>
        /// Set the comments information in the Result SDS Information object
        /// </summary>
        /// <param name="commentsInformation">comments information</param>
        /// <returns>This function doesn't return any value</returns>   
        public void SetCommentsInformation(string commentsInformation)
        {
            this.commentsInformation = commentsInformation;
        }

        /// <summary>
        /// Return the comments information from the Result SDS Information object
        /// </summary>
        /// <param name="commentsInformation">comments information is filled in this argument</param>
        /// <returns>This function doesn't return any value</returns> 
        public void GetCommentsInformation(ref string commentsInformation)
        {
            commentsInformation = this.commentsInformation;
        }

        /// <summary>
        /// Set the SDS File name information in the Result SDS Information object
        /// </summary>
        /// <param name="sdsFileNameInformation">name of SDS File</param>
        /// <returns>This function doesn't return any value</returns>   
        public void SetSDSFileNameInformation(string sdsFileNameInformation)
        {
            this.sdsFileNameInformation = sdsFileNameInformation;
        }

        /// <summary>
        /// Return the SDS File name information from the Result SDS Information object
        /// </summary>
        /// <param name="sdsFileNameInformation">name of SDS File name is filled in this argument</param>
        /// <returns>This function doesn't return any value</returns> 
        public void GetSDSFileNameInformation(ref string sdsFileNameInformation)
        {
            sdsFileNameInformation = this.sdsFileNameInformation;
        }

        /// <summary>
        /// Set the SDS File name information in the Result SDS Information object
        /// </summary>
        /// <param name="PCRVolume">PCR Volume</param>
        /// <returns>This function doesn't return any value</returns>   
        public void SetPCRVolume(int PCRVolume)
        {
            this.PCRVolume = PCRVolume;
        }

        /// <summary>
        /// Return the PCR Volume from the Result SDS Information object
        /// </summary>
        /// <param name="PCRVolume">PCR Volume is filled in this argument</param>
        /// <returns>This function doesn't return any value</returns> 
        public void GetPCRVolume(ref int PCRVolume)
        {
            PCRVolume = this.PCRVolume;
        }

        /// <summary>
        /// Set the Instrument Type Mode information in the Result SDS Information object
        /// </summary>
        /// <param name="instrumentTypeMode">Instrument Type Mode</param>
        /// <returns>This function doesn't return any value</returns>   
        public void SetInstrumentTypeMode(string instrumentTypeMode)
        {
            this.instrumentTypeMode = instrumentTypeMode;
        }

        /// <summary>
        /// Return the Instrument Type Mode information from the Result SDS Information object
        /// </summary>
        /// <param name="instrumentTypeMode">Instrument Type Mode is filled in this argument</param>
        /// <returns>This function doesn't return any value</returns> 
        public void GetInstrumentTypeMode(ref string instrumentTypeMode)
        {
            instrumentTypeMode = this.instrumentTypeMode;
        }

        /// <summary>
        /// Insert the record containing stage, repetitions, time and temperature in the Thermal Cycle Profile Information in Result SDS Information object
        /// </summary>
        /// <param name="stage">stage</param>
        /// <param name="repetitions">repetition</param>
        /// <param name="time">time</param>
        /// <param name="temperature">temperature</param>
        /// <returns>This function doesn't return any value</returns>          
        internal void InsertRecordInThermalCycleProfileInformation(int stage, int repetitions, int time, int temperature)
        {
            this.stage.Add(stage);
            this.repetitions.Add(repetitions);
            this.time.Add(time);
            this.temperature.Add(temperature);
            this.numberOfThermalCyclerProfileEntries++;
        }

        /// <summary>
        /// Get the record containing stage, repetitions, time and temperature at the index location at the Thermal Cycle Profile Information from Result SDS Information object
        /// </summary>
        /// <param name="stage">stage</param>
        /// <param name="repetitions">repetition</param>
        /// <param name="time">time</param>
        /// <param name="temperature">temperature</param>
        /// <returns>This function doesn't return any value</returns>          
        public void GetRecordInThermalCycleProfileInformation(int index, ref int stage, ref int repetitions, ref int time, ref int temperature)
        {
            try
            {
                if (index >= numberOfThermalCyclerProfileEntries)
                {
                    //Log.WriteLog(LogType.system, "Error: Index location argument is beyond the count of total number of entries in the Ct SDS Information object");
                    Console.WriteLine("Error: Index location argument is beyond the count of total number of entries in the Ct SDS Information object");
                }
                stage = this.stage[index];
                repetitions = this.repetitions[index];
                time = this.time[index];
                temperature = this.temperature[index];
            }
            catch (Exception e)
            {
                //Log.WriteLog(LogType.system, "Error while reading parsed information from SDS file: " + e.ToString());
                Console.WriteLine("Error while reading parsed information from SDS file: " + e.ToString());
                throw new System.ArgumentException(e.Message);
            }  
        }

        /// <summary>
        /// Return the count of total number of Thermal Cycler Profile entries in the Result SDS Information object
        /// </summary>
        /// <returns>count of total number of Thermal Cycler Profile entries</returns> 
        public int GetNumberOfThermalCyclerProfileEntries()
        {
            return numberOfThermalCyclerProfileEntries;
        }

        /// <summary>
        /// Set the Run start date information in the Result SDS Information object
        /// </summary>
        /// <param name="runStartDate">Run start date</param>
        /// <returns>This function doesn't return any value</returns>   
        public void SetRunDate(string runStartDate)
        {
            this.runStartDate = runStartDate;
        }

        /// <summary>
        /// Return the Run start date information from the Result SDS Information object
        /// </summary>
        /// <param name="runStartDate">Run start date is filled in this argument</param>
        /// <returns>This function doesn't return any value</returns> 
        public void GetRunDate(ref string runStartDate)
        {
            runStartDate = this.runStartDate;
        }
        
        /// <summary>
        /// Set the Last Modified Date Time information in the Result SDS Information object
        /// </summary>
        /// <param name="lastModifiedDateTime">Last Modified Date Time</param>
        /// <returns>This function doesn't return any value</returns>   
        public void SetLastModifiedDateTime(string lastModifiedDateTime)
        {
            this.lastModifiedDateTime = lastModifiedDateTime;
        }

        /// <summary>
        /// Return the Last Modified Date Time information from the Result SDS Information object
        /// </summary>
        /// <param name="lastModifiedDateTime">Last Modified Date Time is filled in this argument</param>
        /// <returns>This function doesn't return any value</returns> 
        public void GetLastModifiedDateTime(ref string lastModifiedDateTime)
        {
            lastModifiedDateTime = this.lastModifiedDateTime;
        }
    }
}
