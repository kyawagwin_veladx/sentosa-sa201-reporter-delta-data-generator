﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using M_Log;//TODO: uncomment this to enable logging functionality

namespace M_ReadSDSFile
{
    /// <summary>
    /// This class stores information parsed from SDS file similar to the information in 'Component.csv' file
    /// </summary>
    public class ComponentSDSInformation
    {
        private int numberOfSubcomponents;
	    private int numberOfReadings;
        private int[] wellIdentifier;
	    internal string[] stringDyeName;
	    private float[,] signals;

        /// <summary>
        /// Set the count of total number of subcomponents in the Component SDS Information object
        /// </summary>
        /// <param name="numberOfSubcomponents">count of total number of subcomponents</param>
        /// <returns>This function doesn't return any value</returns>        
        internal void SetNumberOfSubcomponents(int numberOfSubcomponents)
        {
            this.numberOfSubcomponents = numberOfSubcomponents;
            this.wellIdentifier = new int[numberOfSubcomponents];
            this.stringDyeName = new string[numberOfSubcomponents];
        }

        /// <summary>
        /// Set the count of total number of readings in the Component SDS Information object
        /// </summary>
        /// <param name="numberOfReadings">count of total number of readings</param>
        /// <returns>This function doesn't return any value</returns>   
        internal void SetNumberOfReadings(int numberOfReadings)
        {
            this.numberOfReadings = numberOfReadings;
            this.signals = new float[numberOfSubcomponents, numberOfReadings];
        }

        /// <summary>
        /// Insert the record containing index, well identifier and dye name in the Component SDS Information object
        /// </summary>
        /// <param name="index">index location of the record</param>
        /// <param name="wellIdentifier">well identifier</param>
        /// <param name="stringDyeName">dye name</param>
        /// <returns>This function doesn't return any value</returns>          
        internal void InsertRecord(int index, int wellIdentifier, string stringDyeName)
        {
            this.wellIdentifier[index] = wellIdentifier;
            this.stringDyeName[index] = stringDyeName;           
        }

        /// <summary>
        /// Insert the signal information in the record in Component SDS Information object
        /// </summary>
        /// <param name="recordIndex">index location of the record</param>
        /// <param name="readingIndex">index location for the signal</param>
        /// <param name="signalValue">signal value</param>
        /// <returns>This function doesn't return any value</returns>          
        internal void InsertSignalInRecord(int recordIndex, int readingIndex, float signalValue)
        {
            this.signals[recordIndex, readingIndex] = signalValue;
        }

        /// <summary>
        /// Read the record containing well identifier, dye name and signals from the Component SDS Information object at the specified index location
        /// </summary>
        /// <param name="index">index location of the record to be read</param>
        /// <param name="wellIdentifier">value of well identifier</param>
        /// <param name="stringDyeName">dye name</param>
        /// <param name="signals">array of signal values</param>
        /// <returns>This function doesn't have any return value</returns>          
        public void GetRecord(int index, ref int wellIdentifier, ref string stringDyeName, ref float [] signals)
        {
            try
            {
                if (index >= numberOfSubcomponents)
                {
                    //Log.WriteLog(LogType.system, "Error: Index location argument is beyond the count of total number of subcomponents in the Component SDS Information object");
                    Console.WriteLine("Error: Index location argument is beyond the count of total number of subcomponents in the Component SDS Information object");
                }

                wellIdentifier = this.wellIdentifier[index];
                stringDyeName = this.stringDyeName[index];
                for (int j = 0; j < this.numberOfReadings; j++)
                {
                    signals[j] = this.signals[index, j];
                }
            }
            catch (Exception e)
            {
                //Log.WriteLog(LogType.system, "Error while reading parsed information from SDS file: " + e.ToString());
                Console.WriteLine("Error while reading parsed information from SDS file: " + e.ToString());
                throw new System.ArgumentException(e.Message);
            }
        }

        /// <summary>
        /// Return the count of total number of subcomponents in the Component SDS Information object
        /// </summary>
        /// <returns>count of total number of subcomponents</returns>        
        public int GetNumberOfSubcomponents()
        {
            return this.numberOfSubcomponents;
        }

        /// <summary>
        /// Return the count of total number of readings in the Component SDS Information object
        /// </summary>
        /// <returns>count of total number of readings</returns> 
        public int GetNumberOfReadings()
        {
            return this.numberOfReadings;
        }

    }
}
