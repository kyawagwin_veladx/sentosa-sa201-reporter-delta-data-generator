﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using M_Log;//TODO: uncomment this to enable logging functionality

namespace M_ReadSDSFile
{
    /// <summary>
    /// This class stores information parsed from SDS file similar to the information in 'Ct.csv' file
    /// </summary>
    public class CtSDSInformation
    {
        private List<string> detectorNameList = null;
        private List<int> baselineStartList = null;
        private List<int> baselineEndList = null;
        private List<float> thresholdList = null;
        private int numberOfEntries = 0;

        public CtSDSInformation()
        {
            detectorNameList = new List<string>();
            baselineStartList = new List<int>();
            baselineEndList = new List<int>();
            thresholdList = new List<float>();
        }

        /// <summary>
        /// Insert the record containing index, detector name, baseline start value, baseline end value and threshold in the Ct SDS Information object
        /// </summary>
        /// <param name="detectorName">name of the detector</param>
        /// <param name="baselineStart">baseline start value</param>
        /// <param name="baselineEnd">baseline end value</param>
        /// <param name="threshold">threshold value</param>
        /// <returns>This function doesn't return any value</returns>          
        internal void InsertRecord(string detectorName, int baselineStart, int baselineEnd, float threshold)
        {
            this.numberOfEntries++;
            this.detectorNameList.Add(detectorName);
            this.baselineStartList.Add(baselineStart);
            this.baselineEndList.Add(baselineEnd);
            this.thresholdList.Add(threshold);
        }

        /// <summary>
        /// Read the record containing detector name, baseline start value, baseline end value and threshold from the Ct SDS Information object at specified index location
        /// </summary>
        /// <param name="index">index location</param>
        /// <param name="detectorName">name of the detector</param>
        /// <param name="baselineStart">baseline start value</param>
        /// <param name="baselineEnd">baseline end value</param>
        /// <param name="threshold">threshold value</param>
        /// <returns>This function doesn't have any return value</returns> 
        public void GetRecord(int index, ref string detectorName, ref int baselineStart, ref int baselineEnd, ref float threshold)
        {
            try
            {
                if (index >= numberOfEntries)
                {
                    //Log.WriteLog(LogType.system, "Error: Index location argument is beyond the count of total number of entries in the Ct SDS Information object");
                    Console.WriteLine("Error: Index location argument is beyond the count of total number of entries in the Ct SDS Information object");
                }

                detectorName = this.detectorNameList[index];
                baselineStart = this.baselineStartList[index];
                baselineEnd = this.baselineEndList[index];
                threshold = this.thresholdList[index];
            }
            catch (Exception e)
            {
                //Log.WriteLog(LogType.system, "Error while reading parsed information from SDS file: " + e.ToString());
                Console.WriteLine("Error while reading parsed information from SDS file: " + e.ToString());
                throw new System.ArgumentException(e.Message);
            }            
        }

        /// <summary>
        /// Return the count of total number of entries in the Ct SDS Information object
        /// </summary>
        /// <returns>count of total number of entries</returns> 
        public int GetNumberOfEntries()
        {
            return numberOfEntries;
        }

    }
}
