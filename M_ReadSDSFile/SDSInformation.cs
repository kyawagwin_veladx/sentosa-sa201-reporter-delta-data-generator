﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;
using System.Collections;//TODO: comment this as this will not be required after logging functionality is enabled
//using M_Log;//TODO: uncomment this to enable logging functionality
using M_Language;
using M_SampleList;
using M_ReportResult;
using M_CommonMethods;

namespace M_ReadSDSFile
{

    /// <summary>
    /// This class is used for parsing SDS File and storing the information in appropriate class objects
    /// </summary>
    public class SDSInformation
    {
        private string SDSFileName = string.Empty;
        internal ComponentSDSInformation componentSDSInformation = null;
        internal DeltaRnSDSInformation deltaRnSDSInformation = null;
        internal CtSDSInformation ctSDSInformation = null;
        internal ResultSDSInformation resultSDSInformation = null;

        /// <summary>
        /// Parse SDS File and Fill SDSInformation object
        /// </summary>
        /// <param name="stringSDSFileName">SDS file path</param>
        /// <returns>true/false</returns>        
        public bool ParseSDSFile(string stringSDSFileName, SMPObject smpObject = null)
        {
            try
            {
                if (stringSDSFileName == null)
                {
                    SysMessage.errorMsg = "SDS file name is empty.";
                    Console.WriteLine("SDS file name is empty.");//replace by log
                    return false;
                }

                if (!File.Exists(stringSDSFileName))
                {
                    SysMessage.errorMsg = "SDS File " + stringSDSFileName + " does not exist!";
                    Console.WriteLine("SDS File " + stringSDSFileName + " does not exist!");//replace by log
                    return false;
                }

                SDSFileName = stringSDSFileName;

                // Check if the Acquisition is complete
                if (!IsAcquisitionComplete(SDSFileName))
                {
                    SysMessage.errorMsg = "SDS File is not completed.";
                    return false;
                }


                //Check if Analysis Result present 
                if (!IsAnalysisResultPresent(SDSFileName))
                {
                    SysMessage.errorMsg = "Analysis status does not exist in SDS File.";
                    return false;
                }


                // Fill the Component information from SDS File
                bool IsFound = FindComplexComponentRmcL(SDSFileName, "RmcL");
                if (!IsFound)
                    return false;

                // Fill the DeltaRN information from SDS File
                IsFound = FindComplexComponentRapL(SDSFileName, "RapL");
                if (!IsFound)
                    return false;

                // Fill the Results information from SDS File
                IsFound = FindARrtComponent(SDSFileName, "ARrt");
                if (!IsFound)
                    return false;

                // Fill the Ct information from SDS File
                IsFound = FindAAdsComponent(SDSFileName, "AAds");
                if (!IsFound)
                    return false;

                IsFound = FindRW32Component(SDSFileName, "RW32");
                if (!IsFound)
                    return false;

                // Fill the Ct information from SDS File
                IsFound = FillRunInfo(SDSFileName);
                if (!IsFound)
                    return false;

                // Fill the Thermal Cycler Profile information from SDS File
                FillThermalCyclerProfile(SDSFileName);
                if (!IsFound)
                    return false;

                if (smpObject != null)
                {
                    bool IsValidSampleList = CheckIfSamplesMatch(smpObject);
                    if (!IsValidSampleList)
                    {
                        SysMessage.errorMsg = "Sample Result does not match with the sample from input entry ID XML.";
                    }
                    return IsValidSampleList;
                }
            }
            catch (Exception e)
            {
                //Log.WriteLog(LogType.system, "Error while parsing SDS file: " + e.ToString());
                Console.WriteLine("Error while parsing SDS file: " + e.ToString());
                return false;
            }

            return true;
        }

        private bool CheckIfSamplesMatch(SMPObject smpObject)
        {
            return IsSamplePositionMatching(smpObject.PageList[0].SampleList, resultSDSInformation.sampleResultList);
        }


        private bool IsSamplePositionMatching(List<SMPSample> origList, List<ResultSDSInformation.SampleResult> listFromFile)
        {
            bool isMatching = false;
            for (int cnter = 0; cnter < origList.Count; cnter++)
            {
                var sampleResultPerPosition = listFromFile.Where(x => InfoConvert.GetNumericPosition(x.wellName) == Convert.ToInt32(origList[cnter].TubePosition)).ToList();

                if (sampleResultPerPosition != null && sampleResultPerPosition.Count > 0)
                {
                    var sampleNamePerPosition = sampleResultPerPosition.Where(x => x.sampleName == origList[cnter].SampleName).ToList();
                    if (sampleNamePerPosition != null && sampleNamePerPosition.Count > 0)
                    {
                        continue;
                    }
                    else
                    {
                        return isMatching;
                    }
                }
                else
                {
                    return isMatching;
                }
            }
            return true;
        }
        /// <summary>
        /// Find String in byte array
        /// </summary>
        /// <param name="Source">Source byte array</param>
        /// <param name="stringToBeSearched">String to be searched</param>
        /// <param name="sourceLength">Lenght of byte array</param>
        /// <param name="searchLength">Length of String to be searched</param>
        /// <returns>location in byte array where string is found / -1 if string not found</returns>
        private static int FindString(byte[] Source, byte[] stringToBeSearched, int sourceLength, int searchLength)
        {
            bool IsFound = false;
            int j = 0;

            for (int i = 0; i < sourceLength; i++)
            {
                if (Source[i] == stringToBeSearched[j])
                {
                    int m = j;
                    for (int k = i; m < searchLength; k++)
                    {
                        if (Source[k] == stringToBeSearched[m])
                        {
                            m++;
                            if (m == searchLength)
                            {
                                IsFound = true;
                                return k - searchLength + 1;
                            }
                        }
                        else
                            break;
                    }
                }
            }

            if (!IsFound)
                return -1;
            else
                return 1;
        }

        private static int FindNextARstString(byte[] Source, int sourceLength, int startSearchIndex)
        {
            bool IsFound = false;
            int j = 0;
            int searchLength = 4;
            byte[] stringToBeSearched = System.Text.Encoding.ASCII.GetBytes("ARst");


            for (int i = startSearchIndex; i < sourceLength - 3; i++)
            {
                if (Source[i] == stringToBeSearched[j])
                {
                    int m = j;
                    for (int k = i; m < searchLength; k++)
                    {
                        if (Source[k] == stringToBeSearched[m])
                        {
                            m++;
                            if (m == searchLength)
                            {
                                IsFound = true;
                                return k - searchLength + 1;
                            }
                        }
                        else
                            break;
                    }
                }
            }

            if (!IsFound)
                return -1;
            else
                return 1;
        }

        /// <summary>
        /// Find String in byte array
        /// </summary>
        /// <param name="Source">Source byte array</param>
        /// <param name="stringToBeSearched">String to be searched</param>
        /// <param name="sourceLength">Lenght of byte array</param>
        /// <param name="searchLength">Length of String to be searched</param>
        /// <returns>location in byte array where string is found / -1 if string not found</returns>
        private static int FindStringDesc(byte[] Source, byte[] stringToBeSearched, int sourceLength, int searchLength)
        {
            bool IsFound = false;
            int j = 0;


            for (int i = sourceLength - 1; i >= searchLength-1; i--)
            {
                if (Source[i] == stringToBeSearched[searchLength-1])
                {
                    int m = searchLength-1;
                    for (int k = i; m >=0 ; k--)
                    {
                        if (Source[k] == stringToBeSearched[m])
                        {
                            m--;
                            if (m < 0)
                            {
                                IsFound = true;
                                return k;
                            }
                        }
                        else
                            break;
                    }
                }
            }

            if (!IsFound)
                return -1;
            else
                return 1;
        }

        /// <summary>
        /// This function is a general helper function for finding a component having text value in a buffer
        /// </summary>
        /// <param name="buffer">SDS file read in a buffer</param>
        /// <param name="length">Length of the buffer</param>
        /// <param name="componentName">Name of the Component to be searched in the buffer by descending order</param>
        /// <returns>Value of the Component found in the buffer / null if the Component is not found</param>
        string FindTextComponent(byte[] buffer, int length, string componentName)
        {
            byte[] stringTagAndSize = new byte[43];
            byte[] stringTag = new byte[4];
            byte[] stringSize = new byte[4];

            int foundLocation = FindStringDesc(buffer, System.Text.Encoding.ASCII.GetBytes(componentName), length, componentName.Length);
            if (foundLocation != -1)
            {
                Buffer.BlockCopy(buffer, foundLocation, stringTagAndSize, 0, 8);
                Buffer.BlockCopy(stringTagAndSize, 0, stringTag, 0, 4);
                string strTag = System.Text.Encoding.UTF8.GetString(stringTag);
                Buffer.BlockCopy(stringTagAndSize, 4, stringSize, 0, 4);

                Array.Reverse(stringSize);
                int size = BitConverter.ToInt32(stringSize, 0);

                byte[] componentValue = new byte[size];
                Buffer.BlockCopy(buffer, foundLocation + 8, componentValue, 0, size);
                string strComponentValue = System.Text.Encoding.UTF8.GetString(componentValue);

                return strComponentValue;
            }

            return null;
        }

        /// <summary>
        /// This function is a general helper function for finding a component having integer value in a buffer
        /// </summary>
        /// <param name="buffer">SDS file read in a buffer</param>
        /// <param name="length">Length of the buffer</param>
        /// <param name="componentName">Name of the Component to be searched in the buffer</param>
        /// <param name="IsBigEndianInteger">true value of this argument means big endian integer or false otherwise</param>
        /// <param name="componentIntegerValue">This argument is filled with the value of the component</param>
        /// <returns>true if value of the Component is found in the buffer / false if the Component is not found</param>
        bool FindIntegerComponent(byte[] buffer, int length, string componentName, bool IsBigEndianInteger, ref int componentIntegerValue)
        {
            byte[] stringTagAndSize = new byte[43];
            byte[] stringTag = new byte[4];
            byte[] stringSize = new byte[4];

            int foundLocation = FindString(buffer, System.Text.Encoding.ASCII.GetBytes(componentName), length, componentName.Length);
            if (foundLocation != -1)
            {
                Buffer.BlockCopy(buffer, foundLocation, stringTagAndSize, 0, 8);
                Buffer.BlockCopy(stringTagAndSize, 0, stringTag, 0, 4);
                string strTag = System.Text.Encoding.UTF8.GetString(stringTag);
                Buffer.BlockCopy(stringTagAndSize, 4, stringSize, 0, 4);

                Array.Reverse(stringSize);
                int size = BitConverter.ToInt32(stringSize, 0);

                byte[] componentValue = new byte[size];
                Buffer.BlockCopy(buffer, foundLocation + 8, componentValue, 0, size);

                if (IsBigEndianInteger)
                    Array.Reverse(componentValue);

                componentIntegerValue = BitConverter.ToInt16(componentValue, 0);

                return true;
            }

            return false;
        }

        /// <summary>
        /// Parse the SDS file to read Run related Information
        /// </summary>
        /// <param name="SDSFileName">Name of the SDS file</param>
        /// <returns>true/false</returns>
        private bool FillRunInfo(string SDSFileName)
        {
            BinaryReader reader = null;
            try
            {
                reader = new BinaryReader(File.Open(SDSFileName, FileMode.Open, FileAccess.Read, FileShare.Delete));
                int length = (int)reader.BaseStream.Length;

                byte[] buffer = reader.ReadBytes(length);

                // Parse the SDS file to read Operator information
                string operatorInformation = FindTextComponent(buffer, length, "OPTR");
                if (operatorInformation == null)
                    return false;

                // Store the Operator information read from SDS file
                resultSDSInformation.SetOperatorInformation(operatorInformation);

                // Parse the SDS file to read Comments information
                string commentsInformation = FindTextComponent(buffer, length, "COMM");
                if (commentsInformation == null)
                    return false;

                // Store the Comments information read from SDS file
                resultSDSInformation.SetCommentsInformation(commentsInformation);

                // Parse the SDS file to read SDS File Name information
                string sdsFileNameInformation = FindTextComponent(buffer, length, "AuFN");
                if (sdsFileNameInformation == null)
                    return false;

                // Store the SDS File Name information read from SDS file
                resultSDSInformation.SetSDSFileNameInformation(sdsFileNameInformation);

                // Parse the SDS file to read PCR Volume information
                int PCRVolume = 0;
                bool IsPCRVolumeFound = FindIntegerComponent(buffer, length, "PCRV", true, ref PCRVolume);
                if (!IsPCRVolumeFound)
                    return false;

                // Store the PCR Volume read from SDS file
                resultSDSInformation.SetPCRVolume(PCRVolume);

                // Parse the SDS file Header to read Instrument Type Mode information
                try
                {
                    byte[] instrumentType = new byte[4];

                    reader.BaseStream.Position = 0;
                    Buffer.BlockCopy(buffer, 60, instrumentType, 0, 4);
                    string strInstrumentTypeMode = System.Text.Encoding.UTF8.GetString(instrumentType);

                    if (strInstrumentTypeMode.ToUpper() == "BETG")
                        resultSDSInformation.SetInstrumentTypeMode("7500");
                    else if (strInstrumentTypeMode.ToUpper() == "HS75")
                        resultSDSInformation.SetInstrumentTypeMode("Fast 7500 mode");
                    else
                        resultSDSInformation.SetInstrumentTypeMode(strInstrumentTypeMode);
                }
                catch (Exception e)
                {
                    //Log.WriteLog(LogType.system, "Error while parsing SDS file: " + e.ToString());
                    Console.WriteLine("Error while parsing SDS file: " + e.ToString());
                    return false;
                }

                return true;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
        }

        /// <summary>
        /// Parse the SDS file to read Thermal Cycler Profile information
        /// </summary>
        /// <param name="stringSDSFileName">Name of the SDS file</param>
        /// <returns>true/false</returns>
        private bool FillThermalCyclerProfile(string stringSDSFileName)
        {
            BinaryReader reader = null;
            try
            {
                // Read SDS file into buffer
                reader = new BinaryReader(File.Open(stringSDSFileName, FileMode.Open, FileAccess.Read, FileShare.Delete));
                int length = (int)reader.BaseStream.Length;

                int foundLocation = 0;
                byte[] buffer = reader.ReadBytes(length);

                // Search for "TPro" component in SDS file
                foundLocation = FindStringDesc(buffer, System.Text.Encoding.ASCII.GetBytes("TPro"), length, 4);
                int parsePointer = foundLocation;

                if (parsePointer != -1)
                {
                    int recordType = 2;
                    int stageCount = 0;

                    int nCurrentBufferLocation = foundLocation + 57;//start of the Thermal Cycler Profile information records is from 57th location from start of TPro Component
                    for (int g = 0; ; g++)
                    {
                        // Declare and initialise temporary variables
                        byte[] bytesRepetitions = new byte[2];
                        byte[] bytesTime = new byte[2];
                        byte[] bytesTemperature = new byte[4];
                        byte[] bytesRecordType = new byte[2];

                        int Stage = 0;
                        int Repetitions = 0;
                        int Time = 0;
                        int Temperature = 0;

                        if (recordType == 2)
                        {
                            // Stage information is not read from SDS file. It is maintained by tracking 'stageCount' local variable
                            stageCount++;
                            Stage = stageCount;

                            // Read Repetition information for Thermal Cycler Profile record
                            Buffer.BlockCopy(buffer, nCurrentBufferLocation, bytesRepetitions, 0, 2);
                            Array.Reverse(bytesRepetitions);
                            Repetitions = BitConverter.ToInt16(bytesRepetitions, 0);

                            // Read Time information for Thermal Cycler Profile record
                            Buffer.BlockCopy(buffer, nCurrentBufferLocation + 11, bytesTime, 0, 2);
                            Time = BitConverter.ToInt16(bytesTime, 0);

                            // Read Temperature information for Thermal Cycler Profile record
                            Buffer.BlockCopy(buffer, nCurrentBufferLocation + 13, bytesTemperature, 0, 4);
                            Temperature = BitConverter.ToInt32(bytesTemperature, 0);

                            // Read information about type of record
                            Buffer.BlockCopy(buffer, nCurrentBufferLocation + 19, bytesRecordType, 0, 2);
                            recordType = BitConverter.ToInt16(bytesRecordType, 0);

                            nCurrentBufferLocation = nCurrentBufferLocation + 20;

                            // Store the Thermal Cycle Profile record information read from SDS file
                            resultSDSInformation.InsertRecordInThermalCycleProfileInformation(Stage, Repetitions, Time, Temperature);
                        }
                        else if (recordType == 3)
                        {
                            // Zero value in Repetition and Stage denotes blank
                            Repetitions = 0;
                            Stage = 0;

                            // Read Time information for Thermal Cycler Profile record
                            Buffer.BlockCopy(buffer, nCurrentBufferLocation + 1, bytesTime, 0, 2);
                            Time = BitConverter.ToInt16(bytesTime, 0);

                            // Read Temperature information for Thermal Cycler Profile record
                            Buffer.BlockCopy(buffer, nCurrentBufferLocation + 3, bytesTemperature, 0, 4);
                            Temperature = BitConverter.ToInt32(bytesTemperature, 0);

                            // Read information about type of record
                            Buffer.BlockCopy(buffer, nCurrentBufferLocation + 9, bytesRecordType, 0, 2);
                            recordType = BitConverter.ToInt16(bytesRecordType, 0);

                            nCurrentBufferLocation = nCurrentBufferLocation + 10;

                            // Store the Thermal Cycle Profile record information read from SDS file
                            resultSDSInformation.InsertRecordInThermalCycleProfileInformation(Stage, Repetitions, Time, Temperature);
                        }
                        else
                            break;
                    }

                    return true;
                }
                else
                    return false;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
        }

        /// <summary>
        /// Parse the SDS file to read AAds component and fill the Ct information
        /// </summary>
        /// <param name="stringSDSFileName">Name of the SDS file</param>
        /// <param name="componentName">Name of the component ('AAds') to be parsed from SDS file</param>
        /// <returns>true/false</returns>
        private bool FindAAdsComponent(string stringSDSFileName, string componentName)
        {
            BinaryReader reader = null;
            try
            {
                reader = new BinaryReader(File.Open(stringSDSFileName, FileMode.Open, FileAccess.Read, FileShare.Delete));
                int length = (int)reader.BaseStream.Length;
                int length2 = length;

                int foundLocation = 0;
                int foundLocation2 = 0;
                int parseLength = 0;
                int parsePointer = -1;
                int parsePointer2 = -1;
                byte[] bufferCopy = reader.ReadBytes(length);

                do
                {
                    parsePointer2 = 0;
                    foundLocation2 = FindStringDesc(bufferCopy, System.Text.Encoding.ASCII.GetBytes("AAss"), length2, componentName.Length);
                    parsePointer2 = foundLocation2;
                    if (parsePointer2 == -1)
                        return false;

                    byte[] strSize = new byte[4];

                    reader.BaseStream.Position = 0;
                    Buffer.BlockCopy(bufferCopy, foundLocation2 + 4, strSize, 0, 4);
                    Array.Reverse(strSize);
                    int sizeOfAAssComponent = BitConverter.ToInt32(strSize, 0);

                    byte[] buffer = reader.ReadBytes(sizeOfAAssComponent + 8);
                    Buffer.BlockCopy(bufferCopy, foundLocation2, buffer, 0, sizeOfAAssComponent + 8);
                    length = sizeOfAAssComponent + 8;

                    foundLocation = FindString(buffer, System.Text.Encoding.ASCII.GetBytes(componentName), sizeOfAAssComponent + 8, componentName.Length);
                    parsePointer = foundLocation;

                    if (parsePointer != -1)
                    {
                        ctSDSInformation = new CtSDSInformation();

                        // Loop to read all AAds components
                        while (parsePointer != -1)
                        {
                            byte[] stringTagAndSize = new byte[43];
                            byte[] stringTag = new byte[4];
                            byte[] stringSize = new byte[4];
                            byte[] stringNumberOfSubcomponents = new byte[2];
                            byte[] stringNumberOfReadings = new byte[4];

                            reader.BaseStream.Position = 0;
                            Buffer.BlockCopy(buffer, foundLocation, stringTagAndSize, 0, 8);
                            Buffer.BlockCopy(stringTagAndSize, 0, stringTag, 0, 4);
                            string strTag = System.Text.Encoding.UTF8.GetString(stringTag);
                            Buffer.BlockCopy(stringTagAndSize, 4, stringSize, 0, 4);

                            Array.Reverse(stringSize);
                            int size = BitConverter.ToInt32(stringSize, 0);

                            byte[] stringContents = new byte[2048];
                            byte[] stringNameSize = new byte[2];
                            byte[] stringDetectorName = new byte[100];
                            byte[] stringBaselineStart = new byte[2];
                            byte[] stringBaselineEnd = new byte[2];

                            Buffer.BlockCopy(buffer, foundLocation + 8, stringNameSize, 0, 2);
                            int nameSize = BitConverter.ToInt16(stringNameSize, 0);

                            Buffer.BlockCopy(buffer, foundLocation + 8 + 2, stringDetectorName, 0, nameSize);
                            string strDetectorName = System.Text.Encoding.UTF8.GetString(stringDetectorName);
                            strDetectorName = strDetectorName.Replace("\0", string.Empty);

                            Buffer.BlockCopy(buffer, foundLocation + 8 + nameSize + 6, stringBaselineStart, 0, 2);
                            int baselineStart = BitConverter.ToInt16(stringBaselineStart, 0);

                            Buffer.BlockCopy(buffer, foundLocation + 8 + nameSize + 8, stringBaselineEnd, 0, 2);
                            int baselineEnd = BitConverter.ToInt16(stringBaselineEnd, 0);

                            float threshold = BitConverter.ToSingle(buffer, foundLocation + 8 + 10 + nameSize);

                            // Store the information read from SDS file
                            ctSDSInformation.InsertRecord(strDetectorName, baselineStart, baselineEnd, threshold);

                            parseLength = length - foundLocation - size - 8;

                            if (parseLength > 3)
                            {
                                byte[] arrayCopy = new byte[parseLength];
                                Buffer.BlockCopy(buffer, foundLocation + size + 8, arrayCopy, 0, parseLength);

                                parsePointer = FindString(arrayCopy, System.Text.Encoding.ASCII.GetBytes(componentName), parseLength, 4);

                                if (parsePointer != -1)
                                    foundLocation = foundLocation + 8 + size + parsePointer;
                                else
                                    break;
                            }
                            else
                                break;
                        };
                        return true; //end from loop
                    }
                    Buffer.BlockCopy(bufferCopy, foundLocation2 + 8 + sizeOfAAssComponent, bufferCopy, 0, length2 - foundLocation2 - sizeOfAAssComponent - 8);
                    length2 = length2 - 8 - sizeOfAAssComponent - foundLocation2;
                } while (true);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
        }

        /// <summary>
        /// Parse the SDS file to read ARrt component and fill the Results information
        /// </summary>
        /// <param name="stringSDSFileName">Name of the SDS file</param>
        /// <param name="componentName">Name of the component ('ARrt') to be parsed from SDS file</param>
        /// <returns>true/false</returns>
        private bool FindARrtComponent(string stringSDSFileName, string componentName)
        {
            BinaryReader reader = null;
            try
            {
                reader = new BinaryReader(File.Open(stringSDSFileName, FileMode.Open, FileAccess.Read, FileShare.Delete));
                int length = (int)reader.BaseStream.Length;

                int foundLocation = 0;
                byte[] buffer2 = reader.ReadBytes(length);

                // Find ARss component first
                foundLocation = FindStringDesc(buffer2, System.Text.Encoding.ASCII.GetBytes("ARss"), length, 4);
                if (foundLocation == -1)
                    return false;

                length = length - foundLocation;
                byte[] buffer = new byte[length];
                Buffer.BlockCopy(buffer2, foundLocation, buffer, 0, length);

                // Start finding actual ARrt component
                foundLocation = FindString(buffer, System.Text.Encoding.ASCII.GetBytes(componentName), length, componentName.Length);

                int foundAtLocation = foundLocation;
                int currentBufferLocation = foundLocation;

                if (currentBufferLocation == -1)
                    return false;

                int kk = 0;

                resultSDSInformation = new ResultSDSInformation();

                // Loop to read all ARrt components
                while ((currentBufferLocation < length) && (foundAtLocation != -1))
                {
                    byte[] stringTagAndSize = new byte[43];
                    byte[] stringTag = new byte[4];
                    byte[] stringSize = new byte[4];
                    byte[] stringContents = new byte[2048];
                    byte[] stringNumberOfSubcomponents = new byte[2];
                    byte[] stringNumberOfReadings = new byte[4];

                    reader.BaseStream.Position = 0;
                    Buffer.BlockCopy(buffer, currentBufferLocation, stringTagAndSize, 0, 8);
                    Buffer.BlockCopy(stringTagAndSize, 0, stringTag, 0, 4);
                    string strTag = System.Text.Encoding.UTF8.GetString(stringTag);
                    Buffer.BlockCopy(stringTagAndSize, 4, stringSize, 0, 4);

                    Array.Reverse(stringSize);
                    int size = BitConverter.ToInt32(stringSize, 0);

                    int wellIdentifier = 0;
                    int wellNameSize = 0;
                    int detectorNameSize = 0;
                    int sampleNameSize = 0;
                    int taskNameSize = 0;

                    byte[] stringWellIdentifier = new byte[2];
                    byte[] stringPlateNameSize = new byte[2];
                    byte[] stringPlateName = new byte[100];
                    byte[] stringWellNameSize = new byte[2];
                    byte[] stringWellName = new byte[100];
                    byte[] stringDetectorNameSize = new byte[2];
                    byte[] stringDetectorName = new byte[100];
                    byte[] stringSampleNameSize = new byte[2];
                    byte[] stringSampleName = new byte[100];
                    byte[] stringTaskNameSize = new byte[2];
                    byte[] stringTaskName = new byte[100];

                    Buffer.BlockCopy(buffer, currentBufferLocation + 8, stringWellIdentifier, 0, 2);
                    wellIdentifier = BitConverter.ToInt16(stringWellIdentifier, 0);

                    Buffer.BlockCopy(buffer, currentBufferLocation + 14, stringWellNameSize, 0, 2);
                    wellNameSize = BitConverter.ToInt16(stringWellNameSize, 0);
                    Buffer.BlockCopy(buffer, currentBufferLocation + 16, stringWellName, 0, wellNameSize);
                    string strWellName = System.Text.Encoding.UTF8.GetString(stringWellName);
                    strWellName = strWellName.Replace("\0", string.Empty);

                    Buffer.BlockCopy(buffer, currentBufferLocation + 16 + wellNameSize, stringDetectorNameSize, 0, 2);
                    detectorNameSize = BitConverter.ToInt16(stringDetectorNameSize, 0);
                    Buffer.BlockCopy(buffer, currentBufferLocation + 18 + wellNameSize, stringDetectorName, 0, detectorNameSize);
                    string strDetectorName = System.Text.Encoding.UTF8.GetString(stringDetectorName);
                    strDetectorName = strDetectorName.Replace("\0", string.Empty);
                    Buffer.BlockCopy(buffer, currentBufferLocation + 18 + wellNameSize + detectorNameSize, stringSampleNameSize, 0, 2);
                    sampleNameSize = BitConverter.ToInt16(stringSampleNameSize, 0);
                    Buffer.BlockCopy(buffer, currentBufferLocation + 20 + wellNameSize + detectorNameSize, stringSampleName, 0, sampleNameSize);
                    string strSampleName = System.Text.Encoding.UTF8.GetString(stringSampleName);
                    strSampleName = strSampleName.Replace("\0", string.Empty);

                    Buffer.BlockCopy(buffer, currentBufferLocation + 20 + wellNameSize + detectorNameSize + sampleNameSize, stringTaskNameSize, 0, 2);
                    taskNameSize = BitConverter.ToInt16(stringTaskNameSize, 0);
                    Buffer.BlockCopy(buffer, currentBufferLocation + 22 + wellNameSize + detectorNameSize + sampleNameSize, stringSampleName, 0, taskNameSize);
                    string strTaskName = System.Text.Encoding.UTF8.GetString(stringTaskName);
                    strTaskName = strTaskName.Replace("\0", string.Empty);

                    float cycleOfTransitionThreshold = BitConverter.ToSingle(buffer, currentBufferLocation + 32 + wellNameSize + detectorNameSize + sampleNameSize + taskNameSize);
                    float cycleOfTransition = BitConverter.ToSingle(buffer, currentBufferLocation + 36 + wellNameSize + detectorNameSize + sampleNameSize + taskNameSize);

                    double cycleOfTransitionMean = BitConverter.ToDouble(buffer, currentBufferLocation + 40 + wellNameSize + detectorNameSize + sampleNameSize + taskNameSize);
                    double cycleOfTransitionStandardDeviation = BitConverter.ToDouble(buffer, currentBufferLocation + 48 + wellNameSize + detectorNameSize + sampleNameSize + taskNameSize);

                    // Store the information read from SDS file
                    resultSDSInformation.InsertRecordInSampleResults(strWellName, strSampleName, strDetectorName, strTaskName, cycleOfTransition, cycleOfTransitionStandardDeviation);

                    int parseLength = length - currentBufferLocation - size - 8;
                    if (parseLength > 3)
                    {
                        byte[] arrayCopy = new byte[parseLength];
                        Buffer.BlockCopy(buffer, currentBufferLocation + size + 8, arrayCopy, 0, parseLength);

                        foundAtLocation = FindString(arrayCopy, System.Text.Encoding.ASCII.GetBytes(componentName), parseLength, 4);

                        if (foundAtLocation != -1)
                        {
                            currentBufferLocation = currentBufferLocation + 8 + size + foundAtLocation;
                            kk++;
                        }
                        else
                            break;
                    }
                    else
                        break;
                };

                return true;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
        }

        /// <summary>
        /// Parse the SDS file to read ARst component and check analyst performed or not
        /// </summary>
        /// <param name="stringSDSFileName">Name of the SDS file</param>
        /// <param name="componentName">Name of the component ('ARrt') to be parsed from SDS file</param>
        /// <returns>true/false</returns>
        static public bool IsAnalysisResultPresent(string stringSDSFileName)
        {
            BinaryReader reader = null;
            try
            {
                reader = new BinaryReader(File.Open(stringSDSFileName, FileMode.Open, FileAccess.Read, FileShare.Delete));
                int length = (int)reader.BaseStream.Length;

                int foundLocation = 0;
                byte[] buffer = reader.ReadBytes(length);
                int startIndex =0;
                // Start finding actual ARrt component
               

                do {
                    foundLocation = FindNextARstString(buffer, length, startIndex);
                    if (foundLocation == -1)
                    {
                        return false;
                    }
                    //ARst Component need to offset 8  
                    else if (foundLocation + 8 > length)
                    {
                        return false;
                    }
                    else if (Convert.ToDecimal(buffer[foundLocation + 8]) != 3)
                    {
                        startIndex = foundLocation + 8;
                    }
                    else
                    {
                        if (Convert.ToDecimal(buffer[foundLocation + 8]) == 3)
                            return true;
                        else
                            return false;
                    }
                }
                while (true);

        
            }
            catch (Exception e)
            {
                //Log.WriteLog(LogType.system, "Error while parsing SDS file: " + e.ToString());
                Console.WriteLine("Error while parsing SDS file: " + e.ToString());
                return false;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
                
            }


        }

        private bool FindComplexComponentRapD(byte[] buffer, int foundLocation, int numberOfSubcomponents, int numberOfReadings)
        {
            byte[] stringTagAndSize = new byte[8];
            byte[] stringTag = new byte[4];
            byte[] stringSize = new byte[4];
            int currentBufferLocation = foundLocation;
            byte[] stringDetectorName = new byte[20];
            byte[] stringSignals = new byte[4];
            float[,] signals = new float[numberOfSubcomponents, numberOfReadings];
            int i = 0;

            // Loop to read all subcomponents
            do
            {
                byte[] stringPlateNameSize = new byte[2];
                byte[] stringWellIdentifier = new byte[2];
                byte[] stringDetectorNameSize = new byte[2];
                int wellIdentifier = 0;
                int detectorNameSize = 0;

                Buffer.BlockCopy(buffer, foundLocation, stringTagAndSize, 0, 8);
                Buffer.BlockCopy(stringTagAndSize, 0, stringTag, 0, 4);
                string strTag = System.Text.Encoding.UTF8.GetString(stringTag);
                Buffer.BlockCopy(stringTagAndSize, 4, stringSize, 0, 4);

                Buffer.BlockCopy(buffer, currentBufferLocation + 8, stringPlateNameSize, 0, 2);
                Buffer.BlockCopy(buffer, currentBufferLocation + 10, stringWellIdentifier, 0, 2);
                Buffer.BlockCopy(buffer, currentBufferLocation + 12, stringDetectorNameSize, 0, 2);

                wellIdentifier = BitConverter.ToInt16(stringWellIdentifier, 0);
                detectorNameSize = BitConverter.ToInt16(stringDetectorNameSize, 0);

                Buffer.BlockCopy(buffer, currentBufferLocation + 14, stringDetectorName, 0, detectorNameSize);
                string strDetectorName = System.Text.Encoding.UTF8.GetString(stringDetectorName, 0, detectorNameSize);
                strDetectorName = strDetectorName.Replace("\0", string.Empty);

                // Store record information
                deltaRnSDSInformation.InsertRecord(i, wellIdentifier + 1, strDetectorName, componentSDSInformation.stringDyeName[i]);

                for (int j = 0; j < numberOfReadings; j++)
                {
                    signals[i, j] = BitConverter.ToSingle(buffer, currentBufferLocation + 16 + (numberOfReadings * 4) + detectorNameSize + (j * 4));

                    // Store signal information in record
                    deltaRnSDSInformation.InsertSignalInRecord(i, j, BitConverter.ToSingle(buffer, currentBufferLocation + 16 + (numberOfReadings * 4) + detectorNameSize + (j * 4)));
                }

                currentBufferLocation = currentBufferLocation + 16 + (numberOfReadings * 4) + detectorNameSize + (4 * numberOfReadings);
                i++;

            } while (i < numberOfSubcomponents);

            return true;
        }

        /// <summary>
        /// Parse the SDS file to read RapL component and fill the DeltaRN information
        /// </summary>
        /// <param name="stringSDSFileName">Name of the SDS file</param>
        /// <param name="componentName">Name of the component ('RapL') to be parsed from SDS file</param>
        /// <returns>true/false</returns>
        private bool FindComplexComponentRapL(string stringSDSFileName, string componentName)
        {
            BinaryReader reader = null;
            try
            {
                reader = new BinaryReader(File.Open(stringSDSFileName, FileMode.Open, FileAccess.Read, FileShare.Delete));
                int length = (int)reader.BaseStream.Length;

                int foundLocation = 0;

                byte[] buffer = reader.ReadBytes(length);

                foundLocation = FindStringDesc(buffer, System.Text.Encoding.ASCII.GetBytes(componentName), length, componentName.Length);
                int parsePointer = foundLocation;

            AGAIN2:
                if (parsePointer != -1)
                {
                    byte[] stringTagAndSize = new byte[43];
                    byte[] stringTag = new byte[4];
                    byte[] stringSize = new byte[4];
                    byte[] stringContents = new byte[2048];
                    byte[] stringNumberOfSubcomponents = new byte[2];
                    byte[] stringNumberOfReadings = new byte[4];

                    reader.BaseStream.Position = 0;
                    Buffer.BlockCopy(buffer, foundLocation, stringTagAndSize, 0, 8);
                    Buffer.BlockCopy(stringTagAndSize, 0, stringTag, 0, 4);
                    string strTag = System.Text.Encoding.UTF8.GetString(stringTag);
                    Buffer.BlockCopy(stringTagAndSize, 4, stringSize, 0, 4);
                    Buffer.BlockCopy(buffer, foundLocation + 8, stringNumberOfSubcomponents, 0, 2);
                    Buffer.BlockCopy(buffer, foundLocation + 10, stringNumberOfReadings, 0, 4);

                    Array.Reverse(stringSize);
                    int size = BitConverter.ToInt32(stringSize, 0);

                    Array.Reverse(stringNumberOfSubcomponents);
                    int numberOfSubcomponents = BitConverter.ToInt16(stringNumberOfSubcomponents, 0);

                    int numberOfReadings = BitConverter.ToInt32(stringNumberOfReadings, 0);
                    if (numberOfReadings > 0)
                    {
                        // Store the information read from SDS file
                        deltaRnSDSInformation = new DeltaRnSDSInformation();
                        deltaRnSDSInformation.SetNumberOfSubcomponents(numberOfSubcomponents);
                        deltaRnSDSInformation.SetNumberOfReadings(numberOfReadings);

                        FindComplexComponentRapD(buffer, foundLocation + 14, numberOfSubcomponents, numberOfReadings);
                    }

                    int parseLength = length - foundLocation - size - 8;
                    if (parseLength > 3)
                    {
                        byte[] arrayCopy = new byte[parseLength];
                        Buffer.BlockCopy(buffer, foundLocation + size + 8, arrayCopy, 0, parseLength);

                        parsePointer = FindString(arrayCopy, System.Text.Encoding.ASCII.GetBytes(componentName), parseLength, 4);

                        if (parsePointer != -1)
                        {
                            foundLocation = foundLocation + 8 + size + parsePointer;
                            goto AGAIN2;
                        }
                        else if (deltaRnSDSInformation.GetNumberOfReadings() > 0 && deltaRnSDSInformation.GetNumberOfSubcomponents() > 0)
                            return true;
                        else
                            goto RETURN2; ;
                    }
                    else if (deltaRnSDSInformation.GetNumberOfReadings() > 0 && deltaRnSDSInformation.GetNumberOfSubcomponents() > 0)
                        return true;
                    else
                        goto RETURN2;
                }

            RETURN2:
                return false;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
        }

        private bool FindComplexComponentRmcD(byte[] buffer, int foundLocation, int numberOfSubcomponents, int numberOfReadings)
        {
            byte[] stringTagAndSize = new byte[8];
            byte[] stringTag = new byte[4];
            byte[] stringSize = new byte[4];
            int currentBufferLocation = foundLocation;
            byte[] stringDyeName = new byte[20];
            byte[] stringSignals = new byte[4];
            float[,] signals = new float[numberOfSubcomponents, numberOfReadings];
            int i = 0;

            // Loop to read all subcomponents
            do
            {
                byte[] stringWellIdentifier = new byte[2];
                byte[] sDyeNameSize = new byte[2];
                int wellIdentifier = 0;
                int nDyeNameSize = 0;

                Buffer.BlockCopy(buffer, foundLocation, stringTagAndSize, 0, 8);
                Buffer.BlockCopy(stringTagAndSize, 0, stringTag, 0, 4);
                string strTag = System.Text.Encoding.UTF8.GetString(stringTag);
                Buffer.BlockCopy(stringTagAndSize, 4, stringSize, 0, 4);

                Buffer.BlockCopy(buffer, currentBufferLocation + 8, stringWellIdentifier, 0, 2);
                Buffer.BlockCopy(buffer, currentBufferLocation + 10, sDyeNameSize, 0, 2);

                wellIdentifier = BitConverter.ToInt16(stringWellIdentifier, 0);
                nDyeNameSize = BitConverter.ToInt16(sDyeNameSize, 0);

                Buffer.BlockCopy(buffer, currentBufferLocation + 12, stringDyeName, 0, nDyeNameSize);
                string strDyeName = System.Text.Encoding.UTF8.GetString(stringDyeName, 0, nDyeNameSize);
                strDyeName = strDyeName.Replace("\0", string.Empty);

                // Store record information
                componentSDSInformation.InsertRecord(i, wellIdentifier + 1, strDyeName);

                for (int j = 0; j < numberOfReadings; j++)
                {
                    signals[i, j] = BitConverter.ToSingle(buffer, currentBufferLocation + 12 + nDyeNameSize + (j * 4));
                    double value = BitConverter.ToDouble(buffer, currentBufferLocation + 12 + nDyeNameSize + (j * 4));

                    // Store signal information in record
                    componentSDSInformation.InsertSignalInRecord(i, j, BitConverter.ToSingle(buffer, currentBufferLocation + 12 + nDyeNameSize + (j * 4)));
                }

                currentBufferLocation = currentBufferLocation + 12 + nDyeNameSize + (4 * numberOfReadings);
                i++;

            } while (i < numberOfSubcomponents);

            return true;
        }

        /// <summary>
        /// Parse the SDS file to read RmcL component and fill the Component information
        /// </summary>
        /// <param name="stringSDSFileName">Name of the SDS file</param>
        /// <param name="componentName">Name of the component ('RmcL') to be parsed from SDS file</param>
        /// <returns>true/false</returns>
        private bool FindComplexComponentRmcL(string stringSDSFileName, string componentName)
        {
            BinaryReader reader = null;
            try
            {
                reader = new BinaryReader(File.Open(stringSDSFileName, FileMode.Open, FileAccess.Read, FileShare.Delete));
                int length = (int)reader.BaseStream.Length;

                int foundLocation = 0;

                byte[] buffer = reader.ReadBytes(length);

                foundLocation = FindStringDesc(buffer, System.Text.Encoding.ASCII.GetBytes(componentName), length, componentName.Length);
                int parsePointer = foundLocation;

            AGAIN1:
                if (parsePointer != -1)
                {
                    byte[] stringTagAndSize = new byte[43];
                    byte[] stringTag = new byte[4];
                    byte[] stringSize = new byte[4];
                    byte[] stringContents = new byte[2048];
                    byte[] stringNumberOfSubcomponents = new byte[2];
                    byte[] stringNumberOfReadings = new byte[4];

                    reader.BaseStream.Position = 0;
                    Buffer.BlockCopy(buffer, foundLocation, stringTagAndSize, 0, 8);
                    Buffer.BlockCopy(stringTagAndSize, 0, stringTag, 0, 4);
                    string strTag = System.Text.Encoding.UTF8.GetString(stringTag);
                    Buffer.BlockCopy(stringTagAndSize, 4, stringSize, 0, 4);
                    Buffer.BlockCopy(buffer, foundLocation + 8, stringNumberOfSubcomponents, 0, 2);
                    Buffer.BlockCopy(buffer, foundLocation + 10, stringNumberOfReadings, 0, 4);

                    Array.Reverse(stringSize);
                    int size = BitConverter.ToInt32(stringSize, 0);

                    Array.Reverse(stringNumberOfSubcomponents);
                    int numberOfSubcomponents = BitConverter.ToInt16(stringNumberOfSubcomponents, 0);

                    int numberOfReadings = BitConverter.ToInt32(stringNumberOfReadings, 0);
                    if (numberOfReadings > 0)
                    {
                        // Store the information read from SDS file
                        componentSDSInformation = new ComponentSDSInformation();
                        componentSDSInformation.SetNumberOfSubcomponents(numberOfSubcomponents);
                        componentSDSInformation.SetNumberOfReadings(numberOfReadings);

                        FindComplexComponentRmcD(buffer, foundLocation + 14, numberOfSubcomponents, numberOfReadings);
                    }

                    int parseLength = length - foundLocation - size - 8;
                    if (parseLength > 3)
                    {
                        byte[] arrayCopy = new byte[parseLength];
                        Buffer.BlockCopy(buffer, foundLocation + size + 8, arrayCopy, 0, parseLength);

                        parsePointer = FindString(arrayCopy, System.Text.Encoding.ASCII.GetBytes(componentName), parseLength, 4);
                        if (parsePointer != -1)
                        {
                            foundLocation = foundLocation + 8 + size + parsePointer;
                            goto AGAIN1;
                        }
                        else if (componentSDSInformation.GetNumberOfReadings() > 0 && componentSDSInformation.GetNumberOfSubcomponents() > 0)
                            return true;
                        else
                            goto RETURN1;
                    }
                    else if (componentSDSInformation.GetNumberOfReadings() > 0 && componentSDSInformation.GetNumberOfSubcomponents() > 0)
                        return true;
                    else
                        goto RETURN1;

                }

            RETURN1:
                return false;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
        }

        /// <summary>
        /// Parse the SDS file to read Run start date from RW32 component and fill the Result information
        /// </summary>
        /// <param name="stringSDSFileName">Name of the SDS file</param>
        /// <param name="componentName">Name of the component ('RW32') to be parsed from SDS file</param>
        /// <returns>true/false</returns>
        private bool FindRW32Component(string stringSDSFileName, string componentName)
        {
            BinaryReader reader = null;
            try
            {
                reader = new BinaryReader(File.Open(stringSDSFileName, FileMode.Open, FileAccess.Read, FileShare.Delete));
                int length = (int)reader.BaseStream.Length;

                int foundLocation = 0;

                byte[] buffer = reader.ReadBytes(length);

                foundLocation = FindStringDesc(buffer, System.Text.Encoding.ASCII.GetBytes(componentName), length, componentName.Length);
                int nCurrentBufferLocation = foundLocation;

                if (nCurrentBufferLocation != -1)
                {
                    byte[] stringTagAndSize = new byte[43];
                    byte[] stringTag = new byte[4];
                    byte[] stringSize = new byte[4];
                    byte[] stringContents = new byte[2048];
                    byte[] stringNumberOfSubcomponents = new byte[2];
                    byte[] stringNumberOfReadings = new byte[4];

                    reader.BaseStream.Position = 0;
                    Buffer.BlockCopy(buffer, foundLocation, stringTagAndSize, 0, 8);
                    Buffer.BlockCopy(stringTagAndSize, 0, stringTag, 0, 4);
                    string strTag = System.Text.Encoding.UTF8.GetString(stringTag);
                    Buffer.BlockCopy(stringTagAndSize, 4, stringSize, 0, 4);
                    Buffer.BlockCopy(buffer, foundLocation + 8, stringNumberOfSubcomponents, 0, 2);
                    Buffer.BlockCopy(buffer, foundLocation + 10, stringNumberOfReadings, 0, 4);

                    Array.Reverse(stringSize);
                    int size = BitConverter.ToInt32(stringSize, 0);

                    byte[] sAcquisitionStartYear = new byte[1];
                    byte[] sAcquisitionStartMonth = new byte[1];
                    byte[] sAcquisitionStartDate = new byte[1];
                    byte[] sAcquisitionStartHour = new byte[1];
                    byte[] sAcquisitionStartMinute = new byte[1];
                    byte[] sAcquisitionStartSecond = new byte[1];
                    byte[] sAcquisitionEndYear = new byte[1];
                    byte[] sAcquisitionEndMonth = new byte[1];
                    byte[] sAcquisitionEndDate = new byte[1];
                    byte[] sAcquisitionEndHour = new byte[1];
                    byte[] sAcquisitionEndMinute = new byte[1];
                    byte[] sAcquisitionEndSecond = new byte[1];

                    Buffer.BlockCopy(buffer, nCurrentBufferLocation + 12, sAcquisitionStartYear, 0, 1);
                    int nAcquisitionStartYear = 2000 + sAcquisitionStartYear[0];

                    Buffer.BlockCopy(buffer, nCurrentBufferLocation + 13, sAcquisitionStartMonth, 0, 1);
                    int nAcquisitionStartMonth = sAcquisitionStartMonth[0];

                    Buffer.BlockCopy(buffer, nCurrentBufferLocation + 14, sAcquisitionStartDate, 0, 1);
                    int nAcquisitionStartDate = sAcquisitionStartDate[0];

                    Buffer.BlockCopy(buffer, nCurrentBufferLocation + 15, sAcquisitionStartHour, 0, 1);
                    int nAcquisitionStartHour = sAcquisitionStartHour[0];

                    Buffer.BlockCopy(buffer, nCurrentBufferLocation + 16, sAcquisitionStartMinute, 0, 1);
                    int nAcquisitionStartMinute = sAcquisitionStartMinute[0];

                    Buffer.BlockCopy(buffer, nCurrentBufferLocation + 17, sAcquisitionStartSecond, 0, 1);
                    int nAcquisitionStartSecond = sAcquisitionStartSecond[0];

                    string runDate = nAcquisitionStartDate + "-" + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(nAcquisitionStartMonth) + "-" + nAcquisitionStartYear + " " + nAcquisitionStartHour + ":" + nAcquisitionStartMinute + ":" + nAcquisitionStartSecond;

                    // Store the run start date information read from SDS file
                    resultSDSInformation.SetRunDate(runDate);

                    DateTime lastModifiedTime = File.GetLastWriteTime(stringSDSFileName);
                    string lastModifiedDateTime = lastModifiedTime.Day + "-" + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(lastModifiedTime.Month) + "-" + lastModifiedTime.Year + " " + lastModifiedTime.Hour + ":" + lastModifiedTime.Minute + ":" + lastModifiedTime.Second;

                    // Store the Last Modified date time information read from SDS file
                    resultSDSInformation.SetLastModifiedDateTime(lastModifiedDateTime);

                    return true;
                }
                return false;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
        }

        /// <summary>
        /// Parse the SDS file to check if the Acquisition is complete
        /// </summary>
        /// <param name="stringSDSFileName">Name of the SDS file</param>
        /// <returns>true/false</returns>
        static public bool IsAcquisitionComplete(string stringSDSFileName)
        {
            Stream str = null;
            BinaryReader reader = null;
            if (stringSDSFileName == null)
            {
                Console.WriteLine("SDS file name is empty.");//replace by log
                return false;
            }

            if (!File.Exists(stringSDSFileName))
            {
                Console.WriteLine("SDS File " + stringSDSFileName + " does not exist!");//replace by log
                return false;
            }

            try
            {
                using (str = File.Open(stringSDSFileName, FileMode.Open, FileAccess.Read, FileShare.Delete))
                //FileInfo fi = new FileInfo(stringSDSFileName);
                //using (Stream str = fi.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    reader = new BinaryReader(str);

                    byte[] buffer = reader.ReadBytes(1024);
                    byte[] acquisitionStatus = new byte[4];

                    reader.BaseStream.Position = 0;
                    Buffer.BlockCopy(buffer, 64, acquisitionStatus, 0, 4);
                    string strAcquisitionStatus = System.Text.Encoding.UTF8.GetString(acquisitionStatus);
                    // return true if the Acquisition Status information in SDS file is "DONE"
                    if (strAcquisitionStatus.ToUpper() == "DONE")

                        return true;

                }
            }
            catch (Exception e)
            {
                //Log.WriteLog(LogType.system, "Error while parsing SDS file: " + e.ToString());
                Console.WriteLine("Error while parsing SDS file: " + e.ToString());
                return false;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (str != null)
                {
                    str.Close();
                }
            }

            return false;
        }

        /// <summary>
        /// Parse the SDS file to check if the Acquisition is complete
        /// </summary>
        /// <param name="stringSDSFileName">Name of the SDS file</param>
        /// <returns>true/false</returns>
        static public string GetAcquisitionStatus(string stringSDSFileName)
        {
            if (stringSDSFileName == null)
            {
                Console.WriteLine("SDS file name is empty.");//replace by log
                return string.Empty;
            }

            if (!File.Exists(stringSDSFileName))
            {
                Console.WriteLine("SDS File " + stringSDSFileName + " does not exist!");//replace by log
                return string.Empty;
            }
            Stream str = null;
            try
            {
                using (str = File.Open(stringSDSFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    BinaryReader reader = new BinaryReader(str);

                    byte[] buffer = reader.ReadBytes(1024);
                    byte[] acquisitionStatus = new byte[4];

                    reader.BaseStream.Position = 0;
                    Buffer.BlockCopy(buffer, 64, acquisitionStatus, 0, 4);
                    string strAcquisitionStatus = System.Text.Encoding.UTF8.GetString(acquisitionStatus);

                    // return true if the Acquisition Status 
                    return strAcquisitionStatus.ToUpper();
                }
            }
            catch (Exception e)
            {
                //Log.WriteLog(LogType.system, "Error while parsing SDS file: " + e.ToString());
                Console.WriteLine("Error while parsing SDS file: " + e.ToString());
                return string.Empty;
            }
            finally
            {
                if (str != null)
                    str.Close();
            }


        }


        /// <summary>
        /// Return object of ComponentSDSInformation class
        /// </summary>
        /// <returns>object of ComponentSDSInformation class</returns>
        public ComponentSDSInformation GetComponentSDSInformation()
        {
            return componentSDSInformation;
        }

        /// <summary>
        /// Return object of DeltaRnSDSInformation class
        /// </summary>
        /// <returns>object of DeltaRnSDSInformation class</returns>
        public DeltaRnSDSInformation GetDeltaRnSDSInformation()
        {
            return deltaRnSDSInformation;
        }

        /// <summary>
        /// Return object of CtSDSInformation class
        /// </summary>
        /// <returns>object of CtSDSInformation class</returns>
        public CtSDSInformation GetCtSDSInformation()
        {
            return ctSDSInformation;
        }

        /// <summary>
        /// Return object of ResultSDSInformation class
        /// </summary>
        /// <returns>object of ResultSDSInformation class</returns>
        public ResultSDSInformation GetResultSDSInformation()
        {
            return resultSDSInformation;
        }

        /// <summary>
        /// Parse the SDS file and return SDSInformation object containing parsed SDS information
        /// </summary>
        /// <param name="stringSDSFileName">Name of the SDS file</param>
        /// <returns>SDSInformation object containing parsed SDS information / null if some error has occured during parsing or Acqusition status is not 'DONE'</param>
        static public SDSInformation GetSDSobject(string stringSDSFileName, SMPObject smpObject = null)
        {
            string tempSDSFileName = string.Empty;
            try
            {
                if (stringSDSFileName == null)
                {
                    Console.WriteLine("SDS file name is empty.");
                    return null;
                }
                System.Threading.Thread.Sleep(100);
                try
                {
                    string FolderName = System.IO.Path.GetDirectoryName(stringSDSFileName);
                    tempSDSFileName = FolderName + "\\" + System.IO.Path.GetFileName(stringSDSFileName).Replace(System.IO.Path.GetExtension(stringSDSFileName), "") + "_temp.sds";
                    File.Copy(stringSDSFileName, tempSDSFileName, true);
                }
                catch (Exception ex)
                {
                }
                SDSInformation objSDSInformation = new SDSInformation();
                if (!objSDSInformation.ParseSDSFile(tempSDSFileName, smpObject))
                {
                    DeleteFile(tempSDSFileName);
                    return null;
                }
                if (File.Exists(tempSDSFileName))
                {
                    DeleteFile(tempSDSFileName);
                }
                //ComponentSDSInformation componentSDSInformation = objSDSInformation.GetComponentSDSInformation();//commented as not required this test code
                return objSDSInformation;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        static void DeleteFile(string tempSDSFileName)
        {
            if (!string.IsNullOrEmpty(tempSDSFileName) && File.Exists(tempSDSFileName))
            {
                try
                {
                    System.Threading.Thread.Sleep(100);
                    File.Delete(tempSDSFileName);
                }
                catch (Exception ex)
                { }
            }
        }


    }
}
