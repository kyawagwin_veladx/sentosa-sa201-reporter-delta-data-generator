﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UI_Help
{
    public class HelpController
    {
        public Help help;

        public HelpController()
        {
            help = new Help();
            help.HelpFilePath = AppDomain.CurrentDomain.BaseDirectory + "SentosaReporterHelp.pdf";

        }

        public void Display()
        {
            help.Display();
        }


    }
}
