﻿
using M_Language;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UI_Help
{
    public class Help
    {
        public string HelpFilePath { get; set; }

        internal void Display()
        {
            try
            {

                Process.Start(HelpFilePath);
            }
            catch
            {
                MessageBox.Show(SysMessage.fileMissMsg + HelpFilePath
                    , SysMessage.errorCaption
                    , MessageBoxButtons.OK
                    , MessageBoxIcon.Error);
            }
        }
    }
}
