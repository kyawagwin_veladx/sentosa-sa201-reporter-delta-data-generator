﻿using ComponentFactory.Krypton.Toolkit;

namespace UI_ImportFiles
{
    partial class Form_ImportFiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_ImportFiles));
            this.btnOK = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnCancel = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.lblComponent = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.lblDeltarn = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.lblResult = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.pbComponent = new System.Windows.Forms.ProgressBar();
            this.pbDeltarn = new System.Windows.Forms.ProgressBar();
            this.pbResult = new System.Windows.Forms.ProgressBar();
            this.lblPrompt = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.txtFolder = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.btnOpen = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.txtResult = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txtDeltaRn = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txtComponent = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txtCt = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.pbCt = new System.Windows.Forms.ProgressBar();
            this.lblCt = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.txtAssayFile = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.pbAssayFile = new System.Windows.Forms.ProgressBar();
            this.lblAssayFile = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.btnShowBaseSoftware = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(121, 550);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(84, 28);
            this.btnOK.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.btnOK.TabIndex = 8;
            this.btnOK.Values.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(219, 550);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(82, 28);
            this.btnCancel.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Values.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblComponent
            // 
            this.lblComponent.Location = new System.Drawing.Point(10, 5);
            this.lblComponent.Name = "lblComponent";
            this.lblComponent.Size = new System.Drawing.Size(130, 20);
            this.lblComponent.TabIndex = 10;
            this.lblComponent.Values.Text = "Component File (.csv):";
            // 
            // lblDeltarn
            // 
            this.lblDeltarn.Location = new System.Drawing.Point(10, 116);
            this.lblDeltarn.Name = "lblDeltarn";
            this.lblDeltarn.Size = new System.Drawing.Size(112, 20);
            this.lblDeltarn.TabIndex = 11;
            this.lblDeltarn.Values.Text = "Delta Rn File (.csv):";
            // 
            // lblResult
            // 
            this.lblResult.Location = new System.Drawing.Point(10, 335);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(99, 20);
            this.lblResult.TabIndex = 12;
            this.lblResult.Values.Text = "Result File (.csv):";
            // 
            // pbComponent
            // 
            this.pbComponent.Location = new System.Drawing.Point(12, 67);
            this.pbComponent.Name = "pbComponent";
            this.pbComponent.Size = new System.Drawing.Size(426, 23);
            this.pbComponent.TabIndex = 16;
            // 
            // pbDeltarn
            // 
            this.pbDeltarn.Location = new System.Drawing.Point(12, 178);
            this.pbDeltarn.Name = "pbDeltarn";
            this.pbDeltarn.Size = new System.Drawing.Size(426, 23);
            this.pbDeltarn.TabIndex = 17;
            // 
            // pbResult
            // 
            this.pbResult.Location = new System.Drawing.Point(12, 399);
            this.pbResult.Name = "pbResult";
            this.pbResult.Size = new System.Drawing.Size(426, 23);
            this.pbResult.TabIndex = 18;
            // 
            // lblPrompt
            // 
            this.lblPrompt.Location = new System.Drawing.Point(10, 2);
            this.lblPrompt.Name = "lblPrompt";
            this.lblPrompt.Size = new System.Drawing.Size(344, 20);
            this.lblPrompt.TabIndex = 19;
            this.lblPrompt.Values.Text = "Please save the required files on the below folder to proceed.";
            this.lblPrompt.Visible = false;
            // 
            // txtFolder
            // 
            this.txtFolder.Enabled = false;
            this.txtFolder.Location = new System.Drawing.Point(12, 28);
            this.txtFolder.Multiline = true;
            this.txtFolder.Name = "txtFolder";
            this.txtFolder.Size = new System.Drawing.Size(426, 45);
            this.txtFolder.StateCommon.Back.Color1 = System.Drawing.SystemColors.Control;
            this.txtFolder.StateCommon.Border.Color1 = System.Drawing.SystemColors.Control;
            this.txtFolder.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txtFolder.StateCommon.Content.Color1 = System.Drawing.SystemColors.ActiveCaption;
            this.txtFolder.StateDisabled.Content.Color1 = System.Drawing.SystemColors.ControlText;
            this.txtFolder.TabIndex = 20;
            this.txtFolder.Visible = false;
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(15, 550);
            this.btnOpen.Margin = new System.Windows.Forms.Padding(4);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(94, 28);
            this.btnOpen.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.btnOpen.TabIndex = 21;
            this.btnOpen.Values.Text = "Open Folder";
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // txtResult
            // 
            this.txtResult.Enabled = false;
            this.txtResult.Location = new System.Drawing.Point(12, 361);
            this.txtResult.Multiline = true;
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(426, 25);
            this.txtResult.StateCommon.Back.Color1 = System.Drawing.SystemColors.Control;
            this.txtResult.StateCommon.Border.Color1 = System.Drawing.SystemColors.Control;
            this.txtResult.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txtResult.StateCommon.Content.Color1 = System.Drawing.SystemColors.ActiveCaption;
            this.txtResult.StateDisabled.Content.Color1 = System.Drawing.SystemColors.ControlText;
            this.txtResult.TabIndex = 22;
            // 
            // txtDeltaRn
            // 
            this.txtDeltaRn.Enabled = false;
            this.txtDeltaRn.Location = new System.Drawing.Point(12, 142);
            this.txtDeltaRn.Multiline = true;
            this.txtDeltaRn.Name = "txtDeltaRn";
            this.txtDeltaRn.Size = new System.Drawing.Size(426, 25);
            this.txtDeltaRn.StateCommon.Back.Color1 = System.Drawing.SystemColors.Control;
            this.txtDeltaRn.StateCommon.Border.Color1 = System.Drawing.SystemColors.Control;
            this.txtDeltaRn.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txtDeltaRn.StateCommon.Content.Color1 = System.Drawing.SystemColors.ActiveCaption;
            this.txtDeltaRn.StateDisabled.Content.Color1 = System.Drawing.SystemColors.ControlText;
            this.txtDeltaRn.TabIndex = 23;
            // 
            // txtComponent
            // 
            this.txtComponent.Enabled = false;
            this.txtComponent.Location = new System.Drawing.Point(12, 31);
            this.txtComponent.Multiline = true;
            this.txtComponent.Name = "txtComponent";
            this.txtComponent.Size = new System.Drawing.Size(426, 25);
            this.txtComponent.StateCommon.Back.Color1 = System.Drawing.SystemColors.Control;
            this.txtComponent.StateCommon.Border.Color1 = System.Drawing.SystemColors.Control;
            this.txtComponent.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txtComponent.StateCommon.Content.Color1 = System.Drawing.SystemColors.ActiveCaption;
            this.txtComponent.StateDisabled.Content.Color1 = System.Drawing.SystemColors.ControlText;
            this.txtComponent.TabIndex = 24;
            // 
            // txtCt
            // 
            this.txtCt.Enabled = false;
            this.txtCt.Location = new System.Drawing.Point(12, 254);
            this.txtCt.Multiline = true;
            this.txtCt.Name = "txtCt";
            this.txtCt.Size = new System.Drawing.Size(426, 25);
            this.txtCt.StateCommon.Back.Color1 = System.Drawing.SystemColors.Control;
            this.txtCt.StateCommon.Border.Color1 = System.Drawing.SystemColors.Control;
            this.txtCt.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txtCt.StateCommon.Content.Color1 = System.Drawing.SystemColors.ActiveCaption;
            this.txtCt.StateDisabled.Content.Color1 = System.Drawing.SystemColors.ControlText;
            this.txtCt.TabIndex = 27;
            // 
            // pbCt
            // 
            this.pbCt.Location = new System.Drawing.Point(12, 292);
            this.pbCt.Name = "pbCt";
            this.pbCt.Size = new System.Drawing.Size(426, 23);
            this.pbCt.TabIndex = 26;
            // 
            // lblCt
            // 
            this.lblCt.Location = new System.Drawing.Point(10, 228);
            this.lblCt.Name = "lblCt";
            this.lblCt.Size = new System.Drawing.Size(77, 20);
            this.lblCt.TabIndex = 25;
            this.lblCt.Values.Text = "Ct File (.csv):";
            // 
            // txtAssayFile
            // 
            this.txtAssayFile.Enabled = false;
            this.txtAssayFile.Location = new System.Drawing.Point(12, 461);
            this.txtAssayFile.Multiline = true;
            this.txtAssayFile.Name = "txtAssayFile";
            this.txtAssayFile.Size = new System.Drawing.Size(426, 25);
            this.txtAssayFile.StateCommon.Back.Color1 = System.Drawing.SystemColors.Control;
            this.txtAssayFile.StateCommon.Border.Color1 = System.Drawing.SystemColors.Control;
            this.txtAssayFile.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txtAssayFile.StateCommon.Content.Color1 = System.Drawing.SystemColors.ActiveCaption;
            this.txtAssayFile.StateDisabled.Content.Color1 = System.Drawing.SystemColors.ControlText;
            this.txtAssayFile.TabIndex = 30;
            // 
            // pbAssayFile
            // 
            this.pbAssayFile.Location = new System.Drawing.Point(12, 499);
            this.pbAssayFile.Name = "pbAssayFile";
            this.pbAssayFile.Size = new System.Drawing.Size(426, 23);
            this.pbAssayFile.TabIndex = 29;
            // 
            // lblAssayFile
            // 
            this.lblAssayFile.Location = new System.Drawing.Point(10, 435);
            this.lblAssayFile.Name = "lblAssayFile";
            this.lblAssayFile.Size = new System.Drawing.Size(88, 20);
            this.lblAssayFile.TabIndex = 28;
            this.lblAssayFile.Values.Text = "Run File (.sdx):";
            // 
            // btnShowBaseSoftware
            // 
            this.btnShowBaseSoftware.Location = new System.Drawing.Point(320, 550);
            this.btnShowBaseSoftware.Margin = new System.Windows.Forms.Padding(4);
            this.btnShowBaseSoftware.Name = "btnShowBaseSoftware";
            this.btnShowBaseSoftware.Size = new System.Drawing.Size(129, 28);
            this.btnShowBaseSoftware.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.btnShowBaseSoftware.TabIndex = 31;
            this.btnShowBaseSoftware.Values.Text = "Show Base Software";
            this.btnShowBaseSoftware.Click += new System.EventHandler(this.btnShowBaseSoftware_Click);
            // 
            // Form_ImportFiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 602);
            this.ControlBox = false;
            this.Controls.Add(this.btnShowBaseSoftware);
            this.Controls.Add(this.txtAssayFile);
            this.Controls.Add(this.pbAssayFile);
            this.Controls.Add(this.lblAssayFile);
            this.Controls.Add(this.txtCt);
            this.Controls.Add(this.pbCt);
            this.Controls.Add(this.lblCt);
            this.Controls.Add(this.txtComponent);
            this.Controls.Add(this.txtDeltaRn);
            this.Controls.Add(this.txtResult);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.txtFolder);
            this.Controls.Add(this.lblPrompt);
            this.Controls.Add(this.pbResult);
            this.Controls.Add(this.pbDeltarn);
            this.Controls.Add(this.pbComponent);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.lblDeltarn);
            this.Controls.Add(this.lblComponent);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_ImportFiles";
            this.Text = "Import Files";
            this.Load += new System.EventHandler(this.Form_ImportRunFiles_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private KryptonButton btnOK;
        private KryptonButton btnCancel;
        private KryptonLabel lblComponent;
        private KryptonLabel lblDeltarn;
        private KryptonLabel lblResult;
        private System.Windows.Forms.ProgressBar pbComponent;
        private System.Windows.Forms.ProgressBar pbDeltarn;
        private System.Windows.Forms.ProgressBar pbResult;
        private KryptonLabel lblPrompt;
        private KryptonTextBox txtFolder;
        private KryptonButton btnOpen;
        private KryptonTextBox txtResult;
        private KryptonTextBox txtDeltaRn;
        private KryptonTextBox txtComponent;
        private KryptonTextBox txtCt;
        private System.Windows.Forms.ProgressBar pbCt;
        private KryptonLabel lblCt;
        private KryptonTextBox txtAssayFile;
        private System.Windows.Forms.ProgressBar pbAssayFile;
        private KryptonLabel lblAssayFile;
        private KryptonButton btnShowBaseSoftware;

    }
}