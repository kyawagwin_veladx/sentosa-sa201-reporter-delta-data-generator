﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 17/06/2014
 * Author: Liu Rui
 * 
 * Description:
 */
#endregion

using M_Assay;
using M_CheckSum;
using M_CommonMethods;
using M_System;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using M_SA201Result;
using M_ReportResult;
using System.Collections;
using M_AssayFile;
using M_CheckExit;
using M_Language;
using M_SampleList;

namespace UI_ImportFiles
{
    public class ImportFiles
    {
        private SystemConfig systemConfig;
        private AssayInfo assayInfo;
        private ReportResult reportResult;
        private bool isSDXFound = false;
        private ReadRunFiles readRunFiles;

        public AssayInfo AssayInfo
        {
            get { return assayInfo; }
        }


        /*
        public SystemConfig SystemConfig
        {
            get { return systemConfig; }
        }

        public AssayInfo AssayInfo
        {
            get { return assayInfo; }
        }

        public ReportResult ReportResult
        {
            get { return reportResult; }
        }
        */
        private void InitializeFlags(bool flag)
        {
            isSDXFound = flag;
        }

        public void UpdateReportResult(string runfilepath, SMPObject smpObject = null)
        {
            //set the software version in the system config file
            reportResult.RunInfo.SetVersion(systemConfig.Version);

            readRunFiles = new ReadRunFiles(runfilepath, reportResult, assayInfo,smpObject);

            readRunFiles.FillReportResult();

        }

        public void FillAssayInfoSDS()
        {
            List<string> m1ItemList = AssayFile.GetM1ItemList(assayInfo.RunSDX); //such as 201161

            string sAssayConfigFile = assayInfo.GetAssayConfigFile(m1ItemList, systemConfig.Folder_AssayConfig);
            if (!string.IsNullOrEmpty(sAssayConfigFile))
            {
                assayInfo.FillAssayInfo(sAssayConfigFile);
                
            }
            else
            {
                SysMessage.errorMsg = SysMessage.noAssayForM1Msg;
                throw new Exception(SysMessage.errorMsg);
            }
            

            //List<string> selectedSamples = readRunFiles.GetSelectedSamples(assayInfo.SampleSizeList);

            assayInfo.UpdateSelectedSizeIndex(reportResult.SampleResultList.Select(x=>x.Position.ToString()).ToList());

            if (assayInfo.SelectedSizeIndex < 0)
            {
                assayInfo.SelectedSizeIndex = 0;
                //SysMessage.errorMsg = "Test size is not supported.";
                //throw new Exception(SysMessage.errorMsg);
            }

            assayInfo.RefillSampleStatus(reportResult.SampleResultList);
            assayInfo.UpdateChannelVirusName(reportResult.SampleResultList);
        }

        internal string GetRunFolder()
        {
            return assayInfo.AnalysisFolder;
        }

        public void SetRunFolder(string assayFolder)
        {
            assayInfo.SetRunFolder(assayFolder);
        }

        public void SetValues(SystemConfig systemConfig, AssayInfo assayInfo, ReportResult reportResult)
        {
            this.systemConfig = systemConfig;
            this.assayInfo = assayInfo;
            this.reportResult = reportResult;
        }

        public void UpdateFlags(string runfilepath)
        {
            InitializeFlags(false);
            UpdateSDXFlag(runfilepath);
        }

        public bool IsAssayXMLFound()
        {
            return isSDXFound;
        }

        private void UpdateSDXFlag(string runfilepath)
        {
            string directoryname = Path.GetDirectoryName(runfilepath);

            string[] sdxFileList = Directory.GetFiles(directoryname, "*.sdx");

            foreach (string file in sdxFileList)
            {
                if(!Path.GetFileName(file).StartsWith("._"))
                {
                    XmlDocument xml = FileCheckSum.Load(file);

                    if (xml.DocumentElement != null)
                    {
                        if (FileOperations.IsAssayXML(file))
                        {
                            isSDXFound = true;
                            assayInfo.RunSDX = file;
                            continue;
                        }
                    }
                }
            }
        }

       
       

      
       

       
    }
}
