﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 17/06/2014
 * Author: Liu Rui
 * 
 * Description:
 */
#endregion

using ComponentFactory.Krypton.Toolkit;
using M_Assay;
using M_Language;
using M_Log;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using M_RunFileCheck;
using System.Collections;
using M_AssayFile;

namespace UI_ImportFiles
{
    public partial class Form_ImportFiles : KryptonForm
    {
        private delegate void dEnableComponentProgressBar(int text);
        private delegate void dFillComponentText(string text);

        private delegate void dEnableDeltaRnProgressBar(int text);
        private delegate void dFillDeltaRnText(string text);

        private delegate void dEnableCtProgressBar(int text);
        private delegate void dFillCtText(string text);

        private delegate void dEnableResultProgressBar(int text);
        private delegate void dFillResultText(string text);

        private delegate void dEnableAssayXMLProgressBar(int text);
        private delegate void dFillAssayXMLText(string text);

        private delegate void dEnableOKButton(bool text);

        private ImportRunFilesController importRunFilesController;

        public Form_ImportFiles(ImportRunFilesController importRunFilesController)
        {
            InitializeComponent();
            this.importRunFilesController = importRunFilesController;
        }

        private void Form_ImportRunFiles_Load(object sender, EventArgs e)
        {
            DefineProgressBars();

            DisplayRunFolder();

            EnableOKButton(false);

            importRunFilesController.MonitorRunFiles();

            importRunFilesController.CheckRunFiles();
            
        }

        private void DisplayRunFolder()
        {
            txtFolder.Text = importRunFilesController.importFiles.GetRunFolder();
        }

        internal void EnableOKButton(bool flag)
        {
            if (this.InvokeRequired)
            {
                dEnableOKButton d = new dEnableOKButton(EnableOKButton);
                this.Invoke(d, new object[] { flag });
            }
            else
            {
                btnOK.Enabled = flag;
            }
        }

        private void DefineProgressBars()
        {
            pbComponent.Minimum = 0;
            pbComponent.Maximum = 100;
            pbDeltarn.Minimum = 0;
            pbDeltarn.Maximum = 100;
            pbCt.Minimum = 0;
            pbCt.Maximum = 100;
            pbResult.Minimum = 0;
            pbResult.Maximum = 100;
            pbAssayFile.Minimum = 0;
            pbAssayFile.Maximum = 100;
        }

        internal void EnableComponentProgressBar(int value)
        {
            if (this.InvokeRequired)
            {
                dEnableComponentProgressBar d = new dEnableComponentProgressBar(EnableComponentProgressBar);
                this.Invoke(d, new object[] { value });
            }
            else
            {
                pbComponent.Value = value;
            }
        }

        private void FillComponentText(string value)
        {
            if (this.InvokeRequired)
            {
                dFillComponentText d = new dFillComponentText(FillComponentText);
                this.Invoke(d, new object[] { value });
            }
            else
            {
                txtComponent.Text = value;
            }
        }

        internal void EnableDeltaRnProgressBar(int value)
        {
            if (this.InvokeRequired)
            {
                dEnableDeltaRnProgressBar d = new dEnableDeltaRnProgressBar(EnableDeltaRnProgressBar);
                this.Invoke(d, new object[] {value });
            }
            else
            {
                pbDeltarn.Value = value;
            }
        }

        private void FillDeltaRnText(string value)
        {
            if (this.InvokeRequired)
            {
                dFillDeltaRnText d = new dFillDeltaRnText(FillDeltaRnText);
                this.Invoke(d, new object[] { value });
            }
            else
            {
                txtDeltaRn.Text = value;
            }
        }

        private void EnableCtProgressBar(int value)
        {
            if (this.InvokeRequired)
            {
                dEnableCtProgressBar d = new dEnableCtProgressBar(EnableCtProgressBar);
                this.Invoke(d, new object[] { value });
            }
            else
            {
                pbCt.Value = value;
            }
        }

        private void FillCtText(string value)
        {
            if (this.InvokeRequired)
            {
                dFillCtText d = new dFillCtText(FillCtText);
                this.Invoke(d, new object[] { value });
            }
            else
            {
                txtCt.Text = value;
            }
        }

        internal void EnableResultProgressBar(int value)
        {
            if (this.InvokeRequired)
            {
                dEnableResultProgressBar d = new dEnableResultProgressBar(EnableResultProgressBar);
                this.Invoke(d, new object[] { value });
            }
            else
            {
                pbResult.Value = value;
            }
        }

        private void FillResultText(string value)
        {
            if (this.InvokeRequired)
            {
                dFillResultText d = new dFillResultText(FillResultText);
                this.Invoke(d, new object[] { value });
            }
            else
            {
                txtResult.Text = value;
            }
        }

        private void FillAssayXMLText(string value)
        {
            if (this.InvokeRequired)
            {
                dFillAssayXMLText d = new dFillAssayXMLText(FillAssayXMLText);
                this.Invoke(d, new object[] { value });
            }
            else
            {
                txtAssayFile.Text = value;
            }
        }

        private void EnableAssayXMLProgressBar(int value)
        {
            if (this.InvokeRequired)
            {
                dEnableAssayXMLProgressBar d = new dEnableAssayXMLProgressBar(EnableAssayXMLProgressBar);
                this.Invoke(d, new object[] { value });
            }
            else
            {
                pbAssayFile.Value = value;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (IsChecksumPassed())
            {
                this.ChangeCursor(Cursors.WaitCursor);

                importRunFilesController.FillReportResult();

                importRunFilesController.FillAssayInfo();

                RunFileCheck check = new RunFileCheck();

                //get config file
                ArrayList m1ItemList = AssayFile.GetM1ItemList(importRunFilesController.importFiles.AssayInfo.RunSDX);
                if (importRunFilesController.importFiles.AssayInfo.GetAssayConfigFile(m1ItemList, importRunFilesController.importFiles.SystemConfig.Folder_AssayConfig) == string.Empty)
                {
                    KryptonMessageBox.Show(SysMessage.noAssayTempMsg, SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Log.WriteLog(LogType.system, "Import Files - Cannot find the respective assay config file. Please install assay packages");
                }
                //Check channel match
                else if (!check.FuncRunFileCheck(importRunFilesController.importFiles.AssayInfo, importRunFilesController.importFiles.ReportResult))
                {
                    this.ChangeCursor(Cursors.Arrow);
                    KryptonMessageBox.Show(SysMessage.invalidRunFileMsg, SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Log.WriteLog(LogType.system, "Import Files - Run files (CSV files) are not valid.");
                }
                //check sample size
                else if (importRunFilesController.importFiles.AssayInfo.SelectedSizeIndex < 0)
                {
                    KryptonMessageBox.Show(SysMessage.invalidSampleSize, SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Log.WriteLog(LogType.system, "Import Files - Run files (CSV files) sample size is not valid.");
                }
                //check barcode
                else if (!importRunFilesController.IsAllBarcodePassed())
                {
                    KryptonMessageBox.Show(SysMessage.invalidBarcode, SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Log.WriteLog(LogType.system, "Import Files - Run files (CSV files) sample size is not valid.");
                }
                else
                {
                    Log.WriteLog(LogType.system, "Import Files - All Run Files are available. Close Import Files Dialog");

                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
        }

        private bool IsChecksumPassed()
        {
            int count = 0;

            if (importRunFilesController.importFiles.IsChecksumPassed(importRunFilesController.importFiles.AssayInfo.ComponentCSV))
            {
                count++;
            }
            else
            {
                KryptonMessageBox.Show(SysMessage.fileModified+Environment.NewLine+importRunFilesController.importFiles.AssayInfo.ComponentCSV,SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.WriteLog(LogType.system, "File checksum error for " + importRunFilesController.importFiles.AssayInfo.ComponentCSV);
            }

            if (importRunFilesController.importFiles.IsChecksumPassed(importRunFilesController.importFiles.AssayInfo.DeltarnCSV))
            {
                count++;
            }
            else
            {
                KryptonMessageBox.Show(SysMessage.fileModified + Environment.NewLine + importRunFilesController.importFiles.AssayInfo.DeltarnCSV, SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.WriteLog(LogType.system, "File checksum error for " + importRunFilesController.importFiles.AssayInfo.DeltarnCSV);
            }

            if (importRunFilesController.importFiles.IsChecksumPassed(importRunFilesController.importFiles.AssayInfo.CtCSV))
            {
                count++;
            }
            else
            {
                KryptonMessageBox.Show(SysMessage.fileModified + Environment.NewLine + importRunFilesController.importFiles.AssayInfo.CtCSV, SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.WriteLog(LogType.system, "File checksum error for " + importRunFilesController.importFiles.AssayInfo.CtCSV);
            }

            if (importRunFilesController.importFiles.IsChecksumPassed(importRunFilesController.importFiles.AssayInfo.ResultCSV))
            {
                count++;
            }
            else
            {
                KryptonMessageBox.Show(SysMessage.fileModified + Environment.NewLine + importRunFilesController.importFiles.AssayInfo.ResultCSV, SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log.WriteLog(LogType.system, "File checksum error for " + importRunFilesController.importFiles.AssayInfo.ResultCSV);
            }

            if (count == 4)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        internal void ChangeCursor(Cursor cursor)
        {
            Cursor.Current = cursor;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "Import Files - Cancel the selection of run folder");

            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        internal void UpdateProgressBars(AssayInfo assayInfo)
        {
            if (importRunFilesController.importFiles.IsComponentCSVFound())
            {
                EnableComponentProgressBar(100);
                FillComponentText(Path.GetFileName(assayInfo.ComponentCSV));
            }
            else
            {
                EnableComponentProgressBar(0);
                FillComponentText(string.Empty);
            }

            if (importRunFilesController.importFiles.IsDeltaRnCSVFound())
            {
                EnableDeltaRnProgressBar(100);
                FillDeltaRnText(Path.GetFileName(assayInfo.DeltarnCSV));
            }
            else
            {
                EnableDeltaRnProgressBar(0);
                FillDeltaRnText(string.Empty);
            }

            if (importRunFilesController.importFiles.IsCtCSVFound())
            {
                EnableCtProgressBar(100);
                FillCtText(Path.GetFileName(assayInfo.CtCSV));
            }
            else
            {
                EnableCtProgressBar(0);
                FillCtText(string.Empty);
            }

            if (importRunFilesController.importFiles.IsResultCSVFound())
            {
                EnableResultProgressBar(100);
                FillResultText(Path.GetFileName(assayInfo.ResultCSV));
            }
            else
            {
                EnableResultProgressBar(0);
                FillResultText(string.Empty);
            }

            if (importRunFilesController.importFiles.IsAssayXMLFound())
            {
                EnableAssayXMLProgressBar(100);
                FillAssayXMLText(Path.GetFileName(assayInfo.RunSDX));
            }
            else
            {
                EnableAssayXMLProgressBar(0);
                FillAssayXMLText(string.Empty);
            }

            if (importRunFilesController.importFiles.IsAllFilesLoaded())
            {
                EnableOKButton(true);
            }
            else
            {
                EnableOKButton(false);
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(txtFolder.Text))
            {
                Log.WriteLog(LogType.system, "Import Files - Open Run Folder");

                Process.Start(txtFolder.Text);
            }
            else
            {
                KryptonMessageBox.Show(SysMessage.invalidDirectory, SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void HideCancel(bool flag)
        {
            this.btnCancel.Visible = flag;
        }

        internal void HideSDX(bool flag)
        {
            this.lblAssayFile.Visible = flag;
            this.pbAssayFile.Visible = flag;
            this.txtAssayFile.Visible = flag;
        }

        internal void ResizeControls()
        {
            btnOpen.Location = new Point(40, 465);
            btnOK.Location = new Point(318, 465);
            btnShowBaseSoftware.Location = new Point(160, 465);
            this.Size = new Size(478, 560);
        }

        internal void HideShowBaseSoftwareButton(bool flag)
        {
            btnShowBaseSoftware.Visible = flag;
        }

        private void btnShowBaseSoftware_Click(object sender, EventArgs e)
        {
            importRunFilesController.importFiles.ShowForm("SDSShell");
        }

        internal void MoveButtons()
        {
            btnCancel.Location = new Point(340, 550);
            btnOK.Location = new Point(190, 550);
        }
    }
}
