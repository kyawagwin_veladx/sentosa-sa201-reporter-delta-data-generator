﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 17/06/2014
 * Author: Liu Rui
 * 
 * Description:
 */
#endregion

using M_Assay;
using M_CommonMethods;
using M_System;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using M_ReportResult;

namespace UI_ImportFiles
{
    public class ImportRunFilesController
    {
        private Form_ImportFiles frmImportFiles;
        internal ImportFiles importFiles;

        private FileSystemWatcher csvWatcher;

        public ImportRunFilesController()
        {
            frmImportFiles = new Form_ImportFiles(this);
            importFiles = new ImportFiles();
            
        }

        public void SetValues(SystemConfig systemConfig, AssayInfo assayInfo, ReportResult reportResult)
        {
            importFiles.SetValues(systemConfig, assayInfo, reportResult);
        }

        internal void MonitorRunFiles()
        {
            csvWatcher = new FileSystemWatcher();
            csvWatcher.Path = importFiles.GetRunFolder();
            csvWatcher.Filter = "*.*";
            csvWatcher.Created += new FileSystemEventHandler(CSVOnCreated);
            csvWatcher.Changed += new FileSystemEventHandler(CSVOnChanged);
            csvWatcher.Deleted += new FileSystemEventHandler(CSVOnDeleted);
            csvWatcher.EnableRaisingEvents = true;
        }

        private void CSVOnDeleted(object sender, FileSystemEventArgs e)
        {
            CheckRunFiles();
        }

        private void CSVOnChanged(object sender, FileSystemEventArgs e)
        {
            try
            {
                csvWatcher.EnableRaisingEvents = false;

                CheckRunFiles();
            }
            catch(Exception ex)
            {
                MessageBox.Show("CSV file change error: " + ex.Message); 
            }
            finally
            {
                csvWatcher.EnableRaisingEvents = true;
            }
        }

        private void CSVOnCreated(object sender, FileSystemEventArgs e)
        {
            try
            {
                CheckRunFiles();
            }
            catch (Exception ex)
            {
                MessageBox.Show("CSV file creation error: " + ex.Message);
            }
        }

        private void EnableOKButton(bool flag)
        {
            frmImportFiles.EnableOKButton(flag);
        }

        public DialogResult Display(bool isCancelButtonDisplay)
        {
            if (!isCancelButtonDisplay)
            {
                frmImportFiles.HideShowBaseSoftwareButton(!isCancelButtonDisplay);
                frmImportFiles.HideCancel(isCancelButtonDisplay);
                frmImportFiles.HideSDX(isCancelButtonDisplay);
                frmImportFiles.ResizeControls();
            }
            else
            {
                frmImportFiles.HideShowBaseSoftwareButton(!isCancelButtonDisplay);
                frmImportFiles.MoveButtons();
            }

            return frmImportFiles.ShowDialog();
        }

        internal void CheckRunFiles()
        {
            try
            {
                importFiles.UpdateFlags();

                frmImportFiles.UpdateProgressBars(importFiles.AssayInfo);
            }
            catch (Exception ex)
            {
                MessageBox.Show("CheckRunFiles error " + ex.Message);
            }
        }

        internal void FillReportResult()
        {
            importFiles.UpdateReportResult();
        }

        internal void FillAssayInfo()
        {
            importFiles.FillAssayInfo();
        }

        internal bool IsAllBarcodePassed()
        {
            importFiles.AssayInfo.RefillSampleStatus(importFiles.ReportResult.SampleResultList);

            return importFiles.IsAllBarcodePassed();
        }
    }
}
