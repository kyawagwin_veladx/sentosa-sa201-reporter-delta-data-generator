﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 01/10/2014
 * Author: Liu Rui
 * 
 * Description: 
 * This class is for retrieving file names in specified directory.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_CommonMethods
{
    public class IDListItem
    {
        private string item = string.Empty;
        private string displayName = string.Empty;
        private string value = string.Empty;
        private bool isDisplayed = false;

        public bool IsDisplayed
        {
            get { return isDisplayed; }
            set { isDisplayed = value; }
        }

        public string Item
        {
            get { return item; }
            set { item = value; }
        }

        public string DisplayName
        {
            get { return displayName; }
            set { displayName = value; }
        }

        public string Value
        {
            get { return this.value; }
            set { this.value = value; }
        }
    }
}
