﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 24/06/2014
 * Author: Liu Rui
 * 
 * Description: 
 * This class is for retrieving file names in specified directory.
 */
#endregion


using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace M_CommonMethods
{
    public class ImportDLL
    {
        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        /// <summary>
        /// Import advapi32.dll to use the LogonUser method to verify user credentials
        /// </summary>
        /// <param name="userName">User name, same as Windows logon user name</param>
        /// <param name="domainName">Windows domain</param>
        /// <param name="password">User password, same as Windows logon user password</param>
        /// <param name="logonType">The type of logon operation to perform</param>
        /// <param name="logonProvider">Specifies the logon provider</param>
        /// <param name="phToken">A pointer to a handle variable that receives a handle to a token that represents the specified user.</param>
        /// <returns>true/false</returns>
        [DllImport("advapi32.dll")]
        public static extern bool LogonUser(string userName,
            string domainName,
            string password,
            int logonType,
            int logonProvider,
            out IntPtr phToken
            );

        public static void ShowForm(string exe)
        {
            IntPtr hWnd = FindWindow(null, "User Login");

            SetForegroundWindow(hWnd);

            foreach (Process process in Process.GetProcesses())
            {
                if (process.ProcessName.ToUpper().Equals(exe.ToUpper()))
                {
                    ShowWindowAsync(process.MainWindowHandle, 1);
                    SetForegroundWindow(process.MainWindowHandle);
                    break;
                }
            }
        }

        public static void SetForeWindow(IntPtr intPtr)
        {
            SetForegroundWindow(intPtr);
        }

        /// <summary>
        /// Verify whether the input username and password same with windows credentials
        /// </summary>
        /// <param name="userName">User name, same as Windows logon user name</param>
        /// <param name="password">User password, same as Windows logon user password</param>
        /// <returns>true/false</returns>
        public static bool IsValidCredentials(string userName, string password)
        {
            //use Environment.MachineName to get current domain
            string domain = null;

            //This logon type is intended for users who will be interactively using the computer, 
            //such as a user being logged on by a terminal server, remote shell, or similar process. 
            const int LOGON32_INTERACTIVE = 2;

            //Use the standard logon provider for the system
            const int LOGON32_PROVIDER_DEFAULT = 0;

            IntPtr tokenHandler = new IntPtr(0);
            tokenHandler = IntPtr.Zero;

            if (userName == string.Empty)
            {
                throw new ArgumentException("Please input User Name");
            }
            else
            {
                //call the LogonUser method to obtain a handle to an access token
                bool isValid = LogonUser(userName,
                    domain,
                    password,
                    LOGON32_INTERACTIVE,
                    LOGON32_PROVIDER_DEFAULT,
                    out tokenHandler);

                int dwError = Marshal.GetLastWin32Error();

                return isValid;
            }
        }
    }
}
