﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 1/12/2013
 * Author: Yang Yi
 * 
 * Description:
 * report class
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace M_CommonMethods
{
    public class SelectedReport
    {
        private List<int> selectedResultSample=new List<int>();

        private List<string> containChannels = new List<string>();

        private List<string> containPages = new List<string>();

        private bool isHideMessage=false;

        private bool isHideQuan=false;

        private bool isHideCycle=false;

        private bool isHideImg=false;

        private bool isHideSumResult = false;

        public SelectedReport()
        {

        }

        public List<int> SelectedResultSample
        {
            get
            {
                return selectedResultSample;
            }
            set
            {
                selectedResultSample = value;
            }
        }

        public List<string> ContainChannels
        {
            get
            {
                return containChannels;
            }
            set
            {
                containChannels = value;
            }
        }

        public List<string> ContainPages
        {
            get
            {
                return containPages;
            }
            set
            {
                containPages = value;
            }
        }

        public bool IsHideMessage
        {
            get
            {
                return isHideMessage;
            }
            set
            {
                isHideMessage = value;
            }
        }

        public bool IsHideQuan
        {
            get
            {
                return isHideQuan;
            }
            set
            {
                isHideQuan = value;
            }
        }

        public bool IsHideCycle
        {
            get
            {
                return isHideCycle;
            }
            set
            {
                isHideCycle = value;
            }
        }

        public bool IsHideImg
        {
            get
            {
                return isHideImg;
            }
            set
            {
                isHideImg = value;
            }
        }

        public bool IsHideSumResult
        {
            get
            {
                return isHideSumResult;
            }
            set
            {
                isHideSumResult = value;
            }
        }
    }
}
