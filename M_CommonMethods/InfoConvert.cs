﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 1/12/2013
 * Author: Yang Yi
 * 
 * Description:
 * report: convert report displaying data
 * 
 */
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace M_CommonMethods
{
    public class InfoConvert
    {
       
        public static string ConvertRunTime(string RunTime)
        {
            return RunTime.Replace("T", " ");
        }

        public static string ConvertTime(string time)
        {
            TimeSpan ts = TimeSpan.FromSeconds(Convert.ToDouble(time));
            
            string str = string.Empty;

            if ((ts.Hours != 0) && (ts.Minutes != 0))
            {
                if (ts.Seconds == 0)
                {
                    str = string.Format("{0:D2}hr:{1:D2}min", ts.Hours, ts.Minutes);
                }
                else
                {
                    str = string.Format("{0:D2}hr:{1:D2}min:{2:D2}sec", ts.Hours, ts.Minutes, ts.Seconds);
                }
            }

            else if (ts.Minutes != 0)
            {
                if (ts.Seconds == 0)
                {
                    str = string.Format("{0:D2}min", ts.Minutes);
                }
                else
                {
                    str = string.Format("{0:D2}min:{1:D2}sec", ts.Minutes, ts.Seconds);

                }
            }
            else
            {
                if (ts.Seconds == 0)
                {
                    str = string.Empty;
                }
                else
                {
                    str = string.Format("{0:D2}sec", ts.Seconds);
                }
            }
            return str;
        }

        public static string ConvertAcquiredTo(ArrayList stepacquiredcolllections)
        {
            string str=string.Empty;

            foreach(string item in stepacquiredcolllections)
            {
               str+=(item +",");
            }
            
            return str;
        }

        public static Color ConvertStrToColor(string colour)
        {
            Color sbrush;

            if (colour != string.Empty)
            {
                int pickedColor = Convert.ToInt32(colour);
                byte[] bytes = BitConverter.GetBytes(pickedColor);
                return sbrush = Color.FromArgb(bytes[0], bytes[1], bytes[2]);
            }
            else
            {
                return Color.Black;
            }
            
        }

        public static string BackgroundColor(string samcolor)
        {
            string rtn = string.Empty;

            if (samcolor != null)
            {
                Color sbrush = Color.FromArgb(Convert.ToInt32(samcolor));

                rtn = "#" + sbrush.B.ToString("X2") + sbrush.G.ToString("X2") + sbrush.R.ToString("X2");
            }
            return rtn;
        }

        public static string ConvertToF2(string ctv)
        {
            string ctGvalue = string.Empty;
            if (ctv != string.Empty)
            {
                double Gvalue = Convert.ToDouble(ctv);
                ctGvalue = Gvalue.ToString("F2");
            }
            return ctGvalue;
        }

        public static string ConvertToFPlace(string ctv, string place)
        {
            string ctGvalue = string.Empty;
            if (ctv != string.Empty)
            {
                double Gvalue = Convert.ToDouble(ctv);
                ctGvalue = Gvalue.ToString(place);
            }
            return ctGvalue;
        }

        public static int GetNumericPosition(string alphanumeric)
        {
            int target = -1;

            if (alphanumeric.Length > 0)
            {
                try
                {
                    char charactor = Convert.ToChar(alphanumeric.Substring(0, 1));

                    int integer = Convert.ToInt32(alphanumeric.Substring(1, alphanumeric.Length - 1));

                    target = (Convert.ToInt32(charactor) - 65) * 12 + integer;
                }
                catch 
                {
 
                }
            }
            return target;
        }

        public static string GetAlphanumericPosition(int numericPosition)
        {
            string target = string.Empty;

            double letterDouble = numericPosition / 12.0;
            int letterInt = numericPosition / 12;
            if (letterInt == letterDouble)
            {
                letterInt= letterInt - 1;
            }

            int intInt = numericPosition % 12;
            if (intInt == 0)
            {
                intInt = 12;
            }

            char letter = ' ';
            try
            {
                letter = Convert.ToChar(letterInt + 65);
            }
            catch
            { 
            
            }

            return target = letter.ToString()+intInt.ToString();
        }

        public static string ConvertONOFF(string text)
        {
            string str = string.Empty;
            if (text.ToUpper().Contains("FALSE"))
            {
                str = "OFF";
            }
            else if (text.ToUpper().Contains("TRUE"))
            {
                str = "ON";
            }
            return str;
        }

        public static string ConvertONOFF(bool text)
        {
            string str = string.Empty;
            if (!text)
            {
                str = "OFF";
            }
            else
            {
                str = "ON";
            }
            return str;
        }
        public static string changeSTDCT(string stdformula)
        {
            if (stdformula.Contains("CT"))
            {
                return stdformula.Replace("CT", "Ct");
            }
            else
            {
                return "N/A";
            }
        }

        public static string ChannelNameConverter(string channelName)
        {
            if (channelName == "GREEN")
            {
                return "Green";
            }
            if (channelName == "ORANGE")
            {
                return "Orange";
            }
            if (channelName == "RED")
            {
                return "Red";
            }
            if (channelName == "YELLOW")
            {
                return "Yellow";
            }
            return channelName;
        }

        public static string GetM1ItemNumber(string m1)
        {
            string target = string.Empty;

            string[] m1s = m1.Split('*');

            if (m1s.Length >= 2)
            {
                target = m1s[1];
            }

            return target;
        }

        public static string StringToF2(string str)
        {
            string ctGvalue = string.Empty;
            if (str != string.Empty)
            {
                double Gvalue = Convert.ToDouble(str);
                ctGvalue = Gvalue.ToString("F2");
            }
            return ctGvalue;
        }

        public static string TitleWithValue(string title, string value)
        {
            if (value == string.Empty)
            {
                value = "-";

            }
            return title + ": " + value;
        }

        public static string GetPageName(string p)
        {
            string target = string.Empty;

            if (p.Contains("_"))
            {
                target = p.Split('_').First().TrimEnd('_');
            }

            return target;
        }

        public static string GetChannelName(string p)
        {
            string target = string.Empty;

            if (p.Contains("("))
            {
                target = p.Split('(').First().Trim();
            }

            return target;
        }

        //public static void ReorderChannelName(List<string> channelNameList)
        //{
        //    List<string> temp = new List<string>();

        //    if (channelNameList.Contains("GREEN"))
        //    {
        //        temp.Add("GREEN");
        //    }

        //    if (channelNameList.Contains("YELLOW"))
        //    {
        //        temp.Add("YELLOW");
        //    }

        //    if (channelNameList.Contains("ORANGE"))
        //    {
        //        temp.Add("ORANGE");
        //    }

        //    if (channelNameList.Contains("RED"))
        //    {
        //        temp.Add("RED");
        //    }

        //    channelNameList.Clear();
        //    channelNameList.AddRange(temp);
        //}

        public static string SpliKitBarcode(string itemName, string itemValue)
        {
          
            if (itemName.ToUpper() == "AssayKit".ToUpper() || itemName.ToUpper() == "LysisKit".ToUpper())
            {
                if (itemValue.Split('*').Length >= 5) //V*AAAAAA*EXTRX*Lot Number(10 digits)*Expiry Date(YYYY-MM-DD)
                {
                    return "Lot: " + itemValue.Split('*')[3] + Environment.NewLine + "Expiry Date: " + itemValue.Split('*')[4];
                }
                else
                {
                    return "Lot: " + Environment.NewLine + "Expiry Date: ";
                }
            }
            else
            {
                return itemValue;
            }
        }

        // Method to return IComparer object for sort helper.
        public static int sortWellAscending(string wellName1, string wellName2)
        {
            int result = 0;
            int suba = 0;
            int subb = 0;
            string a = string.Empty;
            string b = string.Empty;


            if (wellName1 == null || wellName2 == null)
            {
                return 0;
            }
            if (wellName1.Length < 2 || wellName2.Length < 2)
            {
                return 0;
            }
            a = wellName1.Substring(0, 1);
            b = wellName2.Substring(0, 1);
            suba = int.Parse(wellName1.Substring(1));
            subb = int.Parse(wellName2.Substring(1));

            result = string.Compare(a, b);

            if (result == 0)
            {
                if (suba > subb)
                    result = 1;
                else if (suba < subb)
                    result = -1;

            }

            return result;
        }

        public static int sortPositionAscending(string wellName1, string wellName2)
        {
            int result = 0;
            int suba = 0;
            int subb = 0;
            string a = string.Empty;
            string b = string.Empty;


            if (wellName1 == null || wellName2 == null)
            {
                return 0;
            }
            if (wellName1.Length < 2 || wellName2.Length < 2)
            {
                return 0;
            }
            a = wellName1.Substring(0, 1);
            b = wellName2.Substring(0, 1);
            suba = int.Parse(wellName1.Substring(1));
            subb = int.Parse(wellName2.Substring(1));

            if (suba > subb)
                result = 1;
            else if (suba < subb)
                result = -1;

            if (result == 0)
            {
                result = string.Compare(a, b);
            }

            return result;
        }


       
    }
}
