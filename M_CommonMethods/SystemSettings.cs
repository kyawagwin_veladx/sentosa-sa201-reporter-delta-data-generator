﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_CommonMethods
{
    static class Settings
    {
        internal static string hkeyProgram = @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\";
        internal static string pathTag = "Path";
        internal static string dyeName = "Dye Name";
        internal static string detector = "Detector";
        internal static string documentName = "Document Name";
        internal static string assayFile = "AssayFile";
    }
}
