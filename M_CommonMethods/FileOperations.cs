﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 25/11/2013
 * Author: Liu Rui
 * 
 * Description: 
 * This class is for retrieving file names in specified directory.
 */
#endregion

using Microsoft.VisualBasic.FileIO;
using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace M_CommonMethods
{
    public static class FileOperations
    {
        #region Private Methods
        ///// <summary>
        ///// Get the EXE's folder from Windows Regedit
        ///// </summary>
        ///// <param name="exe">EXE name, such as chrome.exe</param>
        ///// <returns>EXE's folder</returns>
        //public static string GetApplicationFolder(string exe)
        //{
        //    string exePath = string.Empty;
        //    if (exe != string.Empty)
        //    {
        //        exePath = (string)Registry.GetValue(Settings.hkeyProgram + exe, Settings.pathTag, null);
        //    }
        //    return exePath;
        //}

        /// <summary>
        /// This is the LoadXML file function, used for loading xml file.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static XmlDocument LoadXML(string path)
        {
            XmlDocument xdoc = new XmlDocument();

            if (!File.Exists(path))
            {
                MessageBox.Show("File is missing");
                return null;
            }
            try
            {
                xdoc.Load(path);
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid XML file");
                return null;
            }

            return xdoc;
        }

        public static List<string> GetAllCorruptedFiles(string[] files)
        {
            List<string> target = new List<string>();

            foreach (string file in files)
            {
                try
                {
                    LoadXML(file);
                }
                catch
                {
                    target.Add(file);
                }
            }

            return target;
        }

        public static bool IsFileLocked(string filePath)
        {
            FileInfo file = new FileInfo(filePath);

            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally 
            {
                if (stream != null)
                {
                    stream.Close();
                }
            }

            return false;
        }


        /// <summary>
        /// Load CSV to a List of string array
        /// </summary>
        /// <param name="path">CSV file path</param>
        /// <returns>string array list</returns>
        public static List<string[]> LoadCSV(string path)
        {
            List<string[]> parsedData = new List<string[]>();
            string[] fields;
            if ((path == string.Empty) || (path == null))
            {
                return parsedData;
            }
            TextFieldParser parser = new TextFieldParser(path);
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(",");
            while (!parser.EndOfData)
            {
                fields = parser.ReadFields();
                parsedData.Add(fields);
            }

            parser.Close();
            return parsedData;
        }

        /// <summary>
        /// Get the whole string which contains the desired info string for further operation
        /// </summary>
        /// <param name="info">desired info string</param>
        /// <param name="parsedData">parsed data of CSV</param>
        /// <returns>whole string which contains the desired info string</returns>
        public static string GetCellValue(string info, List<string[]> parsedData)
        {
            string targetString = string.Empty;
            if (info == string.Empty)
            {
                return targetString;
            }

            if (parsedData.Count == 0)
            {
                return targetString;
            }

            foreach (string[] arrayString in parsedData)
            {
                foreach (string sourceString in arrayString)
                {
                    if (sourceString.ToUpper().Contains(info.ToUpper()))
                    {
                        return targetString = sourceString;
                    }
                }
            }

            return targetString;
        }

        //public static List<string[]> GetCSVRows(string info, List<string[]> parsedData)
        //{
        //    List<string[]> targetStringList = new List<string[]>();
        //    if (info == string.Empty)
        //    {
        //        return targetStringList;
        //    }

        //    if (parsedData.Count == 0)
        //    {
        //        return targetStringList;
        //    }

        //    foreach (string[] arrayString in parsedData)
        //    {
        //        foreach (string sourceString in arrayString)
        //        {
        //            if (sourceString.ToUpper().Equals(info.ToUpper()))
        //            {
        //                targetStringList.Add(arrayString);
        //                break;
        //            }
        //        }
        //    }

        //    return targetStringList;
        //}


        /// <summary>
        /// Get string array if the array contains the desired string
        /// </summary>
        /// <param name="info">desired string</param>
        /// <param name="parsedData">parsed data of CSV</param>
        /// <returns>string array</returns>
        public static string[] GetCSVRow(string info, List<string[]> parsedData)
        {
            string[] targetString;
            if (info == string.Empty)
            {
                return targetString = null;
            }

            if (parsedData.Count == 0)
            {
                return targetString = null;
            }

            foreach (string[] arrayString in parsedData)
            {
                foreach (string sourceString in arrayString)
                {
                    if (sourceString.ToUpper().Contains(info.ToUpper()))
                    {
                        return targetString = arrayString;
                    }
                }
            }

            return targetString = null;
        }

        public static List<string[]> GetCSVSignatureRow(string info, List<string[]> parsedData)
        {
            List<string[]> targetString = new List<string[]>();

            if (info == string.Empty)
            {
                return targetString;
            }

            if (parsedData.Count == 0)
            {
                return targetString;
            }

            foreach (string[] arrayString in parsedData)
            {
                foreach (string sourceString in arrayString)
                {
                    if (sourceString.ToUpper().Equals(info.ToUpper()))
                    {
                        targetString.Add(arrayString);
                        
                    }
                }
            }
            return targetString;
           // return targetString = null;
        }

        /// <summary>
        /// Get index of the target string array
        /// </summary>
        /// <param name="info">target string</param>
        /// <param name="parsedData">parsed data of CSV</param>
        /// <returns>index</returns>
        public static int GetRowIndex(string info, List<string[]> parsedData)
        {
            int targetIndex = -1;
            if (info == string.Empty)
            {
                return targetIndex;
            }
            if (parsedData.Count == 0)
            {
                return targetIndex;
            }

            for (int i = 0; i < parsedData.Count; i++)
            {
                foreach (string sourceString in parsedData[i])
                {
                    if (sourceString.ToUpper().Contains(info.ToUpper()))
                    {
                        return targetIndex = i;
                    }
                }
            }

            return targetIndex;
        }

        /// <summary>
        /// Split string by the colon
        /// </summary>
        /// <param name="wholeString">whole string</param>
        /// <returns>string array</returns>
        public static string[] SplitColon(string wholeString)
        {
            if (wholeString == string.Empty)
            {
                return null;
            }

            string[] targetStringArray = wholeString.Split(':');

            return targetStringArray;
        }
        #endregion

        public static bool IsComponentCSV(string filePath)
        {
            bool target = false;

            List<string[]> parsedData = LoadCSV(filePath);

            if (parsedData[2].Length >= 2)
            {
                if (parsedData[2][1] == Settings.dyeName)
                {
                    target = true;
                }
            }

            return target;
        }

        public static bool IsDeltaRnCSV(string filePath)
        {
            bool target = false;

            List<string[]> parsedData = LoadCSV(filePath);

            if (parsedData[2].Length >= 2)
            {
                if (parsedData[2][1] == Settings.detector)
                {
                    target = true;
                }
            }

            return target;
        }

        public static bool IsResultCSV(string filePath)
        {
            bool target = false;

            List<string[]> parsedData = LoadCSV(filePath);

            if (parsedData.Count > 0)
            {
                if (parsedData[0][0].Contains(Settings.documentName))
                {
                    target = true;
                }
            }

            return target;
        }

        public static bool IsCtCSV(string filePath)
        {
            bool target = false;

            List<string[]> parsedData = LoadCSV(filePath);

            if (parsedData.Count > 2)
            {
                if (parsedData[2][0]==Settings.detector)
                {
                    target = true;
                }
            }

            return target;
        }

        public static bool IsAssayXML(string filePath)
        {
            XmlDocument xml = LoadXML(filePath);

            if (xml.DocumentElement.Name == Settings.assayFile)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static int GetCellofRowIndex(string keyWord, List<string[]> parsedCSV)
        {
            int target = -1;

            foreach (string[] stringArrays in parsedCSV)
            {
                for (int i = 0; i < stringArrays.Length; i++)
                {
                    if (stringArrays[i] == keyWord)
                    {
                        return target = i;
                    }
                }
            }

            return target;
        }

        public static string LocateEXE(string halfPath)
        {
            string target = string.Empty;

            DriveInfo[] allDrives = DriveInfo.GetDrives();

            foreach (DriveInfo d in allDrives)
            {
                if (d.IsReady)
                {
                    if (File.Exists(d.RootDirectory.FullName + halfPath))
                    {
                        return target = d.RootDirectory.FullName + halfPath;
                    }
                }
            }

            return target;
        }

        public static bool IsEmpty(string filePath)
        {
            FileInfo file = new FileInfo(filePath);

            

            try
            {
                if (file.Length == 0)
                    return true;
                else
                    return false;
            }
            catch (IOException)
            {
                return true;
            }
          

        }

        public static int GetWellIndex(string info, List<string[]> parsedData)
        {
            int targetIndex = -1;
            if (info == string.Empty)
            {
                return targetIndex;
            }
            if (parsedData.Count == 0)
            {
                return targetIndex;
            }

            for (int i = 0; i < parsedData.Count; i++)
            {
                foreach (string sourceString in parsedData[i])
                {
                    if (sourceString.ToUpper().Equals(info.ToUpper()))
                    {
                        return targetIndex = i;
                    }
                }
            }

            return targetIndex;
        }
    }
}
