﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 26/06/2014
 * Author: Liu Rui
 * 
 * Description:
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.IO;

namespace M_SA201Run
{
    public class BaseSoftwareController
    {
        public static void LaunchEXE(string exePath, string sParam)
        {
           
            if (exePath != string.Empty)
            {
                Process.Start(exePath, sParam);
            }
        }

        public static bool IsLaunchedEXE(string exeName)
        {
            Process[] pname = Process.GetProcessesByName(exeName);
            if (pname.Length == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
