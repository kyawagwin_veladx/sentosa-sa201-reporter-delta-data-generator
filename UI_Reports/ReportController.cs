﻿using M_Assay;
using M_ReportResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UI_Reports
{   
    /// <summary>
    /// reportcontroller class contain itself constructor and SetValues method
    /// </summary>
    public class ReportController
    {
        public UserControl_ReportOperation ReportOperationDisplay;
        public ReportOperation ReportOperation;

        public ReportController()
        {
            ReportOperationDisplay = new UserControl_ReportOperation();
            ReportOperation = new ReportOperation();
 
        }
        /// <summary>
        /// this method is used to set value for previewing report
        /// </summary>
        /// <param name="reportResult"></param>
        /// <param name="assayLogic"></param>
        public void SetValues(ReportResult reportResult, AssayLogic assayLogic)
        {
            ReportOperationDisplay.SetPreviewReport(reportResult, assayLogic);
        }
    }
}
