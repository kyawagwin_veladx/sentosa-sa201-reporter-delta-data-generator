﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 22/01/2014
 * Author: Yang Yi
 * 
 * Description:
 * Used for report preview display
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using M_ReportResult;
using M_Report;
using M_Assay;
using M_Log;
using M_Language;


namespace UI_Reports
{
    public partial class Form_ReportPreview : Form
    {
        ReportResult reportresult;
        SelectedReport report;
        AssayLogic assaylogic;
        ReportControl reportControl;

        public Form_ReportPreview()
        {
            InitializeComponent();
        }

        //pass value for debug use
        public bool PassValue(ReportResult reportresult, SelectedReport report, AssayLogic assaylogic)
        {
            this.reportresult = reportresult;
            this.report = report;
            this.assaylogic = assaylogic;

            bool Debug = true;

            if (Debug)
            {
                return ShowReport();
            }
            else
            {
                return ShowReportDll();
            }
        }

        private bool ShowReport()
        {
            reportControl = new ReportControl();
            reportControl.DisplayReport(this.report, this.assaylogic, this.reportresult);
            this.panel1.Controls.Add(reportControl);
            reportControl.Dock = DockStyle.Fill;
            reportControl.Show();
            return true;
        }

        public bool ShowReportDll()
        {
            string assayname = string.Empty;
            if (reportresult.RunInfo.AssayName.Contains("/"))
            {
                assayname = reportresult.RunInfo.AssayName.Replace("/", ",");
            }
            else 
            {
                assayname = reportresult.RunInfo.AssayName;
            }
            string path = AppDomain.CurrentDomain.BaseDirectory + "Report\\" + assayname + "_Report.dll";
            Log.WriteLog(LogType.system, path);

            try
            {
                if (!File.Exists(path))
                {
                    path = AppDomain.CurrentDomain.BaseDirectory +  "Report\\ReportModule.dll";
                    if (!File.Exists(path))
                    {
                        MessageBox.Show(SysMessage.ReportDllPathMsg);
                        return false;
                    }
                }
                Log.WriteLog(LogType.system, "Path OK");
                Assembly ass = Assembly.LoadFrom(path);

                Type type;
                Object obj;


                type = ass.GetType("Sentosa_Reporter.ReportModule" + "." + "ReportControl");
                Log.WriteLog(LogType.system, ass.ToString());
                obj = Activator.CreateInstance(type);
                Log.WriteLog(LogType.system, obj.ToString());
                MethodInfo method = type.GetMethod("DisplayReport");

                //MethodInfo method = type.GetMethod("CreatePDF");
                Log.WriteLog(LogType.system, method.ToString());

                object[] parameters = new object[3];
                parameters[0] = report;
                parameters[1] = assaylogic;
                parameters[2] = reportresult;


                Log.WriteLog(LogType.system, obj.ToString());
                method.Invoke(obj, parameters);
                Log.WriteLog(LogType.system, obj.ToString() + "after");
                this.panel1.Controls.Add((UserControl)obj);
                ((UserControl)obj).Dock = DockStyle.Fill;
            }
            catch (Exception e)
            {
                Log.WriteLog(LogType.system, e.ToString());
                throw new System.ArgumentException(e.Message);
            }

            return true;
        }

        private void Form_ReportPreview_Resize(object sender, EventArgs e)
        {
            this.reportControl.Size = this.Size;
        }
    }
}
