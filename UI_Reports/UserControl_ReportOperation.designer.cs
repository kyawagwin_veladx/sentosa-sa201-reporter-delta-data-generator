﻿
using System.Windows.Forms;

namespace UI_Reports
{
    partial class UserControl_ReportOperation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comboBox1 = new ComboBox();
            this.dataGridView1 = new DataGridView();
            this.checkBox_ChkAll = new CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.chk_filter = new CheckBox();
            this.btn_preview = new Button();
            this.lbl_page = new Label();
            this.chk_Message = new CheckBox();
            this.chk_img = new CheckBox();
            this.chk_cycle = new CheckBox();
            this.chk_quan = new CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chk_wellsort = new CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panel3 = new System.Windows.Forms.Panel();
            this.sampleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            //((System.ComponentModel.ISupportInitialize)(this.comboBox1)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.sampleBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.DropDownWidth = 199;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(127, 9);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(199, 21);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dataGridView1.ColumnHeadersHeight = 30;
            this.dataGridView1.Location = new System.Drawing.Point(3, 50);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 30;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(536, 540);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            this.dataGridView1.SortCompare += new System.Windows.Forms.DataGridViewSortCompareEventHandler(this.dataGridView1_SortCompare);
            // 
            // checkBox_ChkAll
            // 
            //this.checkBox_ChkAll.LabelStyle = LabelStyle.NormalControl;
            this.checkBox_ChkAll.Location = new System.Drawing.Point(13, 25);
            this.checkBox_ChkAll.Name = "checkBox_ChkAll";
            this.checkBox_ChkAll.Size = new System.Drawing.Size(19, 13);
            this.checkBox_ChkAll.TabIndex = 2;
            this.checkBox_ChkAll.Text = "";
            this.checkBox_ChkAll.CheckedChanged += new System.EventHandler(this.checkBox_ChkAll_CheckedChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::UI_Reports.Properties.Resources.link;
            this.pictureBox1.Location = new System.Drawing.Point(335, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(29, 27);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Tag = "Coupling";
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // chk_filter
            // 
            //this.chk_filter.LabelStyle = LabelStyle.NormalControl;
            this.chk_filter.Location = new System.Drawing.Point(13, 10);
            this.chk_filter.Name = "chk_filter";
            this.chk_filter.Size = new System.Drawing.Size(50, 20);
            this.chk_filter.TabIndex = 7;
            this.chk_filter.Text = "Filter";
            this.chk_filter.Text = "Filter";
            this.chk_filter.CheckedChanged += new System.EventHandler(this.chk_filter_CheckedChanged);
            // 
            // btn_preview
            // 
            this.btn_preview.Location = new System.Drawing.Point(63, 9);
            this.btn_preview.Name = "btn_preview";
            this.btn_preview.Size = new System.Drawing.Size(97, 27);
            this.btn_preview.TabIndex = 4;
            this.btn_preview.Text = "Preview Report";
            this.btn_preview.Click += new System.EventHandler(this.btn_preview_Click);
            // 
            // lbl_page
            // 
            this.lbl_page.Location = new System.Drawing.Point(69, 10);
            this.lbl_page.Name = "lbl_page";
            this.lbl_page.Size = new System.Drawing.Size(52, 20);
            this.lbl_page.TabIndex = 5;
            this.lbl_page.Text = "Target :";
            // 
            // chk_Message
            // 
            this.chk_Message.Checked = true;
            this.chk_Message.CheckState = System.Windows.Forms.CheckState.Checked;
            //this.chk_Message.LabelStyle = LabelStyle.NormalControl;
            this.chk_Message.Location = new System.Drawing.Point(3, 96);
            this.chk_Message.Name = "chk_Message";
            this.chk_Message.Size = new System.Drawing.Size(72, 20);
            this.chk_Message.TabIndex = 7;
            this.chk_Message.Text = "Message";
            this.chk_Message.Text = "Message";
            this.chk_Message.CheckedChanged += new System.EventHandler(this.chk_Message_CheckedChanged);
            // 
            // chk_img
            // 
            this.chk_img.Checked = true;
            this.chk_img.CheckState = System.Windows.Forms.CheckState.Checked;
            //this.chk_img.LabelStyle = LabelStyle.NormalControl;
            this.chk_img.Location = new System.Drawing.Point(3, 73);
            this.chk_img.Name = "chk_img";
            this.chk_img.Size = new System.Drawing.Size(152, 20);
            this.chk_img.TabIndex = 6;
            this.chk_img.Text = "Raw/ Normalized Curve";
            this.chk_img.Text = "Raw/ Normalized Curve";
            this.chk_img.CheckedChanged += new System.EventHandler(this.chk_img_CheckedChanged);
            // 
            // chk_cycle
            // 
            this.chk_cycle.Checked = true;
            this.chk_cycle.CheckState = System.Windows.Forms.CheckState.Checked;
            //this.chk_cycle.LabelStyle = LabelStyle.NormalControl;
            this.chk_cycle.Location = new System.Drawing.Point(3, 50);
            this.chk_cycle.Name = "chk_cycle";
            this.chk_cycle.Size = new System.Drawing.Size(101, 20);
            this.chk_cycle.TabIndex = 5;
            this.chk_cycle.Text = "Cycling Profile";
            this.chk_cycle.Text = "Cycling Profile";
            this.chk_cycle.CheckedChanged += new System.EventHandler(this.chk_cycle_CheckedChanged);
            // 
            // chk_quan
            // 
            this.chk_quan.Checked = true;
            this.chk_quan.CheckState = System.Windows.Forms.CheckState.Checked;
            //this.chk_quan.LabelStyle = LabelStyle.NormalControl;
            this.chk_quan.Location = new System.Drawing.Point(3, 27);
            this.chk_quan.Name = "chk_quan";
            this.chk_quan.Size = new System.Drawing.Size(160, 20);
            this.chk_quan.TabIndex = 4;
            this.chk_quan.Text = "Quantitation Information";
            this.chk_quan.Text = "Quantitation Information";
            this.chk_quan.CheckedChanged += new System.EventHandler(this.chk_quan_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.chk_Message);
            this.panel2.Controls.Add(this.chk_img);
            this.panel2.Controls.Add(this.chk_quan);
            this.panel2.Controls.Add(this.chk_cycle);
            this.panel2.Location = new System.Drawing.Point(545, 50);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(168, 141);
            this.panel2.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(16, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "Experimental Information";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chk_wellsort);
            this.panel1.Controls.Add(this.chk_filter);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.checkBox_ChkAll);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.lbl_page);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(520, 41);
            this.panel1.TabIndex = 9;
            // 
            // chk_wellsort
            // 
            //this.chk_wellsort.LabelStyle = LabelStyle.NormalControl;
            this.chk_wellsort.Location = new System.Drawing.Point(382, 10);
            this.chk_wellsort.Name = "chk_wellsort";
            this.chk_wellsort.Size = new System.Drawing.Size(90, 20);
            this.chk_wellsort.TabIndex = 9;
            this.chk_wellsort.Text = "Well Sorting";
            this.chk_wellsort.Text = "Well Sorting";
            this.chk_wellsort.CheckedChanged += new System.EventHandler(this.chk_wellsort_CheckedChanged);
            // 
            // toolTip1
            // 
            this.toolTip1.ToolTipTitle = "Couple Same Sample Name";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.btn_preview);
            this.panel3.Location = new System.Drawing.Point(548, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(165, 41);
            this.panel3.TabIndex = 10;
            // 
            // UserControl_ReportOperation
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Name = "UserControl_ReportOperation";
            this.Size = new System.Drawing.Size(727, 598);
            //((System.ComponentModel.ISupportInitialize)(this.comboBox1)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            //((System.ComponentModel.ISupportInitialize)(this.sampleBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ComboBox comboBox1;
        private System.Windows.Forms.BindingSource sampleBindingSource;
        private CheckBox checkBox_ChkAll;
        private Label lbl_page;
        private Button btn_preview;
        private CheckBox chk_Message;
        private CheckBox chk_img;
        private CheckBox chk_cycle;
        private CheckBox chk_quan;
        private CheckBox chk_filter;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel2;
        private DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel panel3;
        private Label label1;
        private CheckBox chk_wellsort;
       
    }
}
