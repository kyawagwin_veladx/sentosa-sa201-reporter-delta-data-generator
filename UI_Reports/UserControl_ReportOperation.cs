﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 28/11/2013
 * Author: Yang Yi
 * 
 * Description:
 * user control used for report operation
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using M_ReportResult;
using M_Report;
using M_Assay;
using M_Log;

using UI_Reports.Properties;
using System.ComponentModel;
using M_Language;
using M_CommonMethods;

namespace UI_Reports
{
    /// <summary>
    /// this class is mainly used for data display on the report page 
    /// </summary>
    public partial class UserControl_ReportOperation : UserControl
    {
        private ReportResult reportResult;
        private AssayLogic assayLogic;
        private M_Report.SelectedReport report;
        private bool isCoupling = true;
        private bool isInitialized = false;
        private bool isCellClicked = false;


        public UserControl_ReportOperation()
        {
            InitializeComponent();
            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
        }

        /// <summary>
        /// Use this method for initial data display on the report page
        /// </summary>
        /// <param name="runResult"></param>
        public void SetPreviewReport(ReportResult runResult, AssayLogic assayLogic)
        {
            this.reportResult = runResult;

            this.assayLogic = assayLogic;
            chk_wellsort.Checked = false;
            GetPageName();

            FillData();

            comboBox1.SelectedIndex = 0;//pagedisplay

            SelectRectangle();
            isInitialized = true;
            HightDataGridView(0);

            
        }
      
        private void SelectRectangle()
        {
            try
            {
                if (dataGridView1.RowCount >= 0)
                {
                    Rectangle rect = dataGridView1.GetCellDisplayRectangle(0, -1, true);

                    int width = dataGridView1[0, 0].Size.Width;

                    checkBox_ChkAll.Location = new Point((width / 2) - 6, 10);

                    dataGridView1.Controls.Add(checkBox_ChkAll);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// page display
        /// </summary>
        /// <param name="reportResult"></param>
        private void GetPageName()
        {
            comboBox1.Items.Clear();
            comboBox1.Items.Add("All Targets");
            List<string> pageList = new List<string>();

            for (int i = 0; i < reportResult.SampleResultList.Count; i++)
            {
                if (!pageList.Contains(reportResult.SampleResultList[i].PageName))
                {
                    pageList.Add(reportResult.SampleResultList[i].PageName);
                }
            }

            foreach (string str in pageList)
            {
                comboBox1.Items.Add(str);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[UserControl_ReportOperation]- target combobox is selected");
            if (comboBox1.SelectedIndex > 0)
            {
                DisplayPage((string)comboBox1.SelectedItem);
            }
            else
            {
                AllPagesDisplay();
            }
        }

        private void DisplayPage(string pagename)
        {
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if ((string)dataGridView1.Rows[i].Cells[6].Value != pagename)
                {
                    dataGridView1.Rows[i].Visible = false;
                    dataGridView1.Rows[i].Cells[0].Value = false;

                }
                else 
                {
                    dataGridView1.Rows[i].Visible = true;
                }
            }
        }

        private void AllPagesDisplay()
        {
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                dataGridView1.Rows[i].Visible = true;
            }
        }

        private void FillData()
        {       
            AddColumns();

            FillSamples();
        }

        private void FillSamples()
        {
            dataGridView1.Rows.Clear();  
            foreach (SampleResult sr in reportResult.SampleResultList)
            {
                object[] obj = new object[9 + sr.CtChannelList.Count];

                obj[0] = true;
                obj[1] = string.Empty;//color
                obj[2] = sr.Position.ToString();
                obj[3] = sr.AlphaNumPos;
                obj[4] = sr.SampleName;
                obj[5] = sr.SampleType;
                obj[6] = sr.PageName;

                int index = 7;
                foreach (CTChannel ctc in sr.CtChannelList)
                {
                    if (ctc.CtValue != string.Empty)
                    {
                        double val = Convert.ToDouble(ctc.CtValue);
                        obj[index] = val.ToString("F2");
                    }
                    else
                    {
                        obj[index] = string.Empty;
                    }
                    index++;
                }
                obj[index] = sr.Validity;
                obj[index + 1] = sr.TestResult;

                dataGridView1.Rows.Add(obj);
                dataGridView1.Rows[dataGridView1.Rows.Count-1].Cells[1].Style.BackColor = sr.Colour;
                SetControlsFix();
            }
            this.dataGridView1.Sort(dataGridView1.Columns[3],ListSortDirection.Ascending);
        }

        private void SetControlsFix()
        {
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if (Convert.ToString(dataGridView1.Rows[i].Cells[5].Value) == "NC" ||
                         Convert.ToString(dataGridView1.Rows[i].Cells[5].Value) == "PC" ||
                         Convert.ToString(dataGridView1.Rows[i].Cells[5].Value) == "NTC" ||
                         Convert.ToString(dataGridView1.Rows[i].Cells[5].Value) == "LPC" ||
                         Convert.ToString(dataGridView1.Rows[i].Cells[5].Value) == "HPC" ||
                         Convert.ToString(dataGridView1.Rows[i].Cells[5].Value) == "Standard")
                {
                    dataGridView1.Rows[i].Cells[0].ReadOnly = true;
                    dataGridView1.Rows[i].Cells[0].Style.ForeColor = Color.DarkGray;
                }
            }
        }

        private void AddColumns()
        {
            dataGridView1.Columns.Clear();

            AddDefaultColumns();

            foreach (CTChannel ctc in reportResult.SampleResultList[0].CtChannelList)
            {
                DataGridViewColumn newCol = new DataGridViewTextBoxColumn();
                newCol.ReadOnly = true;
                //newCol.Resizable = DataGridViewTriState.False;
                newCol.SortMode = DataGridViewColumnSortMode.NotSortable;
                newCol.HeaderText = ctc.ChannelName + " (Ct)";
                newCol.Width = 80;
                newCol.Name = ctc.ChannelName + " (Ct)";
                dataGridView1.Columns.Add(newCol);
            }

            AddValidityResult();
            
            dataGridView1.Refresh();
        }

        private void AddDefaultColumns()
        {
            DataGridViewCheckBoxColumn Check = new DataGridViewCheckBoxColumn();
            DataGridViewTextBoxColumn DisplayColor = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn Position = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn Well = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn SampleID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn SampleType = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn Test = new DataGridViewTextBoxColumn();

            Check.Resizable = DataGridViewTriState.False;
            Check.SortMode = DataGridViewColumnSortMode.NotSortable;
            Check.HeaderText = " ";
            Check.Width = 30;
            Check.Name = "Check";
            dataGridView1.Columns.Add(Check);

            DisplayColor.ReadOnly = true;
            DisplayColor.Resizable = DataGridViewTriState.False;
            DisplayColor.SortMode = DataGridViewColumnSortMode.NotSortable;
            DisplayColor.HeaderText = "Color";
            DisplayColor.Width = 40;
            DisplayColor.Name = "Color";
            dataGridView1.Columns.Add(DisplayColor);

            Position.ReadOnly = true;
            Position.Resizable = DataGridViewTriState.False;
            Position.SortMode = DataGridViewColumnSortMode.NotSortable;
            Position.HeaderText = "Position";
            Position.Width = 40;
            Position.Name = "Position";
            Position.Visible = false;
            dataGridView1.Columns.Add(Position);

            Well.ReadOnly = true;
            Well.Resizable = DataGridViewTriState.False;
            Well.SortMode = DataGridViewColumnSortMode.Automatic;
            Well.HeaderText = "Well";
            Well.Width = 50;
            Well.Name = "Well";
            dataGridView1.Columns.Add(Well);

            SampleID.ReadOnly = true;
            SampleID.Resizable = DataGridViewTriState.False;
            SampleID.SortMode = DataGridViewColumnSortMode.Automatic;
            SampleID.HeaderText = "Sample";
            SampleID.Width = 170;
            SampleID.Name = "SampleID";
            dataGridView1.Columns.Add(SampleID);

            SampleType.ReadOnly = true;
            SampleType.Resizable = DataGridViewTriState.False;
            SampleType.SortMode = DataGridViewColumnSortMode.Automatic;
            SampleType.HeaderText = "SampleType";
            SampleType.Width = 80;
            SampleType.Name = "SampleType";
            dataGridView1.Columns.Add(SampleType);

            Test.ReadOnly = true;
            Test.Resizable = DataGridViewTriState.False;
            Test.SortMode = DataGridViewColumnSortMode.Automatic;
            Test.HeaderText = "Test";
            Test.Width = 100;
            Test.Name = "Test";
            dataGridView1.Columns.Add(Test);
        }

        private void AddValidityResult()
        {
            DataGridViewTextBoxColumn newCol = new DataGridViewTextBoxColumn();
            newCol.ReadOnly = true;
            newCol.Resizable = DataGridViewTriState.False;
            newCol.SortMode = DataGridViewColumnSortMode.NotSortable;
            newCol.HeaderText = "Validity";
            //newCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            newCol.Width = 80;
            newCol.Name = "Validity";
            //DataGridViewCellStyle style = new DataGridViewCellStyle();
            //style.WrapMode = DataGridViewTriState.True;
            //newCol.DefaultCellStyle = style;
            dataGridView1.Columns.Add(newCol);
            DataGridViewTextBoxColumn newColResult = new DataGridViewTextBoxColumn();
            newColResult.ReadOnly = true;
            newColResult.Resizable = DataGridViewTriState.False;
            newColResult.SortMode = DataGridViewColumnSortMode.NotSortable;
            newColResult.HeaderText = "Result";
            newColResult.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            newColResult.MinimumWidth = 200;
            newColResult.Name = "Result";
            DataGridViewCellStyle style = new DataGridViewCellStyle();
            style.WrapMode = DataGridViewTriState.True;
            newColResult.DefaultCellStyle = style;
            dataGridView1.Columns.Add(newColResult);
        }

        private void CouplingFunc(int i)
        {
            if (isCoupling)
            {
                Log.WriteLog(LogType.system, "[UserControl_ReportOperation]- Coupling check");
                for (int n = 0; n < dataGridView1.Rows.Count; n++)
                {
                    if (((string)dataGridView1.Rows[n].Cells[4].Value == (string)dataGridView1.Rows[i].Cells[4].Value) && (i != n))
                    {
                        dataGridView1.Rows[n].Cells[0].Value = (bool)dataGridView1.Rows[i].Cells[0].Value;
                    }
                }
            }
        }

        private void checkBox_ChkAll_CheckedChanged(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[UserControl_ReportOperation]- checkall/uncheck all status is changed");
            if(checkBox_ChkAll.CheckState == CheckState.Checked)
            {
                CheckSelected(true);   
            }
            else if(checkBox_ChkAll.CheckState == CheckState.Unchecked)
            {
                CheckSelected(false);
            }
            dataGridView1.EndEdit();

            this.dataGridView1.Refresh();
        }

        private void CheckSelected(bool IsAllSelected)
        {
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if (Convert.ToString(dataGridView1.Rows[i].Cells[5].Value) == "NC" ||
                        Convert.ToString(dataGridView1.Rows[i].Cells[5].Value) == "PC" ||
                        Convert.ToString(dataGridView1.Rows[i].Cells[5].Value) == "NTC" ||
                        Convert.ToString(dataGridView1.Rows[i].Cells[5].Value) == "LPC" ||
                        Convert.ToString(dataGridView1.Rows[i].Cells[5].Value) == "HPC" ||
                        Convert.ToString(dataGridView1.Rows[i].Cells[5].Value) == "Standard")
                {
                    dataGridView1.Rows[i].Cells[0].ReadOnly = true;
                    dataGridView1.Rows[i].Cells[0].Style.ForeColor = Color.DarkGray;
                }
                else
                {
                    dataGridView1.Rows[i].Cells[0].Value = IsAllSelected;
                }
            }
            dataGridView1.Refresh();
        }

        private void chk_filter_CheckedChanged(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[UserControl_ReportOperation]- filter checked changed");
            if (comboBox1.SelectedIndex > 0)
            {
                FilterFunct((string)comboBox1.SelectedItem);
            }
            else
            {
                FilterFunct();
            }
        }

        private void FilterFunct()
        {
            if (chk_filter.Checked)
            {
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if (!(bool)dataGridView1.Rows[i].Cells[0].Value)
                    {
                        dataGridView1.Rows[i].Visible = false;
                    }
                    else
                    {
                        dataGridView1.Rows[i].Visible = true;
                    }
                }
            }
            else
            {
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    dataGridView1.Rows[i].Visible = true;
                }
            }
        }

        private void FilterFunct(string pagename)
        {
            if (chk_filter.Checked)
            {
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if ((string)dataGridView1.Rows[i].Cells[4].Value == pagename)
                    {
                        if (!(bool)dataGridView1.Rows[i].Cells[0].Value)
                        {
                            dataGridView1.Rows[i].Visible = false;
                        }
                        else
                        {
                            dataGridView1.Rows[i].Visible = true;
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if ((string)dataGridView1.Rows[i].Cells[4].Value == pagename)
                    {
                        dataGridView1.Rows[i].Visible = true;
                    }
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[UserControl_ReportOperation]- coupling status is changed");
            if (isCoupling)
            {
                this.pictureBox1.Image = Resources.unlink;
                isCoupling = false;
            }
            else if (!isCoupling)
            {
                this.pictureBox1.Image = Resources.link;
                isCoupling = true;
            }
        }

        private void btn_preview_Click(object sender, EventArgs e)
        {
            try
            {
                Log.WriteLog(LogType.system, "Report Page - Click to preview report");

                FillReportData(); //Block


                Log.WriteLog(LogType.system, "Finish Fill Report from GUI method");
               
                Form_ReportPreview fm_reportpre = new Form_ReportPreview();

                if (!fm_reportpre.PassValue(reportResult, report, assayLogic))
                {
                    MessageBox.Show("Error: Set value to report form object");
                }
                else
                {
                    fm_reportpre.Show();  
                }
            }
   
            catch (Exception ex)
            {
                throw new System.ArgumentException(ex.Message);   
            }
        }

        private void FillReportData()
        {
            report = new M_Report.SelectedReport();

            report.SelectedResultSample.Clear();

            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                    DataGridViewCheckBoxCell chk = dataGridView1.Rows[i].Cells[0] as DataGridViewCheckBoxCell;
                    if ((bool)chk.Value)
                    {
                        //Problem
                        report.SelectedResultSample.Add(reportResult.GetSampleResultByPos((string)dataGridView1.Rows[i].Cells[2].Value));
                    }
                
            }
            if (report.SelectedResultSample.Count > 0)
            {
                report.SetContetDisplay(true, this.chk_quan.Checked, this.chk_cycle.Checked, this.chk_img.Checked, this.chk_Message.Checked, true);
            }
            else
            {
                report.SetContetDisplay(true, this.chk_quan.Checked, this.chk_cycle.Checked, this.chk_img.Checked, this.chk_Message.Checked, false);
            }
        }

       
        private void dataGridView1_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            
            // Try to sort based on the cells in the current column.
            if (e.Column.HeaderText == "Well")
            {
                if (chk_wellsort.Checked)
                {

                    e.SortResult = InfoConvert.sortWellAscending(e.CellValue1.ToString(), e.CellValue2.ToString());

                }
                else
                {
                    e.SortResult = InfoConvert.sortPositionAscending(e.CellValue1.ToString(), e.CellValue2.ToString());
                }
                e.Handled = true;
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (isInitialized && (e.ColumnIndex == 0) && isCellClicked)
            {
                int i = e.RowIndex;
                CouplingFunc(i);
                this.dataGridView1.Refresh();
                isCellClicked = false;
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0 && e.RowIndex >= 0)
            {
                isCellClicked = true; 
            }
            dataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit);
          
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0 && isInitialized)
            {
                HightDataGridView(dataGridView1.SelectedRows[0].Index);
            }
        }

        private void HightDataGridView(int rowIndex)
        {
            dataGridView1.Rows[rowIndex].Cells[1].Style.SelectionBackColor = dataGridView1.Rows[rowIndex].Cells[1].Style.BackColor;
            dataGridView1.Refresh();
        }

        private void chk_quan_CheckedChanged(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[UserControl_ReportOperation]- is quantitation information checked changed");
        }

        private void chk_cycle_CheckedChanged(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[UserControl_ReportOperation]- is cycle profile checked changed");
        }

        private void chk_img_CheckedChanged(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[UserControl_ReportOperation]- is image checked changed");
        }

        private void chk_Message_CheckedChanged(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[UserControl_ReportOperation]- is Message checked changed");
        }
   
        private void chk_wellsort_CheckedChanged(object sender, EventArgs e)
        {
            dataGridView1.Sort(dataGridView1.Columns[3], ListSortDirection.Ascending);
        }

     
    }
}
