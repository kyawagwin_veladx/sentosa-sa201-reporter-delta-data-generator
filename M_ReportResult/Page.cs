﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 21/11/2013
 * Author: Yang Yi
 * 
 * Description:
 * Report Result: Page class
 * 
 * Date: 19/03/2014 - Liu Rui
 * Modification: Add stdM1
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_ReportResult
{
    /// <summary>
    /// the page class has the list of sample items.
    /// </summary>
    //public class Page
    //{
    //    #region Private Parameters
    //    //private string pageName;
    //    //private string m1;
    //    //private string stdM1;
    //    //private RawInfo rawInfo = new RawInfo();
    //    //private AnalysisInfo analysisInfo = new AnalysisInfo();
    //    //private List<Sample> sampleList = new List<Sample>();
    //    //public bool IsEnable { get; set; }
    //    //public string Unit { get; set; }
    //    #endregion

    //    #region Public Properties
    //    //public string STDM1
    //    //{
    //    //    get { return stdM1; }
    //    //    set { stdM1 = value; }
    //    //}

    //    //public string M1
    //    //{
    //    //    get { return m1.Split('*').Last(); }
    //    //    set { m1 = value; }
    //    //}

    //    //public AnalysisInfo AnalysisInfo
    //    //{
    //    //    get { return analysisInfo; }
    //    //    set { analysisInfo = value; }
    //    //}

    //    //public List<Sample> SampleList
    //    //{
    //    //    get { return sampleList; }
    //    //    set { sampleList = value; }
    //    //}

    //    //public RawInfo RawInfo
    //    //{
    //    //    get { return rawInfo; }
    //    //    set { rawInfo = value; }
    //    //}

    //    //public string PageName
    //    //{
    //    //    get { return pageName; }
    //    //    set { pageName = value; }
    //    //}
    //    #endregion

    //    //public static List<Page> GetPageListByPageName(string pageName, List<Page> pageList)
    //    //{
    //    //    if ((pageName == string.Empty) || (pageList.Count == 0))
    //    //    {
    //    //        throw new ArgumentNullException();
    //    //    }

    //    //    List<Page> target = new List<Page>();

    //    //    foreach (Page page in pageList)
    //    //    {
    //    //        if (page.PageName == pageName)
    //    //        {
    //    //            target.Add(page);
    //    //        }
    //    //    }

    //    //    return target;
    //    //}

       
    //}
}
