﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 15/01/2014
 * Author: Liu Rui
 * 
 * Description:
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_ReportResult
{
    public class AnalysisInfo
    {
        #region Private Member Variables
        private string displayName = string.Empty;
        private string rawChannelReference = string.Empty;
        private double threshold;
        private double leftThreshold;
        private double startX;
        private double stepX;
        private double minXPoints;
        private string importStandardCurve = string.Empty;
        private string xAxisLabel = string.Empty;
        private string yAxisLabel = string.Empty;
        private string slopeCorrection = string.Empty;
        private string dynamicTube = string.Empty;
        private double outliarRemoval;
        private string standardCurveFormula1 = string.Empty;
        private string standardCurveFormula2 = string.Empty;
        private double reactionEfficiency;
        private double rValue;
        private double mValue;
        private double bValue;
        private bool isREThresholdEnabled;
        private bool isDynamicTubeNormalisation;
        private string templateBaseFilename = string.Empty;
        private string n1n2 = string.Empty;
        #endregion

        #region Public Properties
        public string N1N2
        {
            get { return n1n2; }
            set { n1n2 = value; }
        }

        public string ImportStandardCurve
        {
            get { return importStandardCurve; }
            set { importStandardCurve = value; }
        }

        public string DisplayName
        {
            get { return displayName; }
            set { displayName = value; }
        }

        public string TemplateBaseFilename
        {
            get { return templateBaseFilename; }
            set { templateBaseFilename = value; }
        }

        public bool IsDynamicTubeNormalisation
        {
            get { return isDynamicTubeNormalisation; }
            set { isDynamicTubeNormalisation = value; }
        }

        public bool IsREThresholdEnabled
        {
            get { return isREThresholdEnabled; }
            set { isREThresholdEnabled = value; }
        }

        public string RawChannelReference
        {
            get { return rawChannelReference; }
            set { rawChannelReference = value; }
        }

        public string StandardCurveFormula1
        {
            get { return standardCurveFormula1; }
            set { standardCurveFormula1 = value; }
        }

        public string StandardCurveFormula2
        {
            get { return standardCurveFormula2; }
            set { standardCurveFormula2 = value; }
        }

        public double ReactionEfficiency
        {
            get { return reactionEfficiency; }
            set { reactionEfficiency = value; }
        }

        public double RValue
        {
            get { return rValue; }
            set { rValue = value; }
        }

        public double MValue
        {
            get { return mValue; }
            set { mValue = value; }
        }

        public double BValue
        {
            get { return bValue; }
            set { bValue = value; }
        }

        public string XAxisLabel
        {
            get { return xAxisLabel; }
            set { xAxisLabel = value; }
        }

        public string YAxisLabel
        {
            get { return yAxisLabel; }
            set { yAxisLabel = value; }
        }

        public string SlopeCorrection
        {
            get { return slopeCorrection; }
            set { slopeCorrection = value; }
        }

        public string DynamicTube
        {
            get { return dynamicTube; }
            set { dynamicTube = value; }
        }

        public double OutliarRemoval
        {
            get { return outliarRemoval; }
            set { outliarRemoval = value; }
        }

        public double StartX
        {
            get { return startX; }
            set { startX = value; }
        }

        public double MinXPoints
        {
            get { return minXPoints; }
            set { minXPoints = value; }
        }

        public double Threshold
        {
            get { return threshold; }
            set { threshold = value; }
        }

        public double LeftThreshold
        {
            get { return leftThreshold; }
            set { leftThreshold = value; }
        }

        public double StepX
        {
            get { return stepX; }
            set { stepX = value; }
        } 
        #endregion
    }
}
