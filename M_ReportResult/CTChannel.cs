﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 25/01/2014
 * Author: Liu Rui
 * 
 * Description:
 * 
 */
#endregion

using M_CommonMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_ReportResult
{
    public enum CTRange
    {
        belowRange = 1,
        withinRange = 2,
        aboveRange = 3,
        noValue = 4,
        multipleValues = 5,
        noValueNTC = 6,
    }

    public enum ChannelName
    {
        GREEN,
        YELLOW,
        ORANGE,
        RED
    }

    public class CTChannel
    {
        private string channelName = string.Empty;
        private string virusName = string.Empty;
        private string dye = string.Empty;

        private string ctValue = string.Empty;
        private string givenConc = string.Empty;

        private string calcConc = string.Empty;
        private bool isEnabled = false;
        private double threshold = 0;
        private string start = string.Empty;
        private string end = string.Empty;
        private List<double> analysisPoints = new List<double>();
        private List<double> rawPoints = new List<double>();
        private bool selected = false;
        private List<Custom> customList = new List<Custom>();

        public bool Selected
        {
            get { return selected; }
            set { selected = value; }
        }

        public string GivenConc
        {
            get { return givenConc; }
            set { givenConc = value; }
        }

        public string CalcConc
        {
            get { return calcConc; }
            set { calcConc = value; }
        }

        public string Start
        {
            get { return start; }
            set { start = value; }
        }

        public string End
        {
            get { return end; }
            set { end = value; }
        }

        public double Threshold
        {
            get { return threshold; }
            set { threshold = value; }
        }

        public List<double> RawPoints
        {
            get { return rawPoints; }
            set { rawPoints = value; }
        }

        public List<double> AnalysisPoints
        {
            get { return analysisPoints; }
            set { analysisPoints = value; }
        }

        public string Dye
        {
            get { return dye; }
            set { dye = value; }
        }

        public bool IsEnabled
        {
            get { return isEnabled; }
            set { isEnabled = value; }
        }

        public List<Custom> CustomList
        {
            get { return customList; }
            set { customList = value; }
        }

        public string ChannelName
        {
            get { return channelName; }
            set { channelName = value; }
        }

        public string VirusName
        {
            get { return virusName; }
            set { virusName = value; }
        }

        public string CtValue
        {
            get { return ctValue; }
            set { ctValue = value; }
        }

        public Custom GetCustom(CustomHeadText headText)
        {
            Custom target = new Custom();

            foreach (Custom custom in customList)
            {
                if (custom.HeadText == headText)
                {
                    return target = custom;
                }
            }

            return target;
        }
       

        public static CTChannel GetCTChannel(string channelName, List<CTChannel> ctChannelList)
        {
            CTChannel target = new CTChannel();

            foreach (CTChannel ctChannel in ctChannelList)
            {
                if (ctChannel.ChannelName.Equals(channelName,StringComparison.OrdinalIgnoreCase))
                {
                    return target = ctChannel;
                }
            }

            return target;
        }

        public void FillDeltaRnPoints(float[] analysisreading)
        {
            analysisPoints.Clear();

            for (int i = 0; i < analysisreading.Length; i++)
            {
                analysisPoints.Add(Convert.ToDouble(analysisreading[i]));
            }
        }

        //public void FillRawPoints(string[] csvRow, int start)
        public void FillRawPoints(float[] rawreading)
        {
            rawPoints.Clear();

            for (int i = 0; i < rawreading.Length; i++)
            {
                rawPoints.Add(Convert.ToDouble(rawreading[i]));
            }
        }
      
    }
}
