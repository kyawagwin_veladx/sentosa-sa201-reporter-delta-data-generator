﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 21/01/2014
 * Author: Liu Rui
 * 
 * Description:
 * 
 */
#endregion

using M_CommonMethods;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace M_ReportResult
{
    public enum SampleType
    {
        None = 0,
        Standard = 1,
        Sample = 2,
        NTC = 3,
        PC = 5,
        NC = 6,
        HPC = 7,
        LPC = 8,
        PQ=98,
        DeltaSample = 99,
        DeltaNC = 100
    }

    public class SampleResult 
    {
        private string sampleName = string.Empty;
        private string alphaNumPos = string.Empty;
        private int position = -1;
        private SampleType sampleType;

        private string m1 = string.Empty;
        private string pageName = string.Empty;
        public string colour = string.Empty;
        //private bool selected = false;

        private string binBits = string.Empty;
        private string charBits = string.Empty;
        private string testResult = string.Empty;
        private string validity = string.Empty;

        private List<CTChannel> ctChannelList = new List<CTChannel>();

        public string AlphaNumPos
        {
            get { return alphaNumPos; }
            set { alphaNumPos = value; }
        }

        public string CharBits
        {
            get { return charBits; }
            set { charBits = value; }
        }

        public List<CTChannel> CtChannelList
        {
            get { return ctChannelList; }
            set { ctChannelList = value; }
        }

        public string M1
        {
            get { return m1; }
            set { m1 = value; }
        }

        //public bool Selected
        //{
        //    get { return selected; }
        //    set { selected = value; }
        //}

        public Color Colour
        {
            get { return InfoConvert.ConvertStrToColor(this.colour); }
            set { this.colour = value.ToArgb().ToString(); }
        }

        public SampleType SampleType
        {
            get { return sampleType; }
            set { sampleType = value; }
        }

        public string SampleName
        {
            get { return sampleName; }
            set { sampleName = value; }
        }

        public int Position
        {
            get { return position; }
            set { position = value; }
        }

        public string PageName
        {
            get { return pageName; }
            set { pageName = value; }
        }

        public string Validity
        {
            get { return validity; }
            set { validity = value; }
        }

        public string BinBits
        {
            get { return binBits; }
            set { binBits = value; }
        }

        public string TestResult
        {
            get { return testResult; }
            set { testResult = value; }
        }

        //public Color MyColour
        //{

        //    get
        //    {
        //        int pickedColor = Convert.ToInt32(colour);
        //        byte[] bytes = BitConverter.GetBytes(pickedColor);
        //        return Color.FromArgb(bytes[0], bytes[1], bytes[2]);
        //    }
        //}

        public string SampleTypeString(SampleType sam)
        {   
            string str=string.Empty;
            switch((int)sam)
            {
                case 0:
                    str="None";
                    break;
                case 1:
                    str="Standard";
                    break;
                case 2:
                    str="Sample";
                    break;
                case 3:
                    str = "NTC";
                    break;
                case 5:
                    str = "PC";
                    break;
                case 6:
                    str = "NC";
                    break;
                case 7:
                    str = "HPC";
                    break;
                case 8:
                    str = "LPC";
                    break;
                case 99:
                    str = "DeltaSample";
                    break;
                case 100:
                    str = "DeltaNC";
                    break;
            }
            return str;
        }

        public static SampleType GetSampleType(string sampleName, string arraytask)
        {
            SampleType target = SampleType.None;

            try
            {
                //Notice this
                target = (SampleType)Enum.Parse(typeof(SampleType), arraytask);
            }
            catch
            {
                if (Regex.IsMatch(sampleName, "[AV]\\*\\d{6}\\*NC\\*\\*\\d{10}",RegexOptions.IgnoreCase))
                {
                    target = SampleType.NC;
                }
                else if (Regex.IsMatch(sampleName, "[AV]\\*\\d{6}\\*PC\\*\\*\\d{10}",RegexOptions.IgnoreCase))
                {
                    target = SampleType.PC;
                }
                else
                {
                    target = SampleType.Sample;
                }
            }

            return target;
        }

       

        public CTChannel GetCTChannel(string rawChannelRef)
        {
            CTChannel target = new CTChannel();

            if (rawChannelRef != string.Empty)
            {
                foreach (CTChannel ctChannel in ctChannelList)
                {
                    if (ctChannel.ChannelName.Equals(rawChannelRef,StringComparison.OrdinalIgnoreCase))
                    {
                        return target = ctChannel;
                    }
                }
            }

            return target;
        }

        public static SampleResult GetSampleResultByID(string id, List<SampleResult> sampleResultList)
        {
            SampleResult target = new SampleResult();

            foreach (SampleResult sampleResult in sampleResultList)
            {
                if (sampleResult.AlphaNumPos == id)
                {
                    return target = sampleResult;
                }
            }

            return target;
        }

        public static void UpdateAllCharBits(string charBits, List<SampleResult> sampleResultList)
        {
            foreach (SampleResult sampleResult in sampleResultList)
            {
                if (sampleResult.CharBits == string.Empty)
                {
                    sampleResult.CharBits = charBits;
                }
            }
        }
     
        public string GetCombinedCtValue()
        {
            string str = string.Empty;

            for (int i = 0; i < this.ctChannelList.Count; i++)
            {
                if (this.CtChannelList[i].IsEnabled)
                {
                    if (i > 0)
                    {
                        str = str + Environment.NewLine;

                    }

                    str = str + InfoConvert.TitleWithValue(ctChannelList[i].ChannelName, InfoConvert.StringToF2(ctChannelList[i].CtValue));
                }
            }
            return str;
        }

        //internal void ReorderCtChannels(List<CTChannel> channelList)
        //{
        //    // To ensure that every channellist are same with NC.
        //    List<CTChannel> temp = new List<CTChannel>();

        //    ctChannelList.AddRange(temp);
        //}

     

    }
}
