﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 24/03/2014
 * Author: Liu Rui
 * 
 * Description:
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_ReportResult
{
    public enum CustomHeadText
    { 
        DeltaCTValue,
        Ratio,
        IS,
        CalcConc
    }

    public class Custom
    {
        private CustomHeadText headText;
        private string value = string.Empty;
        private bool isShow = false;

        public CustomHeadText HeadText
        {
            get { return headText; }
            set { headText = value; }
        }

        public string Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        public bool IsShow
        {
            get { return isShow; }
            set { isShow = value; }
        }
    }
}
