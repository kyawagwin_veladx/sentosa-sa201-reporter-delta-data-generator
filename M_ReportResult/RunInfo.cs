﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 21/11/2013
 * Author: Yang Yi
 * 
 * Modified Date: 08/01/2014 - Liu Rui
 * Modification: Add List<AnalysisChannel>
 * 
 * Modified Date: 13/01/2014 - Liu Rui
 * Modification: Add List<RawChannel>
 * 
 * Description:
 * Report Result: Run information class
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace M_ReportResult
{   
    /// <summary>
    ///This class includes the qualitative report information(all relevant information for a PCR run)
    /// </summary>
    public class RunInfo
    {
        #region Private Variables
        private string assayName = string.Empty;
        private string runFilePath = string.Empty;
        private string notes = string.Empty;
        private string baseNotes = string.Empty;
        private string runID = string.Empty;
        private string operatorName = string.Empty;
        private string baseOperator = string.Empty;
        private string sysVersion = string.Empty;
        private string runStart = string.Empty;
        private string lastModified = string.Empty;
        private string runEnd = string.Empty;

        private string instumentType = string.Empty;
        private string pcrVolume = string.Empty;
        private string accessMode = string.Empty;//Import SMP or Rex or Start new run

        private bool isSignatureValid;
        #endregion

        #region Public Properties
        public string CombinedVersions
        {
            get
            {
                return GetBaseVersionFromComments(baseNotes)// "Sentosa SA201 Series Software Version: "
                   // + GetBaseVersionFromComments(baseNotes)
                    + Environment.NewLine
                    + "Sentosa SA201 Reporter Version: "
                    + sysVersion;
            }
        }

        private string GetBaseVersionFromComments(string baseNotes)
        {
            if (baseNotes.Contains("Sentosa SA201 Series Software"))
            {
                return baseNotes.Replace("Sentosa SA201 Series Software ", "Sentosa SA201 Series Software Version: ");
            }
            else
            {
                return "Sentosa SA201 Series Software Version: " + " Unknown";
            }
        }

        public string CombinedUsers
        {
            get
            {
                return
                    "Sentosa SA201 Series Software User: "
                    + baseOperator
                    + Environment.NewLine
                    + "Sentosa SA201 Reporter User: "
                    + operatorName;
            }
        }

        public string CombinedComments
        {
            get
            {
                return
                    "Sentosa SA201 Series Software Comments: "
                    + RemoveDefaultComment(baseNotes)
                    + Environment.NewLine 
                    + "Sentosa SA201 Reporter Notes: "
                    + notes;
            }
        }

        private string RemoveDefaultComment(string baseNotes)
        {
            if (baseNotes.Contains("Sentosa SA201 Series Software v1.0.1"))
            {
                return baseNotes.Replace("Sentosa SA201 Series Software v1.0.1", "");
            }
            else if (baseNotes.Contains("Sentosa SA201 Series Software v1.0"))
            {
                return baseNotes.Replace("Sentosa SA201 Series Software v1.0", "");
            }
            else
            {
                return baseNotes;
            }
        }

        public string AccessMode
        {
            get { return accessMode; }
        }

        public string RunFilePath
        {
            get { return runFilePath; }
            set { runFilePath = value; }
        }

        public string AssayName
        {
            get { return assayName; }
            set { assayName = value; }
        }

        public bool IsSignatureValid
        {
            get { return isSignatureValid; }
            set { isSignatureValid = value; }
        }

        //public string Notes
        //{
            //get { return notes; }
            //set { notes = value; }
        //}

        public string BaseNotes
        {
            get { return baseNotes; }
            set { baseNotes = value; }
        }

        //public string SysVersion
        //{
        //    get { return sysVersion; }
        //    set { sysVersion = value; }
        //}

        //public string OperatorName
        //{
            //get
            //{
            //    return operatorName;
            //}
            //set
            //{
            //    operatorName = value;
            //}
        //}

        //public string BaseOperator
        //{
            //get { return baseOperator; }
            //set { baseOperator = value; }
        //}

        public string RunID
        {
            get
            {
                return runID;
            }
            set
            {
                runID = value;
            }
        }

        public string RunStart
        {
            get
            {
                return runStart;
            }
            set
            {
                runStart = value;
            }
        }

        public string LastModified
        {
            get { return lastModified; }
            set { lastModified = value; }
        }

        public string RunEnd
        {
            get
            {
                return runEnd;
            }
            set
            {
                runEnd = value;
            }
        }

        public string InstumentType
        {
            get { return instumentType; }
            set { instumentType = value; }
        }

        public string PCRVolume
        {
            get { return pcrVolume.Replace('�', 'u'); }
            set { pcrVolume = value; }
        }
        #endregion
       
        public void SetAccessMode(string accessMode)
        {
            this.accessMode = accessMode;
        }

        public void SetOperator(string operatorName)
        {
            this.operatorName = operatorName;
        }

        public void SetNotes(string notes)
        {
            this.notes = notes;
        }

        public void SetBaseOperator(string baseOperator)
        {
            this.baseOperator = baseOperator;
        }

        public string GetNotes()
        {
            return this.notes;
        }

        public string GetOperator()
        {
            return this.operatorName;
        }

        public string GetVersion()
        {
            return this.sysVersion;
        }

        public string GetBaseOperator()
        {
            return this.baseOperator;
        }

        public void SetVersion(string version)
        {
            this.sysVersion = version;
        }
    }
}
