﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 21/11/2013
 * Author: Yang Yi
 * 
 * Description:
 * Report Result: Cycling class
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_ReportResult
{
    /// <summary>
    /// The cycling class contains the list of step items
    /// </summary>
    public class Cycling
    {
        #region Private Member Variables
        private string cycleName = string.Empty;
        private string repeatCount = string.Empty;
        private List<Step> stepList = new List<Step>();
        #endregion

        #region Public Properties
        public string CycleName
        {
            get
            {
                return cycleName;
            }
            set
            {
                cycleName = value;
            }
        }

        public string RepeatCount
        {
            get
            {
                return repeatCount;
            }
            set
            {
                repeatCount = value;
            }
        }

        public List<Step> StepList
        {
            get
            {
                return stepList;
            }
            set
            {
                stepList = value;
            }
        }
        #endregion


        //private int stage = 0;
        //private int temperature = 0;
        //private int time = 0;
        //private int step = 0;
        //private int repetition = 0;
        //private bool isShowStep = false;
        //private bool isShowRepetition = false;
        //private bool isShowStage = false;

        //public int Stage
        //{
        //    get 
        //    {
        //        return stage;
        //    }
        //}

        //public int Temperature
        //{
        //    get
        //    {
        //        return temperature;
        //    }
        //}

        //public int Time
        //{
        //    get
        //    {
        //        return time;
        //    }
        //}

        //public int Step
        //{
        //    get
        //    {
        //        return step;
        //    }
        //}

        //public int Repetition
        //{
        //    get
        //    {
        //        return repetition;
        //    }
        //}

        //public bool IsShowStep
        //{
        //    get 
        //    {
        //        return isShowStep;
        //    }
        //    set
        //    {
        //        if (repetition > 1 || )
        //        {
        //            isShowStep = true;
        //        }
        //    }
        //}

        //public bool IsShowStage
        //{
        //    get 
        //    {
        //        return isShowStage;
        //    }
        //    set
        //    {
        //        if (stage != 0)
        //        {
        //            isShowStage = true;
        //        }
        //    }
        //}

        //public bool IsShowRepetition
        //{
        //    get
        //    {
        //        return isShowRepetition;
        //    }
        //    set
        //    {
        //        if (repetition > 1 && IsShowStage)
        //        {
        //            isShowRepetition = true;
        //        }
        //    }
        //}
    }
}
