﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 21/11/2013
 * Author: Yang Yi
 * 
 * Description:
 * Report Result: Hold class
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_ReportResult
{
    /// <summary>
    /// 
    /// </summary>
    public class Hold
    {
        #region Private Member Varibles
        private string holdname = string.Empty;
        private string temperature = string.Empty;
        private string second = string.Empty;
        #endregion

        #region Public Properties
        public string HoldName 
        {
            get
            {
                return holdname;
            }
            set
            {
                holdname = value;
            }
        }

        public string Temperature
        {
            get
            {
                return temperature.Replace('�', (char)176);
            }
            set
            {
                temperature = value;
            }
        }
        
        public string Second 
        {
            get
            {
                return second;
            }
            set
            {
                second = value;
            }
        }
        #endregion
      
    }
}
