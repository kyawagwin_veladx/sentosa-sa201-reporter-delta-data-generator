﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 21/11/2013
 * Author: Yang Yi
 * 
 * Description:
 * Report Result: step class
 * 
 */
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_ReportResult
{
    /// <summary>
    /// 
    /// </summary>
    public class Step
    {
        #region Private Member Variables
        private string stepName = string.Empty;
        private string temperature = string.Empty;
        private string second = string.Empty;
        //private ArrayList acquiredTo = new ArrayList();
        #endregion

        #region Public Properties
        public string StepName 
        {
            get
            {
                return stepName;
            }
            set
            {
                stepName = value;
            }
        }

        public string Temperature
        {
            get
            {
                  return temperature.Replace('�', (char)176);;
            }
            set
            {
                temperature = value;
            }
        }

        public string Second
        {
            get
            {
                return second;
            }
            set
            {
                second = value;
            }
        }

        //public ArrayList AcquiredTo 
        //{
        //    get
        //    {
        //        return acquiredTo;
        //    }
        //    set
        //    {
        //        acquiredTo = value;
        //    }
        //}
        #endregion
    }
}
