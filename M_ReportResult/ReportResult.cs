﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 21/11/2013
 * Author: Yang Yi
 * 
 * Description:
 * Report Result: Report result class
 * 
 */
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using M_CommonMethods;

namespace M_ReportResult
{
    /// <summary>
    /// this class named of ReportResult which contains the runinfo and runprofile these two classes
    /// </summary>
    public class ReportResult
    {
        #region Private Variables
        private string message = string.Empty;
        private RunInfo runInfo = new RunInfo();
        private List<IDListItem> iDListItems = new List<IDListItem>();
        private RunProfile runProfile = new RunProfile();
        private RawInfo rawInfo = new RawInfo();
        private List<SampleResult> sampleResultList = new List<SampleResult>();
        #endregion

        #region Public Properties
        public List<IDListItem> IDListItems
        {
            get { return iDListItems; }
            set { iDListItems = value; }
        }

        public RawInfo RawInfo
        {
            get { return rawInfo; }
            set { rawInfo = value; }
        }

        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        public RunProfile RunProfile
        {
            get { return runProfile; }
            set { runProfile = value; }
        }

        public RunInfo RunInfo
        {
            get { return runInfo; }
            set { runInfo = value; }
        }

        public List<SampleResult> SampleResultList
        {
            get { return sampleResultList; }
            set { sampleResultList = value; }
        }
        #endregion

        public static string GetChannelShortName(string channelName)
        {
            string targetShortName = string.Empty;

            foreach (ChannelName channel in Enum.GetValues(typeof(ChannelName)))
            {
                if (channelName.ToUpper().Contains(channel.ToString()))
                {
                    targetShortName = channel.ToString();
                    break;
                }
            }
            return targetShortName;
        }

        public List<SampleResult> GetSampleResultsByPage(string pageName)
        {
            List<SampleResult> targetSampleResultList = new List<SampleResult>();
            foreach (SampleResult sampleResult in sampleResultList)
            {
                if (sampleResult.PageName == pageName)
                {
                    targetSampleResultList.Add(sampleResult);
                }
            }
            return targetSampleResultList;
        }

        public List<string> GetUniquePageNameList()
        {
            List<string> pageList = new List<string>();

            //Get from sampleResultList:
            if (sampleResultList.Count > 0)
            {
                foreach (SampleResult sampleResult in sampleResultList)
                {
                    if (!pageList.Contains(sampleResult.PageName))
                    {
                        pageList.Add(sampleResult.PageName);
                    }
                }
            }

            return pageList;
        }

        private string GetSummaryResult(List<int> groupIndex)
        {
            string str = string.Empty;
            foreach (int i in groupIndex)
            {
                str = str + sampleResultList[i].TestResult + Environment.NewLine;
            }
            str = str.Substring(0, str.Length - 1);
            return str;
        }

        private string GetSummaryRatio(List<int> groupIndex)
        {
            string str = string.Empty;

            foreach (int i in groupIndex)
            {
                foreach (CTChannel j in sampleResultList[i].CtChannelList)
                {
                    if (j.CustomList[0].Value != string.Empty)
                    {
                        str = str + j.CustomList[0].Value + Environment.NewLine;
                    }
                    else
                    {
                        str = str + "-" + Environment.NewLine;
                    }
                }
            }
            str = str.Substring(0, str.Length - 1);
            return str;
        }

        private string GetSummaryIs(List<int> groupIndex)
        {
            string str = string.Empty;

            foreach (int i in groupIndex)
            {
                foreach (CTChannel j in sampleResultList[i].CtChannelList)
                {
                    if (j.CustomList[1].Value != string.Empty)
                    {
                        str = str + j.CustomList[1].Value + Environment.NewLine;
                    }
                    else
                    {
                        str = str + "-" + Environment.NewLine;
                    }
                }
            }
            str = str.Substring(0, str.Length - 1);
            return str;
        }

        private string GetSummaryValidity(List<int> groupIndex)
        {
            string str=sampleResultList[0].Validity;

            foreach (int i in groupIndex)
            {
                if(sampleResultList[i].Validity.Contains("Invalid"))
                {
                   str = sampleResultList[i].Validity;
                   break;
                }
            }
           
            return str;
        }

        private string GetPositions(List<int> groupIndex)
        {
            string str = string.Empty;

            for (int i = 0; i < groupIndex.Count; i++)
            {
                str = str + (sampleResultList[groupIndex[i]].Position).ToString() + Environment.NewLine;

            }
            str = str.Substring(0, str.Length - 1);
            return str;
        }

        //Get All positions base on sample ID and remove the indexs from SelectedIndex
        private List<int> GroupSameSamples(List<int> SelectedIndex, int index)
        {
            List<int> GroupIndexs = new List<int>();
            GroupIndexs.Add(SelectedIndex[index]);
            int i = index + 1;

            while (i < SelectedIndex.Count)
            {
                if (sampleResultList[SelectedIndex[index]].SampleName == sampleResultList[SelectedIndex[i]].SampleName)
                {
                    GroupIndexs.Add(SelectedIndex[i]);
                    SelectedIndex.RemoveAt(i);
                }
                else
                {
                    i++;
                }
            }
            return GroupIndexs;
        }
              
        public int GetSampleResultIndex(SampleType sampleType)
        {
            int target = -1;

            for (int i =0; i<sampleResultList.Count;i++)
            {
                if (sampleResultList[i].SampleType == sampleType)
                {
                    return target = i;
                }
            }

            return target;
        }

      
        public void FillDye(string position, string channelName, string dye)
        {
            foreach (SampleResult sampleResult in sampleResultList)
            {
                if (sampleResult.Position.ToString()==position)
                {
                    foreach (CTChannel ctChannel in sampleResult.CtChannelList)
                    {
                        if (channelName.ToUpper().Contains(ctChannel.ChannelName.ToUpper()))
                        {
                            ctChannel.Dye = dye;
                            break;
                        }
                    }
                    break;
                }
            }
        }

        public CTChannel GetCTChannelByDye(string dye, string position)
        {
            CTChannel target = new CTChannel();

            foreach (SampleResult sampleResult in sampleResultList)
            {
                if (sampleResult.Position.ToString() == position)
                {
                    foreach (CTChannel ctChannel in sampleResult.CtChannelList)
                    {
                        if (ctChannel.Dye == dye)
                        {
                            return target = ctChannel;
                        }
                    }
                }
            }

            return target;
        }

        public CTChannel GetCTChannelByChannel(string channel, string position)
        {
            CTChannel target = new CTChannel();

            foreach (SampleResult sampleResult in sampleResultList)
            {
                if (sampleResult.Position.ToString() == position)
                {
                    foreach (CTChannel ctChannel in sampleResult.CtChannelList)
                    {
                        if (ctChannel.ChannelName.Equals(channel,StringComparison.OrdinalIgnoreCase))
                        {
                            return target = ctChannel;
                        }
                    }
                }
            }

            return target;
        }

        public static SampleResult GetSampleResultByType(SampleType sampleType, string pageName, List<SampleResult> sampleResultList)
        {
            SampleResult targetSampleResult = new SampleResult();
            foreach (SampleResult sampleResult in sampleResultList)
            {
                if ((sampleResult.SampleType == sampleType) && (sampleResult.PageName == pageName))
                {
                    return targetSampleResult = sampleResult;
                }
            }
            return targetSampleResult;
        }

        public static List<SampleResult> ExcludeSampleResultByType(string pageName, SampleType sampleType, List<SampleResult> sampleResultList)
        {
            List<SampleResult> targetSampleList = new List<SampleResult>();

            foreach (SampleResult sampleResult in sampleResultList)
            {
                if (!((sampleResult.PageName == pageName)
                    && (sampleResult.SampleType == sampleType)))
                {
                    targetSampleList.Add(sampleResult);
                }
            }
            return targetSampleList;
        }

        public bool IsAssayIncluded(string assayName, string[] assayList)
        {
            bool target = false;

            foreach (string assay in assayList)
            {
                if (assay == assayName)
                {
                    return target = true;
                }
            }

            return target;
        }

        public void UpdateThreshold(string channelName, double threshold)
        {
            foreach (SampleResult sampleResult in sampleResultList)
            {
                foreach (CTChannel ctChannel in sampleResult.CtChannelList)
                {
                    if (ctChannel.ChannelName.Equals(channelName,StringComparison.OrdinalIgnoreCase))
                    {
                        ctChannel.Threshold = threshold;
                        break;
                    }
                }
            }
        }

        public void UpdateStartEnd(string channelName, string start, string end)
        {
            foreach (SampleResult sampleResult in sampleResultList)
            {
                foreach (CTChannel ctChannel in sampleResult.CtChannelList)
                {
                    if (ctChannel.ChannelName.Equals(channelName,StringComparison.OrdinalIgnoreCase))
                    {
                        if (start == "0")
                        {
                            ctChannel.Start = "Auto";
                        }
                        else
                        {
                            ctChannel.Start = start;
                        }

                        if (end == "0")
                        {
                            ctChannel.End = "Auto";
                        }
                        else
                        {
                            ctChannel.End = end;
                        }
                        break;
                    }
                }
            }
        }

        public void UpdateVirusName(string channelName, string virusName)
        {
            foreach (SampleResult sampleResult in sampleResultList)
            {
                foreach (CTChannel ctChannel in sampleResult.CtChannelList)
                {
                    ctChannel.VirusName = virusName;
                }
            }
        }

        public void FillPageNames(string pageName)
        {
            foreach (SampleResult sampleResult in sampleResultList)
            {
                sampleResult.PageName = pageName;
            }
        }

        public void FillM1s(string m1)
        {
            foreach (SampleResult sampleResult in sampleResultList)
            {
                sampleResult.M1 = m1;
            }
        }

        public SampleResult GetSampleResultByPos(string pos)
        {
            SampleResult ret = null;
            foreach (SampleResult sr in sampleResultList)
            {
                if (sr.Position.ToString() == pos)
                {
                    ret = sr;
                    break;
                }
            }
            return ret;
        }


        public string GetM1Lot()
        {
            return this.sampleResultList[0].M1.Split('*').Last();
        }


        public int GetChannelIndex(string channelName, string pageName)
        {
            int target = -1;

            foreach (SampleResult sr in sampleResultList)
            {
                if (sr.PageName == pageName)
                {
                    for (int i = 0; i < sr.CtChannelList.Count; i++)
                    {
                        if (sr.CtChannelList[i].ChannelName.Equals(channelName,StringComparison.OrdinalIgnoreCase))
                        {
                            return target = i;
                        }
                    }
                }
            }

            return target;
        }

        public CTChannel GetCTChannel(string currentChannel, string currentPage)
        {
            CTChannel target = new CTChannel();

            foreach (SampleResult sr in sampleResultList)
            {
                if (sr.PageName == currentPage)
                {
                    foreach (CTChannel channel in sr.CtChannelList)
                    {
                        if (channel.ChannelName.Equals(currentChannel,StringComparison.OrdinalIgnoreCase))
                        {
                            return target = channel;
                        }
                    }
                }
            }

            return target;
        }

        public void UpdateSelected(string well, string currentChannel, string currentPage, bool flag)
        {
            foreach (SampleResult sr in this.sampleResultList)
            {
                if ((sr.PageName == currentPage)&&(sr.AlphaNumPos==well))
                {
                    foreach (CTChannel channel in sr.CtChannelList)
                    {
                        if (channel.ChannelName.Equals(currentChannel,StringComparison.OrdinalIgnoreCase))
                        {
                            channel.Selected = flag;
                            return;
                        }
                    }
                }
            }
        }
    
        //public void ReorderCtChannels()
        //{
        //    if (sampleResultList != null && sampleResultList.Count() > 0)
        //    {
                
        //        foreach (SampleResult sr in sampleResultList)
        //        {
        //            var channelList = sampleResultList.Where(x => x.PageName.Equals(sr.PageName)).First().CtChannelList;
        //            sr.CtChannelList = channelList;
        //        }
        //    }
            
        //}
    }
}
