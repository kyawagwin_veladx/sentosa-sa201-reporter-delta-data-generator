﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 21/11/2013
 * Author: Yang Yi
 * 
 * Modified Date: 08/01/2014 - Liu Rui
 * Modification: Add all methods
 * 
 * Description:
 * Report Result: sample class
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace M_ReportResult
{
    //public enum SampleType
    //{
    //    None = 0,
    //    Standard = 1,
    //    Sample = 2,
    //    NTC = 3,
    //    PC = 5,
    //    NC = 6,
    //    HPC = 7,
    //    LPC = 8,
    //    DeltaSample = 99,
    //    DeltaNC = 100
    //}

    public class Sample
    {
        /// <summary>
        /// Sample: No.=position, color=color, Sample ID= name, Name of Assay=assayname, sample Type= sampleType,
        /// ct green channel=ctGreenValue, ct red channel=ctRedValue, ct Yellow channel=ctYellowValue, 
        /// ct Orange channel=ctOrangeValue, validity=validity, Result= testResult.
        /// </summary>

        #region Private Member Variables
        //id = No.
        //private string id = string.Empty;
        //private string colour = string.Empty;
        //private int position;
        //private string pageName = string.Empty;
        //private string name = string.Empty;
        //private SampleType sampleType;
        //private string ctValue = string.Empty;
        //private string ctComment = string.Empty;
        //private string givenConc = string.Empty;
        //private string calcConc = string.Empty;
        //private string repCT = string.Empty;
        //private bool selected = false;

        //private List<double> analysisPoints = new List<double>();
        //private List<double> rawPoints = new List<double>();
        #endregion

        #region Public Properties
        //public bool Selected
        //{
        //    get { return selected; }
        //    set { selected = value; }
        //}

        //public string RepCT
        //{
        //    get { return repCT; }
        //    set { repCT = value; }
        //}

        //public List<double> AnalysisPoints
        //{
        //    get { return analysisPoints; }
        //    set { analysisPoints = value; }
        //}

        //public List<double> RawPoints
        //{
        //    get { return rawPoints; }
        //    set { rawPoints = value; }
        //}

        //public string CalcConc
        //{
        //    get { return calcConc; }
        //    set { calcConc = value; }
        //}

        //public string GivenConc
        //{
        //    get { return givenConc; }
        //    set { givenConc = value; }
        //}

        //public string ID
        //{
        //    get
        //    {
        //        return id;
        //    }
        //    set
        //    {
        //        id = value;
        //    }
        //}
        //Delete in future
        //public string Colour
        //{
        //    get
        //    {
        //        return colour;
        //    }
        //    set
        //    {
        //        colour = value;
        //    }
        //}

        //public Color MyColour
        //{

        //    get
        //    {
        //            int pickedColor = Convert.ToInt32(colour);
        //            byte[] bytes = BitConverter.GetBytes(pickedColor);
        //            return Color.FromArgb(bytes[0], bytes[1], bytes[2]);
        //     }
        //}

        //public int Position
        //{
        //    get
        //    {
        //        return position;
        //    }
        //    set
        //    {
        //        position = value;
        //    }
        //}

        //public string Name
        //{
        //    get
        //    {
        //        return name;
        //    }
        //    set
        //    {
        //        name = value;
        //    }
        //}

        //public string PageName
        //{
        //    get
        //    {
        //        return pageName;
        //    }
        //    set
        //    {
        //        pageName = value;
        //    }
        //}

        //public SampleType SampleType
        //{
        //    get
        //    {
        //        return sampleType;
        //    }
        //    set
        //    {
        //        sampleType = value;
        //    }
        //}

        //public string SampleTypeString(SampleType sam)
        //{
        //    string str = string.Empty;
        //    switch ((int)sam)
        //    {
        //        case 0:
        //            str = "None";
        //            break;
        //        case 1:
        //            str = "Standard";
        //            break;
        //        case 2:
        //            str = "Sample";
        //            break;
        //        case 3:
        //            str = "NTC";
        //            break;
        //        case 5:
        //            str = "PC";
        //            break;
        //        case 6:
        //            str = "NC";
        //            break;
        //        case 7:
        //            str = "HPC";
        //            break;
        //        case 8:
        //            str = "LPC";
        //            break;
        //        case 99:
        //            str = "DeltaSample";
        //            break;
        //        case 100:
        //            str = "DeltaNC";
        //            break;
        //    }
        //    return str;
        //}

        //public string CTValue
        //{
        //    get
        //    {
        //        return ctValue;
        //    }
        //    set
        //    {
        //        ctValue = value;
        //    }
        //}

        //public string CTComment
        //{
        //    get
        //    {
        //        return ctComment;
        //    }
        //    set
        //    {
        //        ctComment = value;
        //    }
        //}

        //public string CalConDisplay()
        //{
        //    if (calcConc != string.Empty)
        //    {
        //        return Math.Pow(10, Convert.ToDouble(calcConc)).ToString();
        //    }
        //    else
        //    {
        //        return string.Empty;
        //    }
 
        //}
        #endregion

        //public static SampleType GetSampleType(string sampleName, int taskIndex, int sampleNameIndex, string[] array)
        //{
        //    SampleType target = SampleType.None;

        //    try
        //    {
        //        target = (SampleType)Enum.Parse(typeof(SampleType), array[taskIndex]);
        //    }
        //    catch
        //    {
        //        try
        //        {
        //            target = (SampleType)Enum.Parse(typeof(SampleType), array[sampleNameIndex].ToUpper());
        //        }
        //        catch
        //        {
        //            switch (sampleName.ToUpper())
        //            {
        //                case "NEGATIVE CONTROL":
        //                case "NC":
        //                    target = SampleType.NC;
        //                    break;

        //                case "POSITIVE CONTROL":
        //                case "PC":
        //                    target = SampleType.PC;
        //                    break;

        //                case "NO TEMPLATE CONTROL":
        //                case "NTC":
        //                    target = SampleType.NTC;
        //                    break;

        //                case "QS":
        //                case "STANDARD":
        //                    target = SampleType.Standard;
        //                    break;

        //                default:
        //                    target = SampleType.Sample;
        //                    break;
        //            }
        //        }
        //    }

        //    return target;
        //}
    }
}
