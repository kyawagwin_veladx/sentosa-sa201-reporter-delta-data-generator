﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 21/11/2013
 * Author: Yang Yi
 * 
 * Description:
 * Report Result: Run profile class
 * 
 */
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_ReportResult
{
    /// <summary>
    /// The class includes Hold, Cycling and ArrayList these three sub-class.
    /// </summary>
    public class RunProfile
    {
        #region Private Variables
        private ArrayList holdCycleList = new ArrayList();
      
        private string dataCollection = string.Empty;
        #endregion

        #region Public Properties
        public ArrayList HoldCycleList
        {
            get
            {
                return holdCycleList;
            }
            set
            {
                holdCycleList = value;
            }
        }

        public string DataCollection
        {
            get
            {
                return dataCollection;
            }
            set
            {
                dataCollection = value;
            }
        }

        #endregion
    }
}
