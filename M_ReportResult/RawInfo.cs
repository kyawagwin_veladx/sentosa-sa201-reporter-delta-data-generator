﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 13/01/2013
 * Author: Liu Rui
 * 
 * Description:
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M_ReportResult
{
    public class RawInfo
    {
        #region Private Member Variables
        private string startX;
        private string stepX;
        private string minXPoints;
        private string xAxisLabel;
        private string yAxisLabel;
        #endregion

        #region Public Properties
        public string StartX
        {
            get
            {
                return startX;
            }
            set
            {
                startX = value;
            }
        }

        public string StepX
        {
            get
            {
                return stepX;
            }
            set
            {
                stepX = value;
            }
        }

        public string MinXPoints
        {
            get
            {
                return minXPoints;
            }
            set
            {
                minXPoints = value;
            }
        }

        public string XAxisLabel
        {
            get
            {
                return xAxisLabel;
            }
            set
            {
                xAxisLabel = value;
            }
        }

        public string YAxisLabel
        {
            get
            {
                return yAxisLabel;
            }
            set
            {
                yAxisLabel = value;
            }
        }
        #endregion
    }
}
