﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 24/04/2013
 * Author: Liu Rui
 * 
 * Description:
 * String settings for Report XML
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_ReportResult
{
    internal static class Settings
    {
        #region Public Member Variables

        public static string pageNodePath = "/Experiment/Samples/Page";
        //public static string sampleNodePath = "/Experiment/Samples/Page/Sample";
        public static string rowNodePath = "/Experiment/Analyses/Quantitate/DataTable/Row";
        public static string analysisChannelNodePath = "/Experiment/Analyses/Quantitate";
        public static string rawChannelNodePath = "/Experiment/RawChannels/RawChannel";

        public static string columnNodePath = "/Experiment/Analyses/Quantitate/DataTable/Row/Col";
        public static string templateName = "/Experiment/TemplateFilename";
        public static string runIDNodePath = "/Experiment/RunId";
        public static string operatorNode = "Operator";
        public static string softwareVersionNode = "RunOnSoftwareVersion";
        public static string notesNodePath = "/Experiment/Notes";
        public static string signatureNodePath = "/Experiment/SignatureState";
        public static string runStartNode = "StartTime";
        public static string runFinishNode = "FinishTime";

        public static string profileNodePath = "/Experiment/Profile";
        public static string holdNodePath = "/Experiment/Profile/HoldCycle";
        public static string cycleNodePath = "/Experiment/Profile/Cycle";
        public static string stepNodePath = "/Experiment/Profile/Cycle/NormalCyclePoint";

        public static string experimentNode = "Experiment";
        //public static string pageName = "Name";
        public static string pageNode = "Page";
        public static string typeNode = "Type";
        public static string colour = "Colour";
        public static string color = "Color";
        public static string sampleID = "ID";
        public static string sampleNode = "Sample";
        public static string tubePosition = "TubePosition";
        //public static string sampleName = "Name";
        public static string selectedNode = "Selected";
        public static string dataColumn = "DataCol";
        public static string idNoNode = "No.";
        public static string idNode = "ID";
        public static string ct = "Ct";
        public static string ctComment = "Ct Comment";
        public static string data = "Data";
        public static string column = "Col";
        public static string holdCycle = "HoldCycle";
        public static string cycle = "Cycle";
        public static string holdFor = "HoldFor";
        public static string repeatCount = "RepeatCount";
        public static string temperature = "Temperature";
        public static string remainFor = "RemainFor";
        public static string acquireTo = "AcquireTo";
        public static string channelType = "ChannelTypeRef";
        public static string green = "Green";
        public static string red = "Red";
        public static string yellow = "Yellow";
        public static string orange = "Orange";
        public static string recordNode = "Record";
        public static string fieldNode = "Field";
        public static string nameNode = "Name";
        public static string trueUpper = "TRUE";
        public static string serialNumberValue = "Machine Serial No";
        public static string valueNode = "Value";
        public static string rootNode = "Experiment";
        public static string displayNameNode = "DisplayName";
        public static string thresholdNode = "Threshold";
        public static string leftThresholdNode = "LeftThreshold";
        public static string processdChannelNode = "ProcessedChannel";
        public static string minXPointsNode = "MinXPoints";
        public static string startXNode = "StartX";
        public static string stepXNode = "StepX";
        public static string xAxisLabelNode = "XAxisLabel";
        public static string yAxisLabelNode = "YAxisLabel";
        public static string normalisationSettingsNode = "NormalisationSettings";
        public static string slopCorrectionNode = "NormaliseSlopeCorrection";
        public static string dynamicTubeNode = "NormaliseDynamicTube";
        public static string outliarRemovalNode = "NormaliseNTCPercentage";
        public static string formula1Node = "StandardCurveFormula1";
        public static string formula2Node = "StandardCurveFormula2";
        public static string efficiency = "ReactionEfficiency";
        public static string rValueNode = "RValue";
        public static string mValueNode = "MValue";
        public static string bValueNode = "BValue";
        public static string rawChannelRefNode = "RawChannelRef";
        public static string dataTableNode = "DataTable";
        public static string rowNode = "Row";
        public static string givenConc = "Given Conc";
        public static string calcConc = "Calc Conc";
        public static string analysisFlag = "Analysis";
        public static string rawFlag = "Raw";
        public static string reading = "Reading";
        public static string pc = "Positive Control";
        public static string nc = "Negative Control";
        public static string standard = "Standard";
        public static string unknown = "Unknown";
        public static string repCT = "Rep. Ct";
        public static string samplesNode = "Samples";
        public static string concentrationUnitNode = "ConcentrationUnit";
        public static string notesNode = "Notes";
        public static string signatureStateNode = "SignatureState";
        public static string channelTypeNode = "ChannelType";
        public static string channelTypesNode = "ChannelTypes";
        public static string gainNode = "Gain";
        public static string templateBaseFilenameNode = "TemplateBaseFilename";
        public static string normaliseMinimumReactionEffOnNode = "NormaliseMinimumReactionEffOn";
        public static string normaliseDynamicTubeNode = "NormaliseDynamicTube";
        public static string reagentsNode = "Reagents";
        public static string reagentNode = "Reagent";
        public static string bestFitFixedNode = "BestFitFixed";
        public static string dynamicTubeOptimisationPath = "/NormalisationSettings/DynamicTubeOptimisation";
        public static string minTakeOffPointNode = "MinTakeOffPoint";
        public static string takeOffSetValueNode = "TakeOffSetValue";
        public static string dynamicTubeOptimisationNode = "DynamicTubeOptimisation";
        #endregion

    }
}
