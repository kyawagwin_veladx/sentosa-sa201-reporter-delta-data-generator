﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using M_ReportResult;
using M_Assay;

namespace M_RunFileCheck
{
    public class RunFileCheck
    {
        public bool FuncRunFileCheck(AssayInfo assayInfo, ReportResult reportResult)
        {
            if (assayInfo.ChannelList.Count == reportResult.SampleResultList[0].CtChannelList.Count)
            {
                int count = 0;
                for (int i = 0; i < reportResult.SampleResultList[0].CtChannelList.Count; i++)
                {
                   for(int j=0; j<assayInfo.ChannelList.Count;j++)
                   {
                       if (assayInfo.ChannelList[j].ShortName.ToUpper() == reportResult.SampleResultList[0].CtChannelList[i].ChannelName.ToUpper())
                       {
                           count++;
                           break;
                       }
                   }
                }

                if (reportResult.SampleResultList[0].CtChannelList.Count == count)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
