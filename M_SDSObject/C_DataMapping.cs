﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using M_ReportResult;

namespace M_SDSObject
{
    public class C_DataMapping
    {
        ReportResult C_DataMapping_reportresult;
        SDSObject C_DataMapping_sdsobject;

        public C_DataMapping(SDSObject sdsobj, ReportResult reportresult)
        {
            this.C_DataMapping_sdsobject = sdsobj;
            this.C_DataMapping_reportresult = reportresult;
        }

        public void Func_DataMapping()
        {
            
        }

        private void RunInfo_DataMapping()
        {
            this.C_DataMapping_reportresult.RunInfo.InstumentType = this.C_DataMapping_sdsobject.Instrument_type;

            this.C_DataMapping_reportresult.RunInfo.RunFilePath = this.C_DataMapping_sdsobject.Doc_name;

           // this.C_DataMapping_reportresult.RunInfo.GetOperator = this.C_DataMapping_sdsobject.Operator_name;

            this.C_DataMapping_reportresult.RunInfo.LastModified = this.C_DataMapping_sdsobject.Last_modified_datetime;

           // this.C_DataMapping_reportresult.RunInfo.

        }

        private void Samplelist_DataMapping()
        {
           foreach(SDS_Sample sds_sample in this.C_DataMapping_sdsobject.SDS_samples)
           {
              SampleResult sample_result=new SampleResult();

              sample_result.Position = Convert.ToInt32(sds_sample.Numeric_position);

              sample_result.AlphaNumPos = sds_sample.Alphanumeric_position;

              sample_result.SampleName = sds_sample.Sample_name;

             // sample_result.SampleType = (SampleType)sample.Sample_types;

              sample_result.CtChannelList = Channellist_DataMapping(sds_sample);

              this.C_DataMapping_reportresult.SampleResultList.Add(sample_result);
           }
          
        }

        private List<CTChannel> Channellist_DataMapping(SDS_Sample sample)
        {
            List<CTChannel> channelist = new List<CTChannel>();

            foreach(SDS_Channel sds_channel in sample.SDS_channels)
            {
                CTChannel ctChannel = new CTChannel();

                ctChannel.ChannelName = sds_channel.Detector;
                ctChannel.Dye = sds_channel.Dye;

                ctChannel.RawPoints = sds_channel.Raw_points;
                ctChannel.AnalysisPoints = sds_channel.Analysis_points;

                ctChannel.CtValue = sds_channel.CT_value;

            }

            return channelist;
        }
    }
}
