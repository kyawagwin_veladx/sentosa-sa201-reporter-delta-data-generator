﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_SDSObject
{
    public class SDSObject
    {
        //public string threshold{get;set;}

        private List<SDS_Sample> sds_samples = new List<SDS_Sample>();

        public List<SDS_Sample> SDS_samples
        {
            get { return sds_samples; }
            set { sds_samples = value; }
        }

        public string Doc_name { get; set; }

        public string User { get; set; }

        public string Operator_name { get; set; }

        public string Run_datetime { get; set; }

        public string Last_modified_datetime { get; set; }

        public string Instrument_type { get; set;}

        public string Comments { get; set; }

        public string PCRvolume { get; set; }

        string cycle_profile;   
    }

    public class SDS_Sample
    {
        public string Numeric_position { get; set; }
        
        public string Alphanumeric_position { get; set; }

        public string Sample_name { get; set; }

        public string Sample_types { get; set; }

        private List<SDS_Channel> sds_channels = new List<SDS_Channel>();

        public List<SDS_Channel> SDS_channels
        {
            get { return sds_channels; }
            set { sds_channels = value; }
        }
    }

    public class SDS_Channel
    {
        public string Dye { get; set; }

        public string Detector { get; set; }

        private List<double> raw_points = new List<double>();
        public List<double> Raw_points
        {
            get { return raw_points; }
            set { raw_points = value; }
        }

        private List<double> analysis_points = new List<double>();
        public List<double> Analysis_points
        {
            get { return analysis_points; }
            set { analysis_points = value; }
        }

        public string CT_value { get; set; }
    }
}
