﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 01/11/2013
 * Author: Liu Rui
 * 
 * Description:
 * 
 */
 
#endregion

using System;
using System.Windows.Forms;

using M_Log;

namespace UI_Login
{
    public partial class Form_Login : Form
    {
        private LoginController loginController;

        public Form_Login(LoginController loginController)
        {
            InitializeComponent();
            this.loginController = loginController;
        }
        #region Private Methods

        private void Form_Login_Shown(object sender, EventArgs e)
        {
            loginController.SetForeWindow(this.Handle);
            txtUsername.Focus(); //dock the mouse location to username textbox 
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[Form_Login]- OK button click");
            loginController.CheckCredentials(txtUsername.Text.Trim(), txtPassword.Text.Trim());
        }

        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                Log.WriteLog(LogType.system, "[Form_Login]- Enter button pressed");
                e.Handled = true;
                btnOK_Click(sender, e);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[Form_Login]- Cancel button click");
            loginController.Cancel();
        }
        #endregion

      
    }
}
