﻿
using M_Language;
using M_Log;
using M_Template;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UI_Login
{
    public class LoginController
    {
        private Login login;
        private Form_Login myForm;

        public LoginController()
        {
            myForm = new Form_Login(this);
            login = new Login();
        }

        public void SetValues(RunTemplate templateObject)
        {
            login.SetValues(templateObject);
        }

        public DialogResult Display()
        {
            return myForm.ShowDialog();
        }

        internal void CheckCredentials(string userName, string password)
        {
            if (userName != string.Empty)
            {
                Log.WriteLog(LogType.system, "[LoginController]- Checking user:" + userName);
                SysMessage.username = "";
                SysMessage.password = "";
                if (login.IsValidCredentials(userName, password))
                {
                    login.templateObject.OperatorName = userName;
                    SysMessage.username = userName;
                    SysMessage.password = password;
                    myForm.DialogResult = DialogResult.OK;
                    myForm.Close();
                    Log.WriteLog(LogType.system, SysMessage.user + userName + SysMessage.login);
                }
                else
                {
                    Log.WriteLog(LogType.system, "[LoginController]- Checking user:" + userName + "is invalid user name and/or password");
                    MessageBox.Show(SysMessage.invalidCredential, SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                Log.WriteLog(LogType.system, "[LoginController] - User name is empty");
                MessageBox.Show(SysMessage.noUserName, SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void Cancel()
        {
            myForm.Close();
            Log.WriteLog(LogType.system, SysMessage.user + SysMessage.cancelloginMsg);
            Environment.Exit(1);
        }

        internal void SetForeWindow(IntPtr intPtr)
        {
            login.SetForeWindow(intPtr);
        }
    }
}
