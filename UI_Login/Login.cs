﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using M_CommonMethods;
using System.Runtime.InteropServices;
using M_Template;

namespace UI_Login
{
    class Login
    {
        internal void SetForeWindow(IntPtr intPtr)
        {
            ImportDLL.SetForeWindow(intPtr);
        }

        internal bool IsValidCredentials(string userName, string password)
        {
            return ImportDLL.IsValidCredentials(userName, password);
        }

        //public string OperatorName { get; set; }
        internal RunTemplate templateObject;

        internal void SetValues(RunTemplate templateObject)
        {
            this.templateObject = templateObject;
        }
    }
}
