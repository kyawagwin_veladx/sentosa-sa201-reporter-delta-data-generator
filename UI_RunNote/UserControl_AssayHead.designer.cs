﻿
using System.Windows.Forms;

namespace UI_RunNote
{
    partial class UserControl_AssayHead
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new Label();
            this.label3 = new Label();
            this.label4 = new Label();
            this.txtNotes = new TextBox();
            this.lblOperator = new Label();
            this.lblVersion = new Label();
            this.kryptonLabel2 = new Label();
            this.txtAssayName = new TextBox();
            this.txtOperator = new TextBox();
            this.txtRetVersion = new TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Test Name:";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Operator:";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 267);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "Notes:";
            // 
            // txtNotes
            // 
            this.txtNotes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNotes.BackColor = System.Drawing.SystemColors.Window;
            this.txtNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNotes.Location = new System.Drawing.Point(6, 293);
            this.txtNotes.Multiline = true;
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.Size = new System.Drawing.Size(126, 247);
            this.txtNotes.TabIndex = 5;
            // 
            // lblOperator
            // 
            this.lblOperator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOperator.Location = new System.Drawing.Point(6, 126);
            this.lblOperator.Name = "lblOperator";
            this.lblOperator.Size = new System.Drawing.Size(45, 20);
            this.lblOperator.TabIndex = 7;
            this.lblOperator.Text = "Empty";
            // 
            // lblVersion
            // 
            this.lblVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.Location = new System.Drawing.Point(6, 228);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(45, 20);
            this.lblVersion.TabIndex = 9;
            this.lblVersion.Text = "Empty";
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kryptonLabel2.Location = new System.Drawing.Point(3, 187);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.Size = new System.Drawing.Size(64, 20);
            this.kryptonLabel2.TabIndex = 8;
            this.kryptonLabel2.Text = "Template:";
            // 
            // txtAssayName
            // 
            this.txtAssayName.BackColor = System.Drawing.SystemColors.Window;
            this.txtAssayName.Enabled = false;
            this.txtAssayName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAssayName.Location = new System.Drawing.Point(6, 34);
            this.txtAssayName.Multiline = true;
            this.txtAssayName.Name = "txtAssayName";
            this.txtAssayName.Size = new System.Drawing.Size(126, 50);
            this.txtAssayName.TabIndex = 10;
            // 
            // txtOperator
            // 
            this.txtOperator.BackColor = System.Drawing.SystemColors.Window;
            this.txtOperator.Enabled = false;
            this.txtOperator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOperator.Location = new System.Drawing.Point(6, 116);
            this.txtOperator.Multiline = true;
            this.txtOperator.Name = "txtOperator";
            this.txtOperator.Size = new System.Drawing.Size(126, 53);
            this.txtOperator.TabIndex = 11;
            // 
            // txtRetVersion
            // 
            this.txtRetVersion.BackColor = System.Drawing.SystemColors.Window;
            this.txtRetVersion.Enabled = false;
            this.txtRetVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRetVersion.Location = new System.Drawing.Point(6, 210);
            this.txtRetVersion.Multiline = true;
            this.txtRetVersion.Name = "txtRetVersion";
            this.txtRetVersion.Size = new System.Drawing.Size(126, 49);
            this.txtRetVersion.TabIndex = 12;
            // 
            // UserControl_AssayHead
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.txtRetVersion);
            this.Controls.Add(this.txtOperator);
            this.Controls.Add(this.txtAssayName);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.kryptonLabel2);
            this.Controls.Add(this.lblOperator);
            this.Controls.Add(this.txtNotes);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Name = "UserControl_AssayHead";
            this.Size = new System.Drawing.Size(138, 553);
            this.Load += new System.EventHandler(this.UserControl_AssayHead_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private Label label3;
        private Label label4;
        private TextBox txtNotes;
        private Label lblOperator;
        private Label lblVersion;
        private Label kryptonLabel2;
        private TextBox txtAssayName;
        private TextBox txtOperator;
        private TextBox txtRetVersion;

    }
}
