﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI_RunNote
{
    public partial class UserControl_AssayHead : UserControl
    {
        RunNoteController runNoteController;

        public UserControl_AssayHead(RunNoteController runNoteController)
        {
            InitializeComponent();
            this.runNoteController = runNoteController;
        }

        private void UserControl_AssayHead_Load(object sender, EventArgs e)
        {
            this.txtNotes.Text = runNoteController.runNote.Notes;
            this.txtAssayName.Text = runNoteController.runNote.AssayName;
            this.txtRetVersion.Text = runNoteController.runNote.TemplateName;
            this.txtOperator.Text = runNoteController.runNote.OperatorName;
            ////1.2 17627
            //this.txtNotes.Enabled = false;
        }

        internal void SetReadOnly(bool flag)
        {
            txtNotes.Enabled = flag;
        }

        internal string GetUINotes()
        {
            return this.txtNotes.Text;
        }

        internal void EnableNotes(bool text)
        {
            this.txtNotes.Enabled = false;
        }
    }
}
