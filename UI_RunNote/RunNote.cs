﻿using M_Assay;
using M_Template;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UI_RunNote
{
    public class RunNote
    {
        public string AssayName { get; set; }
        public string OperatorName { get; set; }
        public string TemplateName { get; set; }
        public string Notes { get; set; }

        //internal string GetVersionAndNotes()
        //{
        //    if (TemplateName == string.Empty)
        //    {
        //        return Notes;
        //    }
        //    else
        //    {
        //        return TemplateName + "|" + Notes;
        //    }
        //}

        internal void SetValues(AssayInfo assayInfo, RunTemplate template)
        {
            AssayName = assayInfo.AssayName;
            this.OperatorName = template.OperatorName;
            this.TemplateName = assayInfo.TemplateName;

        }
    }
}
