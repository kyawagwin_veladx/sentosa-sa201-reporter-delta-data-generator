﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using M_Assay;
using M_Template;

namespace UI_RunNote
{
    public class RunNoteController
    {
        public RunNote runNote;
        public UserControl_AssayHead runNoteUserControl;

        public RunNoteController()
        {
            this.runNote = new RunNote();
            runNoteUserControl = new UserControl_AssayHead(this);

        }
        public void SetValues(AssayInfo assayInfo, RunTemplate template)
        {
            runNote.SetValues(assayInfo, template);
        }

        public void SetReadOnly(bool flag)
        {
            runNoteUserControl.SetReadOnly(flag);
        }

        public string UpdateNotesFromUI()
        {
            return runNoteUserControl.GetUINotes();
        }

        public void EnableNotes(bool text)
        {
            //1.2  uncommented code
            runNoteUserControl.EnableNotes(text);
        }

        public void GetNotesFromSDX(string notes)
        {
            runNote.Notes = notes;
        }
    }
}

