﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UI_Contact
{
    public class ContactController
    {
        public Form_Contact myContactForm;
        public Contact contact;

        public ContactController()
        {
            myContactForm = new Form_Contact();
            contact = new Contact();
 
        }

        public DialogResult Display()
        {
            return myContactForm.ShowDialog();
 
        }
    }
}
