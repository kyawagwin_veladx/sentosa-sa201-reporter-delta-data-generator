﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 01/11/2013
 * Author: Liu Rui
 * 
 * Description:
 */
#endregion


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI_Contact
{
    public partial class Form_Contact : Form
    {
        #region Constructors
        public Form_Contact()
        {
            InitializeComponent();
        } 
        #endregion

        #region Private Methods
        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        } 
        #endregion

        private void linklblAsia_LinkClicked(object sender, EventArgs e)
        {
            SendEmail(linklblAsia.Text);
           
        }

        private void linklblEuro_LinkClicked(object sender, EventArgs e)
        {
            SendEmail(linklblEuro.Text);

        }

        private void linklblAmeri_LinkClicked(object sender, EventArgs e)
        {
            SendEmail(linklblAmeri.Text);

        }
        private void SendEmail(string p)
        {
            try
            {
                Process process = new Process();
                process.StartInfo.FileName = "mailto:" + p;
                process.Start();
                
            }
            catch (Exception e)
            {
                MessageBox.Show("Email application error! " + e.Message);
            }
        }
    }
}
