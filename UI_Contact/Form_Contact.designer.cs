﻿
using System.Windows.Forms;

namespace UI_Contact
{
    partial class Form_Contact
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Contact));
            this.label5 = new Label();
            this.btnOK = new Button();
            this.linklblAsia = new LinkLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new Label();
            this.linklblEuro = new LinkLabel();
            this.kryptonLabel1 = new Label();
            this.linklblAmeri = new LinkLabel();
            this.kryptonLabel2 = new Label();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(19, 270);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 20);
            this.label5.TabIndex = 5;
            this.label5.Text = "Asia Pacific:";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(121, 341);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 28);
            //this.btnOK.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // linklblAsia
            // 
            this.linklblAsia.Location = new System.Drawing.Point(181, 270);
            this.linklblAsia.Name = "linklblAsia";
            this.linklblAsia.Size = new System.Drawing.Size(154, 20);
            this.linklblAsia.TabIndex = 9;
            this.linklblAsia.Text = "support.apac@veladx.com";
            this.linklblAsia.LinkClicked += new LinkLabelLinkClickedEventHandler(this.linklblAsia_LinkClicked);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::UI_Contact.Properties.Resources.contact;
            this.pictureBox1.Location = new System.Drawing.Point(-1, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(355, 153);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(5, 172);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(362, 68);
            this.label3.TabIndex = 3;
            this.label3.Text = resources.GetString("label3.Text");
            // 
            // linklblEuro
            // 
            this.linklblEuro.Location = new System.Drawing.Point(181, 290);
            this.linklblEuro.Name = "linklblEuro";
            this.linklblEuro.Size = new System.Drawing.Size(158, 20);
            this.linklblEuro.TabIndex = 11;
            this.linklblEuro.Text = "support.emea@veladx.com";
            this.linklblEuro.LinkClicked += new LinkLabelLinkClickedEventHandler(this.linklblEuro_LinkClicked);
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.Location = new System.Drawing.Point(19, 290);
            this.kryptonLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(169, 20);
            this.kryptonLabel1.TabIndex = 10;
            this.kryptonLabel1.Text = "Europe, Middle East && Africa:";
            // 
            // linklblAmeri
            // 
            this.linklblAmeri.Location = new System.Drawing.Point(181, 250);
            this.linklblAmeri.Name = "linklblAmeri";
            this.linklblAmeri.Size = new System.Drawing.Size(141, 20);
            this.linklblAmeri.TabIndex = 13;
            this.linklblAmeri.Text = "support.us@veladx.com";
            this.linklblAmeri.LinkClicked += new LinkLabelLinkClickedEventHandler(this.linklblAmeri_LinkClicked);
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.Location = new System.Drawing.Point(19, 250);
            this.kryptonLabel2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.Size = new System.Drawing.Size(93, 20);
            this.kryptonLabel2.TabIndex = 12;
            this.kryptonLabel2.Text = "North America:";
            // 
            // Form_Contact
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(353, 386);
            this.Controls.Add(this.linklblAmeri);
            this.Controls.Add(this.kryptonLabel2);
            this.Controls.Add(this.linklblEuro);
            this.Controls.Add(this.kryptonLabel1);
            this.Controls.Add(this.linklblAsia);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Contact";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Contact Us";
            //((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label5;
        private Button btnOK;
        private System.Windows.Forms.PictureBox pictureBox1;
        private LinkLabel linklblAsia;
        private Label label3;
        private LinkLabel linklblEuro;
        private Label kryptonLabel1;
        private LinkLabel linklblAmeri;
        private Label kryptonLabel2;
    }
}