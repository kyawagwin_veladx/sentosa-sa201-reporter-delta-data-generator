﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 08/11/2013
 * Author: Liu Rui
 * 
 * Description:
 */
#endregion

using System;
using System.Drawing;
using System.Windows.Forms;

using M_Language;
using M_Log;
using System.Diagnostics;
using System.IO;
using UI_Access;

namespace UI_Main
{
    public partial class Form_Main : Form
    {
        #region Private Member Variables
        private MainController mainController;
        #endregion

        #region Constructors
        public Form_Main(MainController mainController)
        {
            InitializeComponent();
            this.mainController = mainController;
            
        }
        #endregion

        #region Private Methods
        private void btnNext_Click(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[Form_Main]- Next button click");
            this.mainController.Next();
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[Form_Main]- previous button click");
            this.mainController.Previous();
        }

        private void LinklblAbout_LinkClicked(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[Form_Main]- About link click");
            this.mainController.About();
        }

        private void LinklblContact_LinkClicked(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[Form_Main]- Contact link click");
            this.mainController.Contact();
        }

        private void picLogoff_Click(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[Form_Main]- Log off button click");
            if (!mainController.IsProcessExisting("SDSShell"))
            {
                if (MessageBox.Show(SysMessage.confirmLogOffMsg
                , SysMessage.confirmCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                {
                    this.mainController.runControl.KillAutoItScripts();
                    this.mainController.Logoff();
                }
            }
            else
            {
                MessageBox.Show(SysMessage.runningMsg
                    , SysMessage.errorCaption
                    , MessageBoxButtons.OK
                    , MessageBoxIcon.Error);
            }
        }

        private void Form_Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (mainController.GetAccessMode() == AccessMode.LOGOFF)
            {
                mainController.runControl.KillAutoItScripts();
                this.Close();
               
            }
            else if (!mainController.IsProcessExisting("SDSShell"))
            {
                if (MessageBox.Show(SysMessage.confirmExitMsg
                , SysMessage.confirmCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                {
                    mainController.runControl.KillAutoItScripts();
                    mainController.Exit();
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                MessageBox.Show(SysMessage.runningMsg
                    , SysMessage.errorCaption
                    , MessageBoxButtons.OK
                    , MessageBoxIcon.Error);
                e.Cancel = true;
            }
        }
        #endregion

        private void LinkllblHelp_LinkClicked(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[Form_Main]- Help button click");
            this.mainController.Help();
        }

        private void pnlUserControls_Resize(object sender, EventArgs e)
        {
            foreach (Control c in pnlUserControls.Controls)
            {
                c.Size = pnlUserControls.Size;
            }
        }

        internal void AddControls(UserControl addControl)
        {
            this.pnlUserControls.Controls.Add(addControl);
        }

        internal void SetButton(bool isPreDisplay,bool isNextDisplay)
        {
            if (isPreDisplay)
                this.btnPrevious.Show();
            else
                this.btnPrevious.Hide();

            if (isNextDisplay)
                this.btnNext.Show();
            else
                this.btnNext.Hide();
        }

        internal void DisplayControl(int index)
        {
            foreach (UserControl myControl in this.pnlUserControls.Controls)
            {
                myControl.Hide();
            }
            this.pnlUserControls.Controls[index].Show();
        }

        internal void AddRunNotes(UserControl runNoteUserControl)
        {
            this.kryptonPanel1.Controls.Add(runNoteUserControl);
            this.kryptonPanel1.Controls[0].Show();
        }

        internal void DisplayNaviImage(Bitmap bitmap)
        {
            this.pictureBox2.Image = bitmap ;
        }

        internal void EnableNextButton(bool text)
        {
            this.btnNext.Enabled = text;
        }

        internal void EnablePreviousButton(bool text)
        {
            this.btnPrevious.Enabled = text;
        }

        internal void ChangeAnalysisTitle()
        {
            this.Text = "Analysis-Only " + this.Text;
        }
    }

}
