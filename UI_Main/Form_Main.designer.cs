﻿
using ComponentFactory.Krypton.Toolkit;
using System.Windows.Forms;

namespace UI_Main
{
    partial class Form_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Main));
            this.pnlAll = new Panel();
            this.pnlUserControls = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LinklblContact = new LinkLabel();
            this.LinklblAbout = new LinkLabel();
            this.kryptonSeparator1 = new KryptonSeparator();
            this.kryptonSeparator2 = new KryptonSeparator();
            this.LinklblHelp = new LinkLabel();
            this.picLogoff = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.kryptonPanel1 = new Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnPrevious = new Button();
            this.btnNext = new Button();
            //((System.ComponentModel.ISupportInitialize)(this.pnlAll)).BeginInit();
            this.pnlAll.SuspendLayout();
            this.panel1.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.kryptonSeparator1)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.kryptonSeparator2)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.picLogoff)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.kryptonPanel1)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlAll
            // 
            this.pnlAll.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlAll.AutoScroll = true;
            this.pnlAll.Controls.Add(this.pnlUserControls);
            this.pnlAll.Controls.Add(this.panel1);
            this.pnlAll.Controls.Add(this.picLogoff);
            this.pnlAll.Controls.Add(this.pictureBox2);
            this.pnlAll.Controls.Add(this.pictureBox1);
            this.pnlAll.Controls.Add(this.kryptonPanel1);
            this.pnlAll.Controls.Add(this.panel2);
            this.pnlAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlAll.Location = new System.Drawing.Point(0, 0);
            this.pnlAll.Name = "pnlAll";
            this.pnlAll.Size = new System.Drawing.Size(1004, 730);
            //this.pnlAll.StateCommon.Color1 = System.Drawing.Color.White;
            this.pnlAll.TabIndex = 0;
            // 
            // pnlUserControls
            // 
            this.pnlUserControls.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlUserControls.Location = new System.Drawing.Point(159, 68);
            this.pnlUserControls.Name = "pnlUserControls";
            this.pnlUserControls.Size = new System.Drawing.Size(833, 576);
            this.pnlUserControls.TabIndex = 35;
            this.pnlUserControls.Resize += new System.EventHandler(this.pnlUserControls_Resize);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.LinklblContact);
            this.panel1.Controls.Add(this.LinklblAbout);
            this.panel1.Controls.Add(this.kryptonSeparator1);
            this.panel1.Controls.Add(this.kryptonSeparator2);
            this.panel1.Controls.Add(this.LinklblHelp);
            this.panel1.Location = new System.Drawing.Point(0, 669);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(186, 31);
            this.panel1.TabIndex = 33;
            // 
            // LinklblContact
            // 
            this.LinklblContact.Location = new System.Drawing.Point(66, 3);
            this.LinklblContact.Name = "LinklblContact";
            this.LinklblContact.Size = new System.Drawing.Size(70, 20);
            this.LinklblContact.TabIndex = 27;
            this.LinklblContact.Text = "Contact Us";
            this.LinklblContact.LinkClicked += new LinkLabelLinkClickedEventHandler(this.LinklblContact_LinkClicked);
            // 
            // LinklblAbout
            // 
            this.LinklblAbout.Location = new System.Drawing.Point(6, 3);
            this.LinklblAbout.Name = "LinklblAbout";
            this.LinklblAbout.Size = new System.Drawing.Size(44, 20);
            this.LinklblAbout.TabIndex = 25;
            this.LinklblAbout.Text = "About";
            this.LinklblAbout.LinkClicked += new LinkLabelLinkClickedEventHandler(this.LinklblAbout_LinkClicked);
            // 
            // kryptonSeparator1
            // 
            this.kryptonSeparator1.Location = new System.Drawing.Point(50, 3);
            this.kryptonSeparator1.Name = "kryptonSeparator1";
            this.kryptonSeparator1.Size = new System.Drawing.Size(10, 20);
            this.kryptonSeparator1.TabIndex = 26;
            // 
            // kryptonSeparator2
            // 
            this.kryptonSeparator2.Location = new System.Drawing.Point(136, 3);
            this.kryptonSeparator2.Name = "kryptonSeparator2";
            this.kryptonSeparator2.Size = new System.Drawing.Size(10, 20);
            this.kryptonSeparator2.TabIndex = 28;
            // 
            // LinklblHelp
            // 
            this.LinklblHelp.Location = new System.Drawing.Point(147, 3);
            this.LinklblHelp.Name = "LinklblHelp";
            this.LinklblHelp.Size = new System.Drawing.Size(36, 20);
            this.LinklblHelp.TabIndex = 29;
            this.LinklblHelp.Text = "Help";
            this.LinklblHelp.LinkClicked += new LinkLabelLinkClickedEventHandler(this.LinkllblHelp_LinkClicked);
            // 
            // picLogoff
            // 
            this.picLogoff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picLogoff.BackColor = System.Drawing.Color.Transparent;
            this.picLogoff.Image = global::UI_Main.Properties.Resources.logout_256;
            this.picLogoff.Location = new System.Drawing.Point(954, 17);
            this.picLogoff.Name = "picLogoff";
            this.picLogoff.Size = new System.Drawing.Size(42, 38);
            this.picLogoff.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picLogoff.TabIndex = 32;
            this.picLogoff.TabStop = false;
            this.picLogoff.Click += new System.EventHandler(this.picLogoff_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::UI_Main.Properties.Resources.menu_samples;
            this.pictureBox2.Location = new System.Drawing.Point(159, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(808, 50);
            this.pictureBox2.TabIndex = 31;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::UI_Main.Properties.Resources.corporate_logo_Two_color;
            this.pictureBox1.Location = new System.Drawing.Point(11, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(142, 55);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 30;
            this.pictureBox1.TabStop = false;
            // 
            // kryptonPanel1
            // 
            this.kryptonPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.kryptonPanel1.Location = new System.Drawing.Point(11, 68);
            this.kryptonPanel1.Name = "kryptonPanel1";
            this.kryptonPanel1.Size = new System.Drawing.Size(142, 576);
            //this.kryptonPanel1.StateCommon.Color1 = System.Drawing.Color.Transparent;
            this.kryptonPanel1.TabIndex = 24;
            //this.kryptonPanel1.Resize += new System.EventHandler(this.kryptonPanel1_Resize);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.btnPrevious);
            this.panel2.Controls.Add(this.btnNext);
            this.panel2.Location = new System.Drawing.Point(189, 650);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(808, 68);
            this.panel2.TabIndex = 34;
            // 
            // btnPrevious
            // 
            this.btnPrevious.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrevious.Location = new System.Drawing.Point(5, 10);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(130, 50);
            //this.btnPrevious.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.btnPrevious.TabIndex = 21;
            this.btnPrevious.Text = "<< Previous";
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.Location = new System.Drawing.Point(673, 10);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(130, 50);
            //this.btnNext.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.btnNext.TabIndex = 22;
            this.btnNext.Text = "Next >>";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.pnlAll);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sentosa SA201 Reporter";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Main_FormClosing);
            //this.DoubleClick += new System.EventHandler(this.Form_Main_DoubleClick);
            //((System.ComponentModel.ISupportInitialize)(this.pnlAll)).EndInit();
            this.pnlAll.ResumeLayout(false);
            this.pnlAll.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            //((System.ComponentModel.ISupportInitialize)(this.kryptonSeparator1)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.kryptonSeparator2)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.picLogoff)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.kryptonPanel1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Panel pnlAll;
        private Button btnNext;
        private Button btnPrevious;
        private LinkLabel LinklblContact;
        private KryptonSeparator kryptonSeparator1;
        private LinkLabel LinklblAbout;
        private Panel kryptonPanel1;
        private LinkLabel LinklblHelp;
        private KryptonSeparator kryptonSeparator2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox picLogoff;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlUserControls;

    }
}

