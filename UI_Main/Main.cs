﻿using M_System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using M_Assay;
using M_SampleList;
using M_Template;
using M_ReportResult;

using UI_Access;
using System.IO;
using M_Rer;
using System.Windows.Forms;

namespace UI_Main
{
    public class Main
    {
        private SystemConfig systemConfig;
        private AssayInfo assayInfo;
        private SMPObject smpObject;
        private RunTemplate template;
        private ReportResult reportResult;
        private AssayLogic assayLogic;
        public AccessMode AccessMode{ get; set;}

        public Main()
        {
            assayLogic = new AssayLogic();
        }

        public SystemConfig SystemConfig
        {
            get { return systemConfig; }
        }

        public AssayInfo AssayInfo
        {
            get { return assayInfo; }
        }

        public SMPObject SmpObject
        {
            get { return smpObject; }
        }

        public RunTemplate Template
        {
            get { return template; }
        }

        public ReportResult ReportResult
        {
            get { return reportResult; }
        }

        public AssayLogic AssayLogic
        {
            get { return assayLogic; }
        }

        internal void SetValues(AccessMode accessMode, SystemConfig systemConfig, AssayInfo assayInfo, SMPObject smpObject, RunTemplate template, ReportResult reportResult)
        {
            this.AccessMode = accessMode;
            this.systemConfig = systemConfig;
            this.assayInfo = assayInfo;
            this.smpObject = smpObject;
            this.template = template;
            this.reportResult = reportResult;
        }

        //internal void SetAccessMode(AccessMode accessMode)
        //{
        //    this.accessMode = accessMode;
        //}

        //internal AccessMode GetAccessMode()
        //{
        //    return this.accessMode;
        //}

        internal void CreateRER(AccessMode accessMode)
        {
            try
            {
                if (!Directory.Exists(systemConfig.Folder_LinkRun))
                {
                    Directory.CreateDirectory(systemConfig.Folder_LinkRun);
                }

                RERFile rer = new RERFile();

                string rerPath = string.Empty;

                if (accessMode == AccessMode.IMPORT)
                {
                    rerPath = systemConfig.Folder_LinkRun + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(assayInfo.TxtFilePath) + ".rer";
                }
                else if (accessMode == AccessMode.ANALYSIS)
                {
                    rerPath = systemConfig.Folder_LinkRun + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(assayInfo.AnalysisFolder) + ".rer";
                }

                rer.GenerateRER(reportResult, rerPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + Environment.NewLine + "RER file is not created. Please ensure the write permission to this folder.");
            }
        }
    }
}
