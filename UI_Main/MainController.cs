﻿using UI_About;
using M_System;
using UI_Access;
using UI_Sample;
using UI_Run;
using UI_Analysis;
using UI_Reports;
using UI_RunNote;
using UI_Contact;
using UI_Help;
using M_Assay;
using M_ReportResult;
using M_SampleList;
using M_Template;
using UI_Main.Properties;
using M_Log;
using M_Language;
using System;
using System.Windows.Forms;

using M_RunFileCheck;
using M_CheckExit;

namespace UI_Main
{
    public delegate void dLogoff();

    public class MainController
    {
        Form_Main mainForm;
        Main main;

        private SampleController sampleControl;
        public RunController runControl; //check the access modifier
        private AnalysisController analysisControl;
        private ReportController reportControl;
      
        private DisplayStatus currentDisplay;
        
        private RunNoteController runNoteControl;
        private RunFileCheck check = new RunFileCheck();
       
        public dLogoff dLogoffCallback;
        
        public MainController()
        {
            // TODO: Complete member initialization
            mainForm = new Form_Main(this);
            main = new Main();

            sampleControl = new SampleController();
            runControl = new RunController();
            analysisControl = new AnalysisController();
            reportControl = new ReportController();

            runNoteControl = new RunNoteController();
            
            runControl.EnableNextButton = new dEnableNextButton(this.EnableNextButton);
            runControl.EnablePreviousButton = new dEnablePreviousButton(this.EnablePreviousButton);
            runControl.EnableNotes = new dEnableNotes(this.EnableNotes);
            runControl.GetRunNotesCallBack = new dGetRunNotes(this.GetRunNotes);

            mainForm.AddControls(sampleControl.GetSampleDisplay());
            mainForm.AddControls(runControl.GetRunDisplay());
            mainForm.AddControls(analysisControl.AnalysisDisplay);
            mainForm.AddControls(reportControl.ReportOperationDisplay);
            mainForm.AddRunNotes(runNoteControl.runNoteUserControl);
        }

        private void EnableNotes(bool text)
        {
            runNoteControl.EnableNotes(text);
        }

        private void EnablePreviousButton(bool text)
        {
            mainForm.EnablePreviousButton(text);
        }

        private void EnableNextButton(bool text)
        {
            mainForm.EnableNextButton(text);
        }

        private string GetRunNotes()
        {
            return runNoteControl.UpdateNotesFromUI();
        }

        public void SetValues(AccessMode accessMode, SystemConfig systemConfig, AssayInfo assayInfo, SMPObject smpObject, RunTemplate template, ReportResult reportResult)
        {
            main.SetValues(accessMode, systemConfig, assayInfo, smpObject, template, reportResult);
            sampleControl.SetValues(smpObject, assayInfo);
            runControl.SetValues(systemConfig, assayInfo, smpObject, template, reportResult);
        }
      
        internal void About()
        {
            AboutController ac = new AboutController();
            ac.SetValues(main.SystemConfig);
            ac.Display();
        }

        internal void Contact()
        {
            ContactController cc = new ContactController();
            cc.Display();
        }

        internal void Help()
        {
            HelpController hc = new HelpController();
            hc.Display();
        }

        public void Display(AccessMode accessmode, SystemMode systemMode)
        {
            if (accessmode == AccessMode.IMPORT)
            {
                currentDisplay = DisplayStatus.SAMPLE;         
                sampleControl.SetIsReadOnly(true);
                PageDisplay(accessmode);
            }
            if (accessmode == AccessMode.SELECT)
            {
                currentDisplay = DisplayStatus.SAMPLE;
                sampleControl.SetIsReadOnly(false);
                PageDisplay(accessmode);
            }
            if (accessmode == AccessMode.ANALYSIS)
            {
              //  runControl.InterpretResult(main.AssayLogic);
                currentDisplay = DisplayStatus.ANALYSIS;
              //  runNoteControl.GetNotesFromSDX(runControl.GetRunNotesCallBack);
                PageDisplay(accessmode);
            }

            runNoteControl.SetValues(main.AssayInfo, main.Template);

            if (systemMode == SystemMode.AnalysisOnly)
            {
                mainForm.ChangeAnalysisTitle();
            }
            
            mainForm.ShowDialog();
        }

        internal void Next()
        {
            if (currentDisplay == DisplayStatus.SAMPLE)
            {
                //check both sampletype and M1
                if (!sampleControl.AllBarcodePassed())
                {
                    sampleControl.ShowBarcodeError();
                    Log.WriteLog(LogType.system, "Sample Page - Incorrect barcodes");
                }
                else
                {
                    if (!sampleControl.AllM1Passed())
                    {
                        sampleControl.ShowM1Error();
                        Log.WriteLog(LogType.system, "Sample Page - Incorrect M1");
                    }
                    else
                    {
                        currentDisplay++;
                        PageDisplay(main.AccessMode);
                    }
                }
            }
            else
            {
                currentDisplay++;
                PageDisplay(main.AccessMode);
            }
        }

        internal void Previous()
        {
            currentDisplay--;
            PageDisplay(main.AccessMode);
        }

        private void PageDisplay(AccessMode accessmode)
        {
            switch (currentDisplay)
            {
                case DisplayStatus.SAMPLE:
                    {
                        mainForm.SetButton(false, true);
                        mainForm.EnableNextButton(true);
                        mainForm.DisplayNaviImage(Resources.menu_samples);
                        mainForm.DisplayControl((int)currentDisplay);
                        Log.WriteLog(LogType.system, "Display Sample Page");
                        Log.WriteLog(LogType.system, "Assay Name: " + main.AssayInfo.AssayName);
                        break;
                    }
                case DisplayStatus.RUN:
                    {
                        mainForm.SetButton(true, true);
                        mainForm.EnableNextButton(false);
                        mainForm.DisplayNaviImage(Resources.menu_run);
                        mainForm.DisplayControl((int)currentDisplay);
                        Log.WriteLog(LogType.system, "Display Run Page");
                        break;
                    }
                case DisplayStatus.ANALYSIS:
                    {
                        SysMessage.errorMsg = string.Empty;
                        try
                        {

                            if (accessmode == AccessMode.IMPORT)
                            {
                                runControl.SDSFileUpdate();
                            }
                            runControl.InterpretResult(main.AssayLogic);
                            runNoteControl.EnableNotes(false);
                            mainForm.SetButton(false, true);
                            mainForm.EnablePreviousButton(true);
                            mainForm.DisplayNaviImage(Resources.menu_analysis);
                            mainForm.DisplayControl((int)currentDisplay);
                            analysisControl.SetValue(main.ReportResult, main.AssayLogic);
                            Log.WriteLog(LogType.system, "Display Analysis Page");
                        }
                        catch (Exception e)
                        {
                            sampleControl.ShowInCompleteSDSError();
                            currentDisplay--;
                        }

                        finally
                        {
                            SysMessage.errorMsg = "";
                        }

                       
                        break;
                    }
                case DisplayStatus.REPORT:
                    {
                        CreateRER(main.AccessMode);
                        mainForm.SetButton(true, false);
                        mainForm.DisplayControl((int)currentDisplay);
                        mainForm.DisplayNaviImage(Resources.menu_report);
                        reportControl.SetValues(main.ReportResult, main.AssayLogic);
                        Log.WriteLog(LogType.system, "Display Report Page");
                        break;
                    }
            }
        }

        private void CreateRER(AccessMode accessMode)
        {
            main.CreateRER(accessMode);
        }

        internal bool IsProcessExisting(string exe)
        {
            return ExitCheck.IsProcessExisting(exe);
        }

        internal void Exit()
        {
            Log.WriteLog(LogType.system, SysMessage.user + Log.UserName + SysMessage.exists);

            Environment.Exit(1);
        }

        internal void Logoff()
        {
            Log.WriteLog(LogType.system, "User logs off");
            main.AccessMode = AccessMode.LOGOFF;
            this.mainForm.Dispose();
            dLogoffCallback();
        }

        internal AccessMode GetAccessMode()
        {
            return main.AccessMode;
        }
    }

    public enum DisplayStatus
    {
        SAMPLE = 0,
        RUN,
        ANALYSIS,
        REPORT,
    }
}
