﻿
using System.Windows.Forms;

namespace UI_Analysis
{
    partial class UserControl_AnalysisDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dtgSamples = new DataGridView();
            this.Selected = new DataGridViewCheckBoxColumn();
            this.Position = new DataGridViewTextBoxColumn();
            this.SampleName = new DataGridViewTextBoxColumn();
            this.Type = new DataGridViewTextBoxColumn();
            this.CT = new DataGridViewTextBoxColumn();
            this.Page = new DataGridViewTextBoxColumn();
            this.btn_allon = new Button();
            this.btn_alloff = new Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.btn_rawdata = new Button();
            this.btn_autoscale = new Button();
            this.btn_log = new Button();
            this.btn_stdCurve = new Button();
            this.btn_zoomin = new Button();
            this.btn_zoomout = new Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblEndValue = new Label();
            this.lblEnd = new Label();
            this.lblStartValue = new Label();
            this.lblStart = new Label();
            this.lblThreshold = new Label();
            this.label1 = new Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            //((System.ComponentModel.ISupportInitialize)(this.dtgSamples)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtgSamples
            // 
            this.dtgSamples.AllowUserToAddRows = false;
            this.dtgSamples.AllowUserToDeleteRows = false;
            this.dtgSamples.AllowUserToResizeRows = false;
            this.dtgSamples.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dtgSamples.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgSamples.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Selected,
            this.Position,
            this.SampleName,
            this.Type,
            this.CT,
            this.Page});
            this.dtgSamples.Location = new System.Drawing.Point(3, 70);
            this.dtgSamples.MultiSelect = false;
            this.dtgSamples.Name = "dtgSamples";
            this.dtgSamples.RowHeadersVisible = false;
            this.dtgSamples.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgSamples.Size = new System.Drawing.Size(321, 489);
            this.dtgSamples.TabIndex = 18;
            this.dtgSamples.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgSamples_CellContentClick);
            this.dtgSamples.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgSamples_CellValueChanged);
            this.dtgSamples.SelectionChanged += new System.EventHandler(this.dtgSamples_SelectionChanged);
            this.dtgSamples.DoubleClick += new System.EventHandler(this.dtgSamples_DoubleClick);
            // 
            // Selected
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.NullValue = false;
            this.Selected.DefaultCellStyle = dataGridViewCellStyle1;
            this.Selected.FalseValue = null;
            this.Selected.Frozen = true;
            this.Selected.HeaderText = " ";
            this.Selected.IndeterminateValue = null;
            this.Selected.Name = "Selected";
            this.Selected.TrueValue = null;
            this.Selected.Width = 20;
            // 
            // Position
            // 
            this.Position.Frozen = true;
            this.Position.HeaderText = "Well";
            this.Position.Name = "Position";
            this.Position.ReadOnly = true;
            this.Position.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Position.Width = 40;
            // 
            // SampleName
            // 
            this.SampleName.Frozen = true;
            this.SampleName.HeaderText = "Sample ID";
            this.SampleName.Name = "SampleName";
            this.SampleName.ReadOnly = true;
            this.SampleName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SampleName.Width = 100;
            // 
            // Type
            // 
            this.Type.HeaderText = "Type";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            this.Type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Type.Width = 70;
            // 
            // CT
            // 
            dataGridViewCellStyle2.Format = "suffix=\"T\"";
            dataGridViewCellStyle2.NullValue = null;
            this.CT.DefaultCellStyle = dataGridViewCellStyle2;
            this.CT.HeaderText = "Ct";
            this.CT.Name = "CT";
            this.CT.ReadOnly = true;
            this.CT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CT.Width = 50;
            // 
            // Page
            // 
            this.Page.HeaderText = "Page";
            this.Page.Name = "Page";
            this.Page.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Page.Width = 100;
            // 
            // btn_allon
            // 
            this.btn_allon.Location = new System.Drawing.Point(3, 3);
            this.btn_allon.Name = "btn_allon";
            this.btn_allon.Size = new System.Drawing.Size(86, 28);
            //this.btn_allon.StatePressed.Content.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_allon.TabIndex = 20;
            this.btn_allon.Text = "Check All";
            this.btn_allon.Click += new System.EventHandler(this.btn_allon_Click);
            // 
            // btn_alloff
            // 
            this.btn_alloff.Location = new System.Drawing.Point(95, 3);
            this.btn_alloff.Name = "btn_alloff";
            this.btn_alloff.Size = new System.Drawing.Size(86, 28);
            this.btn_alloff.TabIndex = 21;
            this.btn_alloff.Text = "Uncheck All ";
            this.btn_alloff.Click += new System.EventHandler(this.btn_alloff_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Location = new System.Drawing.Point(330, 70);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(510, 489);
            this.tabControl1.TabIndex = 22;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            this.tabControl1.Resize += new System.EventHandler(this.tabControl1_Resize);
            // 
            // btn_rawdata
            // 
            this.btn_rawdata.Location = new System.Drawing.Point(227, 16);
            this.btn_rawdata.Name = "btn_rawdata";
            this.btn_rawdata.Size = new System.Drawing.Size(100, 28);
            this.btn_rawdata.TabIndex = 23;
            this.btn_rawdata.Text = "Delta Rn";
            this.btn_rawdata.Click += new System.EventHandler(this.btn_rawdata_Click);
            // 
            // btn_autoscale
            // 
            this.btn_autoscale.Location = new System.Drawing.Point(95, 3);
            this.btn_autoscale.Name = "btn_autoscale";
            this.btn_autoscale.Size = new System.Drawing.Size(86, 28);
            this.btn_autoscale.TabIndex = 24;
            this.btn_autoscale.Text = "Auto-Scale";
            this.btn_autoscale.Click += new System.EventHandler(this.btn_autoscale_Click);
            // 
            // btn_log
            // 
            this.btn_log.Enabled = false;
            this.btn_log.Location = new System.Drawing.Point(3, 3);
            this.btn_log.Name = "btn_log";
            this.btn_log.Size = new System.Drawing.Size(86, 28);
            this.btn_log.TabIndex = 26;
            this.btn_log.Text = "Linear Scale";
            this.btn_log.Click += new System.EventHandler(this.btn_log_Click);
            // 
            // btn_stdCurve
            // 
            this.btn_stdCurve.Location = new System.Drawing.Point(111, 16);
            this.btn_stdCurve.Name = "btn_stdCurve";
            this.btn_stdCurve.Size = new System.Drawing.Size(100, 28);
            this.btn_stdCurve.TabIndex = 28;
            this.btn_stdCurve.Text = "Standard Curve";
            this.btn_stdCurve.Visible = false;
            // 
            // btn_zoomin
            // 
            this.btn_zoomin.Location = new System.Drawing.Point(187, 3);
            this.btn_zoomin.Name = "btn_zoomin";
            this.btn_zoomin.Size = new System.Drawing.Size(86, 28);
            this.btn_zoomin.TabIndex = 29;
            this.btn_zoomin.Text = "Zoom In";
            this.btn_zoomin.Click += new System.EventHandler(this.btn_zoomin_Click);
            // 
            // btn_zoomout
            // 
            this.btn_zoomout.Location = new System.Drawing.Point(279, 3);
            this.btn_zoomout.Name = "btn_zoomout";
            this.btn_zoomout.Size = new System.Drawing.Size(86, 28);
            this.btn_zoomout.TabIndex = 30;
            this.btn_zoomout.Text = "Zoom Out";
            this.btn_zoomout.Click += new System.EventHandler(this.btn_zoomout_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblEndValue);
            this.panel1.Controls.Add(this.lblEnd);
            this.panel1.Controls.Add(this.lblStartValue);
            this.panel1.Controls.Add(this.lblStart);
            this.panel1.Controls.Add(this.lblThreshold);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(480, 61);
            this.panel1.TabIndex = 31;
            // 
            // lblEndValue
            // 
            this.lblEndValue.Location = new System.Drawing.Point(216, 16);
            this.lblEndValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEndValue.Name = "lblEndValue";
            this.lblEndValue.Size = new System.Drawing.Size(39, 20);
            this.lblEndValue.TabIndex = 7;
            // 
            // lblEnd
            // 
            this.lblEnd.Location = new System.Drawing.Point(165, 16);
            this.lblEnd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEnd.Name = "lblEnd";
            this.lblEnd.Size = new System.Drawing.Size(34, 20);
            this.lblEnd.TabIndex = 6;
            this.lblEnd.Text = "End:";
            // 
            // lblStartValue
            // 
            this.lblStartValue.Location = new System.Drawing.Point(79, 16);
            this.lblStartValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStartValue.Name = "lblStartValue";
            this.lblStartValue.Size = new System.Drawing.Size(39, 20);
            this.lblStartValue.TabIndex = 5;
            // 
            // lblStart
            // 
            this.lblStart.Location = new System.Drawing.Point(22, 16);
            this.lblStart.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(39, 20);
            this.lblStart.TabIndex = 4;
            this.lblStart.Text = "Start:";
            // 
            // lblThreshold
            // 
            this.lblThreshold.Location = new System.Drawing.Point(390, 16);
            this.lblThreshold.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblThreshold.Name = "lblThreshold";
            this.lblThreshold.Size = new System.Drawing.Size(39, 20);
            this.lblThreshold.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(315, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Threshold:";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.btn_rawdata);
            this.panel2.Controls.Add(this.btn_stdCurve);
            this.panel2.Location = new System.Drawing.Point(492, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(345, 61);
            this.panel2.TabIndex = 32;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.AutoSize = true;
            this.panel3.Controls.Add(this.btn_zoomout);
            this.panel3.Controls.Add(this.btn_log);
            this.panel3.Controls.Add(this.btn_zoomin);
            this.panel3.Controls.Add(this.btn_autoscale);
            this.panel3.Location = new System.Drawing.Point(283, 565);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(409, 42);
            this.panel3.TabIndex = 33;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel4.AutoSize = true;
            this.panel4.Controls.Add(this.btn_allon);
            this.panel4.Controls.Add(this.btn_alloff);
            this.panel4.Location = new System.Drawing.Point(3, 565);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(275, 42);
            this.panel4.TabIndex = 34;
            // 
            // UserControl_AnalysisDisplay
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.dtgSamples);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UserControl_AnalysisDisplay";
            this.Size = new System.Drawing.Size(843, 617);
            this.Load += new System.EventHandler(this.UserControl_AnalysisDisplay_Load);
            //((System.ComponentModel.ISupportInitialize)(this.dtgSamples)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        // private Label label9;
        private DataGridView dtgSamples;
        private Button btn_allon;
        private Button btn_alloff;
        private System.Windows.Forms.TabControl tabControl1;
        private Button btn_rawdata;
        private Button btn_autoscale;
        private Button btn_log;
        private Button btn_stdCurve;
        private Button btn_zoomin;
        private Button btn_zoomout;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private Label lblThreshold;
        private Label label1;
        private Label lblEndValue;
        private Label lblEnd;
        private Label lblStartValue;
        private Label lblStart;
        private DataGridViewCheckBoxColumn Selected;
        private DataGridViewTextBoxColumn Position;
        private DataGridViewTextBoxColumn SampleName;
        private DataGridViewTextBoxColumn Type;
        private DataGridViewTextBoxColumn CT;
        private DataGridViewTextBoxColumn Page;


    }
}
