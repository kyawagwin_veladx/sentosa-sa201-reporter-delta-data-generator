﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 8/1/2014
 * Author: Yang Yi
 * 
 * Modification Date: 07/25/2014 - Liu Rui
 * Modification: Add methods: GetCurrentChannelPage, FillAnalysisInfo, dtgSamples_CellValueChanged
 * , UpdateSampleSelected, ControlIndividualCurveDisplay, EnableCurve, dtgSamples_SelectionChanged, GetCurrentChannelPage
 * , HighlightCurve, tabControl1_SelectedIndexChanged, btn_zoomin_Click, btn_zoomout_Click
 * 
 * Description:
 * user control used for display data analysis curve
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using M_Assay;
using M_ReportResult;
using M_Curve;
using System.Windows.Forms.DataVisualization.Charting;
using M_CommonMethods;

using M_Log;
using System.ComponentModel;

namespace UI_Analysis
{
    /// <summary>
    /// this class is used to display assay information after third party machine running.
    /// </summary>
    public partial class UserControl_AnalysisDisplay : UserControl
    {
        #region 
        public ReportResult reportResult;
        private AssayLogic assayLogic;
        private List<DisplayCurve> curveList = new List<DisplayCurve>();
        private CurveMode myMode;

        private string currentChannel = string.Empty;
        private string currentPage = string.Empty;
        private int tabIndex = -1;
        bool Isdefault = false;
        #endregion

        public UserControl_AnalysisDisplay()
        {
            InitializeComponent();
            Log.WriteLog(LogType.system, "[UserControl_AnalysisDisplay]- InitializeComponent is done");
            myMode = CurveMode.RAW;
        }
        /// <summary>
        /// this method is mainly used to display analysis data, 
        /// which contains the methods of FillAnalysisInfo, FillDataGridViewTable,  
        /// LoadDataCurve and GetCurrentChannelPage.
        /// </summary>
        /// <param name="reportResult"></param>
        /// <param name="assayLogic"></param>
        /// <returns></returns>
        public bool DataAnalysisDisplay(ReportResult reportResult, AssayLogic assayLogic)
        {
            this.Cursor = Cursors.Arrow;
            this.reportResult = reportResult;
            this.assayLogic = assayLogic;

            try
            {
                this.reportResult.SampleResultList.Sort((a, b) => (InfoConvert.sortPositionAscending(a.AlphaNumPos,b.AlphaNumPos)));
                FillAnalysisInfo(0);
                FillDataGridViewTable(0, reportResult.SampleResultList[0].PageName);
                LoadDataCurve();

                GetCurrentChannelPage();

                btn_stdCurve.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

                return false;
            }

            return true;
        }

        private void FillAnalysisInfo(int channelIndex)
        {
            lblThreshold.Text = reportResult.SampleResultList[0].CtChannelList[channelIndex].Threshold.ToString();
            lblStartValue.Text = reportResult.SampleResultList[0].CtChannelList[channelIndex].Start.ToString();
            lblEndValue.Text = reportResult.SampleResultList[0].CtChannelList[channelIndex].End.ToString();
        }

        private void FillDataGridViewTable(int channelIndex, string pageName)
        {
            dtgSamples.Rows.Clear();

            for (int i = 0; i < reportResult.SampleResultList.Count;i++ )
            {
                SampleResult sampleResult = reportResult.SampleResultList[i];

                if (sampleResult.PageName == pageName)
                {
                    object[] obj = new object[6];

                    obj[0] = sampleResult.CtChannelList[channelIndex].Selected;

                    obj[1] = sampleResult.AlphaNumPos;

                    obj[2] = sampleResult.SampleName;

                    obj[3] = sampleResult.SampleType;

                    if (sampleResult.CtChannelList[channelIndex].CtValue != string.Empty)
                    {
                        double var = Convert.ToDouble(sampleResult.CtChannelList[channelIndex].CtValue);
                        obj[4] = var.ToString("F2");
                    }
                    else
                    {
                        obj[4] = string.Empty;
                    }

                    obj[5] = sampleResult.PageName;

                    dtgSamples.Rows.Add(obj);

                    dtgSamples.Rows[i].Cells[2].Style.BackColor = sampleResult.Colour;

                    dtgSamples.Rows[i].Cells[2].Style.ForeColor = Color.WhiteSmoke;
                }
            }

          //  dtgSamples.ClearSelection();
        }
       
        private void btn_allon_Click(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[UserControl_AnalysisDisplay]- Select all samples");

            CheckAllState(true);
        }
      
        private void btn_alloff_Click(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[UserControl_AnalysisDisplay]- Unselect all samples");

            CheckAllState(false);
        }

        private void dtgSamples_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Log.WriteLog(LogType.system, "[UserControl_AnalysisDisplay]- Cell Content Click");
            dtgSamples.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void dtgSamples_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            
            if (e.RowIndex>=0)
            {
                DataGridViewRow row = dtgSamples.Rows[e.RowIndex];
                
                ControlIndividualCurveDisplay(row);

                UpdateSampleSelected(row);
            }
        }

        private void UpdateSampleSelected(DataGridViewRow row)
        {
            //CTChannel ctChannel = reportResult.GetCTChannel(currentChannel, currentPage);

            //if (ctChannel.ChannelName != string.Empty)
            //{
            
            DataGridViewCheckBoxCell checkBox = row.Cells[0] as DataGridViewCheckBoxCell;

            string well = row.Cells[1].Value.ToString();   

            if (checkBox.Value.ToString() == true.ToString())
            {
                reportResult.UpdateSelected(well, currentChannel, currentPage, true);
            }
            else
            {
                reportResult.UpdateSelected(well, currentChannel, currentPage, false);
            }
            //}
        }

        private void ControlIndividualCurveDisplay(DataGridViewRow row)
        {
            DataGridViewCheckBoxCell checkBox = row.Cells[0] as DataGridViewCheckBoxCell;

            string alphanumeric = row.Cells[1].Value.ToString();

            int position = InfoConvert.GetNumericPosition(alphanumeric);

            int curveIndex = FindInCurveList(currentChannel, currentPage);

            if (checkBox.Value.ToString() == true.ToString())
            {
                if (curveIndex >= 0)
                {
                    EnableCurve(curveIndex, position, 1);
                }
            }
            else
            {
                if (curveIndex >= 0)
                {
                    curveList[curveIndex].RemoveHighLight();
                    EnableCurve(curveIndex, position, 0);
                }
            }
        }

        private void EnableCurve(int curveIndex, int position, int lineWidth)
        {
            curveList[curveIndex].EnableCurve(position, lineWidth);
        }

        private void dtgSamples_SelectionChanged(object sender, EventArgs e)
        {
            if (dtgSamples.SelectedRows.Count>0)
            {
                string alphanumeric = dtgSamples.SelectedRows[0].Cells[1].Value.ToString();

                int position = InfoConvert.GetNumericPosition(alphanumeric);

                int curveIndex = FindInCurveList(currentChannel, currentPage);

                if (curveIndex >= 0)
                {
                    HighlightCurve(curveIndex, position);

                    HightDataGridView(dtgSamples.SelectedRows[0].Index, dtgSamples.SelectedRows[0].Cells[2].Style.BackColor);
                }
            }
        }

        private void HighlightCurve(int curveIndex, int position)
        {
            curveList[curveIndex].RemoveHighLight();
            curveList[curveIndex].HighlightCurve(position);
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab != null)
            {
                GetCurrentChannelPage();

                if ((currentChannel != string.Empty) && (currentPage != string.Empty))
                {
                    int channelIndex = reportResult.GetChannelIndex(currentChannel, currentPage);

                    FillAnalysisInfo(channelIndex);

                    FillDataGridViewTable(channelIndex, currentPage);

                    ControlAllCurveDisplay();
                }
            }
        }

        private void GetCurrentChannelPage()
        {
            tabIndex = tabControl1.SelectedIndex;
            currentChannel = InfoConvert.GetChannelName(tabControl1.SelectedTab.Text);
            currentPage = InfoConvert.GetPageName(tabControl1.SelectedTab.Name);
          
        }

        private void CleanDataGridViewSelection()
        {
            dtgSamples.ClearSelection();
            dtgSamples.Refresh();
        }

        private void HightDataGridView(int rowIndex, Color color)
        {
            dtgSamples.CurrentCell = dtgSamples.Rows[rowIndex].Cells[0];
            dtgSamples.Rows[dtgSamples.CurrentCell.RowIndex].Cells[2].Style.SelectionBackColor = color;
            dtgSamples.Rows[dtgSamples.CurrentCell.RowIndex].Cells[2].Style.SelectionForeColor = Color.WhiteSmoke;
            dtgSamples.Refresh();
        }

        private void UserControl_AnalysisDisplay_Load(object sender, EventArgs e)
        {
            this.Size = this.Parent.Size;
        }
       
        private void btn_log_Click(object sender, EventArgs e)
        {
           Log.WriteLog(LogType.system, "[UserControl_AnalysisDisplay]- log button Click");
           ChangeLogButtonText();

           int currentindex = tabControl1.SelectedIndex;

           RefreshTabs();

           tabControl1.SelectedIndex = currentindex;
           
        }

        private void LoadDataCurve()
        {
            tabControl1.TabPages.Clear();
            curveList.Clear();

            foreach (SampleResult SR in reportResult.SampleResultList)
            {
                int index = 0;
                foreach (CTChannel channel in SR.CtChannelList)
                {
                    int curveID = FindInCurveList(channel.ChannelName, SR.PageName);
                    if (curveID== -1)
                    {
                        //New
                        DisplayCurve dc = new DisplayCurve(channel.ChannelName, SR.PageName,channel.VirusName);
                        dc.HighlightDataGridView = new dHighlightDataGridView(this.HightDataGridView);
                        dc.CleanDataGridViewSelection = new dCleanDataGridViewSelection(this.CleanDataGridViewSelection);
                        dc.ChangeCurSorToCrossForZoom = new ChangeCurSorToCrossForZoom(this.ChangeCursorToCross);
                        dc.ChangeCurSorToDefault = new ChangeCurSorToDefault(this.ChangeCursorToDefault);
                        dc.AddCurve(SR, index);
                        curveList.Add(dc); 
                    }
                    else
                    {   //Add exsiting
                        curveList[curveID].AddCurve(SR, index);
                    }
                    index++;
                }
            }
           
            RefreshTabs();
           

            Log.WriteLog(LogType.system, "[UserControl_AnalysisDisplay]- Load Data Curve is done");
        }
        private void ChangeCursorToCross()
        {
            Cursor = Cursors.Cross;
        }
        private void ChangeCursorToDefault()
        {
            Cursor = Cursors.Default;
        }

        private void RefreshTabs()
        {
            tabControl1.TabPages.Clear();

            int index = 0;

            try
            {
                foreach (DisplayCurve dc in curveList)
                {
                    TabPage tabPage = new TabPage();

                    double threshold = reportResult.SampleResultList[0].CtChannelList[index].Threshold;
             
                    Chart chart = dc.GetCurve(this.myMode, threshold);

                    tabPage.Controls.Add(chart);

                    chart.Show();

                    tabControl1.TabPages.Add(tabPage);

                    tabControl1.TabPages[index].Text = dc.GetTitle();
                    tabControl1.TabPages[index].Name = dc.getPageName() + "_" + index;
                    index++;
                }
            }
            catch (Exception e)
            { 
                MessageBox.Show(e.Message);
            }

            ControlAllCurveDisplay(); 
        }

        private void ControlAllCurveDisplay()
        {
            foreach (DataGridViewRow row in dtgSamples.Rows)
            {
                ControlIndividualCurveDisplay(row);
            }
        }

        private int FindInCurveList(string ChannelName, string PageName)
        {
            int index = -1;
            int indexCurve = 0;
            foreach (DisplayCurve dc in curveList)
            {   
                if (dc.Check(ChannelName,PageName))
                {
                    index = indexCurve;
                    break;
                }
                indexCurve++;
            }
            return index;
        }

        private void ChangeLogButtonText()
        {
            if (btn_log.Text == "Linear Scale")
            {
                Log.WriteLog(LogType.system, "Data Analysis - Display Linear Scale");

                btn_log.Text = "Log Scale";
                this.myMode = CurveMode.LINEAR;
            }
            else
            {
                Log.WriteLog(LogType.system, "Data Analysis - Display Log Scale");

                btn_log.Text = "Linear Scale";
                this.myMode = CurveMode.LOG;
            }
        }
      
        private void btn_autoscale_Click(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "Data Analysis - Auto Scale");
            Cursor = Cursors.Default;
            int tabindex = tabControl1.SelectedIndex;
            curveList[tabindex].Refresh();
            curveList[tabindex].AutoScale();
            DisplayCurve.IsZoomEnabled = false;
        }

        private void CheckAllState(bool isSelected)
        {
            foreach (DataGridViewRow row in dtgSamples.Rows)
            {
                row.Cells[0].Value = (object)isSelected;
            }
        }

        private void btn_rawdata_Click(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[UserControl_AnalysisDisplay]- RAW/Normalized button click");
            if (this.myMode == CurveMode.RAW)
            {
                if (this.btn_log.Text == "Linear Scale")
                {
                    Log.WriteLog(LogType.system, "[UserControl_AnalysisDisplay]-  Display Log Scale");
                    

                    this.myMode = CurveMode.LOG;
                }
                else
                {
                    Log.WriteLog(LogType.system, "[UserControl_AnalysisDisplay]-  Display Linear Scale");
                    

                    this.myMode = CurveMode.LINEAR;
                }
                
                this.btn_rawdata.Text = "Raw";
                this.btn_log.Enabled = true;
            }
            else
            {

                this.myMode = CurveMode.RAW;
                this.btn_rawdata.Text = "Delta Rn";
                this.btn_log.Enabled = false;
            }

            int currentindex = tabControl1.SelectedIndex;
            RefreshTabs();
            tabControl1.SelectedIndex = currentindex;
           
        }

        private void dtgSamples_DoubleClick(object sender, EventArgs e)
        {
            Log.WriteLog(LogType.system, "[UserControl_AnalysisDisplay]-  DataGridView Double Click");
            DataGridViewRow row = dtgSamples.Rows[dtgSamples.CurrentCell.RowIndex];

            DataGridViewCheckBoxCell checkBox = row.Cells[0] as DataGridViewCheckBoxCell;

            if ((bool)checkBox.Value)
            {
                MessageBox.Show(row.Cells[2].Value.ToString());
            }
        }

        private void btn_zoomin_Click(object sender, EventArgs e)
        {
            int curveIndex = FindInCurveList(currentChannel, currentPage);

            if (curveIndex>=0)
            {
                Log.WriteLog(LogType.system, "[UserControl_AnalysisDisplay] - Zoom In");

                curveList[curveIndex].ZoomIn();
                //Cursor = Cursors.Cross;
                DisplayCurve.IsZoomEnabled= true;
            }
        }

        private void btn_zoomout_Click(object sender, EventArgs e)
        {
            int curveIndex = FindInCurveList(currentChannel, currentPage);

            if (curveIndex >= 0)
            {
                //Log.WriteLog(LogType.system, "[UserControl_AnalysisDisplay] - Zoom Out");

                curveList[curveIndex].ZoomOut();
                Cursor = Cursors.Default;
                DisplayCurve.IsZoomEnabled = false;
            }
        }

        private void tabControl1_Resize(object sender, EventArgs e)
        {
            //Log.WriteLog(LogType.system, "[UserControl_AnalysisDisplay] - tab control resize");
            foreach (Control c in tabControl1.Controls)
            {
                c.Size = new Size(tabControl1.Size.Width-20,tabControl1.Size.Height-10);
            }
        }
       
    }


}
