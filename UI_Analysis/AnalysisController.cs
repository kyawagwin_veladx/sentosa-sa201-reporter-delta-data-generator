﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using M_ReportResult;
using M_Assay;

namespace UI_Analysis
{
    /// <summary>
    /// AnalysisController class contains AnalysisDisplay and AnalysisValue classes,
    /// which also contains a pubilc method named SetValue.
    /// </summary>
    public class AnalysisController
    {
        public UserControl_AnalysisDisplay AnalysisDisplay;
        public AnalysisPage AnalysisValue;

        public AnalysisController()
        {
            AnalysisDisplay = new UserControl_AnalysisDisplay();
            AnalysisValue = new AnalysisPage();
        }
        /// <summary>
        /// this method is used to set value for data display on the analysis page.
        /// </summary>
        /// <param name="reportResult"></param>
        /// <param name="assayLogic"></param>
        public void SetValue(ReportResult reportResult, AssayLogic assayLogic)
        {
            AnalysisDisplay.DataAnalysisDisplay(reportResult, assayLogic);

        }
    }
}
