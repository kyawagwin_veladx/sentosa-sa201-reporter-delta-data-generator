﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M_Language
{
    public static class SysMessage
    {
        //A
        public static string analyzeResultCompletedMsg = "Run in progress. Analyze results after run completion.";
        //B
        //C
        public static string confirmExitMsg = "Please click OK to exit.";
        public static string cancelMulselecteMsg = " cancels multi-assay selection";
        public static string cancelloginMsg = " cancels log in";
        public static string confirmSTDValidMsg = "Please confirm that the standard curve is still valid.";

        public static string cancelTestSelectMsg = " cancels test selection";
        public static string confirmWarmUpMsg = "Please click OK to perform warm up?";
        public static string confirmLogOffMsg = "Please click OK to log off.";
        public static string confirmCaption = "Confirm";
        public static string confirmRun = "Run cannot be cancelled once started. Please confirm.";
        public static string copySoftwareRunningMsg = "Software is currently running on this machine. Launching 'Analysis-Only' copy of the program.";
        public static string correctBarcode = "Please scan the correct barcode format.";
        public static string correctM1 = "Please scan the correct M1.";

        //E
        public static string exists = " exits the application";
        public static string errorCaption = "Error";

        public static string emptyConfig = "Assay config files not found. Please install assay packages.";
        public static string emptyPath = "Unable to locate file";

        //F
        public static string fileMissMsg ="Unable to find " ;
        public static string fileChecksumCaption = "File checksum error";
        public static string fileLocked = "The file is locked by other application.";
        public static string fileModified = "Empty/invalid checksum for the below CSV file. Please re-export the file to the run folder and don't modify the file content.";

        //G
        //H
  
        //I
        public static string invalidChechsum = "File Signature is invalid";
        public static string invalidSampleSize = "The sample size from the CSV file doesn't match the settings in assay config file";
        public static string InfoCaption = "Information";

        public static string invalidSampleFileMsg = "Invalid sample file. Please check the validity of the sample file.";
        public static string invalidRunFileMsg = "Invalid run files compared to the assay definition. Please check the validity of the run files.";
        public static string invalidCredential = "Invalid user name and/or password. Please try again.";
        public static string invalidInterpretationDll = "Invalid/Missing result interpretation dll file. Please ensure that the dll file for the assay is valid, for correct Results and Validities in the report.";
        public static string incorrectM1Barcode = "M1 barcode is inconsistent with test.\nPlease scan the correct M1 barcode.";
        public static string incorrectBarcode = "Sample ID barcode is inconsistent with the sample type.\nPlease scan correct barcode.";
        public static string incorrectSampleName = "Barcode is missing/incorrect for ";
        public static string invalidPath = "Unable to locate file";
        public static string corruptedConfig = "Assay config file is corrupted. Please ensure that file is valid: ";
        public static string invalidDirectory = "Cannot find the directory ";
        public static string incorrectM1 = "M1 is missing/incorrect for ";
        public static string invalidBarcode = "Sample barcode(s) inconsistent with the sample type. Please check the barcode(s) in CSV files.";


        //J
        //K
        //L
        public static string login = " Log In";
        public static string logoff = " Log Off";
        //M
        //N
        public static string notFoundMsg = " is not found. Please check your installation.";
        public static string noAssayTempMsg = "No respective assay found. Please install assay packages.";
        public static string noSupportedAssayMsg = "No supported assay found. Please install assay packages.";
        public static string noAssayForM1Msg = "No respective assay found. Please check the M1 number in the file or install assay packages.";
        public static string noDataAnalysisTempMsg = "No data analysis template found. Please install assay packages.";
        public static string noSamplesize = "Sample size for the imported file is not supported.";
        public static string noAnalysisData = "This run/file does not contain data. Cannot proceed to Analysis or Report page";
        public static string noUserName = "Please key in user name.";
        public static string noRetFile = "No template file found.";
        public static string noSTDHistoryFile = "No standard curve history file. Please ensure the file is existing: ";
        public static string noSDXFile = "No SDX file found. Please ensure the file is existing in the selected folder: ";
        public static string notInCSV = "CSV File doesn't have this data!";
        public static string noInterpretation = "Not available. Please contact Vela support.";
        public static string noTemplateMsg = "The template file(.sds) is not existing. Please install the respective assay package.";

        //O
        //P
        public static string programRunMsg = "Program is currently running.";
        public static string putFiles = "Run files should be placed into the below folder: ";

        //Q
        //R
        public static string rexObjEmptyMsg = "Run file missing. Unable to display real-time curve.";
        public static string runningMsg = "Run in progress. Do not close the Sentosa SA201 Reporter Software.";
        public static string ReportDllPathMsg = "Program can't find the relevant report .dll path.";
        public static string rgqCompletedMsg ="Run has completed.\nPlease click on the Next button to proceed to data analysis.";
        public static string rgqCompletedWithConfirmMdg = "Run has completed.\nPlease click Yes to analyze the results automatically, or click No to analyze manually.";

        //S
        public static string softwareinitializedFailMsg = "Software Initialization failed. Please ensure the base software has been successfully installed, and the default settings of the folder/files unchanged.";
        public static string selectManualMsg = " selects Create Sample List";
        public static string selectImportSampleFileMsg = " selects Import Sample List";
        public static string selectDataAnalysisMsg = " selects Data Analysis";
        public static string selectWarmUpMsg = " selects Warm Up";
        public static string selectMulassayMsg = "selects multi-assay";
        public static string systemStarts = "System has started";
        public static string selectTestMsg = "Select test message";
        public static string SA201 = "SA201";
        
         public static string stdHisFileMsg = "Standard curve history file for ";
         public static string selectedRunMsg = " has been selected. Please click OK to proceed.";

        //T
        public static string testMsg = " tests.";

        //U
        public static string user = "User ";
        public static string username = "User ";
        public static string password = "Password ";
        //V
        //W
        public static string WarningCaption = "Warning";
        //X

        //Y
        public static string smpDismatchedTempMsg = "Your smp file doesn't match with selected template file";
        public static string hasSelectedMsg = "You have selected ";
        //Z
        //Other
        public static string filePath = "\r\nFile Path:";

        public static string errorMsg = "";


    }
}
