﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 13/11/2013
 * Author: Yang Yi
 * 
 * Description:
 * SMP Controller: TubeObject class
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M_SampleList
{
    /// <summary>
    /// 
    /// </summary>
    public class SMPSample /*:ICloneable*/
    {
        #region Private variables
        private string sampleName = string.Empty;
        private string type = string.Empty;
        private string givenConC = string.Empty;
        private string selected = string.Empty;
        private string tubePosition = string.Empty;
        private string reagentType = string.Empty;
        private string reagentName = string.Empty;

        public string SampleName
        {
            get { return sampleName; }
            set { sampleName = value; }
        }

        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        public string GivenConC
        {
            get { return givenConC; }
            set { givenConC = value; }
        }

        public string Selected
        {
            get { return selected; }
            set { selected = value; }
        }

        public string TubePosition
        {
            get { return tubePosition; }
            set { tubePosition = value; }
        }
        public string ReagentType
        {
            get { return reagentType; }
            set { reagentType = value; }
        }

        public string ReagentName
        {
            get { return reagentName; }
            set { reagentName = value; }
        }


        #endregion

      

      
    }
}
