﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 13/11/2013
 * Author: Yang Yi
 * 
 * Description:
 * SMP Controller: Page class
 * 
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M_SampleList
{
    /// <summary>
    /// the page class contains the list of tubeobject items;
    /// </summary>
    public class SMPPage 
    {
        #region Private variables
        private string pageName = string.Empty; //M1 Item number
        private string m1 = string.Empty;
        private List<SMPSample> sampleList = new List<SMPSample>();
        #endregion

        #region Public properties
        public string PageName
        {
            get { return pageName; }
            set { pageName = value; }
        }

        public List<SMPSample> SampleList
        {
            get { return sampleList; }
            set { sampleList = value; }
        }

        public string M1
        {
            get { return m1; }
            set { m1 = value; }
        }
        #endregion
    }
}
