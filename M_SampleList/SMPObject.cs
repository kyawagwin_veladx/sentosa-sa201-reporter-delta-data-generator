﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 13/11/2013
 * Author: Yang Yi
 * 
 * Modification Date: 02/25/2014 - Liu Rui
 * Modification: Add all methods
 * 
 * Description:
 * SMP Controller: SMPObject class
 * 
 */
#endregion

using M_CheckSum;
using M_CommonMethods;
using M_ReportResult;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace M_SampleList
{
    /// <summary>
    /// The SMP object class has list of page items
    /// </summary>
    public class SMPObject
    {
        #region Private variables
        private string unit = string.Empty;
        private List<SMPPage> pageList = new List<SMPPage>();
        private List<IDListItem> iDListItems = new List<IDListItem>();
        private string givenConcFormat = string.Empty;
        #endregion

        #region Public properties

        public List<IDListItem> IDListItems
        {
            get { return iDListItems; }
        }

        public string Unit
        {
            get { return unit; }
            set { unit = value; }
        }

        public List<SMPPage> PageList
        {
            get { return pageList; }
            set { pageList = value; }
        }
        #endregion

        public List<string> GetReagentIDList()
        {
            List<string> targetList = new List<string>();
            foreach (SMPPage page in this.PageList)
            {
                if (!targetList.Contains(page.M1))
                {
                    targetList.Add(page.M1);
                }
            }
            return targetList;
        }

        /// <summary>
        /// Fill in the extra-information for assay, for instance, 
        /// ListName, ListType, CreationDate, AssayKit, ListID, 
        /// LysisKit, PlateID
        /// </summary>
        /// <param name="xdoc"></param>
        private void LoadIDListItems(XmlDocument xdoc)
        {
            iDListItems.Clear();
            XmlNodeList nodes = xdoc.DocumentElement.ChildNodes;

            foreach (XmlNode node in nodes)
            {
                if ((node.ChildNodes.Count <= 1) && (node.Name.ToLower() != "checksum") && (node.Name.ToLower() != "signature"))
                {
                    IDListItem iDListItem = new IDListItem();

                    iDListItem.Item = node.Name;
                    iDListItem.Value = node.InnerText;

                    iDListItems.Add(iDListItem);
                }
            }
        }

        public bool ReadFromSMPFile(string tag, string path)
        {
            this.pageList.Clear();

            XmlDocument xdoc = FileCheckSum.Load(path);

            if (xdoc.DocumentElement != null)
            {
                try
                {
                    givenConcFormat = xdoc.SelectSingleNode(tag + "/Format").InnerText;
                    unit = xdoc.SelectSingleNode(tag + "/ConcentrationUnit").InnerText;

                    XmlNodeList pageNodeList = xdoc.SelectNodes(tag + "/Page");

                    foreach (XmlNode pageNode in pageNodeList)
                    {
                        SMPPage smpPage = new SMPPage();
                        smpPage.PageName = pageNode.SelectSingleNode("Name").InnerText.Trim();

                        XmlNodeList sampleNodeList = pageNode.SelectNodes("Sample");

                        string m1 = string.Empty;

                        foreach (XmlNode sampleNode in sampleNodeList)
                        {
                            SMPSample smpSample = new SMPSample();

                            smpSample.TubePosition = sampleNode.SelectSingleNode("TubePosition").InnerText;
                            smpSample.SampleName = sampleNode.SelectSingleNode("Name").InnerText;
                            smpSample.GivenConC = sampleNode.SelectSingleNode("GivenConc").InnerText;
                            smpSample.Selected = sampleNode.SelectSingleNode("Selected").InnerText;
                            smpSample.Type = ((SampleType)(Convert.ToInt32(sampleNode.SelectSingleNode("Type").InnerText))).ToString();

                            if (sampleNode.SelectSingleNode("Reagents") != null)
                            {
                                XmlNodeList reagentNodes = sampleNode.SelectSingleNode("Reagents").SelectNodes("Reagent");

                                if (reagentNodes.Count > 0)
                                {
                                    foreach (XmlNode reagentNode in reagentNodes)
                                    {
                                        XmlNode typeNode = reagentNode.SelectSingleNode("Type");

                                        if ((typeNode != null) && (typeNode.InnerText.ToUpper() == "M1".ToUpper()))
                                        {
                                            smpSample.ReagentType = typeNode.InnerText;
                                            smpSample.ReagentName = reagentNode.SelectSingleNode("Name").InnerText;
                                            break;
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(smpSample.ReagentName) && smpSample.ReagentName.Contains("*M1*"))
                                    {
                                        m1 = smpSample.ReagentName;
                                    }
                                }
                            }

                            smpPage.SampleList.Add(smpSample);
                        }

                        var NCSample = smpPage.SampleList.Where(x => x.SampleName.Contains("*NC*"));
                        if (NCSample.Count() == 0)
                        {
                            return false;
                        }
                        smpPage.M1 = NCSample.First().ReagentName;
                        smpPage.PageName = InfoConvert.GetM1ItemNumber(smpPage.M1);
                        pageList.Add(smpPage);
                    }
                    iDListItems.Clear();
                    XmlNode EntryListNode = xdoc.SelectSingleNode(SMPSettings.smp_entryListXpath);

                    string fileName = Path.GetFileNameWithoutExtension(path);
                    string creationDate = File.GetCreationTime(path).ToString("M/d/yyyy h:m tt");
                    IDListItem idListItem = new IDListItem();
                    idListItem.Item = "ListID";
                    idListItem.Value = EntryListNode != null ? EntryListNode.InnerText : fileName;
                    iDListItems.Add(idListItem);
                    idListItem = new IDListItem();
                    idListItem.Item = "ListName";
                    idListItem.Value = EntryListNode != null ? EntryListNode.InnerText : fileName;
                    iDListItems.Add(idListItem);
                    idListItem = new IDListItem();
                    idListItem.Item = "CreationDate";
                    idListItem.Value = creationDate;
                    iDListItems.Add(idListItem);
                    return true;
                }
                catch
                {
                    throw new System.ArgumentException("Testing has some problem");
                }
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// fill in the smpobject by M1 string
        /// </summary>
        /// <param name="idListFile"></param>
        /// <returns></returns>
        public bool FillSMPObjectByIDList(string idListFile)
        {
            this.pageList.Clear();

            XmlDocument xdoc = FileCheckSum.Load(idListFile);

            if (xdoc.DocumentElement == null)
            {
                return false;
            }

            //Load IDListItems
            LoadIDListItems(xdoc);

            //Add SMPPages
            XmlNodeList barcodeNodes = xdoc.SelectNodes("ID_List/Positions/PositionData/ID_2");

            var xNodeM1 = (from XmlNode XN in barcodeNodes select XN.InnerText).Distinct();

            foreach (string XN in xNodeM1)
            {
                if (XN.Contains("*M1*"))
                {
                    SMPPage smpPage = new SMPPage();

                    smpPage.PageName = InfoConvert.GetM1ItemNumber(XN); //This pageName stores the M1 item number

                    smpPage.M1 = XN; //This stores the whole M1 barcode

                    this.pageList.Add(smpPage);
                }
            }

            //Add SMPSamples - These are hardcoding, assume there is only one M1
            XmlNodeList positionDataNodes = xdoc.SelectNodes("ID_List/Positions/PositionData");

            foreach (SMPPage page in pageList)
            {
                List<string> pageSamples = new List<string>();
                page.SampleList.Clear();

                foreach (XmlNode positionDataNode in positionDataNodes)
                {
                    string barcode = positionDataNode.SelectSingleNode("ID_2").InnerText;

                    if (page.M1 != barcode)
                    {
                        string position = positionDataNode.SelectSingleNode("Position").InnerText;

                        if (!pageSamples.Contains(position))
                        {
                            pageSamples.Add(position);

                            SMPSample sample = new SMPSample();
                            sample.TubePosition = position;
                            sample.SampleName = barcode;
                            sample.Selected = "True";
                            sample.ReagentType = "M1";
                            sample.ReagentName = page.M1;
                            page.SampleList.Add(sample);
                        }
                    }
                }
            }


            //Add SMPSamples - !!!!!!!!These are the correct codes, don't delete.!!!!!!!!!!!!
            //XmlNodeList positionDataNodes = xdoc.SelectNodes("ID_List/Positions/PositionData");

            //foreach (SMPPage page in pageList)
            //{
            //    List<string> pageSamples = new List<string>();
            //    page.SampleList.Clear();

            //    foreach (XmlNode positionDataNode in positionDataNodes)
            //    {
            //        string M1 = positionDataNode.SelectSingleNode("ID_2").InnerText;

            //        if (page.M1 == M1)
            //        {
            //            string m1Position = positionDataNode.SelectSingleNode("Position").InnerText;

            //            if (!pageSamples.Contains(m1Position))
            //            {
            //                pageSamples.Add(m1Position);

            //                SMPSample sample = new SMPSample();
            //                sample.TubePosition = m1Position;
            //                sample.SampleName = GetSampleName(m1Position, page.M1, positionDataNodes);
            //                sample.Selected = "True";

            //                page.SampleList.Add(sample);
            //            }
            //        }
            //    }
            //}

            return true;
        }

        //private string GetSampleName(string m1Position, string m1, XmlNodeList positionDataNodes)
        //{
        //    foreach (XmlNode positionDataNode in positionDataNodes)
        //    {
        //        string position = positionDataNode.SelectSingleNode("Position").InnerText;
        //        string barcode = positionDataNode.SelectSingleNode("ID_2").InnerText;

        //        if ((m1Position == position)&&(barcode!=m1))
        //        {
        //            return barcode;
        //        }
        //    }

        //    return string.Empty;
        //}

        public List<string> GetSelectedSamples(List<string> pageNameList)
        {
            List<string> targetArrayList = new List<string>();

            foreach (string pageName in pageNameList)
            {
                foreach (SMPPage page in this.PageList)
                {
                    if (pageName == page.PageName)
                    {
                        foreach (SMPSample sample in page.SampleList)
                        {
                            if (sample.Selected.ToUpper() == true.ToString().ToUpper())
                            {
                                targetArrayList.Add(sample.TubePosition);
                            }
                        }
                    }
                }
            }

            return targetArrayList;
        }

        public List<string> GetM1List()//add the pagename
        {
            List<string> targetPageNameList = new List<string>();

            foreach (SMPPage page in pageList)
            {
                targetPageNameList.Add(page.PageName);
            }

            return targetPageNameList;
        }

        public SMPSample GetSampleByPosition(string position)
        {
            SMPSample target = null;

            foreach (SMPPage smpPage in pageList)
            {
                foreach (SMPSample smpSample in smpPage.SampleList)
                {
                    if (smpSample.TubePosition == position)
                    {
                        return target = smpSample;
                    }
                }
            }

            return target;
        }

        public void UpdateM1(string position, string m1)
        {
            foreach (SMPPage page in pageList)
            {
                foreach (SMPSample sample in page.SampleList)
                {
                    if (position == sample.TubePosition)
                    {
                        page.M1 = m1;
                        break;
                    }
                }
            }
        }
    }
}
