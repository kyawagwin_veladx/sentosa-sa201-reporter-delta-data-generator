﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace M_CheckExit
{
    public class ExitCheck
    {
        public static bool IsProcessExisting(string exeName)
        {
            bool target = false;

            foreach (Process process in Process.GetProcesses())
            {
                if (process.ProcessName.ToUpper().Equals(exeName.ToUpper()))
                {
                    return target = true;
                }
            }

            return target;
        }

        public static bool IsSentosaSA201Existing(string exeName)
        {
            bool target = false;
            int count = 0;
            foreach (Process process in Process.GetProcesses())
            {
                if (process.ProcessName.ToUpper().Equals(exeName.ToUpper()))
                {
                    count++;
                }
            }

            if (count > 1)
            {
                target = true;
            }

            return target;
        }
    }
}
