﻿using M_Assay;
using M_ReportResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using M_CommonMethods;
using M_Log;



namespace QualitativeCombo
{
    public class QualitativeCombo
    {
        private string[] assayList = { "Tropical Multiple Assay" };

        public bool GetAllResults(string assayName
            , AssayLogic assayLogic
            , ReportResult reportResult)
        {
            if (!reportResult.IsAssayIncluded(assayName, assayList))
            {
                return false;
            }

            List<SampleResult> pageSampleResultList;
            List<string> pageNameList2;

            Qualitative qualitative;
            ReportResult childReportResult;
            AssayLogic childAssayLogic;
            string childAssayName;

            List<string> pageNameList = reportResult.GetUniquePageNameList();

            foreach (string pageName in pageNameList)
            {
                pageNameList2 = new List<string>();
                pageNameList2.Add(pageName);

                //get child assayName:
                string assayConfigFile = AssayInfo.GetAssayConfigFile(pageNameList2, assayLogic.AssayConfigFolder);
                childAssayName = AssayInfo.GetAssayName(assayConfigFile);

                //get child assayLogic:
                childAssayLogic = new AssayLogic();
                childAssayLogic.GetAssayLogicByPageName(pageNameList2, assayLogic.AssayConfigFolder);

                //get child reportResult:
                pageSampleResultList = ReportResult.GetSampleResultsByPage(pageName, reportResult.SampleResultList);
                childReportResult = new ReportResult();
                childReportResult.SampleResultList = pageSampleResultList;

                qualitative = new Qualitative();
                qualitative.GetAllResults(childAssayName, childAssayLogic, childReportResult);
            }

            return true;
        }
    }
    }
}
