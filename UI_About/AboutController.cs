﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using M_System;
using System.Windows.Forms;

namespace UI_About
{
    public class AboutController
    {
        internal About about;
        public Form_About myForm;
       

        public AboutController()
        {
            // TODO: Complete member initialization
            about = new About();
            myForm = new Form_About(this);
            
        }
        public void SetValues(SystemConfig systemconfig)
        {
            about.SetValues(systemconfig);

        }

        public DialogResult Display()
        {
            return this.myForm.ShowDialog();
        }

        internal void LaunchWebsite()
        {
            
        }


    }
}
