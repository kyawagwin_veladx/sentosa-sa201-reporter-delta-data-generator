﻿
using System.Windows.Forms;

namespace UI_About
{
    partial class Form_About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_About));
            this.label3 = new Label();
            this.label5 = new Label();
            this.btnOK = new Button();
            this.linklblWeb = new LinkLabel();
            this.kryptonLabel2 = new Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.kryptonLabel1 = new Label();
            this.kryptonLabel3 = new Label();
            this.lblVersion = new Label();
            this.lblAssayPackageVersion = new Label();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(17, 177);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 16);
            //this.label3.StateNormal.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.TabIndex = 3;
            this.label3.Text = "Sentosa®";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(17, 334);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 20);
            this.label5.TabIndex = 5;
            this.label5.Text = "Website:";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(131, 373);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 28);
            //this.btnOK.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // linklblWeb
            // 
            this.linklblWeb.Location = new System.Drawing.Point(98, 334);
            this.linklblWeb.Name = "linklblWeb";
            this.linklblWeb.Size = new System.Drawing.Size(32, 20);
            this.linklblWeb.TabIndex = 9;
            this.linklblWeb.Text = "Link";
            this.linklblWeb.LinkClicked += new LinkLabelLinkClickedEventHandler(this.linklblWeb_LinkClicked);
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.Location = new System.Drawing.Point(17, 314);
            this.kryptonLabel2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.Size = new System.Drawing.Size(335, 20);
            this.kryptonLabel2.TabIndex = 13;
            this.kryptonLabel2.Text = "2014 Vela Operations Singapore Pte Ltd. All rights reserved.";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::UI_About.Properties.Resources.about_us;
            this.pictureBox1.Location = new System.Drawing.Point(-1, -5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(358, 160);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.Location = new System.Drawing.Point(115, 202);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(88, 20);
            this.kryptonLabel1.TabIndex = 14;
            this.kryptonLabel1.Text = "kryptonLabel1";
            // 
            // kryptonLabel3
            // 
            this.kryptonLabel3.Location = new System.Drawing.Point(74, 175);
            this.kryptonLabel3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.kryptonLabel3.Name = "kryptonLabel3";
            this.kryptonLabel3.Size = new System.Drawing.Size(96, 20);
            this.kryptonLabel3.TabIndex = 15;
            this.kryptonLabel3.Text = "SA201 Reporter";
            // 
            // lblVersion
            // 
            this.lblVersion.Location = new System.Drawing.Point(17, 202);
            this.lblVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(90, 20);
            this.lblVersion.TabIndex = 2;
            this.lblVersion.Text = "Version/ Build:";
            // 
            // lblAssayPackageVersion
            // 
            this.lblAssayPackageVersion.Location = new System.Drawing.Point(16, 228);
            this.lblAssayPackageVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAssayPackageVersion.Name = "lblAssayPackageVersion";
            this.lblAssayPackageVersion.Size = new System.Drawing.Size(137, 20);
            this.lblAssayPackageVersion.TabIndex = 16;
            this.lblAssayPackageVersion.Text = "Assay Package Version:";
            // 
            // Form_About
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(356, 422);
            this.Controls.Add(this.lblAssayPackageVersion);
            this.Controls.Add(this.kryptonLabel3);
            this.Controls.Add(this.kryptonLabel1);
            this.Controls.Add(this.kryptonLabel2);
            this.Controls.Add(this.linklblWeb);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblVersion);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_About";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "About";
            this.Load += new System.EventHandler(this.Form_About_Load);
            //((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label3;
        private Label label5;
        private Button btnOK;
        private System.Windows.Forms.PictureBox pictureBox1;
        private LinkLabel linklblWeb;
        private Label kryptonLabel2;
        private Label kryptonLabel1;
        private Label kryptonLabel3;
        private Label lblVersion;
        private Label lblAssayPackageVersion;
    }
}