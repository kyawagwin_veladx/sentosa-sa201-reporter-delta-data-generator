﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using M_System;
using System.IO;

namespace UI_About
{
    public class About
    {
        private SystemConfig system;

        private string version;
        private string website;
        private string assayPackgesVersion;

        public About()
        {
            // TODO: Complete member initialization

        }

        public string Version { get { return this.version; } }

        public string Website { get { return this.website; } }

        public string AssayPackgesVersion {get {return this.assayPackgesVersion;}}


        internal void SetValues(SystemConfig systemconfig)
        {
            this.system = systemconfig;
            this.version = system.Version;
            this.website = system.Website;
            this.assayPackgesVersion = GetAssayPackgesVersion(systemconfig.Folder_AssayConfig);
        }

        private string GetAssayPackgesVersion(string assayConfigFolder)
        {
            string assayPackageVersionFile = assayConfigFolder + Path.DirectorySeparatorChar + "Version.txt";

            if (File.Exists(assayPackageVersionFile))
            {
               return File.ReadAllText(assayPackageVersionFile);
            }
            else
            {
                return "N.A.";
            }
        } 
    }
}
