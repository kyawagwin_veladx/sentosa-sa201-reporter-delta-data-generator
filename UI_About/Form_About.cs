﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 01/11/2013
 * Author: Liu Rui
 * 
 * Description:
 */
#endregion


using M_System;
using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace UI_About
{
    public partial class Form_About : Form
    {
        #region Constructors
        private AboutController aboutController;


        public Form_About(AboutController aboutController)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            this.aboutController = aboutController;
        } 
        #endregion

        #region Private Methods
        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void linklblWeb_LinkClicked(object sender, EventArgs e)
        {
            this.linklblWeb.LinkVisited = true;
            //Process.Start("Http://" + linklblWeb.Text);
            Process.Start(linklblWeb.Text);
            // aboutController.LaunchWebsite();
        }

        private void Form_About_Load(object sender, EventArgs e)
        {
          
            this.kryptonLabel1.Text = aboutController.about.Version;
            this.linklblWeb.Text = aboutController.about.Website;
            this.lblAssayPackageVersion.Text += " " + aboutController.about.AssayPackgesVersion;

        }

    }
}
