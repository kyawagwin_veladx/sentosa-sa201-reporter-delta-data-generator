﻿using M_ReportResult;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace M_Curve
{
    public delegate void dHighlightDataGridView(int text, Color color);
    public delegate void dCleanDataGridViewSelection();
    public delegate void ChangeCurSorToCrossForZoom();
    public delegate void ChangeCurSorToDefault();

    public class DisplayCurve
    {
        private Chart chart;
        private string channelName;
        private string pageName;
        private string virusName;
        private List<SampleResult> sampleList;
        private int channelIndex;
        private int seriesIndex = -1;
        private Color color = new Color();
        private int zoomCount = 0;
        public static bool IsZoomEnabled = false;
        public dHighlightDataGridView HighlightDataGridView;
        public dCleanDataGridViewSelection CleanDataGridViewSelection;
        public ChangeCurSorToCrossForZoom ChangeCurSorToCrossForZoom;
        public ChangeCurSorToDefault ChangeCurSorToDefault;

        public DisplayCurve(string channelName, string pageName, string virusName ="")
        {
            sampleList = new List<SampleResult>();
            this.channelName = channelName;
            this.pageName = pageName;
            this.virusName = virusName;
            
        }

        private void Chart_MouseLeave(object sender, EventArgs e)
        {
            ChangeCurSorToDefault();
        }

        private void Chart_MouseHover(object sender, EventArgs e)
        {
            if (IsZoomEnabled)
            {
                ChangeCurSorToCrossForZoom();
            }
        }

        private void CreateChart(int x, int y)
        {
            chart = new Chart();
            //chart.Size = new Size(480, 360);
            chart.Size = new Size(x, y);

            chart.ChartAreas.Add("Curve");

            chart.Dock = DockStyle.Fill;
            chart.MouseClick += chart_MouseClick;
            chart.SelectionRangeChanged += chart_SelectionRangeChanged;
            chart.SizeChanged += chart_SizeChanged;
            chart.MouseHover += Chart_MouseHover;
            chart.MouseLeave += Chart_MouseLeave;
        }

        private void chart_SizeChanged(object sender, EventArgs e)
        {

        }

        private void chart_SelectionRangeChanged(object sender, CursorEventArgs e)
        {
            zoomCount++;
        }

        private void chart_MouseClick(object sender, MouseEventArgs e)
        {
            seriesIndex = -1;

            HighlightCurve(e);

            if ((seriesIndex > 0) && (!color.IsEmpty))
            {
                HighlightDataGridView(seriesIndex, color);
            }
            else
            {
                CleanDataGridViewSelection();
            }
        }

        private void HighlightCurve(MouseEventArgs e)
        {
            try
            {
                var pos = e.Location;

                HitTestResult result = chart.HitTest(pos.X, pos.Y);

                RemoveHighLight();

                if (result.Series != null)
                {
                    if (result.Series.Name.Equals("Threshold"))
                    {
                        MessageBox.Show("This is the threshold line.");
                        return;
                    }

                    int seriesName = Convert.ToInt16(result.Series.Name);


                    HighlightCurve(seriesName);

                    seriesIndex = GetSeriesIndex(seriesName);

                    if (seriesIndex > 0)
                    {
                        color = chart.Series[seriesIndex].Color;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private int GetSeriesIndex(int serIndex)
        {
            int target = -1;

            for (int i = 0; i < chart.Series.Count; i++)
            {
                if (chart.Series[i].Name == serIndex.ToString())
                {
                    return target = i;
                }
            }

            return target;
        }

        public void AutoScale()
        {
            chart.ChartAreas["Curve"].CursorX.IsUserEnabled = false;
            chart.ChartAreas["Curve"].CursorY.IsUserEnabled = false;
            chart.ChartAreas["Curve"].CursorX.IsUserSelectionEnabled = false;
            chart.ChartAreas["Curve"].CursorY.IsUserSelectionEnabled = false;
            chart.ChartAreas["Curve"].AxisX.ScaleView.Zoomable = false;
            chart.ChartAreas["Curve"].AxisY.ScaleView.Zoomable = false;

            for (int i = 0; i < zoomCount; i++)
            {
                chart.ChartAreas["Curve"].AxisX.ScaleView.ZoomReset();
                chart.ChartAreas["Curve"].AxisY.ScaleView.ZoomReset();
            }

            zoomCount = 0;
        }

        private void AddChartInfo()
        {
            // chart.ChartAreas["Curve"].AxisX.Maximum = sampleList[0].CtChannelList[0].RawPoints.Count;

            chart.ChartAreas["Curve"].AxisX.Title = "Cycle Number";

            chart.ChartAreas["Curve"].AxisX.Minimum = 0;

            chart.ChartAreas["Curve"].AxisY.IsStartedFromZero = false;

            chart.ChartAreas["Curve"].AxisX.Interval = 5;

            chart.ChartAreas["Curve"].AxisX.MajorGrid.LineColor = Color.LightGray;

            chart.ChartAreas["Curve"].AxisY.MajorGrid.LineColor = Color.LightGray;

            chart.ChartAreas["Curve"].AxisY.IsLabelAutoFit = true;

            chart.ChartAreas["Curve"].AxisY.ScaleView.SmallScrollMinSize = 0.1;

            chart.ChartAreas["Curve"].AxisY.ScaleView.SmallScrollSize = 0.1;

            chart.ChartAreas["Curve"].AxisY.ScaleView.MinSize = 0.1;

        }



        public void AddCurve(SampleResult SR, int ChannelIndex)
        {
            this.sampleList.Add(SR);
            this.channelIndex = ChannelIndex;
        }

        private void AddThreshold(double threshold, bool isLinear)
        {

            chart.Series.Add("Threshold");
            chart.Series["Threshold"].ChartType = SeriesChartType.Line;

            chart.Series["Threshold"].Color = Color.Red;

            if (!isLinear)
            {
                //threshold = Math.Log10(threshold);
            }

            for (int i = 0; i <= chart.ChartAreas["Curve"].AxisX.Maximum; i++)
            {
                chart.Series["Threshold"].Points.Add(threshold);
            }
        }

        private void GetRawChart(double threshold)
        {
            try
            {
                double recordpoint = 0.0;

                CreateChart(500, 350);
                if (!string.IsNullOrEmpty(this.virusName))
                {
                    chart.Titles.Add(channelName + " (" + this.virusName + ")" + " Rn vs Cycle");
                }
                else
                {
                    chart.Titles.Add(channelName + " (" + pageName + ")" + " Rn vs Cycle");
                }
                

                foreach (SampleResult SR in sampleList)
                {
                    chart.Series.Add(SR.Position.ToString());

                    chart.Series[SR.Position.ToString()].ChartType = SeriesChartType.Line;


                    foreach (double d in SR.CtChannelList[channelIndex].RawPoints)
                    {
                        chart.Series[SR.Position.ToString()].Points.Add(d);
                        if (Math.Abs(d) > recordpoint)
                        {
                            recordpoint = d;
                        }
                        //  i++;
                    }

                    chart.Series[SR.Position.ToString()].Color = SR.Colour;
                }

                chart.ChartAreas["Curve"].AxisX.Maximum = sampleList[0].CtChannelList[0].RawPoints.Count;

                // if (recordpoint > 1000)
                {
                    chart.ChartAreas["Curve"].AxisY.LabelStyle.Format = "{0.000##e+000}";
                }

                AddChartInfo();


                chart.ChartAreas["Curve"].AxisY.Title = "Rn";


            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void GetDltaRNLinearChart(double threshold)
        {
            try
            {

                double recordpoint = 0.0;

                CreateChart(500, 350);

                if (!string.IsNullOrEmpty(this.virusName))
                {
                    chart.Titles.Add(channelName + " (" + this.virusName + ")" + " Delta Rn vs Cycle");
                }
                else
                {
                    chart.Titles.Add(channelName + " (" + pageName + ")" + " Delta Rn vs Cycle");
                }

                
                foreach (SampleResult SR in sampleList)
                {
                    chart.Series.Add(SR.Position.ToString());
                    chart.Series[SR.Position.ToString()].ChartType = SeriesChartType.Line;


                    //int i = 0;
                    foreach (double d in SR.CtChannelList[channelIndex].AnalysisPoints)
                    {
                        chart.Series[SR.Position.ToString()].Points.AddXY(0, d);
                        if (Math.Abs(d) > recordpoint)
                        {
                            recordpoint = d;
                        }
                        //i++;

                    }

                    chart.Series[SR.Position.ToString()].Color = SR.Colour;
                }
                chart.ChartAreas["Curve"].AxisX.Maximum = sampleList[0].CtChannelList[0].AnalysisPoints.Count;

                //if (recordpoint > 1000)
                {
                    chart.ChartAreas["Curve"].AxisY.LabelStyle.Format = "{0.000##e+000}";
                }

                AddChartInfo();
                chart.ChartAreas["Curve"].AxisY.Title = "Delta Rn";
                AddThreshold(threshold, true);

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void GetLogChart(double threshold)
        {
            try
            {

                double recordpoint = 0.0;

                CreateChart(500, 350);


                if (!string.IsNullOrEmpty(this.virusName))
                {
                    chart.Titles.Add(channelName + " (" + this.virusName + ")" + " Delta Rn vs Cycle");
                }
                else
                {
                    chart.Titles.Add(channelName + " (" + pageName + ")" + " Delta Rn vs Cycle");
                }
                

                foreach (SampleResult SR in sampleList)
                {
                    chart.Series.Add(SR.Position.ToString());
                    chart.Series[SR.Position.ToString()].ChartType = SeriesChartType.Line;

                    // int i = 0;

                    foreach (double d in SR.CtChannelList[channelIndex].AnalysisPoints)
                    {
                        if (d > 0)
                        {
                            if (Math.Abs(d) > recordpoint)
                            {
                                recordpoint = d;
                            }

                            // chart.Series[SR.Position.ToString()].Points.Add(Math.Log10(d));//.AddXY(0, Math.Log10(d));
                            chart.Series[SR.Position.ToString()].Points.Add(d);
                        }
                        else
                        {
                            DataPoint point = chart.Series[SR.Position.ToString()].Points.Add(0);
                            point.IsEmpty = true;
                            chart.Series[SR.Position.ToString()].EmptyPointStyle.Color = Color.Transparent;
                        }
                    }
                    chart.Series[SR.Position.ToString()].Color = SR.Colour;
                }

                chart.ChartAreas["Curve"].AxisX.Maximum = sampleList[0].CtChannelList[0].AnalysisPoints.Count;

                AddChartInfo();


                chart.ChartAreas["Curve"].AxisY.IsLogarithmic = true;

                // chart.ChartAreas["Curve"].AxisY.Title = "Lg [Delta Rn]";

                chart.ChartAreas["Curve"].AxisY.LabelStyle.Format = "{0.000##e+000}";

                chart.ChartAreas["Curve"].AxisY.Title = "Delta Rn";

                AddThreshold(threshold, false);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

        }

        public Chart GetCurve(CurveMode mode, double threshold)
        {
            if (mode == CurveMode.RAW)
            {
                GetRawChart(threshold);
            }

            if (mode == CurveMode.LINEAR)
            {
                GetDltaRNLinearChart(threshold);
            }

            if (mode == CurveMode.LOG)
            {
                GetLogChart(threshold);
            }

            return this.chart;
        }

        public bool Check(string ChannelName, string PageName)
        {
            if (this.channelName == ChannelName && this.pageName == PageName)
                return true;
            else
                return false;
        }

        public string GetTitle()
        {
            if (!string.IsNullOrEmpty(this.virusName))
            {
                return this.channelName + " (" + this.virusName + ")";
            }
            else
            {
                return this.channelName + " (" + this.pageName + ")";
            }
        }

        public string getPageName()
        {
            return this.pageName;
        }

        public void HighlightCurve(int position)
        {
            if (chart.Series[position.ToString()].BorderWidth > 0)
            {
                chart.Series[position.ToString()].MarkerSize = 5;
                chart.Series[position.ToString()].MarkerColor = Color.HotPink;
                chart.Series[position.ToString()].MarkerStyle = MarkerStyle.Circle;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        public void RemoveHighLight()
        {
            for (int i = 0; i < chart.Series.Count; i++)
            {
                chart.Series[i].MarkerSize = 0;
            }
        }

        public void SaveImage(string filepath)
        {
            /*
            MemoryStream ms = new MemoryStream(4096);
            this.chart.SaveImage(ms, ImageFormat.Png);
            Image img = Image.FromStream(ms);
            Graphics g = Graphics.FromImage(img);
            g.DrawImage(img, 0, 0, 800, 600);
            img.Save(filepath, ImageFormat.Png);
            */

            this.chart.SaveImage(filepath, ChartImageFormat.Png);
        }

        public void EnableCurve(int position, int lineWidth)
        {
            chart.Series[position.ToString()].BorderWidth = lineWidth;
        }

        public void ZoomIn()
        {
            chart.ChartAreas["Curve"].CursorX.IsUserEnabled = true;
            chart.ChartAreas["Curve"].CursorX.IsUserSelectionEnabled = true;
            chart.ChartAreas["Curve"].AxisX.ScaleView.Zoomable = true;
            chart.ChartAreas["Curve"].AxisX.ScrollBar.Enabled = false;
            chart.ChartAreas["Curve"].CursorX.Interval = 0.00001;

            chart.ChartAreas["Curve"].CursorY.IsUserEnabled = true;
            chart.ChartAreas["Curve"].CursorY.IsUserSelectionEnabled = true;
            chart.ChartAreas["Curve"].AxisY.ScaleView.Zoomable = true;
            chart.ChartAreas["Curve"].AxisY.ScrollBar.Enabled = false;
            chart.ChartAreas["Curve"].CursorY.Interval = 0.00001;
        }

        public void ZoomOut()
        {
            chart.ChartAreas["Curve"].AxisX.ScaleView.ZoomReset();
            chart.ChartAreas["Curve"].AxisY.ScaleView.ZoomReset();
        }

        public void Refresh()
        {

        }
    }

    public enum CurveMode
    {
        LOG,
        LINEAR,
        RAW
    }
}
