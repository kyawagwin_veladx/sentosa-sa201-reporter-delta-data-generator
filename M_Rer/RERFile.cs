﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 6/2/2014
 * Author: Yang Yi
 * 
 * Description:
 * RER File Module
 * 
 */
#endregion

using M_CheckSum;
using M_CommonMethods;
using M_ReportResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace M_Rer
{
    public class RERFile
    {
        private ReportResult reportResult;
      
        public void GenerateRER(ReportResult reportResult, string savePath)
        {
            this.reportResult = reportResult;

            XmlDocument doc = new XmlDocument();

            XmlNode xDeclaration = doc.AppendChild(doc.CreateXmlDeclaration("1.0", "UTF-8", null));

            XmlNode xAssayResults = doc.AppendChild(doc.CreateElement("VELAResults"));

            SDSFileName(doc, xAssayResults);

            TubeResults(doc, xAssayResults);

            DataTableBasic(doc, xAssayResults);

            DataTable(doc, xAssayResults);

            NCTable(doc, xAssayResults);

            PCQSTable(doc, xAssayResults);

            doc.Save(savePath);

            Signature(savePath);
        }

        private void Signature(string filePath)
        {
            FileCheckSum.ModifyCheckSum(filePath);
        }

        private void SDSFileName(XmlDocument doc, XmlNode xAssayResults)
        {
            XmlNode xRexfilename = xAssayResults.AppendChild(doc.CreateElement("SDSFilename"));
            xRexfilename.InnerText = reportResult.RunInfo.RunFilePath;
            XmlNode xNotes = xAssayResults.AppendChild(doc.CreateElement("Notes"));
            xNotes.InnerText = reportResult.RunInfo.GetNotes();
            XmlNode xUser = xAssayResults.AppendChild(doc.CreateElement("User"));
            xUser.InnerText = reportResult.RunInfo.GetOperator();
            XmlNode xRunSignature = xAssayResults.AppendChild(doc.CreateElement("RunSignature"));
            xRunSignature.InnerText = InfoConvert.ConvertONOFF(reportResult.RunInfo.IsSignatureValid);
            XmlNode xRunStart = xAssayResults.AppendChild(doc.CreateElement("RunStart"));
            xRunStart.InnerText = InfoConvert.ConvertRunTime(reportResult.RunInfo.RunStart);
            XmlNode xLastModified = xAssayResults.AppendChild(doc.CreateElement("LastModified"));
            xLastModified.InnerText = InfoConvert.ConvertRunTime(reportResult.RunInfo.LastModified);
            XmlNode xSoftwareVersion = xAssayResults.AppendChild(doc.CreateElement("SoftwareVersion"));
            xSoftwareVersion.InnerText = reportResult.RunInfo.GetVersion();
            XmlNode xInstrumentType = xAssayResults.AppendChild(doc.CreateElement("InstrumentType"));
            xInstrumentType.InnerText = reportResult.RunInfo.InstumentType;
            XmlNode xOperator = xAssayResults.AppendChild(doc.CreateElement("Operator"));
            xOperator.InnerText = reportResult.RunInfo.GetBaseOperator();
            XmlNode xComments = xAssayResults.AppendChild(doc.CreateElement("Comments"));
            xComments.InnerText = reportResult.RunInfo.BaseNotes;
            XmlNode xVolume = xAssayResults.AppendChild(doc.CreateElement("PCRVolume"));
            xVolume.InnerText = reportResult.RunInfo.PCRVolume;
            XmlNode xLotNum = xAssayResults.AppendChild(doc.CreateElement("LotNum"));
            xLotNum.InnerText = reportResult.GetM1Lot();
            XmlNode xEntryListFileName = xAssayResults.AppendChild(doc.CreateElement("EntryListFileName"));
            xEntryListFileName.InnerText = "";   
            XmlNode xComputerName = xAssayResults.AppendChild(doc.CreateElement("ComputerName"));
            xComputerName.InnerText = "";

            //2014-Oct-07 Liu Rui
            //foreach (var item in reportResult.IdListInfo)
            //{
            //    XmlNode xnode = xAssayResults.AppendChild(doc.CreateElement(item.Key));
            //    xnode.InnerText = item.Value;
            //}

            foreach (IDListItem item in reportResult.IDListItems)
            {
                XmlNode xnode = xAssayResults.AppendChild(doc.CreateElement(item.Item));
                xnode.InnerText = item.Value;
            }

            XmlNode xLibKit = xAssayResults.AppendChild(doc.CreateElement("LibKit"));
            xLibKit.InnerText = "";

            XmlNode xExtractionKit = xAssayResults.AppendChild(doc.CreateElement("ExtractionKit"));
            xExtractionKit.InnerText = "";

            XmlNode xPlatformSN = xAssayResults.AppendChild(doc.CreateElement("PlatformSN"));
            xPlatformSN.InnerText = "";

            XmlNode xSXSN = xAssayResults.AppendChild(doc.CreateElement("SXSN"));
            xSXSN.InnerText = "";

            XmlNode xLysisBeads = xAssayResults.AppendChild(doc.CreateElement("LysisBeads"));
            xLysisBeads.InnerText = "";
        }

        private void TubeResults(XmlDocument doc, XmlNode xAssayResults)
        {
            XmlNode xTubeResult = xAssayResults.AppendChild(doc.CreateElement("TubeResults"));

            for (int i = 0; i < reportResult.SampleResultList.Count; i++)
            {
                XmlNode xTube = xTubeResult.AppendChild(doc.CreateElement("Tube"));

                XmlAttribute xRotorPos = xTube.Attributes.Append(doc.CreateAttribute("RotorPosition"));
                xRotorPos.Value = reportResult.SampleResultList[i].Position.ToString();

                XmlNode xSampelName = xTube.AppendChild(doc.CreateElement("VELASampleName"));
                xSampelName.InnerText = reportResult.SampleResultList[i].SampleName;

                XmlNode xAssayName = xTube.AppendChild(doc.CreateElement("VELAAssayName"));
                xAssayName.InnerText = reportResult.RunInfo.AssayName;
                /*
                for (int j = 0; j < reportResult.SampleResultList[i].CtChannelList.Count; j++)
                {
                    XmlNode xCt = xTube.AppendChild(doc.CreateElement("Ct"));
                    xCt.InnerText = reportResult.SampleResultList[i].CtChannelList[j].CtValue;

                    XmlAttribute xChannel = xCt.Attributes.Append(doc.CreateAttribute("Channel"));
                    xChannel.Value = reportResult.SampleResultList[i].CtChannelList[j].ChannelName;
                }
                 */
            }
        }

        private void NCTable(XmlDocument doc, XmlNode xAssayResults)
        {
            XmlNode xNCData = xAssayResults.AppendChild(doc.CreateElement("NCData"));

            XmlNode xControlBar = xNCData.AppendChild(doc.CreateElement("ControlBarcode"));
            XmlNode xCtValue = xNCData.AppendChild(doc.CreateElement("CtValue"));
            XmlNode xValidity = xNCData.AppendChild(doc.CreateElement("Validity"));
            XmlNode xResult = xNCData.AppendChild(doc.CreateElement("Result"));
            foreach(var sample in reportResult.SampleResultList)   
            {
                if(sample.SampleType==SampleType.NC)
                {
                    xControlBar.InnerText += sample.SampleName + "\n";

                    foreach(var channel in sample.CtChannelList)
                    {
                        xCtValue.InnerText += channel.ChannelName + ":" + channel.CtValue+ "\n";
                    }
                    xValidity.InnerText += sample.Validity + "\n";

                    xResult.InnerText += sample.TestResult.Replace(Environment.NewLine, String.Empty) + "\n";
                }
            }
        }


        private void PCQSTable(XmlDocument doc, XmlNode xAssayResults)
        {
            XmlNode xPCQSData = xAssayResults.AppendChild(doc.CreateElement("PCQSData"));

            XmlNode xControlBar = xPCQSData.AppendChild(doc.CreateElement("ControlBarcode"));
            
            XmlNode xCtValue = xPCQSData.AppendChild(doc.CreateElement("CtValue"));
            XmlNode xValidity = xPCQSData.AppendChild(doc.CreateElement("Validity"));
            XmlNode xResult = xPCQSData.AppendChild(doc.CreateElement("Result"));
            foreach (var sample in reportResult.SampleResultList)
            {
                if (sample.SampleType == SampleType.PC || sample.SampleType==SampleType.Standard)
                {
                    xControlBar.InnerText += sample.SampleName + "\n";

                    foreach (var channel in sample.CtChannelList)
                    {
                        xCtValue.InnerText += channel.ChannelName + ":" + channel.CtValue + "\n";
                    }
                    xValidity.InnerText += sample.Validity + "\n";

                    xResult.InnerText += sample.TestResult.Replace(Environment.NewLine, String.Empty) + "\n";
                }
            }
        }

        private void DataTableBasic(XmlDocument doc, XmlNode xAssayResults)
        {
            XmlNode xDataTable = xAssayResults.AppendChild(doc.CreateElement("DataTable"));

            XmlNode xTitle = xDataTable.AppendChild(doc.CreateElement("Title"));
            xTitle.InnerText = "Vela Sample Results";

            XmlNode xColHeader = xDataTable.AppendChild(doc.CreateElement("ColHeaders"));
            /*
            XmlNode xHeader = xColHeader.AppendChild(doc.CreateElement("Header"));
            xHeader.InnerText = "No.";
            XmlAttribute xVisible = xHeader.Attributes.Append(doc.CreateAttribute("Visible"));
            xVisible.Value = "Yes";
            XmlAttribute xDataCol = xHeader.Attributes.Append(doc.CreateAttribute("DataCol"));
            xDataCol.Value = "No.";


            XmlNode xHeader1 = xColHeader.AppendChild(doc.CreateElement("Header"));
            xHeader1.InnerText = "Sample ID";
            XmlAttribute xVisible1 = xHeader1.Attributes.Append(doc.CreateAttribute("Visible"));
            xVisible1.Value = "Yes";
            XmlAttribute xDataCol1 = xHeader1.Attributes.Append(doc.CreateAttribute("DataCol"));
            xDataCol1.Value = "Sample ID";


            XmlNode xHeader2 = xColHeader.AppendChild(doc.CreateElement("Header"));
            xHeader2.InnerText = "Sample Type";
            XmlAttribute xVisible2 = xHeader2.Attributes.Append(doc.CreateAttribute("Visible"));
            xVisible2.Value = "Yes";
            XmlAttribute xDataCol2 = xHeader2.Attributes.Append(doc.CreateAttribute("DataCol"));
            xDataCol2.Value = "Sample Type";


            XmlNode xHeader3 = xColHeader.AppendChild(doc.CreateElement("Header"));
            xHeader3.InnerText = "Control Ct";
            XmlAttribute xVisible3 = xHeader3.Attributes.Append(doc.CreateAttribute("Visible"));
            xVisible3.Value = "Yes";
            XmlAttribute xDataCol3 = xHeader3.Attributes.Append(doc.CreateAttribute("DataCol"));
            xDataCol3.Value = "Control Ct";


            XmlNode xHeader4 = xColHeader.AppendChild(doc.CreateElement("Header"));
            xHeader4.InnerText = "Delta Ct";


            XmlAttribute xVisible4 = xHeader4.Attributes.Append(doc.CreateAttribute("Visible"));
            xVisible4.Value = "Yes";

            XmlAttribute xDataCol4 = xHeader4.Attributes.Append(doc.CreateAttribute("DataCol"));
            xDataCol4.Value = "Delta Ct";

            XmlNode xHeader5 = xColHeader.AppendChild(doc.CreateElement("Header"));
            xHeader5.InnerText = "Target";


            XmlAttribute xVisible5 = xHeader5.Attributes.Append(doc.CreateAttribute("Visible"));
            xVisible5.Value = "Yes";

            XmlAttribute xDataCol5 = xHeader5.Attributes.Append(doc.CreateAttribute("DataCol"));
            xDataCol5.Value = "Target";


            XmlNode xHeader6 = xColHeader.AppendChild(doc.CreateElement("Header"));
            xHeader6.InnerText = "Flags/Warnings";


            XmlAttribute xVisible6 = xHeader6.Attributes.Append(doc.CreateAttribute("Visible"));
            xVisible6.Value = "Yes";

            XmlAttribute xDataCol6 = xHeader6.Attributes.Append(doc.CreateAttribute("DataCol"));
            xDataCol6.Value = "Flags/Warnings";


            XmlNode xHeader7 = xColHeader.AppendChild(doc.CreateElement("Header"));
            xHeader7.InnerText = "VELA Mutation Status";


            XmlAttribute xVisible7 = xHeader7.Attributes.Append(doc.CreateAttribute("Visible"));
            xVisible7.Value = "Yes";

            XmlAttribute xDataCol7 = xHeader7.Attributes.Append(doc.CreateAttribute("DataCol"));
            xDataCol7.Value = "VELA Mutation Status";
            */
        }

        private void DataTable(XmlDocument doc, XmlNode xAssayResults)
        {
            XmlNode xDataTable = xAssayResults.AppendChild(doc.CreateElement("DataTable"));

            XmlNode xTitle = xDataTable.AppendChild(doc.CreateElement("Title"));

            xTitle.InnerText=reportResult.RunInfo.AssayName+" Assay Results";

            XmlNode xColHeaders = xDataTable.AppendChild(doc.CreateElement("ColHeaders"));

            //XmlNode xHeader = xColHeaders.AppendChild(doc.CreateElement("Header"));
            //xHeader.InnerText = "No.";

            //XmlAttribute xVisible = xHeader.Attributes.Append(doc.CreateAttribute("Visible"));
            //xVisible.Value = "Yes";

            //XmlAttribute xDataCol = xHeader.Attributes.Append(doc.CreateAttribute("DataCol"));
            //xDataCol.Value = "No.";

            Header(doc, xColHeaders, "No.", "Yes", "No.");
            //XmlNode xHeader2 = xColHeaders.AppendChild(doc.CreateElement("Header"));
            //xHeader2.InnerText = "Well";

            //XmlAttribute xVisible2 = xHeader2.Attributes.Append(doc.CreateAttribute("Visible"));
            //xVisible2.Value = "Yes";

            //XmlAttribute xDataCol2 = xHeader2.Attributes.Append(doc.CreateAttribute("DataCol"));
            //xDataCol2.Value = "Well";

            Header(doc, xColHeaders, "Well", "Yes", "Well");
            //XmlNode xHeader3 = xColHeaders.AppendChild(doc.CreateElement("Header"));
            //xHeader3.InnerText = "Run ID";

            //XmlAttribute xVisible3 = xHeader3.Attributes.Append(doc.CreateAttribute("Visible"));
            //xVisible3.Value = "Yes";

            //XmlAttribute xDataCol3 = xHeader3.Attributes.Append(doc.CreateAttribute("DataCol"));
            //xDataCol3.Value = "Run ID";
            Header(doc, xColHeaders, "Run ID", "Yes", "Run ID");
            //XmlNode xHeader33 = xColHeaders.AppendChild(doc.CreateElement("Header"));
            //xHeader33.InnerText = "DateTime";

            //XmlAttribute xVisible33 = xHeader33.Attributes.Append(doc.CreateAttribute("Visible"));
            //xVisible33.Value = "Yes";

            //XmlAttribute xDataCol33 = xHeader33.Attributes.Append(doc.CreateAttribute("DataCol"));
            //xDataCol33.Value = "DateTime";

            Header(doc, xColHeaders, "DateTime", "Yes", "DateTime");

            //XmlNode xHeader4 = xColHeaders.AppendChild(doc.CreateElement("Header"));
            //xHeader4.InnerText = "Name of Assay";

            //XmlAttribute xVisible4 = xHeader4.Attributes.Append(doc.CreateAttribute("Visible"));
            //xVisible4.Value = "Yes";

            //XmlAttribute xDataCol4 = xHeader4.Attributes.Append(doc.CreateAttribute("DataCol"));
            //xDataCol4.Value = "Name of Assay";

            Header(doc, xColHeaders, "Name of Assay", "Yes", "Name of Assay");

            //XmlNode xHeader5 = xColHeaders.AppendChild(doc.CreateElement("Header"));
            //xHeader5.InnerText = "Sample Type";

            //XmlAttribute xVisible5 = xHeader5.Attributes.Append(doc.CreateAttribute("Visible"));
            //xVisible5.Value = "Yes";

            //XmlAttribute xDataCol5 = xHeader5.Attributes.Append(doc.CreateAttribute("DataCol"));
            //xDataCol5.Value = "Sample Type";

            Header(doc, xColHeaders, "Sample Type", "Yes", "Sample Type");

            for (int j = 0; j < reportResult.SampleResultList[0].CtChannelList.Count; j++)
            {
            
                XmlNode xHeader6 = xColHeaders.AppendChild(doc.CreateElement("Header"));
                xHeader6.InnerText = "Ct " + InfoConvert.ChannelNameConverter(reportResult.SampleResultList[0].CtChannelList[j].ChannelName) + " Channel";


                XmlAttribute xVisible6 = xHeader6.Attributes.Append(doc.CreateAttribute("Visible"));
                xVisible6.Value = "Yes";

                XmlAttribute xDataCol6 = xHeader6.Attributes.Append(doc.CreateAttribute("DataCol"));
                xDataCol6.Value = "Ct " + InfoConvert.ChannelNameConverter(reportResult.SampleResultList[0].CtChannelList[j].ChannelName) + " Channel";
            }
            
            //XmlNode xHeader7 = xColHeaders.AppendChild(doc.CreateElement("Header"));
            //xHeader7.InnerText = "Validity";

            //XmlAttribute xVisible7 = xHeader7.Attributes.Append(doc.CreateAttribute("Visible"));
            //xVisible7.Value = "Yes";

            //XmlAttribute xDataCol7 = xHeader7.Attributes.Append(doc.CreateAttribute("DataCol"));
            //xDataCol7.Value = "Validity";
            Header(doc, xColHeaders, "Validity", "Yes", "Validity");
            //XmlNode xHeader8 = xColHeaders.AppendChild(doc.CreateElement("Header"));
            //xHeader8.InnerText = "Result";

            //XmlAttribute xVisible8 = xHeader8.Attributes.Append(doc.CreateAttribute("Visible"));
            //xVisible8.Value = "Yes";

            //XmlAttribute xDataCol8 = xHeader8.Attributes.Append(doc.CreateAttribute("DataCol"));
            //xDataCol8.Value = "Result";

            Header(doc, xColHeaders, "Result", "Yes", "Result");
            
            //XmlNode xHeader10 = xColHeaders.AppendChild(doc.CreateElement("Header"));
            //xHeader10.InnerText = "M1Barcode";

            //XmlAttribute xVisible10 = xHeader10.Attributes.Append(doc.CreateAttribute("Visible"));
            //xVisible10.Value = "Yes";

            //XmlAttribute xDataCol10 = xHeader10.Attributes.Append(doc.CreateAttribute("DataCol"));
            //xDataCol10.Value = "M1Barcode";
            Header(doc, xColHeaders, "M1Barcode", "Yes", "M1Barcode");

            Header(doc, xColHeaders, "Notes", "Yes", "Notes");
            //XmlNode xHeader11 = xColHeaders.AppendChild(doc.CreateElement("Header"));
            //xHeader11.InnerText = "Notes";

            //XmlAttribute xVisible11 = xHeader11.Attributes.Append(doc.CreateAttribute("Visible"));
            //xVisible11.Value = "Yes";

            //XmlAttribute xDataCol11 = xHeader11.Attributes.Append(doc.CreateAttribute("DataCol"));
            //xDataCol11.Value = "Notes";

            //XmlNode xHeader12 = xColHeaders.AppendChild(doc.CreateElement("Header"));
            //xHeader12.InnerText = "SoftwareVersion";

            //XmlAttribute xVisible12 = xHeader12.Attributes.Append(doc.CreateAttribute("Visible"));
            //xVisible12.Value = "Yes";

            //XmlAttribute xDataCol12 = xHeader12.Attributes.Append(doc.CreateAttribute("DataCol"));
            //xDataCol12.Value = "SoftwareVersion";
            Header(doc, xColHeaders, "SoftwareVersion", "Yes", "SoftwareVersion");
            //XmlNode xHeader13 = xColHeaders.AppendChild(doc.CreateElement("Header"));
            //xHeader13.InnerText = "STDInfo";

            //XmlAttribute xVisible13 = xHeader12.Attributes.Append(doc.CreateAttribute("Visible"));
            //xVisible13.Value = "Yes";

            //XmlAttribute xDataCol13 = xHeader12.Attributes.Append(doc.CreateAttribute("DataCol"));
            //xDataCol13.Value = "STDInfo";

            Header(doc, xColHeaders, "STDInfo", "Yes","STDInfo");

            Header(doc, xColHeaders, "GivenConc", "Yes", "GivenConc");

            Header(doc, xColHeaders, "CalConc", "Yes", "CalConc");

            Header(doc, xColHeaders, "CtComments", "Yes", "CtComments");

            Header(doc, xColHeaders, "ISResult", "Yes", "ISResult");

            Header(doc, xColHeaders, "RatioResult", "Yes", "RatioResult");
           
            Row(doc, xDataTable);
        }


        private void Header(XmlDocument doc, XmlNode xColHeaders, string innertext, string visiblevalue, string data)
        {
            XmlNode xHeader = xColHeaders.AppendChild(doc.CreateElement("Header"));
            xHeader.InnerText = innertext;

            XmlAttribute xVisible = xHeader.Attributes.Append(doc.CreateAttribute("Visible"));
            xVisible.Value = visiblevalue;

            XmlAttribute xDataCol = xHeader.Attributes.Append(doc.CreateAttribute("DataCol"));
            xDataCol.Value = data;
        }

        private void Row(XmlDocument doc, XmlNode xDataTable)
        {
            for (int i = 0; i < reportResult.SampleResultList.Count; i++)
            {
                XmlNode xRow = xDataTable.AppendChild(doc.CreateElement("Row"));

                XmlNode xCol = xRow.AppendChild(doc.CreateElement("Col"));

                XmlAttribute xVisibleCol = xCol.Attributes.Append(doc.CreateAttribute("Visible"));
                xVisibleCol.Value = "Yes";

                XmlAttribute xColoured = xCol.Attributes.Append(doc.CreateAttribute("Coloured"));
                xColoured.Value = "No";

                XmlAttribute xFormat = xCol.Attributes.Append(doc.CreateAttribute("Format"));
                xFormat.Value = "";

                XmlAttribute xColcol = xCol.Attributes.Append(doc.CreateAttribute("Col"));
                xColcol.Value = "No.";

                XmlAttribute xDataCol = xCol.Attributes.Append(doc.CreateAttribute("DataCol"));
                xDataCol.Value = "No.";

                XmlNode xData = xCol.AppendChild(doc.CreateElement("Data"));
                xData.InnerText = reportResult.SampleResultList[i].Position.ToString();
                /////////////////

                   XmlNode xCol2 = xRow.AppendChild(doc.CreateElement("Col"));

                   XmlAttribute xVisibleCol2 = xCol2.Attributes.Append(doc.CreateAttribute("Visible"));
                   xVisibleCol2.Value = "Yes";

                   XmlAttribute xColoured2 = xCol2.Attributes.Append(doc.CreateAttribute("Coloured"));
                   xColoured2.Value = "No";

                   XmlAttribute xFormat2 = xCol2.Attributes.Append(doc.CreateAttribute("Format"));
                   xFormat2.Value = "";

                   XmlAttribute xColcol2 = xCol2.Attributes.Append(doc.CreateAttribute("Col"));
                   xColcol2.Value = "Well";

                   XmlAttribute xDataCol2 = xCol2.Attributes.Append(doc.CreateAttribute("DataCol"));
                   xDataCol2.Value = "Well";

                   XmlNode xData2 = xCol2.AppendChild(doc.CreateElement("Data"));
                   xData2.InnerText =  reportResult.SampleResultList[i].AlphaNumPos;
                /////////////////////////////////////////////////////
                   XmlNode xCol33 = xRow.AppendChild(doc.CreateElement("Col"));

                   XmlAttribute xVisibleCol33 = xCol33.Attributes.Append(doc.CreateAttribute("Visible"));
                   xVisibleCol33.Value = "Yes";

                   XmlAttribute xColoured33 = xCol33.Attributes.Append(doc.CreateAttribute("Coloured"));
                   xColoured33.Value = "No";

                   XmlAttribute xFormat33 = xCol33.Attributes.Append(doc.CreateAttribute("Format"));
                   xFormat33.Value = "";

                   XmlAttribute xColcol33 = xCol33.Attributes.Append(doc.CreateAttribute("Col"));
                   xColcol33.Value = "RunID";

                   XmlAttribute xDataCol33 = xCol33.Attributes.Append(doc.CreateAttribute("DataCol"));
                   xDataCol33.Value = "RunID";

                   XmlNode xData33 = xCol33.AppendChild(doc.CreateElement("Data"));
                   xData33.InnerText = reportResult.RunInfo.RunID;
                ////////////////////////////////////
                   XmlNode xCol331 = xRow.AppendChild(doc.CreateElement("Col"));

                   XmlAttribute xVisibleCol331 = xCol331.Attributes.Append(doc.CreateAttribute("Visible"));
                   xVisibleCol331.Value = "Yes";

                   XmlAttribute xColoured331 = xCol331.Attributes.Append(doc.CreateAttribute("Coloured"));
                   xColoured33.Value = "No";

                   XmlAttribute xFormat331 = xCol331.Attributes.Append(doc.CreateAttribute("Format"));
                   xFormat331.Value = "";

                   XmlAttribute xColcol331 = xCol331.Attributes.Append(doc.CreateAttribute("Col"));
                   xColcol331.Value = "DateTime";

                   XmlAttribute xDataCol331 = xCol331.Attributes.Append(doc.CreateAttribute("DataCol"));
                   xDataCol331.Value = "DateTime";

                   XmlNode xData331 = xCol331.AppendChild(doc.CreateElement("Data"));
                   xData331.InnerText = reportResult.RunInfo.RunStart;
                                       
                /////////////////////////////////////////////////////////////////////////////////////
                XmlNode xCol3 = xRow.AppendChild(doc.CreateElement("Col"));

                XmlAttribute xVisibleCol3 = xCol3.Attributes.Append(doc.CreateAttribute("Visible"));
                xVisibleCol3.Value = "Yes";

                XmlAttribute xColoured3 = xCol3.Attributes.Append(doc.CreateAttribute("Coloured"));
                xColoured3.Value = "No";

                XmlAttribute xFormat3 = xCol3.Attributes.Append(doc.CreateAttribute("Format"));
                xFormat3.Value = "";

                XmlAttribute xColcol3 = xCol3.Attributes.Append(doc.CreateAttribute("Col"));
                xColcol3.Value = "Name of Assay";

                XmlAttribute xDataCol3 = xCol3.Attributes.Append(doc.CreateAttribute("DataCol"));
                xDataCol3.Value = "Name of Assay";

                XmlNode xData3 = xCol3.AppendChild(doc.CreateElement("Data"));
                xData3.InnerText = reportResult.RunInfo.AssayName;

                ////////////////////////////////////////////////////////////////////////////////////
                XmlNode xCol4 = xRow.AppendChild(doc.CreateElement("Col"));

                XmlAttribute xVisibleCol4 = xCol4.Attributes.Append(doc.CreateAttribute("Visible"));
                xVisibleCol4.Value = "Yes";

                XmlAttribute xColoured4 = xCol4.Attributes.Append(doc.CreateAttribute("Coloured"));
                xColoured4.Value = "No";

                XmlAttribute xFormat4 = xCol4.Attributes.Append(doc.CreateAttribute("Format"));
                xFormat4.Value = "";

                XmlAttribute xColcol4 = xCol4.Attributes.Append(doc.CreateAttribute("Col"));
                xColcol4.Value = "Sample Type";

                XmlAttribute xDataCol4 = xCol4.Attributes.Append(doc.CreateAttribute("DataCol"));
                xDataCol4.Value = "Sample Type";

                XmlNode xData4 = xCol4.AppendChild(doc.CreateElement("Data"));
                xData4.InnerText = reportResult.SampleResultList[i].SampleTypeString(reportResult.SampleResultList[i].SampleType);

                ////////////////////////////////////////////////////////////////////////////////////////////////

                for (int j = 0; j < reportResult.SampleResultList[0].CtChannelList.Count; j++)
                {
                    XmlNode xCol5 = xRow.AppendChild(doc.CreateElement("Col"));

                    XmlAttribute xVisibleCol5 = xCol5.Attributes.Append(doc.CreateAttribute("Visible"));
                    xVisibleCol5.Value = "Yes";

                    XmlAttribute xColoured5 = xCol5.Attributes.Append(doc.CreateAttribute("Coloured"));
                    xColoured5.Value = "No";

                    XmlAttribute xFormat5 = xCol5.Attributes.Append(doc.CreateAttribute("Format"));
                    xFormat5.Value = "";

                    XmlAttribute xColcol5 = xCol5.Attributes.Append(doc.CreateAttribute("Col"));
                    xColcol5.Value = "Ct " + InfoConvert.ChannelNameConverter(reportResult.SampleResultList[0].CtChannelList[j].ChannelName) + " Channel";

                    XmlAttribute xDataCol5 = xCol5.Attributes.Append(doc.CreateAttribute("DataCol"));
                    xDataCol5.Value = "Ct " +InfoConvert.ChannelNameConverter( reportResult.SampleResultList[0].CtChannelList[j].ChannelName) + " Channel";

                    XmlNode xData5 = xCol5.AppendChild(doc.CreateElement("Data"));
                    xData5.InnerText = reportResult.SampleResultList[i].CtChannelList[j].CtValue;
                  
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////
                XmlNode xCol6 = xRow.AppendChild(doc.CreateElement("Col"));

                XmlAttribute xVisibleCol6 = xCol6.Attributes.Append(doc.CreateAttribute("Visible"));
                xVisibleCol6.Value = "Yes";

                XmlAttribute xColoured6 = xCol6.Attributes.Append(doc.CreateAttribute("Coloured"));
                xColoured6.Value = "No";

                XmlAttribute xFormat6 = xCol6.Attributes.Append(doc.CreateAttribute("Format"));
                xFormat6.Value = "";

                XmlAttribute xColcol6 = xCol6.Attributes.Append(doc.CreateAttribute("Col"));
                xColcol6.Value = "Validity";

                XmlAttribute xDataCol6 = xCol6.Attributes.Append(doc.CreateAttribute("DataCol"));
                xDataCol6.Value = "Validity";

                XmlNode xData6 = xCol6.AppendChild(doc.CreateElement("Data"));
                xData6.InnerText = reportResult.SampleResultList[i].Validity;

                ////////////////////////////////////////////////////////////////////////////////////////////////
                XmlNode xCol7 = xRow.AppendChild(doc.CreateElement("Col"));

                XmlAttribute xVisibleCol7 = xCol7.Attributes.Append(doc.CreateAttribute("Visible"));
                xVisibleCol7.Value = "Yes";

                XmlAttribute xColoured7 = xCol7.Attributes.Append(doc.CreateAttribute("Coloured"));
                xColoured7.Value = "No";

                XmlAttribute xFormat7 = xCol7.Attributes.Append(doc.CreateAttribute("Format"));
                xFormat7.Value = "";

                XmlAttribute xColcol7 = xCol7.Attributes.Append(doc.CreateAttribute("Col"));
                xColcol7.Value = "Result";

                XmlAttribute xDataCol7 = xCol7.Attributes.Append(doc.CreateAttribute("DataCol"));
                xDataCol7.Value = "Result";

                XmlNode xData7 = xCol7.AppendChild(doc.CreateElement("Data"));
                xData7.InnerText = reportResult.SampleResultList[i].TestResult.Replace(Environment.NewLine, String.Empty);
                ////////////////////////////////////////////////////////////////////////////////////////////////
                /*
                XmlNode xCol8 = xRow.AppendChild(doc.CreateElement("Col"));

                XmlAttribute xVisibleCol8 = xCol8.Attributes.Append(doc.CreateAttribute("Visible"));
                xVisibleCol8.Value = "Yes";

                XmlAttribute xColoured8 = xCol8.Attributes.Append(doc.CreateAttribute("Coloured"));
                xColoured7.Value = "No";

                XmlAttribute xFormat8 = xCol8.Attributes.Append(doc.CreateAttribute("Format"));
                xFormat8.Value = "";

                XmlAttribute xColcol8 = xCol8.Attributes.Append(doc.CreateAttribute("Col"));
                xColcol8.Value = "Intepretation";

                XmlAttribute xDataCol8 = xCol8.Attributes.Append(doc.CreateAttribute("DataCol"));
                xDataCol8.Value = "Intepretation";

                XmlNode xData8 = xCol8.AppendChild(doc.CreateElement("Data"));
                xData8.InnerText = reportResult.SampleResultList[i].TestResult.Replace(Environment.NewLine, String.Empty);
                 * */
                ////////////////////////////////////////////////////////////////////////////////////////////////
                XmlNode xCol9 = xRow.AppendChild(doc.CreateElement("Col"));

                XmlAttribute xVisibleCol9 = xCol9.Attributes.Append(doc.CreateAttribute("Visible"));
                xVisibleCol9.Value = "Yes";

                XmlAttribute xColoured9 = xCol9.Attributes.Append(doc.CreateAttribute("Coloured"));
                xColoured9.Value = "No";

                XmlAttribute xFormat9 = xCol9.Attributes.Append(doc.CreateAttribute("Format"));
                xFormat9.Value = "";

                XmlAttribute xColcol9 = xCol9.Attributes.Append(doc.CreateAttribute("Col"));
                xColcol9.Value = "M1Barcode";

                XmlAttribute xDataCol9 = xCol9.Attributes.Append(doc.CreateAttribute("DataCol"));
                xDataCol9.Value = "M1Barcode";

                XmlNode xData9 = xCol9.AppendChild(doc.CreateElement("Data"));
                xData9.InnerText = reportResult.SampleResultList[i].M1;//reportresult.SampleResultList[i].TestResult.Replace(Environment.NewLine, String.Empty);

                ////////////////////////////////////////////////////////////////////////////////////////////////
                XmlNode xCol99 = xRow.AppendChild(doc.CreateElement("Col"));

                XmlAttribute xVisibleCol99 = xCol99.Attributes.Append(doc.CreateAttribute("Visible"));
                xVisibleCol99.Value = "Yes";

                XmlAttribute xColoured99 = xCol99.Attributes.Append(doc.CreateAttribute("Coloured"));
                xColoured99.Value = "No";

                XmlAttribute xFormat99 = xCol99.Attributes.Append(doc.CreateAttribute("Format"));
                xFormat99.Value = "";

                XmlAttribute xColcol99 = xCol99.Attributes.Append(doc.CreateAttribute("Col"));
                xColcol99.Value = "Notes";

                XmlAttribute xDataCol99 = xCol99.Attributes.Append(doc.CreateAttribute("DataCol"));
                xDataCol9.Value = "Notes";

                XmlNode xData99 = xCol99.AppendChild(doc.CreateElement("Data"));
                xData99.InnerText = reportResult.RunInfo.GetNotes();


                ////////////////////////////////////////////////////////////////////////////////////////////////
                XmlNode xCol10 = xRow.AppendChild(doc.CreateElement("Col"));

                XmlAttribute xVisibleCol10 = xCol10.Attributes.Append(doc.CreateAttribute("Visible"));
                xVisibleCol10.Value = "Yes";

                XmlAttribute xColoured10 = xCol10.Attributes.Append(doc.CreateAttribute("Coloured"));
                xColoured10.Value = "No";

                XmlAttribute xFormat10 = xCol10.Attributes.Append(doc.CreateAttribute("Format"));
                xFormat10.Value = "";

                XmlAttribute xColcol10 = xCol10.Attributes.Append(doc.CreateAttribute("Col"));
                xColcol10.Value = "SoftwareVersion";

                XmlAttribute xDataCol10 = xCol10.Attributes.Append(doc.CreateAttribute("DataCol"));
                xDataCol10.Value = "SoftwareVersion";

                XmlNode xData10 = xCol10.AppendChild(doc.CreateElement("Data"));
                xData10.InnerText = reportResult.RunInfo.GetVersion();//reportresult.SampleResultList[i].TestResult.Replace(Environment.NewLine, String.Empty);

                ////////////////////////////////////////////////////////////////////////////////////////////////
                
                XmlNode xCol11 = xRow.AppendChild(doc.CreateElement("Col"));

                XmlAttribute xVisibleCol11 = xCol11.Attributes.Append(doc.CreateAttribute("Visible"));
                xVisibleCol11.Value = "Yes";

                XmlAttribute xColoured11 = xCol11.Attributes.Append(doc.CreateAttribute("Coloured"));
                xColoured11.Value = "No";

                XmlAttribute xFormat11 = xCol11.Attributes.Append(doc.CreateAttribute("Format"));
                xFormat11.Value = "";

                XmlAttribute xColcol11 = xCol11.Attributes.Append(doc.CreateAttribute("Col"));
                xColcol11.Value = "STDInfo";

                XmlAttribute xDataCol11 = xCol11.Attributes.Append(doc.CreateAttribute("DataCol"));
                xDataCol11.Value = "STDInfo";
                
                XmlNode xData11 = xCol11.AppendChild(doc.CreateElement("Data"));
                xData11.InnerText = "";
                  //  reportResult.SampleResultList[i].CtChannelList[0].;
                //////////////////////////////////////////////////////////////////////////////////////////////////
                XmlNode xCol12 = xRow.AppendChild(doc.CreateElement("Col"));

                XmlAttribute xVisibleCol12 = xCol12.Attributes.Append(doc.CreateAttribute("Visible"));
                xVisibleCol12.Value = "Yes";

                XmlAttribute xColoured12 = xCol12.Attributes.Append(doc.CreateAttribute("Coloured"));
                xColoured12.Value = "No";

                XmlAttribute xFormat12 = xCol12.Attributes.Append(doc.CreateAttribute("Format"));
                xFormat12.Value = "";

                XmlAttribute xColcol12 = xCol12.Attributes.Append(doc.CreateAttribute("Col"));
                xColcol12.Value = "GivenConc";

                XmlAttribute xDataCol12 = xCol12.Attributes.Append(doc.CreateAttribute("DataCol"));
                xDataCol12.Value = "GivenConc";

                XmlNode xData12 = xCol12.AppendChild(doc.CreateElement("Data"));

                foreach(var item in reportResult.SampleResultList[i].CtChannelList)
                {
                    xData12.InnerText += item.ChannelName + ": " + item.GivenConc.ToString();
                }
                ///////////////////////////////////////////////////////////////////////////////////////////
                XmlNode xCol13 = xRow.AppendChild(doc.CreateElement("Col"));

                XmlAttribute xVisibleCol13 = xCol13.Attributes.Append(doc.CreateAttribute("Visible"));
                xVisibleCol13.Value = "Yes";

                XmlAttribute xColoured13 = xCol13.Attributes.Append(doc.CreateAttribute("Coloured"));
                xColoured13.Value = "No";

                XmlAttribute xFormat13 = xCol13.Attributes.Append(doc.CreateAttribute("Format"));
                xFormat13.Value = "";

                XmlAttribute xColcol13 = xCol13.Attributes.Append(doc.CreateAttribute("Col"));
                xColcol13.Value = "CalConc";

                XmlAttribute xDataCol13 = xCol13.Attributes.Append(doc.CreateAttribute("DataCol"));
                xDataCol13.Value = "CalConc";

                XmlNode xData13 = xCol13.AppendChild(doc.CreateElement("Data"));

                foreach (var item in reportResult.SampleResultList[i].CtChannelList)
                {
                    xData13.InnerText += item.ChannelName + ": " + item.CalcConc.ToString();
                }
                ///////////////////////////////////////////////////////////////////////////////////////////
                XmlNode xCol14 = xRow.AppendChild(doc.CreateElement("Col"));

                XmlAttribute xVisibleCol14 = xCol14.Attributes.Append(doc.CreateAttribute("Visible"));
                xVisibleCol14.Value = "Yes";

                XmlAttribute xColoured14 = xCol14.Attributes.Append(doc.CreateAttribute("Coloured"));
                xColoured14.Value = "No";

                XmlAttribute xFormat14 = xCol14.Attributes.Append(doc.CreateAttribute("Format"));
                xFormat14.Value = "";

                XmlAttribute xColcol14 = xCol14.Attributes.Append(doc.CreateAttribute("Col"));
                xColcol14.Value = "CtComments";

                XmlAttribute xDataCol14 = xCol14.Attributes.Append(doc.CreateAttribute("DataCol"));
                xDataCol14.Value = "CtComments";

                XmlNode xData14 = xCol14.AppendChild(doc.CreateElement("Data"));

                
                xData14.InnerText += "";
               
                 ///////////////////////////////////////////////////////////////////////////////////////////
                XmlNode xCol15 = xRow.AppendChild(doc.CreateElement("Col"));

                XmlAttribute xVisibleCol15 = xCol15.Attributes.Append(doc.CreateAttribute("Visible"));
                xVisibleCol15.Value = "Yes";

                XmlAttribute xColoured15 = xCol15.Attributes.Append(doc.CreateAttribute("Coloured"));
                xColoured15.Value = "No";

                XmlAttribute xFormat15 = xCol15.Attributes.Append(doc.CreateAttribute("Format"));
                xFormat15.Value = "";

                XmlAttribute xColcol15 = xCol15.Attributes.Append(doc.CreateAttribute("Col"));
                xColcol15.Value = "ISResult";

                XmlAttribute xDataCol15 = xCol15.Attributes.Append(doc.CreateAttribute("DataCol"));
                xDataCol15.Value = "ISResult";

                XmlNode xData15 = xCol15.AppendChild(doc.CreateElement("Data"));

               
                xData15.InnerText ="";
                
                 ///////////////////////////////////////////////////////////////////////////////////////////
                XmlNode xCol16 = xRow.AppendChild(doc.CreateElement("Col"));

                XmlAttribute xVisibleCol16 = xCol16.Attributes.Append(doc.CreateAttribute("Visible"));
                xVisibleCol16.Value = "Yes";

                XmlAttribute xColoured16 = xCol16.Attributes.Append(doc.CreateAttribute("Coloured"));
                xColoured16.Value = "No";

                XmlAttribute xFormat16 = xCol16.Attributes.Append(doc.CreateAttribute("Format"));
                xFormat16.Value = "";

                XmlAttribute xColcol16 = xCol16.Attributes.Append(doc.CreateAttribute("Col"));
                xColcol16.Value = "RatioResult";

                XmlAttribute xDataCol16 = xCol16.Attributes.Append(doc.CreateAttribute("DataCol"));
                xDataCol16.Value = "RatioResult";

                XmlNode xData16 = xCol16.AppendChild(doc.CreateElement("Data"));

                xData16.InnerText = "";
            }
        }

     //   private string GetResult(int i)
      //  {
           // string retresult = reportResult.SampleResultList[i].TestResult.Replace(Environment.NewLine, String.Empty);
            /*
            for (int j = 0; j < reportresult.ChannelList.Count; j++)
            {
                string result = string.Empty;
                if (reportresult.SampleResultList[i].CtChannelList[j].GivenConc != string.Empty)
                {
                    result = result + " , Given Conc.:" + reportresult.SampleResultList[i].CtChannelList[j].GivenConc;
                }
                if (reportresult.SampleResultList[i].CtChannelList[j].CalcConc != string.Empty)
                {
                    result = result + " , Cal. Conc.:" + reportresult.SampleResultList[i].CtChannelList[j].CalcConc;
                }
                if (reportresult.SampleResultList[i].CtChannelList[j].CtComment != string.Empty)
                {
                    result = result + " , Ct Comment:" + reportresult.SampleResultList[i].CtChannelList[j].CtComment;
                }
                foreach (Custom cus in reportresult.SampleResultList[i].CtChannelList[j].CustomList)
                {
                    if (cus.HeadText != string.Empty)
                    {
                        if (cus.IsShow)
                        {

                            result = result + " , " + cus.HeadText + ":" + cus.Value;
                        }
                    }
                }
                if (result != string.Empty)
                {
                    result = " , " + reportresult.ChannelList[j].ChannelShortName + " Channel:" + result;
                }
                retresult = retresult + result;
            }
             */
          //  retresult = retresult + " , SN:" + reportresult.RunInfo.SerialNum;

            //retresult = retresult + " , FileVer:" + reportresult.RunInfo.GetSARVersionFromNotes();
        //    retresult = retresult + " , SystemVer:" + reportResult.RunInfo.SysVersion;

            //retresult = retresult + " , PlatformVer:" + reportresult.RunInfo.PlatformVersion;
           // retresult = retresult + " , M1:" + reportresult.GetM1ByPosition(i);

          //  return retresult;
    //    }
    }
}
