﻿using ComponentFactory.Krypton.Toolkit;
namespace Sentosa_Reporter
{
    partial class Form_AssaySelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_AssaySelection));
            this.btnOK = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnCancel = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.lsvMode = new System.Windows.Forms.ListView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.lsvAssayName = new System.Windows.Forms.ListView();
            this.lsvSampleSize = new System.Windows.Forms.ListView();
            this.label2 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.label3 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.l = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(583, 432);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 28);
            this.btnOK.TabIndex = 5;
            this.btnOK.Values.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(708, 432);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Values.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lsvMode
            // 
            this.lsvMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lsvMode.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lsvMode.HideSelection = false;
            this.lsvMode.LargeImageList = this.imageList1;
            this.lsvMode.Location = new System.Drawing.Point(14, 57);
            this.lsvMode.MultiSelect = false;
            this.lsvMode.Name = "lsvMode";
            this.lsvMode.Size = new System.Drawing.Size(235, 368);
            this.lsvMode.SmallImageList = this.imageList1;
            this.lsvMode.TabIndex = 8;
            this.lsvMode.UseCompatibleStateImageBehavior = false;
            this.lsvMode.View = System.Windows.Forms.View.SmallIcon;
            this.lsvMode.SelectedIndexChanged += new System.EventHandler(this.lsvMode_SelectedIndexChanged);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "folder.png");
            this.imageList1.Images.SetKeyName(1, "Sentosa-SA-Icon.ico");
            // 
            // lsvAssayName
            // 
            this.lsvAssayName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lsvAssayName.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lsvAssayName.HideSelection = false;
            this.lsvAssayName.LargeImageList = this.imageList1;
            this.lsvAssayName.Location = new System.Drawing.Point(282, 57);
            this.lsvAssayName.MultiSelect = false;
            this.lsvAssayName.Name = "lsvAssayName";
            this.lsvAssayName.Size = new System.Drawing.Size(289, 368);
            this.lsvAssayName.SmallImageList = this.imageList1;
            this.lsvAssayName.TabIndex = 9;
            this.lsvAssayName.UseCompatibleStateImageBehavior = false;
            this.lsvAssayName.View = System.Windows.Forms.View.List;
            this.lsvAssayName.SelectedIndexChanged += new System.EventHandler(this.lsvAssayName_SelectedIndexChanged);
            // 
            // lsvSampleSize
            // 
            this.lsvSampleSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lsvSampleSize.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lsvSampleSize.HideSelection = false;
            this.lsvSampleSize.Location = new System.Drawing.Point(611, 57);
            this.lsvSampleSize.MultiSelect = false;
            this.lsvSampleSize.Name = "lsvSampleSize";
            this.lsvSampleSize.Size = new System.Drawing.Size(181, 368);
            this.lsvSampleSize.TabIndex = 10;
            this.lsvSampleSize.UseCompatibleStateImageBehavior = false;
            this.lsvSampleSize.View = System.Windows.Forms.View.List;
            this.lsvSampleSize.Click += new System.EventHandler(this.lsvSampleSize_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 23);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 20);
            this.label2.TabIndex = 11;
            this.label2.Values.Text = "Disease Area:";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(282, 23);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 20);
            this.label3.TabIndex = 12;
            this.label3.Values.Text = "Test:";
            // 
            // l
            // 
            this.l.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l.Location = new System.Drawing.Point(611, 23);
            this.l.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.l.Name = "l";
            this.l.Size = new System.Drawing.Size(61, 20);
            this.l.TabIndex = 13;
            this.l.Values.Text = "Test Size:";
            // 
            // Form_AssaySelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(821, 467);
            this.Controls.Add(this.l);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lsvSampleSize);
            this.Controls.Add(this.lsvAssayName);
            this.Controls.Add(this.lsvMode);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_AssaySelection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Test Selection";
            this.Load += new System.EventHandler(this.Form_AssaySelection_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private KryptonButton btnOK;
        private KryptonButton btnCancel;

        private System.Windows.Forms.ListView lsvMode;
        private System.Windows.Forms.ListView lsvAssayName;
        private System.Windows.Forms.ListView lsvSampleSize;
        private KryptonLabel label2;
        private KryptonLabel label3;
        private KryptonLabel l;
        private System.Windows.Forms.ImageList imageList1;

    }
}