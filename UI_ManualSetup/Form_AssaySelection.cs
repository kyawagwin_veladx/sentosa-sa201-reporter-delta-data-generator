﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 01/11/2013
 * Author: Liu Rui
 * 
 * Description:
 */
#endregion
using System;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using ComponentFactory.Krypton.Toolkit;

using M_Assay;

namespace Sentosa_Reporter
{
    public partial class Form_AssaySelection : KryptonForm
    {
        #region Private Member Variables
        //private Form_ComboAssaySelection frmComboAssaySelection;
        private string assayConfigFolder = string.Empty;
        private List<AssayInfo> assayInfoList;
        private List<ArrayList> indexMatrix;
        private string[] assayConfigFileList;
        private List<string> validConfigFileList;
        private List<string> corruptedConfigFileList;

        private string selectedConfigFile;
        private int selectedSizeIndex;
        private ArrayList selectedPageNames;
        #endregion

        #region Public Properties
        public string SelectedConfigFile
        {
            get { return selectedConfigFile; }
        }

        public int SelectedSizeIndex
        {
            get { return selectedSizeIndex; }
        }

        public ArrayList SelectedPageNames
        {
            get { return selectedPageNames; }
            set { selectedPageNames = value; }
        }
        #endregion

        #region Constructors
        public Form_AssaySelection(string assayConfigFolder)
        {
            InitializeComponent();
            this.assayConfigFolder = assayConfigFolder; 
        }
        #endregion

        #region Private Methods
        private void Form_AssaySelection_Load(object sender, EventArgs e)
        {
            IntializeControls();

            if (LoadAssayInfoList())
            {
                MakeIndexMatrix();

                FillinControls();
            }
        }

        private bool LoadAssayInfoList()
        {
            assayConfigFileList = Directory.GetFiles(assayConfigFolder,"*.config");

            if (assayConfigFileList.Length == 0)
            {
                //KryptonMessageBox.Show(SysMessage.emptyConfig, SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else 
            {
                assayInfoList = new List<AssayInfo>();

                validConfigFileList = new List<string>();

                //corruptedConfigFileList = FileOperations.GetAllCorruptedFiles(assayConfigFileList);

                if (corruptedConfigFileList.Count>0)
                {
                    foreach (string file in corruptedConfigFileList)
                    {
                        //KryptonMessageBox.Show(SysMessage.corruptedConfig+file, SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                foreach (string strPath in assayConfigFileList)
                {
                    if (!corruptedConfigFileList.Contains(strPath))
                    {
                        AssayInfo ai = new AssayInfo();

                        if (ai.FillAssayInfo(strPath))
                        {
                            this.assayInfoList.Add(ai);
                            validConfigFileList.Add(strPath);
                        }
                    }
                }

                return true;
            }
            //else
            //{
            //    KryptonMessageBox.Show(SysMessage.invalidConfig, SysMessage.errorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return false;
            //}
        }

        private void IntializeControls()
        {
            btnOK.Enabled = false;
            lsvMode.Clear();
            lsvAssayName.Clear();
            lsvSampleSize.Clear();
        }

        private void MakeIndexMatrix()
        {
            indexMatrix = new List<ArrayList>();

            int index = 0;
            foreach (AssayInfo ai in assayInfoList)
            {
                string diseaseArea = ai.DiseaseArea;

                bool bFind = false;

                for (int i = 0; i < indexMatrix.Count; i++)
                {
                    ArrayList al = indexMatrix[i];
                    //Found
                    if (diseaseArea == assayInfoList[(int)al[0]].DiseaseArea)
                    {
                        bFind = true;
                        al.Add(index);
                        break;
                    }
                }
                //Not found
                if (!bFind)
                {
                    ArrayList al = new ArrayList();
                    al.Add(index);
                    indexMatrix.Add(al);
                }
                index++;
            }
        }

        private void FillinControls()
        {
            for (int i = 0; i < indexMatrix.Count; i++)
            {
                lsvMode.Items.Add(assayInfoList[(int)indexMatrix[i][0]].DiseaseArea);
                lsvMode.Items[i].ImageIndex = 0;
            }

            lsvMode.Items[0].Selected = true;
        }
      
        private void LoadAssayName(int index)
        {
            lsvAssayName.Clear();

            ArrayList al = indexMatrix[index];

            for (int i = 0; i < al.Count; i++)
            {
                lsvAssayName.Items.Add(assayInfoList[(int)al[i]].AssayName);
                lsvAssayName.Items[i].ImageIndex = 1;
            }
        }

        private void LoadSampleSize(int index)
        {
            lsvSampleSize.Clear();
            ArrayList al = indexMatrix[lsvMode.SelectedItems[0].Index];

            int selectsample = (int)al[index];
            AssayInfo selectedAssay = assayInfoList[selectsample];

            List<SampleSize> sampleSizeList = selectedAssay.SampleSizeList;

            for (int i = 0; i < sampleSizeList.Count; i++)
            {
                lsvSampleSize.Items.Add(sampleSizeList[i].Name);
            }
        }

        private void lsvMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lsvMode.SelectedItems.Count > 0)
            {
                LoadAssayName(lsvMode.SelectedItems[0].Index);

                lsvSampleSize.Clear();
                btnOK.Enabled = false;
            }
        }

        private void lsvAssayName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lsvAssayName.SelectedItems.Count > 0)
            {
                LoadSampleSize(lsvAssayName.SelectedItems[0].Index);
            }
        }

        private void lsvSampleSize_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
            {
               
            }
        }

        //private void lsvSize_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (lsvSampleSize.SelectedItems.Count > 0)
        //    {
        //        int indexX = lsvMode.SelectedItems[0].Index;
        //        int indexY = lsvAssayName.SelectedItems[0].Index;
        //        int indexZ = lsvSampleSize.SelectedItems[0].Index;

        //        List<ComboPage> selectedComboPages = assayInfoList[(int)indexMatrix[indexX][indexY]].SampleSizeList[indexZ].ComboPageList;
        //        int pageCount = assayInfoList[(int)indexMatrix[indexX][indexY]].SampleSizeList[indexZ].PositionList.Count;

        //        if (selectedComboPages.Count > 0)
        //        {
        //            frmComboAssaySelection = new Form_ComboAssaySelection(selectedComboPages, pageCount);
        //            frmComboAssaySelection.ShowDialog();

        //            if (frmComboAssaySelection.DialogResult == DialogResult.OK)
        //            {
        //                selectedPageNames = frmComboAssaySelection.SelectedPages;
        //                btnOK.Enabled = true;
        //            }
        //            else
        //            {
        //                lsvMode.SelectedItems[0].Selected = false;
        //            }

        //        }
        //        else
        //        {
        //            btnOK.Enabled = true;
        //        }
        //    }
        //    else
        //    {
        //        btnOK.Enabled = false;
        //    }
        //}

        private void btnOK_Click(object sender, EventArgs e)
        {
            int indexX = lsvMode.SelectedItems[0].Index;
            int indexY = lsvAssayName.SelectedItems[0].Index;
            int indexZ = lsvSampleSize.SelectedItems[0].Index;

            selectedConfigFile = validConfigFileList[(int)indexMatrix[indexX][indexY]];
            selectedSizeIndex = indexZ;

            DialogResult = DialogResult.OK;

           // Log.IsLogWritten(LogType.system, SysMessage.user + Log.UserName + SysMessage.selectTestMsg + lsvAssayName.SelectedItems[0].Text);

            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
           // Log.IsLogWritten(LogType.system, SysMessage.user + Log.UserName + SysMessage.cancelTestSelectMsg);

            Close();
        }
        #endregion

        private void lsvSampleSize_Click(object sender, EventArgs e)
        {
            /*
            if (lsvSampleSize.SelectedItems.Count > 0)
            {
                int indexX = lsvMode.SelectedItems[0].Index;
                int indexY = lsvAssayName.SelectedItems[0].Index;
                int indexZ = lsvSampleSize.SelectedItems[0].Index;

                List<ComboPage> selectedComboPages = assayInfoList[(int)indexMatrix[indexX][indexY]].SampleSizeList[indexZ].ComboPageList;
                int pageCount = assayInfoList[(int)indexMatrix[indexX][indexY]].SampleSizeList[indexZ].PositionList.Count;

                if (selectedComboPages.Count > 0)
                {
                    frmComboAssaySelection = new Form_ComboAssaySelection(selectedComboPages, pageCount);
                    frmComboAssaySelection.ShowDialog();

                    if (frmComboAssaySelection.DialogResult == DialogResult.OK)
                    {
                        selectedPageNames = frmComboAssaySelection.SelectedPages;
                        btnOK.Enabled = true;
                    }
                    else
                    {
                        //lsvSampleSize.Items[e.ItemIndex].Remove();
                        ////lsvSampleSize.SelectedIndices.Clear();
                        //lsvSampleSize.Focus();
                        //lsvSampleSize.Update();
                    }

                }
                else
                {
                    btnOK.Enabled = true;
                }
            }
            else
            {
                btnOK.Enabled = false;
            }
             * */

        }

     
    }
}
