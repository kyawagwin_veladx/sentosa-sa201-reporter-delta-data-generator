﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Automation;
using TestStack.White;
using TestStack.White.Factory;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.TreeItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.WindowsAPI;

namespace UI_Test
{
    [TestFixture(Description = "Data Analysis test cases for loading SDS Documents data and generate report", Author = "Ko Ko")]
    public class DataAnalysisTest : IDisposable
    {
        Application app;
        Window accessModeWindow;
        Button btnAnalysis;
        Button btnExit;
        
        string dirTestData = string.Empty;
        string dirTestDataToMove = string.Empty;

        public DataAnalysisTest()
        {
            // assign the location of the test data
            dirTestData = Path.Combine(Directory.GetCurrentDirectory(), "UI_Test", "TestData").ToString();
            // default location for the sds documents
            dirTestDataToMove = "C:\\Vela Diagnostics\\SDS Documents\\TestData";

            TestStack.White.Configuration.CoreAppXmlConfiguration.Instance.RawElementBasedSearch = true;

            // login to the system first
            app = ItemSelector.App();
            Window loginWindow = ItemSelector.getWindow(app, "Login");
            TextBox txtUsername = loginWindow.Get<TextBox>(ItemSelector.txtUsername);
            txtUsername.Text = "xk";
            TextBox txtPassword = loginWindow.Get<TextBox>(ItemSelector.txtPassword);
            txtPassword.Text = "veladx";
            Button btnOK = loginWindow.Get<Button>(ItemSelector.btnOK);
            btnOK.Click();

            Thread.Sleep(1000);

            // initialize all controls
            accessModeWindow = ItemSelector.getWindow(app, "Access Mode");
            btnExit = accessModeWindow.Get<Button>(ItemSelector.btnExit);
            btnAnalysis = accessModeWindow.Get<Button>(ItemSelector.btnAnalysis);

            // copy test data to the correct folder path
            try
            {
                if(!Directory.Exists(dirTestDataToMove))
                {
                    DirectoryInfo dir = new DirectoryInfo(dirTestData);
                    dir.MoveTo(dirTestDataToMove);
                }
                
            }
            catch (Exception ex)
            {
                Assert.Fail("Copy test data failed");
            }
        }

        [Test(), Order(1)]
        public void ChooseInvalidSDSDataAnalysisFolderTest()
        {
            btnAnalysis.Click();

            List<Window> myWindows = app.GetWindows();
            Window openDialog = myWindows.Where(n => n.Name == "Browse For Folder").First();

            Tree folderTree = openDialog.Get<Tree>(SearchCriteria.ByControlType(ControlType.Tree).AndByText("Open Run Folder"));

            TreeNode nodeComputer = folderTree.Nodes[0].Nodes.First(node => node.Text == "Computer");
            nodeComputer.Select();
            TreeNode nodeDrive = nodeComputer.Nodes[0];
            nodeDrive.Select();
            TreeNode nodeVeladx = nodeDrive.Nodes.First(node => node.Text == "Vela Diagnostics");
            nodeVeladx.Select();
            TreeNode nodeSds = nodeVeladx.Nodes.First(node => node.Text == "SDS Documents");
            nodeSds.Select();
            TreeNode nodeTestData = nodeSds.Nodes.First(node => node.Text == "TestData");
            nodeTestData.Select();
            TreeNode nodeData = nodeTestData.Nodes.First(node => node.Text == "invalid_sds_data");
            nodeData.Select();

            openDialog.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.RETURN);

            // dialog only can be bound by linq query
            myWindows = app.GetWindows();
            Window checksumErrorWindow = myWindows.Where(n => n.Name == "File checksum error").First();
            checksumErrorWindow.Dispose();

            myWindows = app.GetWindows();
            Window errorWindow = myWindows.Where(n => n.Name == "Error").First();
            errorWindow.Dispose();

            Thread.Sleep(3000);
        }

        [Test(), Order(2)]
        public void ChooseValidSDSDataAnalysisFolderAndShowSummaryReportTest()
        {
            btnAnalysis.Click();

            List<Window> myWindows = app.GetWindows();
            Window openDialog = myWindows.Where(n => n.Name == "Browse For Folder").First();

            Tree folderTree = openDialog.Get<Tree>(SearchCriteria.ByControlType(ControlType.Tree).AndByText("Open Run Folder"));
            
            TreeNode nodeComputer = folderTree.Nodes[0].Nodes.First(node => node.Text == "Computer");
            nodeComputer.Select();
            TreeNode nodeDrive = nodeComputer.Nodes[0];
            nodeDrive.Select();
            TreeNode nodeVeladx = nodeDrive.Nodes.First(node => node.Text == "Vela Diagnostics");
            nodeVeladx.Select();
            TreeNode nodeSds = nodeVeladx.Nodes.First(node => node.Text == "SDS Documents");
            nodeSds.Select();
            TreeNode nodeTestData = nodeSds.Nodes.First(node => node.Text == "TestData");
            nodeTestData.Select();
            TreeNode nodeData = nodeTestData.Nodes.First(node => node.Text == "valid_sds_data");
            nodeData.Select();

            openDialog.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.RETURN);

            // confirm
            myWindows = app.GetWindows();
            Window confirmWindow = myWindows.Where(n => n.Name == "Confirm").First();
            Button btnOK = confirmWindow.Get<Button>(SearchCriteria.ByControlType(ControlType.Button).AndByText("OK"));
            btnOK.Click();

            Thread.Sleep(2000);       

            /*
            Window mainWindow = app.GetWindow("Sentosa SA201 Reporter", InitializeOption.WithCache);
            //Button collection = mainWindow.Items.Single(item => item.Name == "btnNext") as Button;
            Button btnNext = mainWindow.Get<Button>(ItemSelector.btnNext);
            btnNext.Click();

            Thread.Sleep(2000);

            mainWindow = app.GetWindow("Sentosa SA201 Reporter", InitializeOption.WithCache);
            Button btnPreview = mainWindow.Get<Button>(ItemSelector.btnPreview);
            btnPreview.Click();

            Thread.Sleep(10000);
            */
        }
        
        [Test(), Order(3)]
        public void ShowSDSDataAnalysisRunReportTest()
        {
            // gotta cache window for faster control searching
            Window mainWindow = app.GetWindow("Sentosa SA201 Reporter", InitializeOption.WithCache);
            //Button collection = mainWindow.Items.Single(item => item.Name == "btnNext") as Button;
            Button btnNext = mainWindow.Get<Button>(ItemSelector.btnNext);
            btnNext.Click();

            Thread.Sleep(2000);
        }

        [Test(), Order(4)]
        public void ShowSDSDataAnalysisPrintPreviewReportTest()
        {
            // gotta cache window for faster control searching
            Window mainWindow = app.GetWindow("Sentosa SA201 Reporter", InitializeOption.WithCache);
            Button btnPreview = mainWindow.Get<Button>(ItemSelector.btnPreview);
            btnPreview.Click();

            Thread.Sleep(10000);
        }

        public void Dispose()
        {
            app.Kill();
        }
    }
}
