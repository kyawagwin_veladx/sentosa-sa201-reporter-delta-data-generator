﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Automation;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.WindowsAPI;

namespace UI_Test
{
    [TestFixture(Description = "Import ID List test cases for importing ID List file, run the base software and generate report", Author = "Win")]
    public class ImportIDListTest : IDisposable
    {
        Application app;
        Window accessModeWindow;
        Window errorMsgBox;
        Window mainWindow;
        Button btnIDList;
        Button btnNext;
        Button btnExit;
        
        string dirTestData = string.Empty;
        string dirTestDataToMove = string.Empty;
        string fileInvalidIDListTest = "ZIKV - invalidcsum.xml";
        string fileValidIDListTest = "24-1 VirusLysis3 v3-6_180403_130251.smp";

        public ImportIDListTest()
        {
            // assign the location of the test data
            dirTestData = Path.Combine(Directory.GetCurrentDirectory(), "UI_Test", "TestData").ToString();
            // default location for the sds documents
            dirTestDataToMove = "C:\\Vela Diagnostics\\SDS Documents\\TestData";

            // login to the system first
            app = ItemSelector.App();
            Window loginWindow = ItemSelector.getWindow(app, "Login");
            TextBox txtUsername = loginWindow.Get<TextBox>(ItemSelector.txtUsername);
            txtUsername.Text = "xk";
            TextBox txtPassword = loginWindow.Get<TextBox>(ItemSelector.txtPassword);
            txtPassword.Text = "veladx";
            Button btnOK = loginWindow.Get<Button>(ItemSelector.btnOK);
            btnOK.Click();

            Thread.Sleep(1000);

            // initialize all controls
            accessModeWindow = ItemSelector.getWindow(app, "Access Mode");
            btnIDList = accessModeWindow.Get<Button>(ItemSelector.btnIDList);
            btnExit = accessModeWindow.Get<Button>(ItemSelector.btnExit);

            // copy test data to the correct folder path
            try
            {
                if (!Directory.Exists(dirTestDataToMove))
                {
                    DirectoryInfo dir = new DirectoryInfo(dirTestData);
                    dir.MoveTo(dirTestDataToMove);
                }

            }
            catch (Exception ex)
            {
                Assert.Fail("Copy test data failed");
            }

            errorMsgBox = null;
        }
        
        [Test(), Order(1)]
        public void ChooseInvalidIDListFileTest()
        {
            btnIDList.Click();            

            List<Window> myWindows = app.GetWindows();
            Window openDialog = myWindows.Where(n => n.Name == "Open").First();

            TextBox textField = openDialog.Get<TextBox>(SearchCriteria.ByControlType(ControlType.Edit).AndByText("File name:"));

            textField.Text = Path.Combine(dirTestDataToMove, fileInvalidIDListTest);

            openDialog.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.RETURN);

            Thread.Sleep(1000);
            
            // dialog only can be bound by linq query
            myWindows = app.GetWindows();
            Window checksumErrorWindow = myWindows.Where(n => n.Name == "File checksum error").First();
            checksumErrorWindow.Dispose();

            myWindows = app.GetWindows();
            Window errorWindow = myWindows.Where(n => n.Name == "Error").First();
            errorWindow.Dispose();

            Thread.Sleep(3000);
        }

        [Test(), Order(2)]
        public void ChooseValidIDListFileAndRunTest()
        {
            btnIDList.Click();

            List<Window> myWindows = app.GetWindows();
            Window openDialog = myWindows.Where(n => n.Name == "Open").First();

            TextBox textField = openDialog.Get<TextBox>(SearchCriteria.ByControlType(ControlType.Edit).AndByText("File name:"));

            // change the location of the id list file
            //textField.Text = @"C:\Sentosa SA\ID Lists\24-1 VirusLysis3 v3-6_180403_130251.smp";
            textField.Text = Path.Combine(dirTestDataToMove, fileValidIDListTest);

            openDialog.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.RETURN);

            myWindows = app.GetWindows();
            Window confirmWindow = myWindows.Where(n => n.Name == "Confirm").First();
            Button btnOK = confirmWindow.Get<Button>(SearchCriteria.ByControlType(ControlType.Button).AndByText("OK"));
            btnOK.Click();

            Thread.Sleep(1000);

            /*
            // run base software
            //myWindows = app.GetWindows();
            //Window mainWindow = myWindows.Where(n => n.Name == "Sentosa SA201 Reporter").First();
            Window mainWindow = ItemSelector.getWindow(app, "Sentosa SA201 Reporter");

            btnNext = mainWindow.Get<Button>(SearchCriteria.ByControlType(ControlType.Button).AndAutomationId("btnNext"));
            btnNext.Click();

            CheckBox chkInsert = mainWindow.Get<CheckBox>(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId("chkInsert"));
            chkInsert.Checked = true;

            Thread.Sleep(1000);

            Button btnStartSA201 = mainWindow.Get<Button>(SearchCriteria.ByControlType(ControlType.Button).AndAutomationId("button1"));
            btnStartSA201.Click();

            Thread.Sleep(1000);

            // confirm run
            myWindows = app.GetWindows();
            confirmWindow = myWindows.Where(n => n.Name == "Confirm").First();
            btnOK = confirmWindow.Get<Button>(SearchCriteria.ByControlType(ControlType.Button).AndByText("OK"));
            btnOK.Click();

            // confirm save location
            myWindows = app.GetWindows();
            confirmWindow = myWindows.Where(n => n.Name == "Confirm").First();
            btnOK = confirmWindow.Get<Button>(SearchCriteria.ByControlType(ControlType.Button).AndByText("OK"));

            Label label = confirmWindow.Items[2] as Label;
            string path = label.Text;

            string[] pathArr = path.Split('\\');
            string folderPath = pathArr.Last().ToString();

            btnOK.Click();

            // wait 10 seconds to load the base software
            Thread.Sleep(5000);

            //Instrument Check -> cancel
            //myWindows = app.GetWindows();
            //confirmWindow = myWindows.Where(n => n.Name == "Instrument Check").First();
            //Button btnCancel = confirmWindow.Get<Button>(SearchCriteria.ByControlType(ControlType.Button).AndByText("Cancel"));
            //btnCancel.Click();

            // copy sds data to the defined folder path
            String directoryName = Path.Combine("C:\\Vela Diagnostics\\SDS Documents", folderPath).ToString();
            DirectoryInfo dirInfo = new DirectoryInfo(directoryName);
            if (dirInfo.Exists == false)
                Directory.CreateDirectory(directoryName);

            List<String> testFiles = Directory
                               .GetFiles("C:\\Vela Diagnostics\\SDS Documents\\TestData\\valid_sds_data", "*.*", SearchOption.AllDirectories).ToList();

            foreach (string file in testFiles)
            {
                FileInfo mFile = new FileInfo(file);
                // to remove name collisions
                if (new FileInfo(dirInfo + "\\" + mFile.Name).Exists == false)
                {
                    mFile.CopyTo(dirInfo + "\\" + mFile.Name);
                }
            }

            Process[] proc = Process.GetProcessesByName("SDSShell");
            proc[0].Kill();

            // Confirmation Windows
            //myWindows = app.GetWindows();
            //confirmWindow = myWindows.Where(n => n.Name == "Confirmation").First();
            //confirmWindow.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.RETURN);

            Thread.Sleep(2000);

            //proc = Process.GetProcessesByName("AutoAnalyze");
            //proc[0].Kill();

            btnNext.Click();

            Thread.Sleep(2000);

            btnNext.Click();

            Thread.Sleep(2000);

            Button btnPreview = mainWindow.Get<Button>(ItemSelector.btnPreview);
            btnPreview.Click();

            Thread.Sleep(10000);
            */
        }

        [Test(), Order(3)]
        public void RunBaseSoftwareTest()
        {
            // run base software
            //myWindows = app.GetWindows();
            //Window mainWindow = myWindows.Where(n => n.Name == "Sentosa SA201 Reporter").First();
            Window mainWindow = ItemSelector.getWindow(app, "Sentosa SA201 Reporter");

            btnNext = mainWindow.Get<Button>(SearchCriteria.ByControlType(ControlType.Button).AndAutomationId("btnNext"));
            btnNext.Click();

            CheckBox chkInsert = mainWindow.Get<CheckBox>(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId("chkInsert"));
            chkInsert.Checked = true;

            Thread.Sleep(1000);

            Button btnStartSA201 = mainWindow.Get<Button>(SearchCriteria.ByControlType(ControlType.Button).AndAutomationId("button1"));
            btnStartSA201.Click();

            Thread.Sleep(1000);

            // confirm run
            var myWindows = app.GetWindows();
            var confirmWindow = myWindows.Where(n => n.Name == "Confirm").First();
            var btnOK = confirmWindow.Get<Button>(SearchCriteria.ByControlType(ControlType.Button).AndByText("OK"));
            btnOK.Click();

            // confirm save location
            myWindows = app.GetWindows();
            confirmWindow = myWindows.Where(n => n.Name == "Confirm").First();
            btnOK = confirmWindow.Get<Button>(SearchCriteria.ByControlType(ControlType.Button).AndByText("OK"));

            Label label = confirmWindow.Items[2] as Label;
            string path = label.Text;

            string[] pathArr = path.Split('\\');
            string folderPath = pathArr.Last().ToString();

            btnOK.Click();

            // wait 5 seconds to load the base software
            Thread.Sleep(5000);

            //Instrument Check -> cancel
            //myWindows = app.GetWindows();
            //confirmWindow = myWindows.Where(n => n.Name == "Instrument Check").First();
            //Button btnCancel = confirmWindow.Get<Button>(SearchCriteria.ByControlType(ControlType.Button).AndByText("Cancel"));
            //btnCancel.Click();

            // copy sds data to the defined folder path
            String directoryName = Path.Combine("C:\\Vela Diagnostics\\SDS Documents", folderPath).ToString();
            DirectoryInfo dirInfo = new DirectoryInfo(directoryName);
            if (dirInfo.Exists == false)
                Directory.CreateDirectory(directoryName);

            List<String> testFiles = Directory
                               .GetFiles("C:\\Vela Diagnostics\\SDS Documents\\TestData\\valid_sds_data", "*.*", SearchOption.AllDirectories).ToList();

            foreach (string file in testFiles)
            {
                FileInfo mFile = new FileInfo(file);
                // to remove name collisions
                if (new FileInfo(dirInfo + "\\" + mFile.Name).Exists == false)
                {
                    mFile.CopyTo(dirInfo + "\\" + mFile.Name);
                }
            }

            Process[] proc = Process.GetProcessesByName("SDSShell");
            proc[0].Kill();

            // Confirmation Windows
            //myWindows = app.GetWindows();
            //confirmWindow = myWindows.Where(n => n.Name == "Confirmation").First();
            //confirmWindow.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.RETURN);

            Thread.Sleep(2000);

            //proc = Process.GetProcessesByName("AutoAnalyze");
            //proc[0].Kill();
        }

        [Test(), Order(4)]
        public void GenerateSummaryReportTest()
        {
            btnNext.Click();

            Thread.Sleep(2000);
        }

        [Test(), Order(5)]
        public void GenerateRunReportTest()
        {
            btnNext.Click();

            Thread.Sleep(2000);
        }

        [Test(), Order(6)]
        public void GeneratePrintPreviewReportTest()
        {
            Button btnPreview = mainWindow.Get<Button>(ItemSelector.btnPreview);
            btnPreview.Click();

            Thread.Sleep(2000);
        }

        public void Dispose()
        {
            app.Kill();
        }
    }
}
