﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Automation;
using TestStack.White;
using TestStack.White.Factory;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;


namespace UI_Test
{
    public static class ItemSelector
    {
        public static Application App()
        {
            return Application.Launch(
                new ProcessStartInfo(@"C:\Users\Win\source\repos\Sentosa SA201 Reporter\Sentosa SA201 Reporter\bin\Debug\Sentosa SA201 Reporter.exe")
                {
                    //WorkingDirectory = @"Sentosa SA201 Reporter\bin\Debug\Sentosa SA201 Reporter.exe"
                });
        }

        public static Window getWindow(Application application, string title)
        {
            return application.GetWindow(title);
        }
        
        public static Window getMessageBox(Window window, string title)
        {
            return window.MessageBox(title);
        }
        
        public static SearchCriteria txtUsername = SearchCriteria.ByControlType(ControlType.Edit).AndAutomationId("txtUsername");
        public static SearchCriteria txtPassword = SearchCriteria.ByControlType(ControlType.Edit).AndAutomationId("txtPassword");
        public static SearchCriteria btnOK = SearchCriteria.ByControlType(ControlType.Button).AndAutomationId("btnOK");
        public static SearchCriteria btnCancel = SearchCriteria.ByControlType(ControlType.Button).AndAutomationId("btnCancel");
        public static SearchCriteria btnIDList = SearchCriteria.ByControlType(ControlType.Button).AndAutomationId("btnIDList");
        public static SearchCriteria btnAnalysis = SearchCriteria.ByControlType(ControlType.Button).AndAutomationId("btnAnalysis");
        public static SearchCriteria btnExit = SearchCriteria.ByControlType(ControlType.Button).AndAutomationId("btnExit");
        public static SearchCriteria btnNext = SearchCriteria.ByControlType(ControlType.Button).AndAutomationId("btnNext");
        public static SearchCriteria btnPreview = SearchCriteria.ByControlType(ControlType.Button).AndAutomationId("btn_preview");
    }
}
