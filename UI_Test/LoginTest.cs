﻿using System;
using System.Security.Principal;
using System.Threading;
using System.Windows.Automation;
using NUnit.Framework;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;


namespace UI_Test
{
    [TestFixture(Description = "Simple test cases for security checking")]
    public class LoginTest : IDisposable
    {
        Application app;
        TextBox txtUsername;
        TextBox txtPassword;
        Button btnOK;
        Button btnCancel;
        Window loginWindow;
        Window errorMsgBox;

        public LoginTest()
        {
            // initialize all controls
            app = ItemSelector.App();
            loginWindow = ItemSelector.getWindow(app, "Login");
            btnOK = loginWindow.Get<Button>(ItemSelector.btnOK);
            btnCancel = loginWindow.Get<Button>(ItemSelector.btnCancel);
            txtUsername = loginWindow.Get<TextBox>(ItemSelector.txtUsername);
            txtPassword = loginWindow.Get<TextBox>(ItemSelector.txtPassword);
            errorMsgBox = null;
        }

        [Test(Description = "Access will be denied when user login without username and password")]
        public void LoginWithoutUsernameAndPasswordTest()
        {
            // click the OK button to login
            btnOK.Click();            
            
            errorMsgBox = loginWindow.MessageBox("Error");
            errorMsgBox.Dispose();

            Assert.Fail("Purposely failing test case for demo");
        }

        [Test(Description = "Show error message box when user using invalid username and password")]
        public void LoginWithInvalidUsernameAndPasswordTest()
        {
            // add invalid username and password
            txtUsername.Text = "test";
            txtPassword.Text = "test";

            // click the OK button to login
            btnOK.Click();

            // show error message box
            errorMsgBox = loginWindow.MessageBox("Error");
            errorMsgBox.Dispose();
        }

        [Test(Description = "Successfullly login by using valid username and password")]
        public void LoginWithValidUsernameAndPasswordTest()
        {
            // add valid username and password
            txtUsername.Text = "xk";
            txtPassword.Text = "veladx";

            // click the OK button to login
            btnOK.Click();

            // must show Access Mode window after successfully logged in
            Window accessModeWindow = ItemSelector.getWindow(app, "Access Mode");
            if(accessModeWindow != null)
            {
                // exit from access mode window
                Button btnExit = accessModeWindow.Get<Button>(SearchCriteria.ByAutomationId("btnExit"));
                btnExit.Click();
            }
        }

        public void Dispose()
        {
            app.Kill();
        }
    }
}
