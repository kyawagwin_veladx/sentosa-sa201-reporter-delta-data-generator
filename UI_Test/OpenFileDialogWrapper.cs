﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestStack.White.Factory;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.MenuItems;
using TestStack.White.UIItems.WindowItems;

namespace UI_Test
{
    internal class OpenFileDialogWrapper : TestStack.White.UIItems.WindowItems.Window
    {
        private readonly TestStack.White.UIItems.WindowItems.Window _window;

        public OpenFileDialogWrapper(TestStack.White.UIItems.WindowItems.Window window)
        {
            _window = window;
            LoadControls();
        }

        public TestStack.White.UIItems.Button CancelButton { get; private set; }
        public TestStack.White.UIItems.Button OkButton { get; private set; }
        public TestStack.White.UIItems.ListBoxItems.ComboBox FilePaths { get; private set; }
        public TestStack.White.UIItems.ListBoxItems.ComboBox FileTypeFilter { get; private set; }
        public TestStack.White.UIItems.WindowStripControls.ToolStrip AddressBar { get; private set; }

        #region Overwrite all the TestStack.White.UIItems.WindowItems.Window virtual functions

        // For Example
        public override string Title { get { return _window.Title; } }

        public override PopUpMenu Popup => throw new NotImplementedException();

        public override Window ModalWindow(string title, InitializeOption option)
        {
            throw new NotImplementedException();
        }

        public override Window ModalWindow(SearchCriteria searchCriteria, InitializeOption option)
        {
            throw new NotImplementedException();
        }

        #endregion

        private void LoadControls()
        {
            CancelButton = _window.Get<TestStack.White.UIItems.Button>(SearchCriteria.ByClassName("Button").AndAutomationId("2"));
            OkButton = _window.Get<TestStack.White.UIItems.Button>(SearchCriteria.ByClassName("Button").AndAutomationId("1"));
            FilePaths = _window.Get<TestStack.White.UIItems.ListBoxItems.ComboBox>(SearchCriteria.ByAutomationId("1148"));
            FileTypeFilter = _window.Get<TestStack.White.UIItems.ListBoxItems.ComboBox>(SearchCriteria.ByAutomationId("1136"));
            AddressBar = _window.Get<TestStack.White.UIItems.WindowStripControls.ToolStrip>(SearchCriteria.ByClassName("ToolbarWindow32").AndAutomationId("1001"));
        }
    }
}
