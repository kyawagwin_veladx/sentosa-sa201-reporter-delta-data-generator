﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 08/11/2013
 * Author: Liu Rui
 * 
 * Description:
 * This class contains the WriteLog method which really perform the writting log operations
 * It is managed by Log class
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace M_Log
{
    /// <summary>
    /// Define the log type
    /// - system: record all the login/logout, operations on the system
    /// - run: for each run based on SMP file, there will be a log file recording the operations on the file
    /// </summary>
    public enum LogType
    {
        system,
    }

    public class LogMethods
    {
        #region Private Member Variables
        private LogType logType;
        private string message = string.Empty;
        #endregion

        #region Public Properties
        public LogType LogType
        {
            get
            {
                return logType;
            }
            set
            {
                logType = value;
            }
        }

        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Perform writting log into log file
        /// </summary>
        /// <param name="message">log message</param>
        /// <param name="logFile">log file path</param>
        /// <returns>true/false - indicate whether log is written or not</returns>
        public bool IsLogWritten(string message, string logFile)
        {
            bool isLogRecorded = false;

            if (message == string.Empty)
            {
                message = "Warning: Blank Log Message";
            }
            try
            {
                //Get file full path
                FileInfo fi = new FileInfo(logFile);
                string filePath = fi.Directory.ToString();

                //Create a folder for log file if the folder doesn't exist
                DirectoryInfo di = new DirectoryInfo(filePath);
                if (!di.Exists)
                {
                    Directory.CreateDirectory(filePath);
                }

                //Create and write into log file if a log file doesn't exist
                if (!fi.Exists)
                {
                    using (StreamWriter sw = fi.CreateText())
                    {
                        sw.WriteLine(DateTime.Now + "\t" + message + "\t");
                        sw.Close();
                        isLogRecorded = true;
                    }
                }
                //Append to log file if a log file is already existing
                else
                {
                    using (StreamWriter sw = fi.AppendText())
                    {
                        sw.WriteLine(DateTime.Now + "\t" + message + "\t");
                        sw.Close();
                        isLogRecorded = true;
                    }
                }
            }
            catch 
            {
               // throw new IOException();
            }
            return isLogRecorded;
        }
        #endregion
    }
}
