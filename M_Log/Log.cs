﻿#region Copyright © 2014 Vela Diagnostics
/*
 * Date: 08/11/2013
 * Author: Liu Rui
 * 
 * Description:
 * This general static log class is exposed to user for writing log file based on log type and log message,
 * it contains the method for specifying log file path based on log type; 
 * it is the hub that manages the LogMethods class which performs writing log operations
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using M_System;

namespace M_Log
{
    public static class Log
    {
        #region Private Member Variables
        private static LogMethods logMethods;
        private static string userName = string.Empty;
        #endregion

        #region Private Methods
        /// <summary>
        /// Specify the log file path according to the log type
        /// </summary>
        /// <param name="logType">log file type: system/run</param>
        /// <returns>log file path</returns>
        private static string GetLogFile(LogType logType)
        {
            string logFile = string.Empty;

            
            
            string logFolder = GetLogFolder();


            switch (logType)
            {
                case LogType.system:
                    logFile = logFolder + "System.log";
                    break;
            }

            if (!File.Exists(logFile))
            {
                File.Create(logFile);
            }

            return logFile;
        }

        private static string GetLogFolder()
        {
            string Folder_Log = AppDomain.CurrentDomain.BaseDirectory + "\\Log\\";
            if (!Directory.Exists(Folder_Log))
            {
                Directory.CreateDirectory(Folder_Log);
            }

            return Folder_Log;

            //To be consider to use System Config.
            //SystemConfig systemConfig = new SystemConfig();
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Instantiate an instance of LogMethods class
        /// </summary>
        static Log()
        {
            logMethods = new LogMethods();
        }
        #endregion

        #region Public Properties
        public static string UserName
        {
            get
            {
                userName = Environment.UserName;
                return userName;
            }
            set
            {
                userName = value;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Write message into log file according to the log type
        /// it will invoke the method in LogMethods class to perform the real action
        /// </summary>
        /// <param name="logType">log file type: system/run in enumeration type</param>
        /// <param name="message">log message</param>
        /// <returns>true/false - indicate whether log is written or not</returns>
        public static bool WriteLog(LogType logType, string message)
        {
            string logFile = GetLogFile(logType);

            if (logFile != string.Empty)
            {
                return logMethods.IsLogWritten(message, logFile);
            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}
